<?php defined('BASEPATH') || exit('No direct script access allowed');
class Content extends Admin_Controller
{
    protected $permissionCreate = 'University_graduates.Content.Create';
    protected $permissionDelete = 'University_graduates.Content.Delete';
    protected $permissionEdit   = 'University_graduates.Content.Edit';
    protected $permissionView   = 'University_graduates.Content.View';

    public function __construct()
    {
        parent::__construct();
        $this->auth->restrict($this->permissionView);
        $this->lang->load( 'university_graduates' );
        ////////////////////////////////////////////////////////////////////////
        $this->load->model( 'agsis_table__anuniversitar__model' );
        $this->load->model( 'agsis_table__tipcicluinvatamant__model' );
        $this->load->model( 'agsis_view_absolvire_fds_model' );
        $this->load->model( 'agsis_view_absolvire_fds_probeabsolventi_model' );

        $this->load->model( 'diplomas_table__diplomas_config__model' );
        $this->load->model( 'diplomas_table__diplomas_graduates__model' );
        $this->load->model( 'diplomas_table__diplomas_codes__model' );
        $this->load->model( 'diplomas_view__diplomas_codes_2_graduate__model' );
        $this->load->model( 'diplomas_view__diplomas_graduates_2_diploma_and_code__model' );
        $this->load->model( 'diplomas_table__diplomas_config_fields__model' );
        ////////////////////////////////////////////////////////////////////////
        $this->form_validation->set_error_delimiters( "<span class='error'>", "</span>" );
        Template::set_block('sub_nav', 'content/_sub_nav');
        //Assets::add_module_js('university_graduates', 'university_graduates.js');
        Template::set( 'diplomas_design_type',              $this->config->item( 'diplomas_design_type' ) ); 
        Template::set( 'diplomas_design_form',              $this->config->item( 'diplomas_design_form' ) ); 
        Template::set( 'diplomas_design_field_type',        $this->config->item( 'diplomas_design_field_type' ) ); 
        Template::set( 'diplomas_design_field_default',     $this->config->item( 'diplomas_design_field_default' ) ); 
        Template::set( 'diploma_img_max_size_in_MB',        $this->config->item( 'diploma_img_max_size_in_MB' ) ); 
        Template::set( 'diploma_img_max_width___PX',        $this->config->item( 'diploma_img_max_width___PX' ) ); 
        Template::set( 'diploma_img_max_height__PX',        $this->config->item( 'diploma_img_max_height__PX' ) ); 
        Template::set( 'diploma_img_resolution_DPI',        $this->config->item( 'diploma_img_resolution_DPI' ) ); 
        Template::set( 'diploma_img_upload_folder_image',   $this->config->item( 'diploma_img_upload_folder_image' ) ); 
        Template::set( 'diploma_img_upload_folder_thumb',   $this->config->item( 'diploma_img_upload_folder_thumb' ) ); 
    }
    private function convert__UTF8__to__Romanian_CI_AI( $string )
    {
        return str_replace("&quot;", "\"", str_replace("\"", "&quot;", str_replace( "â", "â", str_replace( "Â", "Â", str_replace( "Ă", "Ă", str_replace( "ă", "ă", str_replace( "Ș", "Ş", str_replace( "ș", "ş", str_replace( "Ț", "Ţ", str_replace( "ț", "ţ", $string ) ) ) ) ) ) ) ) ) );
    }
	
	private function changeDateTimeFormat($dateString)
	{
		$dateTime = DateTime::createFromFormat('Y-m-d', $dateString);
		return $dateTime->format('d-m-Y');
	}
	
    public function index( $attr = NULL )
    {	  
		$SQL    =   'SELECT TOP 400 '.
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Gratuate_ID, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.ID_AnUniv, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Diploma_Config_Id, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Diploma_Code_Value, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_001_NrCrediteProgram, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_002_SesiuneExamenAbsolvire, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_003_AnExamenAbsolvire, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_012_NumeIntreg, ' . PHP_EOL . 
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_026_DenumireDomeniu, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_027_DenumireSpecializare, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_028_DenumireProgramDeStudii, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_031_Facultate, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_032_FormaInvatamant, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_033_CicluDeStudii, ' . PHP_EOL .
		$this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.status' . PHP_EOL .
		' FROM  ' . $this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . PHP_EOL .
		' WHERE ' . $this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Gratuate_ID > 0 ' . PHP_EOL;

		if( isset( $_POST['ABSOLVIRE'] ) && ( $_POST['ABSOLVIRE'] != '' ) )
		{
			$SQL    .= 	' AND ( ' . $this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_003_AnExamenAbsolvire = \''.$_POST['ABSOLVIRE'] .'\' ) ' . PHP_EOL;
		}
		if( isset( $_POST['FACULTATE'] ) && ( $_POST['FACULTATE'] != '' ) )
		{
			$SQL    .= 	' AND ( ' . $this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_031_Facultate = \''.$_POST['FACULTATE'] .'\' ) ' . PHP_EOL;
		}
		if( isset( $_POST['CICLU'] ) && ( $_POST['CICLU'] != '' ) )
		{
			$SQL    .= 	' AND ( ' . $this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_033_CicluDeStudii = \''.$_POST['CICLU'] .'\' ) ' . PHP_EOL;
		}
		if( isset( $_POST['DOMENIU'] ) && ( $_POST['DOMENIU'] != '' ) )
		{
			$SQL    .= 	' AND ( ' . $this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_026_DenumireDomeniu = \''.$_POST['DOMENIU'] .'\' ) ' . PHP_EOL;
		}
		if( isset( $_POST['SPECIALIZARE'] ) && ( $_POST['SPECIALIZARE'] != '' ) )
		{
			$SQL    .= 	' AND ( ' . $this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Graduate_027_DenumireSpecializare = \''.$_POST['SPECIALIZARE'] .'\' ) ' . PHP_EOL;
		}
                if( isset( $_POST['STATUS'] ) && ( $_POST['STATUS'] != '' ) )
		{
			$SQL    .= 	' AND ( ' . $this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.status = \''.$_POST['STATUS'] .'\' ) ' . PHP_EOL;
		}
		$SQL    .=  ' ORDER BY  ' . $this->db->dbprefix . $this->diplomas_table__diplomas_graduates__model->table_name . '.Gratuate_ID DESC ' . PHP_EOL ;
		//echo( nl2br( $SQL ) );
		Template::set( 'GRADUATES', ( $GRADUATES = $this->db->query( $SQL )->result() ) );
		//die( print_r( $GRADUATES ) );
		if( empty( ( $FACULTATI = array() ) ) && !empty( ( $Facultati = $this->diplomas_table__diplomas_graduates__model->distinct()->select('Graduate_031_Facultate')->find_all() ) ) )
		{
			foreach( $Facultati as $F )
			{
				if( $F->Graduate_031_Facultate ) $FACULTATI[] = $F->Graduate_031_Facultate;
			}
		}
		Template::set( 'FACULTATI', array_unique( $FACULTATI ) );

		if( empty( ( $CICLURI = array() ) ) && !empty( ( $Cicluri = $this->diplomas_table__diplomas_graduates__model->distinct()->select('Graduate_033_CicluDeStudii')->find_all() ) ) )
		{
			foreach( $Cicluri as $C )
			{
				if( $C->Graduate_033_CicluDeStudii ) $CICLURI[] = $C->Graduate_033_CicluDeStudii;
			}
		}
		Template::set( 'CICLURI', array_unique( $CICLURI ) );

		if( empty( ( $DOMENII = array() ) ) && !empty( ( $Domenii = $this->diplomas_table__diplomas_graduates__model->distinct()->select('Graduate_026_DenumireDomeniu')->find_all() ) ) )
		{
			foreach( $Domenii as $D )
			{
				if( $D->Graduate_026_DenumireDomeniu ) $DOMENII[] = $D->Graduate_026_DenumireDomeniu;
			}
		}
		Template::set( 'DOMENII', array_unique( $DOMENII ) );

		if( empty( ( $SPECIALIZARI = array() ) ) && !empty( ( $Specializari = $this->diplomas_table__diplomas_graduates__model->distinct()->select('Graduate_027_DenumireSpecializare')->find_all() ) ) )
		{
			foreach( $Specializari as $S )
			{
				if( $S->Graduate_027_DenumireSpecializare ) $SPECIALIZARI[] = $S->Graduate_027_DenumireSpecializare;
			}
		}
		Template::set( 'SPECIALIZARI', array_unique( $SPECIALIZARI ) );
                
                if( empty( ( $STATUSURI = array() ) ) && !empty( ( $Statusuri = $this->diplomas_table__diplomas_graduates__model->distinct()->select('status')->find_all() ) ) )
		{
			foreach( $Statusuri as $S )
			{
				if( $S->status ) $STATUSURI[] = $S->status;
			}
		}
		Template::set( 'STATUSURI', array_unique( $STATUSURI ) );
		Template::set( 'ATTRIBUTE', 'search' );
		Template::set( 'toolbar_title', lang( 'university_graduates_manage' ) );
		Template::render();
        
    }
    public function create()
    {
        if( isset( $_POST ) && !empty( $_POST ) )
        {
            $i = $this->diplomas_table__diplomas_graduates__model->insert( $_POST );
            if( $i )
            {
                echo( $i );
            }
            die();
        } 
        else 
        {
            Template::set(  'ANIUNIV', 
                            $this->agsis_table__anuniversitar__model
                                ->select( 'ID_AnUniv' )
                                ->select( 'Denumire' )
                                ->find_all() );
            Template::set(  'DIPLOME', 
                            $this->diplomas_table__diplomas_config__model
                                ->select( 'Diploma_Config_Id' )
                                ->select( 'Diploma_Config_Title' )
                                ->select( 'Diploma_Config_Type' )
                                ->where('deleted', null)
                                ->find_all() );
            Template::render( 'blank' );
        }
    }

    public function edit( $Gratuate_ID = NULL )
    {
        if (    
                ( $Gratuate_ID = ( isset( $_POST['Gratuate_ID'] ) && $_POST['Gratuate_ID'] ? $_POST['Gratuate_ID'] : $Gratuate_ID ) ) &&
                ( is_int( ( $Gratuate_ID = intval( $Gratuate_ID ) ) ) && !empty( $Gratuate_ID ) ) 
                && 
                ( $GRADUATE = $this->diplomas_table__diplomas_graduates__model->find( $Gratuate_ID ) )
            ) 
        {
            $GRADUATE = ( array ) $GRADUATE;
			//die(print_r($GRADUATE));

            if( isset( $_POST ) && !empty( $_POST ) )
            {
                //die(print_r($_POST));
                if( $this->diplomas_table__diplomas_graduates__model
                        ->update(   $Gratuate_ID,
                                    array(  
                                            'ID_AnUniv'                                 => ( isset( $_POST['ID_AnUniv'] ) && intval( ( $_POST['ID_AnUniv'] = trim( $_POST['ID_AnUniv'] ) ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['ID_AnUniv'] ) : NULL ),
                                            'Diploma_Config_Id'                         => ( isset( $_POST['Diploma_Config_Id'] ) && intval( ( $_POST['Diploma_Config_Id'] = trim( $_POST['Diploma_Config_Id'] ) ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Diploma_Config_Id'] ) : NULL ),
                                            'NrInregistrare'                            => ( isset( $_POST['NrInregistrare'] ) && intval( ( $_POST['NrInregistrare'] = trim( $_POST['NrInregistrare'] ) ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['NrInregistrare'] ) : NULL ),
                                            
                                            'Graduate_001_NrCrediteProgram'             => ( isset( $_POST['Graduate_001_NrCrediteProgram'] ) && ( $_POST['Graduate_001_NrCrediteProgram'] = trim( $_POST['Graduate_001_NrCrediteProgram'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_001_NrCrediteProgram'] ) : NULL ),
                                            'Graduate_002_SesiuneExamenAbsolvire'       => ( isset( $_POST['Graduate_002_SesiuneExamenAbsolvire'] ) && ( $_POST['Graduate_002_SesiuneExamenAbsolvire'] = trim( $_POST['Graduate_002_SesiuneExamenAbsolvire'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_002_SesiuneExamenAbsolvire'] ) : NULL ),
                                            'Graduate_003_AnExamenAbsolvire'            => ( isset( $_POST['Graduate_003_AnExamenAbsolvire'] ) && ( $_POST['Graduate_003_AnExamenAbsolvire'] = trim( $_POST['Graduate_003_AnExamenAbsolvire'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_003_AnExamenAbsolvire'] ) : NULL ),
                                            'Graduate_004_DenumireProba1'               => ( isset( $_POST['Graduate_004_DenumireProba1'] ) && ( $_POST['Graduate_004_DenumireProba1'] = trim( $_POST['Graduate_004_DenumireProba1'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_004_DenumireProba1'] ) : NULL ),
                                            'Graduate_005_NrCrediteProba1'              => ( isset( $_POST['Graduate_005_NrCrediteProba1'] ) && ( $_POST['Graduate_005_NrCrediteProba1'] = trim( $_POST['Graduate_005_NrCrediteProba1'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_005_NrCrediteProba1'] ) : NULL ),
                                            'Graduate_006_Nota1'                        => ( isset( $_POST['Graduate_006_Nota1'] ) && ( $_POST['Graduate_006_Nota1'] = trim( $_POST['Graduate_006_Nota1'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_006_Nota1'] ) : NULL ),
                                            'Graduate_007_NotaInCifreSiLitere1'         => ( isset( $_POST['Graduate_007_NotaInCifreSiLitere1'] ) && ( $_POST['Graduate_007_NotaInCifreSiLitere1'] = trim( $_POST['Graduate_007_NotaInCifreSiLitere1'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_007_NotaInCifreSiLitere1'] ) : NULL ),
                                            'Graduate_008_DenumireProba2'               => ( isset( $_POST['Graduate_008_DenumireProba2'] ) && ( $_POST['Graduate_008_DenumireProba2'] = trim( $_POST['Graduate_008_DenumireProba2'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_008_DenumireProba2'] ) : NULL ),
                                            'Graduate_009_NrCrediteProba2'              => ( isset( $_POST['Graduate_009_NrCrediteProba2'] ) && ( $_POST['Graduate_009_NrCrediteProba2'] = trim( $_POST['Graduate_009_NrCrediteProba2'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_009_NrCrediteProba2'] ) : NULL ),
                                            'Graduate_010_Nota2'                        => ( isset( $_POST['Graduate_010_Nota2'] ) && ( $_POST['Graduate_010_Nota2'] = trim( $_POST['Graduate_010_Nota2'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_010_Nota2'] ) : NULL ),
                                            'Graduate_011_NotaInCifreSiLitere2'         => ( isset( $_POST['Graduate_011_NotaInCifreSiLitere2'] ) && ( $_POST['Graduate_011_NotaInCifreSiLitere2'] = trim( $_POST['Graduate_011_NotaInCifreSiLitere2'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_011_NotaInCifreSiLitere2'] ) : NULL ),
                                            'Graduate_012_NumeIntreg'                   => ( isset( $_POST['Graduate_012_NumeIntreg'] ) && ( $_POST['Graduate_012_NumeIntreg'] = trim( $_POST['Graduate_012_NumeIntreg'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_012_NumeIntreg'] ) : NULL ),
                                            'Graduate_013_DataNastere'                  => ( isset( $_POST['Graduate_013_DataNastere'] ) && ( $_POST['Graduate_013_DataNastere'] = trim( $_POST['Graduate_013_DataNastere'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_013_DataNastere'] ) : NULL ),
                                            'Graduate_014_ZiNastere'                    => ( isset( $_POST['Graduate_014_ZiNastere'] ) && ( $_POST['Graduate_014_ZiNastere'] = trim( $_POST['Graduate_014_ZiNastere'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_014_ZiNastere'] ) : NULL ),
                                            'Graduate_015_LunaNastere'                  => ( isset( $_POST['Graduate_015_LunaNastere'] ) && ( $_POST['Graduate_015_LunaNastere'] = trim( $_POST['Graduate_015_LunaNastere'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_015_LunaNastere'] ) : NULL ),
                                            'Graduate_016_AnNastere'                    => ( isset( $_POST['Graduate_016_AnNastere'] ) && ( $_POST['Graduate_016_AnNastere'] = trim( $_POST['Graduate_016_AnNastere'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_016_AnNastere'] ) : NULL ),
                                            'Graduate_017_LunaNastereInLitere'          => ( isset( $_POST['Graduate_017_LunaNastereInLitere'] ) && ( $_POST['Graduate_017_LunaNastereInLitere'] = trim( $_POST['Graduate_017_LunaNastereInLitere'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_017_LunaNastereInLitere'] ) : NULL ),
                                            'Graduate_018_MedieFinalaAbsolvire'         => ( isset( $_POST['Graduate_018_MedieFinalaAbsolvire'] ) && ( $_POST['Graduate_018_MedieFinalaAbsolvire'] = trim( $_POST['Graduate_018_MedieFinalaAbsolvire'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_018_MedieFinalaAbsolvire'] ) : NULL ),
                                            'Graduate_019_DenumireTaraNastere'          => ( isset( $_POST['Graduate_019_DenumireTaraNastere'] ) && ( $_POST['Graduate_019_DenumireTaraNastere'] = trim( $_POST['Graduate_019_DenumireTaraNastere'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_019_DenumireTaraNastere'] ) : NULL ),
                                            'Graduate_020_DenumireLocalitateNastere'    => ( isset( $_POST['Graduate_020_DenumireLocalitateNastere'] ) && ( $_POST['Graduate_020_DenumireLocalitateNastere'] = trim( $_POST['Graduate_020_DenumireLocalitateNastere'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_020_DenumireLocalitateNastere'] ) : NULL ),
                                            'Graduate_021_DenumireJudetNastere'         => ( isset( $_POST['Graduate_021_DenumireJudetNastere'] ) && ( $_POST['Graduate_021_DenumireJudetNastere'] = trim( $_POST['Graduate_021_DenumireJudetNastere'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_021_DenumireJudetNastere'] ) : NULL ),
                                            'Graduate_022_FormulaAdresare'              => ( isset( $_POST['Graduate_022_FormulaAdresare'] ) && ( $_POST['Graduate_022_FormulaAdresare'] = trim( $_POST['Graduate_022_FormulaAdresare'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_022_FormulaAdresare'] ) : NULL ),
                                            'Graduate_024_FormulaAdresareArticulat'     => ( isset( $_POST['Graduate_024_FormulaAdresareArticulat'] ) && ( $_POST['Graduate_024_FormulaAdresareArticulat'] = trim( $_POST['Graduate_024_FormulaAdresareArticulat'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_024_FormulaAdresareArticulat'] ) : NULL ),
                                            'Graduate_025_FormulaAdresareSufixF'        => ( isset( $_POST['Graduate_025_FormulaAdresareSufixF'] ) && ( $_POST['Graduate_025_FormulaAdresareSufixF'] = trim( $_POST['Graduate_025_FormulaAdresareSufixF'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_025_FormulaAdresareSufixF'] ) : NULL ),
                                            'Graduate_026_DenumireDomeniu'              => ( isset( $_POST['Graduate_026_DenumireDomeniu'] ) && ( $_POST['Graduate_026_DenumireDomeniu'] = trim( $_POST['Graduate_026_DenumireDomeniu'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_026_DenumireDomeniu'] ) : NULL ),
                                            'Graduate_027_DenumireSpecializare'         => ( isset( $_POST['Graduate_027_DenumireSpecializare'] ) && ( $_POST['Graduate_027_DenumireSpecializare'] = trim( $_POST['Graduate_027_DenumireSpecializare'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_027_DenumireSpecializare'] ) : NULL ),
                                            'Graduate_028_DenumireProgramDeStudii'      => ( isset( $_POST['Graduate_028_DenumireProgramDeStudii'] ) && ( $_POST['Graduate_028_DenumireProgramDeStudii'] = trim( $_POST['Graduate_028_DenumireProgramDeStudii'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_028_DenumireProgramDeStudii'] ) : NULL ),
                                            'Graduate_029_Universitate'                 => ( isset( $_POST['Graduate_029_Universitate'] ) && ( $_POST['Graduate_029_Universitate'] = trim( $_POST['Graduate_029_Universitate'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_029_Universitate'] ) : NULL ),
                                            'Graduate_030_UniversitateArticulat'        => ( isset( $_POST['Graduate_030_UniversitateArticulat'] ) && ( $_POST['Graduate_030_UniversitateArticulat'] = trim( $_POST['Graduate_030_UniversitateArticulat'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_030_UniversitateArticulat'] ) : NULL ),
                                            'Graduate_031_Facultate'                    => ( isset( $_POST['Graduate_031_Facultate'] ) && ( $_POST['Graduate_031_Facultate'] = trim( $_POST['Graduate_031_Facultate'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_031_Facultate'] ) : NULL ),
                                            'Graduate_032_FormaInvatamant'              => ( isset( $_POST['Graduate_032_FormaInvatamant'] ) && ( $_POST['Graduate_032_FormaInvatamant'] = trim( $_POST['Graduate_032_FormaInvatamant'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_032_FormaInvatamant'] ) : NULL ),
                                            'Graduate_033_CicluDeStudii'                => ( isset( $_POST['Graduate_033_CicluDeStudii'] ) && ( $_POST['Graduate_033_CicluDeStudii'] = trim( $_POST['Graduate_033_CicluDeStudii'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_033_CicluDeStudii'] ) : NULL ),
                                            'Graduate_034_LimbaPredare'                 => ( isset( $_POST['Graduate_034_LimbaPredare'] ) && ( $_POST['Graduate_034_LimbaPredare'] = trim( $_POST['Graduate_034_LimbaPredare'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_034_LimbaPredare'] ) : NULL ),
                                            'Graduate_035_NumeRector'                   => ( isset( $_POST['Graduate_035_NumeRector'] ) && ( $_POST['Graduate_035_NumeRector'] = trim( $_POST['Graduate_035_NumeRector'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_035_NumeRector'] ) : NULL ),
                                            'Graduate_036_NumeSecretarSefUniversitate'  => ( isset( $_POST['Graduate_036_NumeSecretarSefUniversitate'] ) && ( $_POST['Graduate_036_NumeSecretarSefUniversitate'] = trim( $_POST['Graduate_036_NumeSecretarSefUniversitate'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_036_NumeSecretarSefUniversitate'] ) : NULL ),
                                            'Graduate_037_NumeDecan'                    => ( isset( $_POST['Graduate_037_NumeDecan'] ) && ( $_POST['Graduate_037_NumeDecan'] = trim( $_POST['Graduate_037_NumeDecan'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_037_NumeDecan'] ) : NULL ),
                                            'Graduate_038_NumeSecretarSefFacultate'     => ( isset( $_POST['Graduate_038_NumeSecretarSefFacultate'] ) && ( $_POST['Graduate_038_NumeSecretarSefFacultate'] = trim( $_POST['Graduate_038_NumeSecretarSefFacultate'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_038_NumeSecretarSefFacultate'] ) : NULL ),
                                            'Graduate_039_DurataStudiilor'              => ( isset( $_POST['Graduate_039_DurataStudiilor'] ) && ( $_POST['Graduate_039_DurataStudiilor'] = trim( $_POST['Graduate_039_DurataStudiilor'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_039_DurataStudiilor'] ) : NULL ),
                                            'Graduate_040_FacultateArticulat'           => ( isset( $_POST['Graduate_040_FacultateArticulat'] ) && ( $_POST['Graduate_040_FacultateArticulat'] = trim( $_POST['Graduate_040_FacultateArticulat'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_040_FacultateArticulat'] ) : NULL ),
                                            'Graduate_041_MedieFinalaAbsolvireCifre'    => ( isset( $_POST['Graduate_041_MedieFinalaAbsolvireCifre'] ) && ( $_POST['Graduate_041_MedieFinalaAbsolvireCifre'] = trim( $_POST['Graduate_041_MedieFinalaAbsolvireCifre'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_041_MedieFinalaAbsolvireCifre'] ) : NULL ),
                                            'Graduate_042_TitlulObtinut'                => ( isset( $_POST['Graduate_042_TitlulObtinut'] ) && ( $_POST['Graduate_042_TitlulObtinut'] = trim( $_POST['Graduate_042_TitlulObtinut'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_042_TitlulObtinut'] ) : NULL ),
											'Graduate_043_FormulaAdresareSufixM'        => ( isset( $_POST['Graduate_043_FormulaAdresareSufixM'] ) && ( $_POST['Graduate_043_FormulaAdresareSufixM'] = trim( $_POST['Graduate_043_FormulaAdresareSufixM'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_043_FormulaAdresareSufixM'] ) : NULL ),
											'Graduate_044_PrenumeTata'                  => ( isset( $_POST['Graduate_044_PrenumeTata'] ) && ( $_POST['Graduate_044_PrenumeTata'] = trim( $_POST['Graduate_044_PrenumeTata'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_044_PrenumeTata'] ) : NULL ),
											'Graduate_045_PrenumeMama'                  => ( isset( $_POST['Graduate_045_PrenumeMama'] ) && ( $_POST['Graduate_045_PrenumeMama'] = trim( $_POST['Graduate_045_PrenumeMama'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_045_PrenumeMama'] ) : NULL ),
											'Graduate_046_CNP'                			=> ( isset( $_POST['Graduate_046_CNP'] ) && ( $_POST['Graduate_046_CNP'] = trim( $_POST['Graduate_046_CNP'] ) ) ? ( $_POST['Graduate_046_CNP'] ) : NULL ),
											'Graduate_047_NrCrediteExamenFinal'         => ( isset( $_POST['Graduate_047_NrCrediteExamenFinal'] ) && ( $_POST['Graduate_047_NrCrediteExamenFinal'] = trim( $_POST['Graduate_047_NrCrediteExamenFinal'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_047_NrCrediteExamenFinal'] ) : NULL ),
											'Graduate_048_MedieExamenFinal'             => ( isset( $_POST['Graduate_048_MedieExamenFinal'] ) && ( $_POST['Graduate_048_MedieExamenFinal'] = trim( $_POST['Graduate_048_MedieExamenFinal'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_048_MedieExamenFinal '] ) : NULL ),
											'Graduate_049_MedieExamenFinalInCifreSiLitere' => ( isset( $_POST['Graduate_049_MedieExamenFinalInCifreSiLitere'] ) && ( $_POST['Graduate_049_MedieExamenFinalInCifreSiLitere'] = trim( $_POST['Graduate_049_MedieExamenFinalInCifreSiLitere'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_049_MedieExamenFinalInCifreSiLitere'] ) : NULL ),
											'Field_001_Value'   => ( isset( $_POST['Field_001_Value'] ) && ( $_POST['Field_001_Value'] = trim( $_POST['Field_001_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_001_Value'] ) : NULL ),
                                            'Field_002_Value'   => ( isset( $_POST['Field_002_Value'] ) && ( $_POST['Field_002_Value'] = trim( $_POST['Field_002_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_002_Value'] ) : NULL ),
                                            'Field_003_Value'   => ( isset( $_POST['Field_003_Value'] ) && ( $_POST['Field_003_Value'] = trim( $_POST['Field_003_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_003_Value'] ) : NULL ),
                                            'Field_004_Value'   => ( isset( $_POST['Field_004_Value'] ) && ( $_POST['Field_004_Value'] = trim( $_POST['Field_004_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_004_Value'] ) : NULL ),
                                            'Field_005_Value'   => ( isset( $_POST['Field_005_Value'] ) && ( $_POST['Field_005_Value'] = trim( $_POST['Field_005_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_005_Value'] ) : NULL ),
                                            'Field_006_Value'   => ( isset( $_POST['Field_006_Value'] ) && ( $_POST['Field_006_Value'] = trim( $_POST['Field_006_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_006_Value'] ) : NULL ),
                                            'Field_007_Value'   => ( isset( $_POST['Field_007_Value'] ) && ( $_POST['Field_007_Value'] = trim( $_POST['Field_007_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_007_Value'] ) : NULL ),
                                            'Field_008_Value'   => ( isset( $_POST['Field_008_Value'] ) && ( $_POST['Field_008_Value'] = trim( $_POST['Field_008_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_008_Value'] ) : NULL ),
                                            'Field_009_Value'   => ( isset( $_POST['Field_009_Value'] ) && ( $_POST['Field_009_Value'] = trim( $_POST['Field_009_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_009_Value'] ) : NULL ),
                                            'Field_010_Value'   => ( isset( $_POST['Field_010_Value'] ) && ( $_POST['Field_010_Value'] = trim( $_POST['Field_010_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_010_Value'] ) : NULL ),
                                            'Field_011_Value'   => ( isset( $_POST['Field_011_Value'] ) && ( $_POST['Field_011_Value'] = trim( $_POST['Field_011_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_011_Value'] ) : NULL ),
                                            'Field_012_Value'   => ( isset( $_POST['Field_012_Value'] ) && ( $_POST['Field_012_Value'] = trim( $_POST['Field_012_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_012_Value'] ) : NULL ),
                                            'Field_013_Value'   => ( isset( $_POST['Field_013_Value'] ) && ( $_POST['Field_013_Value'] = trim( $_POST['Field_013_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_013_Value'] ) : NULL ),
                                            'Field_014_Value'   => ( isset( $_POST['Field_014_Value'] ) && ( $_POST['Field_014_Value'] = trim( $_POST['Field_014_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_014_Value'] ) : NULL ),
                                            'Field_015_Value'   => ( isset( $_POST['Field_015_Value'] ) && ( $_POST['Field_015_Value'] = trim( $_POST['Field_015_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_015_Value'] ) : NULL ),
                                            'Field_016_Value'   => ( isset( $_POST['Field_016_Value'] ) && ( $_POST['Field_016_Value'] = trim( $_POST['Field_016_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_016_Value'] ) : NULL ),
                                            'Field_017_Value'   => ( isset( $_POST['Field_017_Value'] ) && ( $_POST['Field_017_Value'] = trim( $_POST['Field_017_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_017_Value'] ) : NULL ),
                                            'Field_018_Value'   => ( isset( $_POST['Field_018_Value'] ) && ( $_POST['Field_018_Value'] = trim( $_POST['Field_018_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_018_Value'] ) : NULL ),
                                            'Field_019_Value'   => ( isset( $_POST['Field_019_Value'] ) && ( $_POST['Field_019_Value'] = trim( $_POST['Field_019_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_019_Value'] ) : NULL ),
                                            'Field_020_Value'   => ( isset( $_POST['Field_020_Value'] ) && ( $_POST['Field_020_Value'] = trim( $_POST['Field_020_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_020_Value'] ) : NULL ),
                                            'Field_021_Value'   => ( isset( $_POST['Field_021_Value'] ) && ( $_POST['Field_021_Value'] = trim( $_POST['Field_021_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_021_Value'] ) : NULL ),
                                            'Field_022_Value'   => ( isset( $_POST['Field_022_Value'] ) && ( $_POST['Field_022_Value'] = trim( $_POST['Field_022_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_022_Value'] ) : NULL ),
                                            'Field_023_Value'   => ( isset( $_POST['Field_023_Value'] ) && ( $_POST['Field_023_Value'] = trim( $_POST['Field_023_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_023_Value'] ) : NULL ),
                                            'Field_024_Value'   => ( isset( $_POST['Field_024_Value'] ) && ( $_POST['Field_024_Value'] = trim( $_POST['Field_024_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_024_Value'] ) : NULL ),
                                            'Field_025_Value'   => ( isset( $_POST['Field_025_Value'] ) && ( $_POST['Field_025_Value'] = trim( $_POST['Field_025_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_025_Value'] ) : NULL ),
                                            'Field_026_Value'   => ( isset( $_POST['Field_026_Value'] ) && ( $_POST['Field_026_Value'] = trim( $_POST['Field_026_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_026_Value'] ) : NULL ),
                                            'Field_027_Value'   => ( isset( $_POST['Field_027_Value'] ) && ( $_POST['Field_027_Value'] = trim( $_POST['Field_027_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_027_Value'] ) : NULL ),
                                            'Field_028_Value'   => ( isset( $_POST['Field_028_Value'] ) && ( $_POST['Field_028_Value'] = trim( $_POST['Field_028_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_028_Value'] ) : NULL ),
                                            'Field_029_Value'   => ( isset( $_POST['Field_029_Value'] ) && ( $_POST['Field_029_Value'] = trim( $_POST['Field_029_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_029_Value'] ) : NULL ),
                                            'Field_030_Value'   => ( isset( $_POST['Field_030_Value'] ) && ( $_POST['Field_030_Value'] = trim( $_POST['Field_030_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Field_030_Value'] ) : NULL ),
											  'Graduate_043_FormulaAdresareSufixM'   => ( isset( $_POST['Graduate_043_FormulaAdresareSufixM'] ) && ( $_POST['Graduate_043_FormulaAdresareSufixM'] = trim( $_POST['Graduate_043_FormulaAdresareSufixM'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Graduate_043_FormulaAdresareSufixM'] ) : NULL ),

                                            'Diploma_Code_Value'    => ( $code_val = ( isset( $_POST['Diploma_Code_Value'] ) && ( $_POST['Diploma_Code_Value'] = trim( $_POST['Diploma_Code_Value'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['Diploma_Code_Value'] ) : NULL ) ),
                                            'status'                => ( $status = ( isset( $_POST['status'] ) && ( $_POST['status'] = trim( $_POST['status'] ) ) ? $this->convert__UTF8__to__Romanian_CI_AI( $_POST['status'] ) : NULL  )),                   
                                        ) ) )
                {
                    if( $code_val && $status )
                    {
                        $this->diplomas_table__diplomas_codes__model->update($code_val, array('Diploma_Code_Status' => $status));
                        
                    }
                    unset( $_POST );
                    redirect( SITE_AREA . '/content/university_graduates/edit/'.$Gratuate_ID );
                }
                die();
            } 
            else 
            {
                $DIPLOMA    = array();
                $SERIIsiNR  = array();
                $DIPLOMELE  = $this->diplomas_table__diplomas_config__model
                                ->select( $this->diplomas_table__diplomas_config__model->table_name . '.Diploma_Config_Id' )
                                ->select( $this->diplomas_table__diplomas_config__model->table_name . '.Diploma_Config_Title' )
                                ->select( $this->diplomas_table__diplomas_config__model->table_name . '.Diploma_Config_Type' )
                                ->where('deleted', null)        
                                ->find_all();
                if( isset( $GRADUATE['Diploma_Config_Id'] ) && is_int( ( $GRADUATE['Diploma_Config_Id'] = intval( $GRADUATE['Diploma_Config_Id'] ) ) ) && $GRADUATE['Diploma_Config_Id'] )
                {
                    $DIPLOMA = ( array ) $this->diplomas_table__diplomas_config__model->find( $GRADUATE['Diploma_Config_Id'] );
                    $SERIIsiNR = $this->diplomas_view__diplomas_codes_2_graduate__model
                                    ->select( $this->diplomas_view__diplomas_codes_2_graduate__model->table_name.".Diploma_Code_Value" )
                                    ->where( $this->diplomas_view__diplomas_codes_2_graduate__model->table_name.".Diploma_Code_Value like '%-" . strtoupper( $DIPLOMA['Diploma_Config_Type'] ) . "-%'" )
                                    ->where( $this->diplomas_view__diplomas_codes_2_graduate__model->table_name.".Gratuate_ID IS NULL" )
                                    ->where( $this->diplomas_view__diplomas_codes_2_graduate__model->table_name.".Diploma_Code_Status <>", 'cancel' )
                                    ->order_by( $this->diplomas_view__diplomas_codes_2_graduate__model->table_name.".Diploma_Code_Value" )
                                    ->find_all();
                }                
                Template::set( 'GRADUATE',  $GRADUATE );
                Template::set( 'DIPLOMA',   $DIPLOMA );
                Template::set( 'DIPLOMELE', $DIPLOMELE );
                Template::set( 'SERIIsiNR', $SERIIsiNR );
                Template::render( 'blank' );
            }
        } 
        else 
        {
            Template::set_message( lang('university_graduates_invalid_id'), 'error' );
            redirect(SITE_AREA . '/content/university_graduates');
        }
    }
	
	public function delete( $Gratuate_ID = NULL )
    {		
        if ( ( $Gratuate_ID = ( isset( $_POST['Gratuate_ID'] ) && $_POST['Gratuate_ID'] ? $_POST['Gratuate_ID'] : $Gratuate_ID ) ) &&
                ( is_int( ( $Gratuate_ID = intval( $Gratuate_ID ) ) ) && !empty( $Gratuate_ID ) ) 
                && 
                ( $GRADUATE = $this->diplomas_table__diplomas_graduates__model->find( $Gratuate_ID ) )
            ) 
        {
			$this->diplomas_table__diplomas_graduates__model->delete($Gratuate_ID); 
			Template::set_message( lang('university_graduates_delete'), 'success' );
            redirect(SITE_AREA . '/content/university_graduates');
        } 
        else 
        {
            Template::set_message( lang('university_graduates_invalid_id'), 'error' );
			redirect(SITE_AREA . '/content/university_graduates');
        }
    }
	
    public function report()
    {
        if( ( isset( $_POST['graduate_id'] ) && !empty( $_POST['graduate_id'] ) ) &&
            ( ( $graduates = $this->diplomas_table__diplomas_graduates__model->where_in( 'Gratuate_ID', $_POST['graduate_id']  )->find_all() ) && !empty( $graduates ) ) ) 
        {
            $MyHtml  =  '';
            $MyHtml .=  '<html>' . PHP_EOL; 
            $MyHtml .=      '<body>' . PHP_EOL;
            $MyHtml .=          '<table width="100%" border="1" cellspacing="0" cellpadding="2">' . PHP_EOL;
            $MyHtml .=              '<thead>' . PHP_EOL;
            $MyHtml .=                  '<tr>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Nr. crt.</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Seria și nr. actului de studii</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Numele, Inițiala/inițialele, prenumele titularului</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Locul și data nașterii</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Prenumele părinților</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Anul și sesiunea examenului de finalizare a studiilor</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Instituția care a asigurat școlarizarea</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Domeniul, specializarea, programul de studii</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Media examenului de finalizare a studiilor</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Promoția</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Data eliberării</td>' . PHP_EOL;
            $MyHtml .=                      '<td colspan="2" style="font-size: 10px; text-align: center;">Persoana care eliberează actul de studii</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Semnătura titularului</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Semnătura împuternicitului</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Seria și nr. actului de identitate</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Mențiuni</td>' . PHP_EOL;
            $MyHtml .=                      '<td rowspan="2" style="font-size: 10px; text-align: center;">Nr. crt.</td>' . PHP_EOL;
            $MyHtml .=                  '</tr>' . PHP_EOL;
            $MyHtml .=                  '<tr>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center;">Numele și prenumele</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center;">Semnătura</td>' . PHP_EOL;
            $MyHtml .=                  '</tr>' . PHP_EOL;
            $MyHtml .=                  '<tr>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">1</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">2</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">3</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">4</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">5</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">6</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">7</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">8</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">9</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">10</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">11</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">12</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">13</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">14</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">15</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">16</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">17</td>' . PHP_EOL;
            $MyHtml .=                      '<td style="font-size: 10px; text-align: center; background-color: #EEEEEE;">18</td>' . PHP_EOL;
            $MyHtml .=                  '</tr>' . PHP_EOL;
            $MyHtml .=              '</thead>' . PHP_EOL;
            $MyHtml .=              '<tbody>' . PHP_EOL;
            foreach( $graduates as $g )
            {
                $s = explode( '-', $g->Diploma_Code_Value );
                if( ( $g->status == 'printed' ) && ( sizeof( $s ) == 5 ) )
                {
                    $MyHtml .=      '<tr>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;">'.$s[4].'</td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;">'.$s[2].' '.$s[3].'</td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;">'.trim($g->Graduate_012_NumeIntreg).'</td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;"></td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;"></td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;">'.trim($g->Graduate_003_AnExamenAbsolvire).' '.trim($g->Graduate_002_SesiuneExamenAbsolvire).'</td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;"></td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;">'.trim($g->Graduate_026_DenumireDomeniu).' '.trim($g->Graduate_027_DenumireSpecializare).'</td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;">'.trim($g->Graduate_018_MedieFinalaAbsolvire).'</td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;">'.trim($g->Graduate_003_AnExamenAbsolvire).'</td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;"></td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;"></td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;"></td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;"></td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;"></td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;"></td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;"></td>' . PHP_EOL . 
                                        '<td style="font-size: 8px; text-align: center;">'.$s[4].'</td>' . PHP_EOL . 
                                    '</tr>' . PHP_EOL;
                }
            }
            $MyHtml .=              '</tbody>' . PHP_EOL;
            $MyHtml .=          '</table>' . PHP_EOL;
            $MyHtml .=      '</body>' . PHP_EOL;
            $MyHtml .=  '<html>' . PHP_EOL; 
            //die( $MyHtml );
            require_once 'vendor/autoload.php';
            $mpdf = new mPDF( 'utf-8', 'A3-L', 0, '', 10, 10, 10, 10, 10, 10 );
            $mpdf->SetDirectionality('ltr');
            $mpdf->useOnlyCoreFonts = true;
            $mpdf->use_kwt = true;
            $mpdf->allow_charset_conversion=true;
            $mpdf->charset_in='UTF-8';
            $mpdf->WriteHTML( $MyHtml );
            $mpdf->Output();
        }
        die();
    }
    public function pdf( $Gratuate_ID, $Diploma_Config_Face = "front", $Diploma_Action = 'print' )
    {
        if (    (   is_int( ( $Gratuate_ID = intval( $Gratuate_ID ) ) ) && 
                    !empty( $Gratuate_ID ) && 
                    ( $GRADUATE = $this->diplomas_table__diplomas_graduates__model->find( $Gratuate_ID ) ) && 
                    !empty( $GRADUATE )
                ) &&
                ( isset( $GRADUATE->Diploma_Config_Id ) && is_int( ( $Diploma_Config_Id = intval( $GRADUATE->Diploma_Config_Id ) ) ) ) && 
                (   $DIPLOMA_CONFIG =   $this->diplomas_table__diplomas_config__model
                                            ->select( 'Diploma_Config_Id' )
                                            ->select( 'Diploma_Config_Title' )
                                            ->select( 'Diploma_Config_Type' )
                                            ->select( 'Diploma_Config_Form' )
                                            ->select( 'Diploma_Config_Image_Front' )
                                            ->select( 'Diploma_Config_Image_Verso' )
                                            ->find( $Diploma_Config_Id )
                ) && !empty( $DIPLOMA_CONFIG ) &&
                in_array( $Diploma_Config_Face, array( 'front', 'verso' ) ) &&
                in_array( $Diploma_Action, array( 'print', 'view' ) )
            )
        {
            $GRADUATE       = ( array ) $GRADUATE;
            $DIPLOMA_CONFIG = ( array ) $DIPLOMA_CONFIG;
            if( $Diploma_Config_Face == 'front' )
            {
                $MyHtml = '';
                $BkgImg = $DIPLOMA_CONFIG['Diploma_Config_Image_Front'];
                $Fields = $this->diplomas_table__diplomas_config_fields__model->find_all_by( array( 'Diploma_Config_Id' => $DIPLOMA_CONFIG['Diploma_Config_Id'], 'Diploma_Config_Face' => 'front', 'Diploma_Field_Status' => 'live' ) );
            } else {
                $MyHtml = '';
                $BkgImg = $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'];
                $Fields = $this->diplomas_table__diplomas_config_fields__model->find_all_by( array( 'Diploma_Config_Id' => $DIPLOMA_CONFIG['Diploma_Config_Id'], 'Diploma_Config_Face' => 'verso', 'Diploma_Field_Status' => 'live' ) );
            }
            if  ( 
                    ( isset( $MyHtml ) && $MyHtml == '' ) && 
                    ( isset( $BkgImg ) && ( $BkgImg != '' ) && file_exists( $this->config->item( 'diploma_img_upload_folder_image' ) . $BkgImg ) ) && 
                    ( isset( $Fields ) && !empty( $Fields ) ) 
                )
            {
                list( $width, $height ) = getimagesize( $this->config->item( 'diploma_img_upload_folder_image' ) . $BkgImg );
                $MyHtml .=  '<html>' . PHP_EOL . PHP_EOL; 
                $MyHtml .=      '<head>' . PHP_EOL . PHP_EOL .
                                    '<style>' . PHP_EOL . PHP_EOL .
                                        'body {' . 
                                                'position: relative !important; ' . PHP_EOL .
                                                'display: table !important; ' . PHP_EOL .
                                                'margin: 0 !important; ' . PHP_EOL .
                                                'padding: 0 !important; ' . PHP_EOL .
                                                ( $Diploma_Action == 'view' 
                                                ?   'background-color: lightyellow !important; ' .
                                                    'background-image: url( \''.base_url( $this->config->item( 'diploma_img_upload_folder_image' ) . $BkgImg ) . '\' ) !important; ' .
                                                    'background-repeat: no-repeat;' 
                                                :   'background: white !important;' )  . PHP_EOL .
                                        '}'  . PHP_EOL .
                                    '</style>' . PHP_EOL . PHP_EOL .
                                    
                                '</head>';
                $MyHtml .=      '<body>' . PHP_EOL . PHP_EOL;
                foreach ( $Fields as $f => $field ) 
                {
                    if( isset( $GRADUATE[$field->Diploma_Field_Name] ) )
                    {
                        $MyHtml .=      '<div'
                                            . ' id="Field' . $field->Diploma_Field_Id . '"'
                                            . ' style="z-index: ' . $field->Diploma_Field_Id . ';'
                                                    . ' position: absolute;'
                                                    . ' background: transparent;'
                                                    . ' display: table; ' 
                                                    . ' top: ' . $field->Diploma_Field_Coord_Y . 'px; '
                                                    . ' left: ' . $field->Diploma_Field_Coord_X . 'px; '
                                                    . ' width: ' . $field->Diploma_Field_Area_Width . 'px; '
                                                    . ' height: ' . $field->Diploma_Field_Area_Height . 'px; '
                                                    . ' font-family: ' . $field->Diploma_Field_Font_Family . '; '
                                                    . ' font-size: ' . $field->Diploma_Field_Font_Size . 'px; '
                                                    . ' font-style: ' . $field->Diploma_Field_Font_Style . '; '
                                                    . ' font-weight: ' . $field->Diploma_Field_Font_Weight . '; '
                                                    . ' line-height: ' . $field->Diploma_Field_Line_Height . 'px; '
                                                    . ' text-transform: ' . $field->Diploma_Field_Text_Transform . '; '
                                                    . ' text-align: ' . $field->Diploma_Field_Text_Align . '; '
                                                    . ' ovewrflow: hidden;' .
                                         '">' . PHP_EOL . PHP_EOL;
                        $MyHtml .=      (   $field->Diploma_Field_Indent_Width 
                                        ?   '<span style="float: left; display: inline-block; width: ' . $field->Diploma_Field_Indent_Width . 'px; height: ' . $field->Diploma_Field_Line_Height . 'px;"></span>' 
                                        :   '' );
                        switch( $field->Diploma_Field_Type ) 
                        {
                            case 'alfanumeric-text':
                                $MyHtml .=  ( ( ( $field->Diploma_Field_Format_Value = intval( $field->Diploma_Field_Format_Value ) ) && is_int( $field->Diploma_Field_Format_Value ) )
                                            ?   '<span>' . 
                                                    substr( $GRADUATE[$field->Diploma_Field_Name], 0, $field->Diploma_Field_Format_Value ) . 
                                                '</span>' . PHP_EOL . PHP_EOL . PHP_EOL
                                            :   '' );
                                break;
                            case 'alfanumeric-html':

                                $MyHtml .=  ( ( ( $field->Diploma_Field_Format_Value = intval( $field->Diploma_Field_Format_Value ) ) && is_int( $field->Diploma_Field_Format_Value ) ) 
                                            ?   '<span>' . 
                                                    substr( $GRADUATE[$field->Diploma_Field_Name], 0, $field->Diploma_Field_Format_Value ) . 
                                                '</span>' . PHP_EOL . PHP_EOL . PHP_EOL
                                            :   '' );
                                break;
                            case 'numeric-integer':
                                $MyHtml .=  ( ( ( $field->Diploma_Field_Format_Value = intval( $field->Diploma_Field_Format_Value ) ) &&  is_int( $field->Diploma_Field_Format_Value ) ) 
                                            ?   '<span>' . 
                                                    substr( $GRADUATE[$field->Diploma_Field_Name], 0, $field->Diploma_Field_Format_Value ) . 
                                                '</span>' . PHP_EOL . PHP_EOL . PHP_EOL
                                            :   '' );
                                break;
                            case 'numeric-decimal':
                                $Diploma_Field_Format = explode( ',', $field->Diploma_Field_Format_Value );

                                $MyHtml .=  ( isset( $Diploma_Field_Format[1] ) && is_int( ( $Diploma_Field_Format[1] = intval( $Diploma_Field_Format[1] ) ) &&  $format_value ) 
                                            ?   '<span>' . 
                                                    number_format( $GRADUATE[$field->Diploma_Field_Name], $Diploma_Field_Format[1], ',', ' ' ) . 
                                                '</span>' . PHP_EOL
                                            :   '' );
                                break;
                            case 'datetime':
                                $MyHtml .= '<span>' . 
                                                $GRADUATE[$field->Diploma_Field_Name] . 
                                            '</span>' . PHP_EOL . PHP_EOL;
                                break;
                            default:
                                $MyHtml .= '<span></span>' . PHP_EOL . PHP_EOL;
                        } 
                        $MyHtml .= '</div>' . PHP_EOL . PHP_EOL;
                    }
                }
                $MyHtml .=      '</body>' . PHP_EOL . PHP_EOL;
                $MyHtml .=  '</html>'; 
                if( $Diploma_Action == "print" )
                {
                    require_once 'vendor/autoload.php';
                    $mpdf = new mPDF(   'utf-8', 
                                        ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Form'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Form'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Form'] ) ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Form'], $this->config->item( 'diplomas_design_form' ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Form'] : 'A3-L' ), 
                                        0, '', 0, 0, 0, 0, 0, 0 );
                    $mpdf->dpi = 100;
                    $mpdf->SetDirectionality('ltr');
                    $mpdf->useOnlyCoreFonts = true;
                    $mpdf->use_kwt = true;
                    $mpdf->allow_charset_conversion=true;
                    $mpdf->charset_in='UTF-8';
                    $mpdf->WriteHTML( $MyHtml );
                    $mpdf->Output();
                    die();
                } else {
                        die( $MyHtml );
                }
            } else {
                die( 'ERROR' );
            }
        } 
        else 
        {
            die( 'ERROR' );
        }
    }
    public function import( $action = '' )
    {
        $this->auth->restrict($this->permissionCreate);
        if( in_array( $action, array( '', 'absolvire', 'absolventi', 'inregistrare' ) ) )
        {
            if( ( $action == 'inregistrare' ) && isset( $_POST['absolvent'] ) && !empty( $_POST['absolvent'] ) )
            {
                $return = array();
                foreach( $_POST['absolvent'] as $a => $graduateToInsert )
                {
                    //die( print_r( $A ) );
                    if  ( isset( $graduateToInsert['ID_Student'] ) && ( $graduateToInsert['ID_Student'] = intval( $graduateToInsert['ID_Student'] ) ) )
                    {
                        if( empty( $this->diplomas_table__diplomas_graduates__model->select( $this->diplomas_table__diplomas_graduates__model->table_name.'.'.$this->diplomas_table__diplomas_graduates__model->key )->find_by( $this->diplomas_table__diplomas_graduates__model->table_name.'.ID_Student', $graduateToInsert['ID_Student'] ) ) )
                        {
                            $this->diplomas_table__diplomas_graduates__model->insert( $graduateToInsert );
                            $return[$graduateToInsert['ID_Student']] = 'succes';
                        } else {
                            $return[$graduateToInsert['ID_Student']] = 'duplicat';
                        }
                    } 
                }
                $this->diplomas_table__diplomas_graduates__model->close();
                header('Content-Type: application/json');
                die( json_encode( $return ) );
            }
            else if (   ( $action == 'absolventi' ) && 
                        ( isset( $_GET['ID_AnUnivAbsolvire'] ) && is_int( ( $_GET['ID_AnUnivAbsolvire'] = intval( $_GET['ID_AnUnivAbsolvire'] ) ) ) ) &&
                        ( isset( $_GET['ID_ProgramStudiuAbsolvit'] ) && is_int( ( $_GET['ID_ProgramStudiuAbsolvit'] = intval( $_GET['ID_ProgramStudiuAbsolvit'] ) ) ) ) 
                    )
            {
                $Array_AbsolvireFDSprobeAbsolventi = array();
                $SelectAbsolvireFDSprobeAbsolventi = 
                    $this->agsis_view_absolvire_fds_probeabsolventi_model
                        ->where( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.ID_AnUnivAbsolvire', $_GET['ID_AnUnivAbsolvire'] )
                        ->where( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.ID_ProgramStudiuAbsolvit', $_GET['ID_ProgramStudiuAbsolvit'] )
                        ->order_by( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.ID_Student' )
                        ->find_all();
                if( !empty( $SelectAbsolvireFDSprobeAbsolventi ) )
                {
                    foreach( $SelectAbsolvireFDSprobeAbsolventi as $SAFDSpA )
                    {
                        if( isset( $Array_AbsolvireFDSprobeAbsolventi[trim($SAFDSpA->ID_Student)] ) )
                        {
                            $Array_AbsolvireFDSprobeAbsolventi[trim($SAFDSpA->ID_Student)]['Graduate_008_DenumireProba2'] = $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->DenumireProba ) );
                            $Array_AbsolvireFDSprobeAbsolventi[trim($SAFDSpA->ID_Student)]['Graduate_009_NrCrediteProba2'] = trim( $SAFDSpA->NrCrediteProba );
                            $Array_AbsolvireFDSprobeAbsolventi[trim($SAFDSpA->ID_Student)]['Graduate_010_Nota2'] = trim( number_format( floatval( $SAFDSpA->Nota ), 2 ) );
                            $Array_AbsolvireFDSprobeAbsolventi[trim($SAFDSpA->ID_Student)]['Graduate_011_NotaInCifreSiLitere2'] = $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->NotaInCifreSiLitere ) );
                        } else {
                            //die( sizeof( $this->diplomas_table__diplomas_graduates__model->find( $SAFDSpA->ID_Student ) ) );
                            $Array_AbsolvireFDSprobeAbsolventi[trim($SAFDSpA->ID_Student)] = 
                            array( 
                                    'ID_Student'                                => trim( $SAFDSpA->ID_Student ),
                                    'Graduate_000_InregistrareImportata'        => ( empty( $this->diplomas_table__diplomas_graduates__model->select( $this->diplomas_table__diplomas_graduates__model->table_name.'.'.$this->diplomas_table__diplomas_graduates__model->key )->find_by( $this->diplomas_table__diplomas_graduates__model->table_name.'.ID_Student', $SAFDSpA->ID_Student ) ) ? 0 : 1 ),
                                    'Graduate_001_NrCrediteProgram'             => trim( $SAFDSpA->NrCrediteProgram ),
                                    'Graduate_002_SesiuneExamenAbsolvire'       => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->SesiuneExamenAbsolvire ) ),
                                    'Graduate_003_AnExamenAbsolvire'            => trim( $SAFDSpA->AnExamenAbsolvire ),
                                    'Graduate_004_DenumireProba1'               => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->DenumireProba ) ),
                                    'Graduate_005_NrCrediteProba1'              => trim( $SAFDSpA->NrCrediteProba ),
                                    'Graduate_006_Nota1'                        => trim( number_format( floatval( $SAFDSpA->Nota ), 2 ) ),
                                    'Graduate_007_NotaInCifreSiLitere1'         => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->NotaInCifreSiLitere ) ),
                                    'Graduate_012_NumeIntreg'                   => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->NumeIntreg ) ),
                                    'Graduate_013_DataNastere'                  => $this->changeDateTimeFormat(substr( trim( $SAFDSpA->DataNastere ), 0, 10 )),
                                    'Graduate_014_ZiNastere'                    => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->ZiNastere ) ),
                                    'Graduate_015_LunaNastere'                  => trim( $SAFDSpA->LunaNastere ),
                                    'Graduate_016_AnNastere'                    => trim( $SAFDSpA->AnNastere ),
                                    'Graduate_017_LunaNastereInLitere'          => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->LunaNastereInLitere ) ),
                                    'Graduate_018_MedieFinalaAbsolvire'         => $this->convert__UTF8__to__Romanian_CI_AI( trim( number_format( floatval( $SAFDSpA->MedieFinalaAbsolvire ), 2 ) ) ),
                                    'Graduate_019_DenumireTaraNastere'          => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->DenumireTaraNastere ) ),
                                    'Graduate_020_DenumireLocalitateNastere'    => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->DenumireLocalitateNastere ) ),
                                    'Graduate_021_DenumireJudetNastere'         => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->DenumireJudetNastere ) ),
                                    'Graduate_022_FormulaAdresare'              => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->FormulaAdresare ) ),
                                    'Graduate_024_FormulaAdresareArticulat'     => ( $SAFDSpA->FormulaAdresare == 'oamna' ? $this->convert__UTF8__to__Romanian_CI_AI( 'oamnei' ) : ( $SAFDSpA->FormulaAdresare == 'omnul' ? 'omnului' : '' ) ),
                                    'Graduate_025_FormulaAdresareSufixF'        => ( $SAFDSpA->FormulaAdresare == 'oamna' ? $this->convert__UTF8__to__Romanian_CI_AI( 'ă' ) : '' ),
                                    'Graduate_026_DenumireDomeniu'              => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->DenumireDomeniu ) ),
                                    'Graduate_027_DenumireSpecializare'         => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->DenumireSpecializare ) ),
                                    'Graduate_028_DenumireProgramDeStudii'      => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->DENUMIRE ) ),
                                    'Graduate_029_Universitate'                 => $this->convert__UTF8__to__Romanian_CI_AI( str_replace("\"", "&quot ", trim( $SAFDSpA->Universitate )) ),
                                    'Graduate_030_UniversitateArticulat'        => $this->convert__UTF8__to__Romanian_CI_AI( str_replace("\"", "&quot " , str_replace("ATEA", "ĂȚII", $SAFDSpA->Universitate)) ),
                                    'Graduate_031_Facultate'                    => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->Facultate ? $SAFDSpA->Facultate : $SAFDSpA->DenumireFacultate ) ),
                                    'Graduate_032_FormaInvatamant'              => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->FormaInvatamant ? $SAFDSpA->FormaInvatamant : $SAFDSpA->DenumireFormaInv ) ),
                                    'Graduate_033_CicluDeStudii'                => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->CicluDeStudii ? $SAFDSpA->CicluDeStudii : $SAFDSpA->DenumireCicluInv ) ),
                                    'Graduate_034_LimbaPredare'                 => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->LimbaPredare ) ),
                                    'Graduate_035_NumeRector'                   => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->NumeRector ) ),
                                    'Graduate_036_NumeSecretarSefUniversitate'  => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->NumeSecretarSefUniversitate ) ),
                                    'Graduate_037_NumeDecan'                    => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->NumeDecan ) ),
                                    'Graduate_038_NumeSecretarSefFacultate'     => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->NumeSecretarSefFacultate ) ),
                                    'Graduate_039_DurataStudiilor'              => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->DURATA ) ),
                                    'Graduate_040_FacultateArticulat'           => $this->convert__UTF8__to__Romanian_CI_AI( str_replace("ATEA", "ĂȚII", $SAFDSpA->Facultate) ),
                                    'Graduate_041_MedieFinalaAbsolvireCifre'    => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->MedieFinalaAni ) ),
                                    'Graduate_042_TitlulObtinut'                => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->TitlulObtinut ) ),
									'Graduate_043_FormulaAdresareSufixM'        => ($SAFDSpA->FormulaAdresare == 'oamna' ? '' : 'l'  ),
									'Graduate_044_PrenumeTata'                	=> $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->PrenumeTata ) ),
									'Graduate_045_PrenumeMama'                	=> $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->PrenumeMama ) ),
									'Graduate_046_CNP'                			=> $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->CNP ) ),
									'Graduate_047_NrCrediteExamenFinal'         => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->NrCrediteExamenFinal ) ),
									'Graduate_048_MedieExamenFinal'             => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->MedieExamenFinal ) ),
									'Graduate_049_MedieExamenFinalInCifreSiLitere' => $this->convert__UTF8__to__Romanian_CI_AI( trim( $SAFDSpA->MedieExamenFinalInCifreSiLitere ) ),
                                 );
                        }
                    }
                }
                $this->agsis_view_absolvire_fds_probeabsolventi_model->close();
                $this->diplomas_table__diplomas_graduates__model->close();
                header('Content-Type: application/json');
                die( json_encode( $Array_AbsolvireFDSprobeAbsolventi ) );
            } 
            else if( ( $action == 'absolvire' ) && isset( $_GET['ID_AnUnivAbsolvire'] ) && is_int( ( $_GET['ID_AnUnivAbsolvire'] = intval( $_GET['ID_AnUnivAbsolvire'] ) ) ) )
            {
                $Array_ProgramStudiuAbsolvit    =   array();
                $SelectProgramStudiuAbsolvit    =   $this->agsis_view_absolvire_fds_probeabsolventi_model
                                                    ->distinct()
                                                    ->select( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.ID_ProgramStudiuAbsolvit' )
                                                    ->select( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.DenumireFacultate' )
                                                    ->select( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.DenumireDomeniu' )
                                                    ->select( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.DenumireSpecializare' )
                                                    ->select( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.DenumireCicluInv' )
                                                    ->select( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.DenumireFormaInv' )
                                                    ->where( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.ID_AnUnivAbsolvire', $_GET['ID_AnUnivAbsolvire'] )
                                                    ->order_by( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.DenumireFacultate' )
                                                    ->order_by( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.DenumireDomeniu' )
                                                    ->order_by( $this->agsis_view_absolvire_fds_probeabsolventi_model->table_name.'.DenumireSpecializare' )
                                                    ->find_all();
                $this->agsis_view_absolvire_fds_probeabsolventi_model->close();
                $this->diplomas_table__diplomas_graduates__model->close();
                if( !empty( $SelectProgramStudiuAbsolvit ) )
                {
                    foreach( $SelectProgramStudiuAbsolvit as $SPSA )
                    {
                        if( !isset( $Array_ProgramStudiuAbsolvit[$SPSA->ID_ProgramStudiuAbsolvit] ) )
                        {
                            $Array_ProgramStudiuAbsolvit[$SPSA->ID_ProgramStudiuAbsolvit] = 
                                trim( $SPSA->DenumireFacultate ) . ' ' 
                                .   '{' . trim( $SPSA->DenumireDomeniu ) . '} '
                                .   '[' . trim( $SPSA->DenumireSpecializare ) . '] '
                                .   '(' . trim( $SPSA->DenumireCicluInv ) . ' / ' . trim( $SPSA->DenumireFormaInv ) . ')';
                        }
                    }
                }
                header('Content-Type: application/json');
                die( json_encode( $Array_ProgramStudiuAbsolvit ) );
            } else {
                Template::set( 'UniversityYears',       $this->agsis_table__anuniversitar__model
                                                            ->select( 'ID_AnUniv' )
                                                            ->select( 'Denumire' )
                                                            ->order_by( 'ID_AnUniv', 'DESC' )
                                                            ->find_all() );
                Template::set( 'UniversityDiplomas',    $this->diplomas_table__diplomas_config__model
                                                            ->select( $this->diplomas_table__diplomas_config__model->table_name . '.Diploma_Config_Id' )
                                                            ->select( $this->diplomas_table__diplomas_config__model->table_name . '.Diploma_Config_Title' )
                                                            ->select( $this->diplomas_table__diplomas_config__model->table_name . '.Diploma_Config_Type' )
                                                            ->where('deleted', null)                                    
                                                            ->find_all() );
                Template::set( 'toolbar_title',         lang( 'university_graduates_import_button' ) );
                Template::render();
            }
        }
    }
    public function help()
    {
        Template::set( 'toolbar_title', get_class( $this ) . ': ' . lang('university_graduates_help_manual'));
        Template::render();
    }
}
