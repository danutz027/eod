<?php defined('BASEPATH') || exit('No direct script access allowed');
$config['module_config']    =   array   (
                                            'description'   => 'Modul de Gestiune al Absolvenților Unversitari',
                                            'name'          => 'University Graduates',
                                            'version'       => '0.0.1',
                                            'author'        => 'Victor Afteni / victor.afteni@ontotech.ro',
                                        );