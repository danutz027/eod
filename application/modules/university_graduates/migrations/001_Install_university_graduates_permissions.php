<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_university_graduates_permissions extends Migration
{
	/**
	 * @var array Permissions to Migrate
	 */
	private $permissionValues = array(
		array(
			'name' => 'University_graduates.Content.View',
			'description' => 'View University_graduates Content',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Content.Create',
			'description' => 'Create University_graduates Content',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Content.Edit',
			'description' => 'Edit University_graduates Content',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Content.Delete',
			'description' => 'Delete University_graduates Content',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Reports.View',
			'description' => 'View University_graduates Reports',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Reports.Create',
			'description' => 'Create University_graduates Reports',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Reports.Edit',
			'description' => 'Edit University_graduates Reports',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Reports.Delete',
			'description' => 'Delete University_graduates Reports',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Settings.View',
			'description' => 'View University_graduates Settings',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Settings.Create',
			'description' => 'Create University_graduates Settings',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Settings.Edit',
			'description' => 'Edit University_graduates Settings',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Settings.Delete',
			'description' => 'Delete University_graduates Settings',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Developer.View',
			'description' => 'View University_graduates Developer',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Developer.Create',
			'description' => 'Create University_graduates Developer',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Developer.Edit',
			'description' => 'Edit University_graduates Developer',
			'status' => 'active',
		),
		array(
			'name' => 'University_graduates.Developer.Delete',
			'description' => 'Delete University_graduates Developer',
			'status' => 'active',
		),
    );

    /**
     * @var string The name of the permission key in the role_permissions table
     */
    private $permissionKey = 'permission_id';

    /**
     * @var string The name of the permission name field in the permissions table
     */
    private $permissionNameField = 'name';

	/**
	 * @var string The name of the role/permissions ref table
	 */
	private $rolePermissionsTable = 'role_permissions';

    /**
     * @var numeric The role id to which the permissions will be applied
     */
    private $roleId = '1';

    /**
     * @var string The name of the role key in the role_permissions table
     */
    private $roleKey = 'role_id';

	/**
	 * @var string The name of the permissions table
	 */
	private $tableName = 'permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$rolePermissionsData = array();
		foreach ($this->permissionValues as $permissionValue) {
			$this->db->insert($this->tableName, $permissionValue);

			$rolePermissionsData[] = array(
                $this->roleKey       => $this->roleId,
                $this->permissionKey => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->rolePermissionsTable, $rolePermissionsData);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
        $permissionNames = array();
		foreach ($this->permissionValues as $permissionValue) {
            $permissionNames[] = $permissionValue[$this->permissionNameField];
        }

        $query = $this->db->select($this->permissionKey)
                          ->where_in($this->permissionNameField, $permissionNames)
                          ->get($this->tableName);

        if ( ! $query->num_rows()) {
            return;
        }

        $permissionIds = array();
        foreach ($query->result() as $row) {
            $permissionIds[] = $row->{$this->permissionKey};
        }

        $this->db->where_in($this->permissionKey, $permissionIds)
                 ->delete($this->rolePermissionsTable);

        $this->db->where_in($this->permissionNameField, $permissionNames)
                 ->delete($this->tableName);
	}
}