<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['university_graduates_manage']    = 'Manage University Graduates';
$lang['university_graduates_edit']      = 'Edit';
$lang['university_graduates_true']      = 'True';
$lang['university_graduates_false']     = 'False';
$lang['university_graduates_create']    = 'Create';
$lang['university_graduates_list']      = 'List';
$lang['university_graduates_new']       = 'New';
$lang['university_graduates_import']    = 'Import';
$lang['university_graduates_help']      = 'Help';
$lang['university_graduates_refresh']   = 'Refresh';
$lang['university_graduates_save']      = 'Save';

$lang['university_graduates_list_title']    = 'University Graduates Index';
$lang['university_graduates_list_go_to']    = 'Go To Index';
$lang['university_graduates_refresh_title']   = 'Refresh University Graduates Table';
$lang['university_graduates_edit_title']     = 'Edit Graduate Record';
$lang['university_graduates_no_records']    = 'There are no university_graduates in the system.';
$lang['university_graduates_create_new']    = 'Create a new University Graduates.';
$lang['university_graduates_create_success']  = 'University Graduates successfully created.';
$lang['university_graduates_create_failure']  = 'There was a problem creating the university_graduates: ';
$lang['university_graduates_create_new_button'] = 'Create New University Graduates';
$lang['university_graduates_import_button'] = 'Import University Graduates from AGSIS';
$lang['university_graduates_help_manual'] = 'University Graduates Manual';
$lang['university_graduates_invalid_id']    = 'Invalid University Graduates ID.';
$lang['university_graduates_edit_success']    = 'University Graduates successfully saved.';
$lang['university_graduates_edit_failure']    = 'There was a problem saving the university_graduates: ';
$lang['university_graduates_delete_success']  = 'record(s) successfully deleted.';
$lang['university_graduates_delete_failure']  = 'We could not delete the record: ';
$lang['university_graduates_delete_error']    = 'You have not selected any records to delete.';
$lang['university_graduates_actions']     = 'Actions';
$lang['university_graduates_cancel']      = 'Cancel';
$lang['university_graduates_delete_record']   = 'Delete this University Graduates';
$lang['university_graduates_delete_confirm']  = 'Are you sure you want to delete this university_graduates?';
$lang['university_graduates_edit_heading']    = 'Edit University Graduates';
$lang['university_graduates_delete_student_confirm'] = 'Are you sure you want to delete this student?';
$lang['university_graduates_delete']    		     = 'Student successfully deleted!';

// Create/Edit Buttons
$lang['university_graduates_action_edit']   = 'Save University Graduates';
$lang['university_graduates_action_create']   = 'Create University Graduates';

// Activities
$lang['university_graduates_act_create_record'] = 'Created record with ID';
$lang['university_graduates_act_edit_record'] = 'Updated record with ID';
$lang['university_graduates_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['university_graduates_records_empty']    = 'No records found that match your selection.';
$lang['university_graduates_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['university_graduates_column_created']  = 'Created';
$lang['university_graduates_column_deleted']  = 'Deleted';
$lang['university_graduates_column_modified'] = 'Modified';
$lang['university_graduates_column_deleted_by'] = 'Deleted By';
$lang['university_graduates_column_created_by'] = 'Created By';
$lang['university_graduates_column_modified_by'] = 'Modified By';

// Module Details
$lang['university_graduates_module_name'] = 'University Graduates';
$lang['university_graduates_module_description'] = 'Your module description';
$lang['university_graduates_area_title'] = 'University Graduates';

// Fields