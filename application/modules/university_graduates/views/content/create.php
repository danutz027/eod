<?php
    $can_view       = $this->auth->has_permission( 'University_graduates.Content.View' );
    $can_create     = $this->auth->has_permission( 'University_graduates.Content.Create' );
    $can_edit       = $this->auth->has_permission( 'University_graduates.Content.Edit' );
    $can_delete     = $this->auth->has_permission( 'University_graduates.Content.Delete' );
?>
<html>
    <head>
        <link rel='stylesheet' type='text/css' href="<?php echo base_url( 'themes/admin/css/bootstrap.css' ); ?>" media='screen' />
        <link rel='stylesheet' type='text/css' href="<?php echo base_url( 'themes/admin/css/jqueryui.bootstrap.css' ); ?>" media='screen' />
        <link rel='stylesheet' type='text/css' href="<?php echo base_url( 'themes/admin/css/bootstrap-responsive.css' ); ?>" media='screen' />
        <link rel='stylesheet' type='text/css' href="<?php echo base_url( 'themes/admin/css/bootstrap-responsive.css' ); ?>" media='screen' />
        <link rel='stylesheet' type='text/css' href="<?php echo base_url( 'themes/admin/assets/jquery-ui/jquery-ui.min.css' ); ?>" media='screen' />
        <script src="<?php echo base_url( 'themes/admin/js/jquery-1.7.2.min.js' ); ?>" type='text/javascript'></script>
        <script src="<?php echo base_url( 'themes/admin/js/bootstrap.min.js' ); ?>" type='text/javascript'></script>
        <script src="<?php echo base_url( 'themes/admin/js/bootstrap-dropdown.js' ); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url( 'themes/admin/js/jquery-ui-1.8.13.min.js"' ); ?>" type="text/javascript"></script>
        <style>
            input[role="create"]
            {
                padding: 4px 6px !important;
                height: 30px !important;
                width: 100% !important;
            }
            select[role="create"]
            {
                padding: 4px 6px !important;
                height: 30px !important;
                width: 100% !important;
            }
            input[required='required'], 
            select[required='required'],
            textarea[required='required']
            {
                background-image: url( '<?php echo base_url( 'themes/admin/images/required.png' ); ?>' );
                background-repeat: no-repeat;
                background-position: right center;
            }
            .error {
                font-size: 10px !important;
                color: red !important;;
            }
        </style>
    </head>
    <body style="width: 96% !important; margin: 1em auto !important;">
        <fieldset>
            <div id="DivALERT" class="spanoffset-2 span10 alert alert-danger" style="display: none;">
                <span></span>
                <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">&times;</button>
            </div>
            <legend>
                <img id="spinner" src="<?php echo base_url('themes/admin/images/ajax-loader.gif'); ?>" style="height:1em; width:1em; display: none; float:right;"/>
                <span class="icon icon-file">&nbsp;</span>
                <?php echo lang('university_graduates_create_new'); ?>
            </legend>
            <div id="DivAJAX"  class="row row-fluid" style="width: 100%; display: none;"></div>
            <a id="anchor" name="anchor"></a>
            <?php echo form_open( $this->uri->uri_string(), array( 'id' => 'formCREATEDIPLOMA' ) ); ?>
                <table id="tableCr" class='table table-responsive table-striped'>
                    <thead>
                        <tr>
                            <td style='width: 15% !important;' class='text-right'>
                                <label class="small text-right" for="Diploma_Config_Id">
                                    Diploma:
                                </label>
                            </td>
                            <td style='width: 60% !important;'>
                                <select role="create" required="required" id="Diploma_Config_Id">
                                    <option></option>
                                    <?php if( isset( $DIPLOME ) && !empty( $DIPLOME ) ) : ?>
                                        <?php foreach( $DIPLOME as $D ) : ?>
                                            <option value="<?php echo $D->Diploma_Config_Id; ?>"><?php echo $D->Diploma_Config_Title; ?> [<?php echo $D->Diploma_Config_Type; ?>]</option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </td>
                            <td style='width: 10% !important;' class="muted">numeric-integer</td>
                            <td style='width: 15% !important;' class="error Gratuate_ID"></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class='text-right'>
                                <label class="small text-right" for="ID_AnUniv">
                                    An Universitar:
                                </label>
                            </td>
                            <td>
                                <select role="create" required="required" id="ID_AnUniv">
                                    <option></option>
                                    <?php if( isset( $ANIUNIV ) && !empty( $ANIUNIV ) ) : ?>
                                    <?php foreach( $ANIUNIV as $A ) : ?>
                                    <option value="<?php echo $A->ID_AnUniv; ?>"><?php echo $A->Denumire; ?> </option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </td>
                            <td class="muted">numeric-integer</td>
                            <td class="error ID_AnUniv"></td>
                        </tr>
                        <?php foreach ( $diplomas_design_field_default as $ddfd => $DDFD ) : ?>
                            <?php if ( $DDFD['required'] == true ) : ?>

                            <tr>
                                <td><label class='text-right' for="<?php echo $ddfd; ?>"><i><?php echo $DDFD['text']; ?></i>:</label></td>
                                <td><input type="<?php echo ( in_array( $DDFD['type'], array( 'numeric-integer', 'numeric-decimal' ) ) ? 'number' : ( $DDFD['type'] == 'datetime' ? 'text' : 'text' ) ); ?>" <?php if( $DDFD['type'] == 'datetime' ){ echo ' readonly="readonly" '; } else if( in_array( $DDFD['type'], array( 'numeric-integer', 'numeric-decimal' ) ) ) { echo ' min="0" step="' . ( $DDFD['type'] == 'numeric-integer' ? 1 : 0.01 ) . '" ';  } ?> role="create" id="<?php echo $ddfd; ?>" field-type="<?php echo $DDFD['type']; ?>" name="<?php echo $ddfd; ?>" value="<?php echo isset( $GRADUATE[$ddfd] ) ? trim( $GRADUATE[$ddfd] ) : ''; ?>" required="required" class="span8" /></td>
                                <td class="muted"><small><?php echo $DDFD['type']; ?></small></td>
                                <td class="error <?php echo $ddfd; ?>"></td>
                            </tr>

                            <?php endif; ?>
                        <?php endforeach; ?>
                                             
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td colspan="3">
                                <button id='CreateDiploma' type="button" formnovalidate class="btn btn-primary">
                                    <i class="icon icon-white icon-folder-close"></i> 
                                    <?php echo lang('university_graduates_save'); ?>
                                </button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            
            <?php echo form_close(); ?>
        </fieldset>
        <script type="text/javascript">
            $( document ).ready( function() {
                $('#CreateDiploma').click(function (event) {
                    event.preventDefault();
                    var valid = true;
                    $.each( $( '#tableCr [required="required"]' ), function ( index, value ) {
                        if( $( this ).val() )
                        {
                           $( this ).closest('tr').children('td.error').html('');
                        } else {
                           $( this ).closest('tr').children('td.error').html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                           valid = false; 
                        }
                    });
                    if ( valid == true ) {
                        var formData = new FormData();
                        $.each( $( '[role="create"]' ), function () {
                            formData.append( $(this).attr( 'id' ), $(this).val() );
                        });
                        console.log( 'formData', formData );
                        $.ajax({
                            url: '<?php echo site_url( 'admin/content/university_graduates/create'); ?>',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function( $i ){
                                if( $i )
                                {
                                    window.location.href = '<?php echo site_url( 'admin/content/university_graduates/edit/'); ?>/'+$i;
                                } else {
                                    alert( '<?php echo lang( 'bf_ajax_error' ); ?>' );
                                }
                            },
                            error: function(e){
                                console.log( e );
                                alert( 'Error: ' + e );
                            }
                        });
                    }
                });
            });
        </script>
    </body>
</html>