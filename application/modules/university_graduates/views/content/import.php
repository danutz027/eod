<style type="text/css">
    .popover{
        min-width: 600px !important;
        max-width: 600px !important;
        width: 600px !important;
    }
    .popover-content {
        height: 400px !important;
        overflow-y: scroll !important;
    }
    #Table_University_Graduates tbody tr td { text-align: center; }
</style>

<a id="anchor" name="anchor"></a>
<div id="Div_Alert" class="alert" style="display: none;"></div>

<table id='Table_Import_Absolventi' class="table-responsive table-striped" style="width: 100%; display: none;">
    <thead>
        <tr>
            <td class="text-center" colspan="4">
                <h3><?php echo lang( 'university_graduates_import_button' ); ?></h3>
            </td>
        </tr>
        <tr>
            <td class="text-center" colspan="4">
                <select id='ID_AnUnivAbsolvire' name='ID_AnUnivAbsolvire' class="row-fluid" style="display: inline-block;">
                        <option></option>
                    <?php if ( isset( $UniversityYears ) && !empty( $UniversityYears ) ) : ?>
                        <?php foreach( $UniversityYears as $UY ) : ?>
                        <option value='<?php e( $UY->ID_AnUniv ); ?>'><?php echo $UY->Denumire; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="text-center" colspan="4">
                <select id='ID_ProgramStudiuAbsolvit' name='ID_ProgramStudiuAbsolvit' class="row-fluid" style="display: none;">
                </select>
            </td>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr>
            <td class="text-center" colspan="4">
                <select id="Diploma_Config_Id" name="Diploma_Config_Id" class="row-fluid">
                    <option></option>
                    <?php if( isset( $UniversityDiplomas ) && !empty( $UniversityDiplomas ) ) : ?>
                        <?php foreach( $UniversityDiplomas as $Diploma ) : ?>
                            <option type="<?php echo( $Diploma->Diploma_Config_Type ); ?>" value="<?php echo( $Diploma->Diploma_Config_Id ); ?>" <?php echo ( ( isset( $GRADUATE['Diploma_Config_Id'] ) ? intval( trim( $GRADUATE['Diploma_Config_Id'] ) ) : 0 ) == $Diploma->Diploma_Config_Id ? 'selected' : '' ); ?>>
                                <?php echo( $Diploma->Diploma_Config_Title ); ?> [ <?php echo( $Diploma->Diploma_Config_Type ); ?> ]
                            </option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="text-center" colspan="4">
                <button id="Submit_Import" type="button" class="row-fluid btn btn-primary">
                    Importă Absolvenții Selectați
                </button>
            </td>
        </tr>
    </tfoot>
</table>
<script type="text/javascript">
    $( document ).ready( function() {
        
        if  ( 
                ( typeof $( 'a[name="anchor"]' ) == 'object' ) &&
                ( typeof $( 'div#Div_Alert' ) == 'object' ) &&
                ( typeof $( 'table#Table_Import_Absolventi' ) == 'object' ) &&
                ( typeof $( 'table#Table_Import_Absolventi tbody' ) != 'undefined' ) &&
                ( typeof $( 'select#ID_AnUnivAbsolvire' ) == 'object' ) &&
                ( typeof $( 'select#ID_ProgramStudiuAbsolvit' ) == 'object' ) &&
                ( typeof $( 'select#Diploma_Config_Id' ) == 'object' ) && 
                ( typeof $( 'button#Submit_Import' ) == 'object' ) 
            )
        {
            
            //var $anchor                         = $( 'a#anchor' );
            var $Div_Alert                      = $( 'div#Div_Alert' );
            var $Select_AnUnivAbsolvire         = $( 'select#ID_AnUnivAbsolvire' );
            var $Select_ProgramStudiuAbsolvit   = $( 'select#ID_ProgramStudiuAbsolvit' );   
            var $Table_Import_Absolventi        = $( 'table#Table_Import_Absolventi' ); 
            var $Table_Import_Absolventi_Body   = $Table_Import_Absolventi.find('tbody');
            var $Select_Diploma_Config          = $( 'select#Diploma_Config_Id' );
            var $Button_Submit_Import           = $( 'button#Submit_Import' );

            $Div_Alert.hide();
            $Table_Import_Absolventi.show();
            $Table_Import_Absolventi_Body.empty().html('');
            $Select_AnUnivAbsolvire.show();
            $Select_ProgramStudiuAbsolvit.hide();
            $Select_Diploma_Config.hide();
            $Button_Submit_Import.hide();

            $Button_Submit_Import
                .click( function() {
                    $Div_Alert.empty().html('').hide();
                    var ID_AnUnivAbsolvire          = parseInt( $Select_AnUnivAbsolvire.val() );
                    var ID_ProgramStudiuAbsolvit    = parseInt( $Select_ProgramStudiuAbsolvit.val() );
                    var Diploma_Config_Id           = parseInt( $Select_Diploma_Config.val() );
                    if( !isNaN( ID_AnUnivAbsolvire ) && !isNaN( ID_ProgramStudiuAbsolvit ) && !isNaN( Diploma_Config_Id ) )
                    {
                        var $arr = [];
                        var $row = ( typeof $Table_Import_Absolventi_Body.children( 'tr' ) != 'undefined' ? $Table_Import_Absolventi_Body.children( 'tr' ) : [] );
                        if( $row.length )
                        {
                            $.each( $row, function( k, $r ) { 
                                var $inp = $( this ).find( 'input[role="absolvent"]' );
                                if( $inp.prop( 'checked' ) )
                                {
                                    var obj =   {
                                                    ID_Student: parseInt( $inp.attr('value') ),
                                                    Diploma_Config_Id: Diploma_Config_Id,
                                                    <?php foreach ( $diplomas_design_field_default as $ddfd => $DDFD ) : ?>
                                                    <?php echo $ddfd ?>: $inp.attr( '<?php echo $ddfd ?>' ),
                                                    <?php endforeach; ?>
                                                };
                                    $arr.push( obj );
                                }
                            } );
                        }
                        console.log( 'arr', $arr );
                        if( $arr.length )
                        {
                            var $url = "<?php echo site_url( 'admin/content/university_graduates/import/inregistrare' ); ?>";
                            var $obj = { 'absolvent': $arr };
                            $.post  ( $url, $obj )
                            .done( function( data ) {
                                if( data )
                                {
                                    var $JSON = JSON.parse(JSON.stringify(data));
                                    console.log( 'JSON', $JSON );
                                    if( $JSON ) 
                                    {
                                        $j = 0;
                                        $.each( $JSON, function( $i, $v ) {
                                            if( $v && ( typeof $Table_Import_Absolventi_Body.children( 'tr[student="'+$i+'"]' ).children( 'td[role="alert"]' ) != 'undefined') )
                                            {
                                                $j++;
                                                $Table_Import_Absolventi_Body.children( 'tr[student="'+$i+'"]' ).children( 'td[role="alert"]' ).html( $v );
                                            } else {
                                                console.log( $i, 'no object' );
                                            }
                                        });
                                        $Div_Alert.removeAttr('class').addClass('alert').addClass('alert-success').html( 'Lista de absolvenți ['+$j+' înregistrări] a fost importată!' ).fadeIn();
                                    } 
                                    else 
                                    {
                                        $Div_Alert.removeAttr('class').addClass('alert').addClass('alert-error').html( 'Empty Data Array!' ).fadeIn();
                                    }
                                } 
                                else 
                                {
                                    $Div_Alert.removeAttr('class').addClass('alert').addClass('alert-error').html( 'Data Reception Error!' ).fadeIn();
                                }
                            });
                        } 
                        else 
                        {
                            $Div_Alert.removeAttr('class').addClass('alert').addClass('alert-error').html( 'Lista de absolvenți selectați este goală!' ).fadeIn();
                        }
                    } 
                    else 
                    {
                        $Div_Alert.removeAttr('class').addClass('alert').addClass('alert-error').html( 'Selectați toate câmpurile necesare importului de date!' ).fadeIn();
                    }
                    window.location.href = "#anchor";
                } );
            
            $Select_Diploma_Config
                .change( function() {
                    $Div_Alert.empty().html('').hide();
                    var Diploma_Config_Id = parseInt( $Select_Diploma_Config.val() );
                    if( isNaN( Diploma_Config_Id ) )
                    {
                        $Button_Submit_Import.fadeOut()
                    } else {
                        $Button_Submit_Import.fadeIn();
                    }
                });
            $Select_ProgramStudiuAbsolvit
                .change( function() {

                    $Div_Alert.empty().html('').hide();
                    $Table_Import_Absolventi.show();
                    $Table_Import_Absolventi.children('tbody').html('');
                    $Select_AnUnivAbsolvire.show();
                    $Select_Diploma_Config.find( 'option:first-child' ).prop( 'selected', true ).trigger( 'change' ).parent().hide();
                    $Button_Submit_Import.hide();
                    
                    var ID_AnUnivAbsolvire          = parseInt( $Select_AnUnivAbsolvire.val() );
                    var ID_ProgramStudiuAbsolvit    = parseInt( $( this ).val() );

                    if( !isNaN( ID_AnUnivAbsolvire ) && !isNaN( ID_ProgramStudiuAbsolvit ) )
                    {
                        $.get( 
                                "<?php echo site_url( 'admin/content/university_graduates/import/absolventi' ); ?>", 
                                { ID_AnUnivAbsolvire: ID_AnUnivAbsolvire, ID_ProgramStudiuAbsolvit: ID_ProgramStudiuAbsolvit }
                            )
                        .done( function( obj ) {
                            if( obj )
                            {
                                var len = Object.keys( obj ).length;
                                if( len )
                                {
                                    $.each( obj, function( k, v ) {
                                        console.log( k );
                                        console.log( v );
                                        var $string =   '<tr student="' + ( ( typeof this.ID_Student != 'undefined' ) && ( this.ID_Student ) ? this.ID_Student : '' ) + '">'
                                                        +       '<td role="index" class="text-center">'+( ( typeof this.ID_Student != 'undefined' ) && ( this.ID_Student ) ? this.ID_Student : '' )+'</td>'
                                                        +       '<td role="check" class="text-center">'
                                                        +           '<input ' + 
                                                                            'role="absolvent" ' + 
                                                                            'type="checkbox" ' + 
                                                                            'value="' + ( ( typeof this.ID_Student != 'undefined' ) && ( this.ID_Student ) ? this.ID_Student : '' ) + '" ' + 
                                                                            <?php foreach ( $diplomas_design_field_default as $ddfd => $DDFD ) : ?>
                                                                            '<?php echo $ddfd ?>="' + ( ( typeof this.<?php echo $ddfd; ?> != 'undefined' ) && ( this.<?php echo $ddfd; ?> ) ? this.<?php echo $ddfd; ?> : '' ) + '" ' +
                                                                            <?php endforeach; ?>
                                                                            ( ( typeof this.Graduate_000_InregistrareImportata != 'undefined' ) && this.Graduate_000_InregistrareImportata ? 'disabled ' : 'checked ' ) +
                                                                    '/>'
                                                        +       '</td>'
                                                        +       '<td role="label" class="text-left">'
                                                        +           '<b ' +
                                                                            'tabindex="0" ' +
                                                                            'role="Info-Graduate" ' +
                                                                            'class="btn-link" ' +
                                                                            'data-html="true" ' +
                                                                            'data-toggle="popover" ' +
                                                                            'data-trigger="focus" ' +
                                                                            'title="<b>' + ( ( typeof this.Graduate_012_NumeIntreg != 'undefined' ) && ( this.Graduate_012_NumeIntreg ) ? this.Graduate_012_NumeIntreg : '-' ) + '</b> [<b>' + ( ( typeof this.ID_Student != 'undefined' ) && ( this.ID_Student ) ? this.ID_Student : '' ) + '</b>]"' +
																			'data-content = "<b>Nr credite program : </b>' + ( ( typeof this.Graduate_001_NrCrediteProgram != 'undefined' ) && ( this.Graduate_001_NrCrediteProgram ) ? this.Graduate_001_NrCrediteProgram : '-' ) + '<br> <b> Sesiune examen absolvire : </b>' + ( ( typeof this.Graduate_002_SesiuneExamenAbsolvire != 'undefined' ) && ( this.Graduate_002_SesiuneExamenAbsolvire ) ? this.Graduate_002_SesiuneExamenAbsolvire : '-' ) + '<br> <b> An examen absolvire : </b>' + ( ( typeof this.Graduate_003_AnExamenAbsolvire != 'undefined' ) && ( this.Graduate_003_AnExamenAbsolvire ) ? this.Graduate_003_AnExamenAbsolvire : '-' ) + '<br><b> Denumire proba 1 : </b>' + ( ( typeof this.Graduate_004_DenumireProba1 != 'undefined' ) && ( this.Graduate_004_DenumireProba1 ) ? this.Graduate_004_DenumireProba1 : '-' ) + '<br> <b> Nr credite proba 1 : </b>' + ( ( typeof this.Graduate_005_NrCrediteProba1 != 'undefined' ) && ( this.Graduate_005_NrCrediteProba1 ) ? this.Graduate_005_NrCrediteProba1 : '-' ) + '<br> <b> Nota proba 1 : </b>' + ( ( typeof this.Graduate_006_Nota1 != 'undefined' ) && ( this.Graduate_006_Nota1 ) ? this.Graduate_006_Nota1 : '-' ) + '<br> <b> Nota 1 in cifre si litere : </b> ' + ( ( typeof this.Graduate_007_NotaInCifreSiLitere1 != 'undefined' ) && ( this.Graduate_007_NotaInCifreSiLitere1 ) ? this.Graduate_007_NotaInCifreSiLitere1 : '-' ) + '<br> <b> Denumire proba 2 : </b>' + ( ( typeof this.Graduate_008_DenumireProba2 != 'undefined' ) && ( this.Graduate_008_DenumireProba2 ) ? this.Graduate_008_DenumireProba2 : '-' ) + '<br> <b>Nr credite proba 2 : </b> ' + ( ( typeof this.Graduate_009_NrCrediteProba2 != 'undefined' ) && ( this.Graduate_009_NrCrediteProba2 ) ? this.Graduate_009_NrCrediteProba2 : '-' ) + '<br> <b> Nota proba 2 : </b>' + ( ( typeof this.Graduate_010_Nota2 != 'undefined' ) && ( this.Graduate_010_Nota2 ) ? this.Graduate_010_Nota2 : '-' ) + '<br><b>Nota 2 in cifre si litere : </b> ' + ( ( typeof this.Graduate_011_NotaInCifreSiLitere2 != 'undefined' ) && ( this.Graduate_011_NotaInCifreSiLitere2 ) ? this.Graduate_011_NotaInCifreSiLitere2 : '-' ) + '<br> <b>Data nasterii : </b>' + ( ( typeof this.Graduate_013_DataNastere != 'undefined' ) && ( this.Graduate_013_DataNastere ) ? this.Graduate_013_DataNastere : '-' ) + '<br> <b>Media finala absolvire : </b>' + ( ( typeof this.Graduate_018_MedieFinalaAbsolvire != 'undefined' ) && ( this.Graduate_018_MedieFinalaAbsolvire ) ? this.Graduate_018_MedieFinalaAbsolvire : '-' ) + '<br> <b>Tara nasterii : </b>' + ( ( typeof this.Graduate_019_DenumireTaraNastere != 'undefined' ) && ( this.Graduate_019_DenumireTaraNastere ) ? this.Graduate_019_DenumireTaraNastere : '-' ) + '<br><b> Localitate nastere : </b>' + ( ( typeof this.Graduate_020_DenumireLocalitateNastere != 'undefined' ) && ( this.Graduate_020_DenumireLocalitateNastere ) ? this.Graduate_020_DenumireLocalitateNastere : '-' ) + '<br><b> Judet nastere : </b>' + ( ( typeof this.Graduate_021_DenumireJudetNastere != 'undefined' ) && ( this.Graduate_021_DenumireJudetNastere ) ? this.Graduate_021_DenumireJudetNastere : '-' ) + '<br><b>Formula adresare : </b>' + ( ( typeof this.Graduate_022_FormulaAdresare != 'undefined' ) && ( this.Graduate_022_FormulaAdresare ) ? this.Graduate_022_FormulaAdresare : '-' ) +  '<br><b>Formula adresare articulat : </b>'+ ( ( typeof this.Graduate_024_FormulaAdresareArticulat != 'undefined' ) && ( this.Graduate_024_FormulaAdresareArticulat ) ? this.Graduate_024_FormulaAdresareArticulat : '-' ) + '<br><b>Formula adresare sufix : </b>'+ ( ( typeof this.Graduate_025_FormulaAdresareSufix != 'undefined' ) && ( this.Graduate_025_FormulaAdresareSufix ) ? this.Graduate_025_FormulaAdresareSufix : '-' ) + '<br><b>Denumire domeniu : </b>' + ( ( typeof this.Graduate_026_DenumireDomeniu != 'undefined' ) && ( this.Graduate_026_DenumireDomeniu ) ? this.Graduate_026_DenumireDomeniu : '-' ) + '<br><b>Denumire specializare : </b>' + ( ( typeof this.Graduate_027_DenumireSpecializare != 'undefined' ) && ( this.Graduate_027_DenumireSpecializare ) ? this.Graduate_027_DenumireSpecializare : '-' ) + '<br><b>Denumire program de studii : </b>' + ( ( typeof this.Graduate_028_DenumireProgramDeStudii != 'undefined' ) && ( this.Graduate_028_DenumireProgramDeStudii ) ? this.Graduate_028_DenumireProgramDeStudii : '-' ) + '<br><b>Universitate : </b>' + ( ( typeof this.Graduate_029_Universitate != 'undefined' ) && ( this.Graduate_029_Universitate ) ? this.Graduate_029_Universitate : '-' ) + '"' +   

																			
                                                                    '>' +  ( ( typeof this.Graduate_012_NumeIntreg != 'undefined' ) && ( this.Graduate_012_NumeIntreg ) ? this.Graduate_012_NumeIntreg : '-' ) + '</b>'

                                                        +       '</td>'
                                                        +       '<td role="alert" class="text-center"></td>'
                                                        +   '</tr>';
                                        $Table_Import_Absolventi.find('tbody').append( $string );
                                    });
                                    $( "b[role='Info-Graduate']" ).popover( { container: 'body' } );
                                }
                                $Table_Import_Absolventi.fadeIn();
                                $Select_Diploma_Config.fadeIn();
                                $Button_Submit_Import.hide();
                            }
                        });
                    }
                });
            $Select_AnUnivAbsolvire
                .fadeIn()
                .change( function() {
                    $Div_Alert.empty().html('').hide();
                    $Table_Import_Absolventi.show();
                    $Table_Import_Absolventi_Body.empty().html('');
                    $Select_AnUnivAbsolvire.show();
                    $Select_ProgramStudiuAbsolvit.empty().html('').hide();
                    $Select_Diploma_Config.find( 'option:first-child' ).prop( 'selected', true ).trigger( 'change' ).parent().hide();
                    $Button_Submit_Import.hide();
                    var ID_AnUnivAbsolvire = parseInt( $( this ).val() );
                    if( !isNaN( ID_AnUnivAbsolvire ) )
                    {
                        $.get( 
                            "<?php echo site_url( 'admin/content/university_graduates/import/absolvire' ); ?>", 
                            { ID_AnUnivAbsolvire: ID_AnUnivAbsolvire }
                        )
                        .done( function( obj ) {
                            if( obj )
                            {
                                $Select_ProgramStudiuAbsolvit.append( '<option></option>' );

                                if( obj )
                                {
                                    $.each( obj, function( key, val ) {
                                        $Select_ProgramStudiuAbsolvit.append( '<option value="'+key+'">'+val+'</option>' );
                                    });
                                }
                                $Select_ProgramStudiuAbsolvit.fadeIn();
                            }
                        });
                    }
                });
        }

    });
</script>
      