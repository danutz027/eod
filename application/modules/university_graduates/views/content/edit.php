<?php
    $can_view       = $this->auth->has_permission( 'University_graduates.Content.View' );
    $can_create     = $this->auth->has_permission( 'University_graduates.Content.Create' );
    $can_edit       = $this->auth->has_permission( 'University_graduates.Content.Edit' );
    $can_delete     = $this->auth->has_permission( 'University_graduates.Content.Delete' );
    $has_records    = isset( $GRADUATE ) && !empty( $GRADUATE );
    $SerieNrProba   = '2017-PROBA-XX-000001-1';
    //print_r( $GRADUATE );
?>
<html>
    <head>
        <link rel='stylesheet' type='text/css' href="<?php echo base_url( 'themes/admin/css/bootstrap.css' ); ?>" media='screen' />
        <link rel='stylesheet' type='text/css' href="<?php echo base_url( 'themes/admin/css/jqueryui.bootstrap.css' ); ?>" media='screen' />
        <link rel='stylesheet' type='text/css' href="<?php echo base_url( 'themes/admin/css/bootstrap-responsive.css' ); ?>" media='screen' />
        <link rel='stylesheet' type='text/css' href="<?php echo base_url( 'themes/admin/css/bootstrap-responsive.css' ); ?>" media='screen' />
        <link rel='stylesheet' type='text/css' href="<?php echo base_url( 'themes/admin/assets/jquery-ui/jquery-ui.min.css' ); ?>" media='screen' />

        <script src="<?php echo base_url( 'themes/admin/js/jquery-1.7.2.min.js' ); ?>" type='text/javascript'></script>
        <script src="<?php echo base_url( 'themes/admin/js/bootstrap.min.js' ); ?>" type='text/javascript'></script>
        <script src="<?php echo base_url( 'themes/admin/js/bootstrap-dropdown.js' ); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url( 'themes/admin/js/jquery-ui-1.8.13.min.js"' ); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url( 'themes/admin/js/jquery-moment.min.js"' ); ?>" type="text/javascript"></script>
        
        <style>
            input[role="edit"],
            select[role="edit"]{
                padding: 4px 6px !important;
                height: 30px !important;
                width: 100% !important;
            }
            textarea[role="edit"]{
                padding: 4px 6px !important;
                width: 100% !important;
                min-height: 44px !important;
            }
            input[required='required'], 
            select[required='required'],
            textarea[required='required']
            {
                background-image: url( '<?php echo base_url( 'themes/admin/images/required.png' ); ?>' );
                background-repeat: no-repeat;
                background-position: right center;
            }
            .error {
                font-size: 10px !important;
                color: red !important;;
            }
        </style>
    </head>
    <body style="width: 96% !important; margin: 1em auto !important;">
    <?php if ( $can_view && $has_records ) : ?>
        <fieldset>
            <a id="anchor" name="anchor"></a>
            <div id="DivALERT" class="spanoffset-2 span10 alert alert-danger" style="display: none;">
                <span></span>
                <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">&times;</button>
            </div>
            <legend>
                <img id="spinner" src="<?php echo base_url('themes/admin/images/ajax-loader.gif'); ?>" style="height:1em; width:1em; display: none; float: right;"/>
                <span class="icon icon-file">&nbsp;</span>
                <?php echo lang('university_graduates_edit_title'); ?>
            </legend>
            <div id="DivAJAX" class="row row-fluid" style="width: 100%; display: none;"></div>
            <?php //echo form_open( '#', array( 'class' => 'form-horizontal', 'autocomplete' => 'off' ) ); ?>
            <form id="FormEditDiploma"  method="post" action="<?php echo site_url( 'admin/content/university_graduates/edit/' ); ?>" class="form-horizontal">
                <table id="TableEditDiploma" class='table table-responsive table-striped' border="0">
                    <thead>
                        <tr>
                            
                            <td style='width: 15% !important;'><label class='text-right' for="Gratuate_ID"><i>Id</i>:</label></td>
                            <td style='width: 15% !important;'><input role="edit" required="required" class="span2" type="number" id="Gratuate_ID"  name="Gratuate_ID" value="<?php echo $GRADUATE['Gratuate_ID']; ?>" readonly /></td>
                            <td style='width: 40% !important;' class='text-right'>
                                <?php if(   ( 
                                                ( isset( $GRADUATE['status'] ) && ( ( $GRADUATE['status'] = trim( $GRADUATE['status'] ) ) == 'updated' ) ) && 
                                                ( isset( $GRADUATE['Diploma_Config_Id'] ) && ( $GRADUATE['Diploma_Config_Id'] = intval( $GRADUATE['Diploma_Config_Id'] ) ) ) && 
                                                ( isset( $DIPLOMA['Diploma_Config_Id'] ) && ( $DIPLOMA['Diploma_Config_Id'] = intval( $DIPLOMA['Diploma_Config_Id'] ) ) ) &&
                                                ( isset( $DIPLOMA['Diploma_Config_Image_Front'] ) && $DIPLOMA['Diploma_Config_Image_Front'] ) &&
                                                ( $GRADUATE['Diploma_Config_Id'] == $DIPLOMA['Diploma_Config_Id'] )
                                            ) ||
                                            ( isset( $GRADUATE['Diploma_Code_Value'] ) && ( $GRADUATE['Diploma_Code_Value'] == $SerieNrProba ) )
                                        ) : ?>
                                    <a href="<?php echo site_url( "admin/content/university_graduates/pdf/".$GRADUATE['Gratuate_ID']."/front" ); ?>" target="_blank" class="btn btn-info pull-right span1" title="Generate PDF">
                                        <i class="icon icon-white icon-print"></i> <small>Print:&nbsp;Front</small>
                                    </a>
                                <?php endif; ?>
                            </td>
                            <td style='width: 10% !important;' class='text-right'>
                                <?php if(   ( 
                                                ( isset( $GRADUATE['status'] ) && ( ( $GRADUATE['status'] = trim( $GRADUATE['status'] ) ) == 'updated' ) ) && 
                                                ( isset( $GRADUATE['Diploma_Config_Id'] ) && ( $GRADUATE['Diploma_Config_Id'] = intval( $GRADUATE['Diploma_Config_Id'] ) ) ) && 
                                                ( isset( $DIPLOMA['Diploma_Config_Id'] ) && ( $DIPLOMA['Diploma_Config_Id'] = intval( $DIPLOMA['Diploma_Config_Id'] ) ) ) &&
                                                ( isset( $DIPLOMA['Diploma_Config_Image_Verso'] ) && $DIPLOMA['Diploma_Config_Image_Verso'] ) &&
                                                ( $GRADUATE['Diploma_Config_Id'] == $DIPLOMA['Diploma_Config_Id'] )
                                            ) ||
                                            ( isset( $GRADUATE['Diploma_Code_Value'] ) && ( $GRADUATE['Diploma_Code_Value'] == $SerieNrProba ) )
                                        ) : ?>
                                    <a href="<?php echo site_url( "admin/content/university_graduates/pdf/".$GRADUATE['Gratuate_ID']."/verso" ); ?>" target="_blank" class="btn btn-info pull-right span1" title="Generate PDF">
                                        <i class="icon icon-white icon-print"></i> <small>Print:&nbsp;Verso</small>
                                    </a>
                                <?php endif; ?>
                            </td>
                            <td style='width: 10% !important;' class='text-right'>
                                <?php if(   (   ( isset( $GRADUATE['status'] ) && ( ( $GRADUATE['status'] = trim( $GRADUATE['status'] ) ) == 'updated' ) ) && 
                                                ( isset( $GRADUATE['Diploma_Config_Id'] ) && ( $GRADUATE['Diploma_Config_Id'] = intval( $GRADUATE['Diploma_Config_Id'] ) ) ) && 
                                                ( isset( $DIPLOMA['Diploma_Config_Id'] ) && ( $DIPLOMA['Diploma_Config_Id'] = intval( $DIPLOMA['Diploma_Config_Id'] ) ) ) &&
                                                ( isset( $DIPLOMA['Diploma_Config_Image_Front'] ) && $DIPLOMA['Diploma_Config_Image_Front'] ) &&
                                                ( $GRADUATE['Diploma_Config_Id'] == $DIPLOMA['Diploma_Config_Id'] )
                                            ) ||
                                            ( isset( $GRADUATE['Diploma_Code_Value'] ) && ( $GRADUATE['Diploma_Code_Value'] == $SerieNrProba ) )
                                        ) : ?>
                                    <a href="<?php echo site_url( "admin/content/university_graduates/pdf/".$GRADUATE['Gratuate_ID']."/front/view" ); ?>" target="_blank" class="btn btn-warning pull-right span1" title="Generate PDF">
                                        <i class="icon icon-white icon-print"></i> <small>View:&nbsp;Front</small>
                                    </a>
                                <?php endif; ?>
                            </td>
                            <td style='width: 10% !important;' class='text-right'>
                                <?php if(   (   ( isset( $GRADUATE['status'] ) && ( ( $GRADUATE['status'] = trim( $GRADUATE['status'] ) ) == 'updated' ) ) && 
                                                ( isset( $GRADUATE['Diploma_Config_Id'] ) && ( $GRADUATE['Diploma_Config_Id'] = intval( $GRADUATE['Diploma_Config_Id'] ) ) ) && 
                                                ( isset( $DIPLOMA['Diploma_Config_Id'] ) && ( $DIPLOMA['Diploma_Config_Id'] = intval( $DIPLOMA['Diploma_Config_Id'] ) ) ) &&
                                                ( isset( $DIPLOMA['Diploma_Config_Image_Verso'] ) && $DIPLOMA['Diploma_Config_Image_Verso'] ) &&
                                                ( $GRADUATE['Diploma_Config_Id'] == $DIPLOMA['Diploma_Config_Id'] )
                                            ) ||
                                            ( isset( $GRADUATE['Diploma_Code_Value'] ) && ( $GRADUATE['Diploma_Code_Value'] == $SerieNrProba ) )
                                        ) : ?>
                                    <a href="<?php echo site_url( "admin/content/university_graduates/pdf/".$GRADUATE['Gratuate_ID']."/verso/view" ); ?>" target="_blank" class="btn btn-warning pull-right span1" title="Generate PDF">
                                        <i class="icon icon-white icon-print"></i> <small>View:&nbsp;Verso</small>
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class='text-right'><label class='text-right' for="Gratuate_ID"><i>Diploma</i>:</label></td>
                            <td colspan="3">
                                <select role="edit" required="required" id="Diploma_Config_Id" name="Diploma_Config_Id">

                                <?php if(   ( isset( $GRADUATE['status'] )                  && ( ( $GRADUATE['status'] = trim( $GRADUATE['status'] ) ) == 'printed' ) ) && 
                                            ( isset( $GRADUATE['Diploma_Config_Id'] )       && ( $GRADUATE['Diploma_Config_Id'] = intval( $GRADUATE['Diploma_Config_Id'] ) ) ) && 
                                            ( isset( $DIPLOMA['Diploma_Config_Id'] )        && ( $DIPLOMA['Diploma_Config_Id'] = intval( $DIPLOMA['Diploma_Config_Id'] ) ) ) &&
                                            ( isset( $DIPLOMA['Diploma_Config_Title'] )     && ( $DIPLOMA['Diploma_Config_Title'] = trim( $DIPLOMA['Diploma_Config_Title'] ) ) ) &&
                                            ( $GRADUATE['Diploma_Config_Id'] == $DIPLOMA['Diploma_Config_Id'] )
                                        ) : ?>

                                    <option type="<?php echo( $DIPLOMA['Diploma_Config_Type'] ); ?>"  value="<?php echo $DIPLOMA['Diploma_Config_Id']; ?>">
                                        <?php echo $DIPLOMA['Diploma_Config_Title']; ?> [ <?php echo( $DIPLOMA['Diploma_Config_Type'] ); ?> ]
                                    </option>

                                <?php else : ?>

                                    <option></option>

                                    <?php if( isset( $DIPLOMELE ) && !empty( $DIPLOMELE ) ) : ?>
                                        <?php foreach( $DIPLOMELE as $Diploma ) : ?>

                                            <option type="<?php echo( $Diploma->Diploma_Config_Type ); ?>" value="<?php echo( $Diploma->Diploma_Config_Id ); ?>" <?php echo ( ( isset( $GRADUATE['Diploma_Config_Id'] ) ? intval( trim( $GRADUATE['Diploma_Config_Id'] ) ) : 0 ) == $Diploma->Diploma_Config_Id ? 'selected' : '' ); ?>>
                                                <?php echo( $Diploma->Diploma_Config_Title ); ?> [ <?php echo( $Diploma->Diploma_Config_Type ); ?> ]
                                            </option>

                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>

                                </select>
                            </td>
                            <td colspan="2" style='width: 10% !important;' class="error Diploma_Config_Id"></td>
                        </tr>
                    </thead>
                    <tbody>

                        <?php foreach ( $diplomas_design_field_default as $ddfd => $DDFD ) : ?>

                            <tr>
                                <td><label class='text-right' for="<?php echo $ddfd; ?>"><i><?php echo $DDFD['text']; ?></i>:</label></td>
                                <td colspan="3"><input type="<?php echo ( in_array( $DDFD['type'], array( 'numeric-integer', 'numeric-decimal' ) ) ? 'number' : ( $DDFD['type'] == 'datetime' ? 'text' : 'text' ) ); ?>" <?php if( $DDFD['type'] == 'datetime' ){ echo ' readonly="readonly" '; } else if( in_array( $DDFD['type'], array( 'numeric-integer', 'numeric-decimal' ) ) ) { echo ' min="0" step="' . ( $DDFD['type'] == 'numeric-integer' ? 1 : 0.01 ) . '" ';  } ?> role="edit" id="<?php echo $ddfd; ?>" field-type="<?php echo $DDFD['type']; ?>" <?php echo ( $DDFD['required'] ? 'required="required"' : '' ); ?> name="<?php echo $ddfd; ?>" value="<?php echo isset( $GRADUATE[$ddfd] ) ? htmlspecialchars (trim( $GRADUATE[$ddfd] )) : ''; ?>" class="span8" /></td>
                                <td colspan="1" class="muted"><small><?php echo $DDFD['type']; ?></small></td>
                                <td colspan="1" class="error <?php echo $ddfd; ?>"></td>
                            </tr>

                        <?php endforeach; ?>

                        <tr>
                            <td><label class='text-right' for="#"><i>Câmpuri</i>:</label></td>
                            <td class='text-right' colspan='5'>
                                <ol>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_001_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_001_Type'] = trim( $DIPLOMA['Diploma_Config_Field_001_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_001_Text'] = trim( $DIPLOMA['Diploma_Config_Field_001_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_001_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_001_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_001_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_001_Type'] ); ?>" id="Field_001_Value" name="Field_001_Value"><?php echo trim( isset( $GRADUATE['Field_001_Value'] ) ? $GRADUATE['Field_001_Value'] : '' ); ?></textarea>
                                        <p class='error Field_001_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_002_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_002_Type'] = trim( $DIPLOMA['Diploma_Config_Field_002_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_002_Text'] = trim( $DIPLOMA['Diploma_Config_Field_002_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_002_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_002_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_002_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_002_Type'] ); ?>" id="Field_002_Value" name="Field_002_Value"><?php echo trim( isset( $GRADUATE['Field_002_Value'] ) ? $GRADUATE['Field_002_Value'] : '' ); ?></textarea>
                                        <p class='error Field_002_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_003_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_003_Type'] = trim( $DIPLOMA['Diploma_Config_Field_003_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_003_Text'] = trim( $DIPLOMA['Diploma_Config_Field_003_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_003_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_003_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_003_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_003_Type'] ); ?>" id="Field_003_Value" name="Field_003_Value"><?php echo trim( isset( $GRADUATE['Field_003_Value'] ) ? $GRADUATE['Field_003_Value'] : '' ); ?></textarea>
                                        <p class='error Field_003_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_004_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_004_Type'] = trim( $DIPLOMA['Diploma_Config_Field_004_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_004_Text'] = trim( $DIPLOMA['Diploma_Config_Field_004_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_004_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_004_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_004_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_004_Type'] ); ?>" id="Field_004_Value" name="Field_004_Value"><?php echo trim( isset( $GRADUATE['Field_004_Value'] ) ? $GRADUATE['Field_004_Value'] : '' ); ?></textarea>
                                        <p class='error Field_004_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_005_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_005_Type'] = trim( $DIPLOMA['Diploma_Config_Field_005_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_005_Text'] = trim( $DIPLOMA['Diploma_Config_Field_005_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_005_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_005_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_005_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_005_Type'] ); ?>" id="Field_005_Value" name="Field_005_Value"><?php echo trim( isset( $GRADUATE['Field_005_Value'] ) ? $GRADUATE['Field_005_Value'] : '' ); ?></textarea>
                                        <p class='error Field_005_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_006_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_006_Type'] = trim( $DIPLOMA['Diploma_Config_Field_006_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_006_Text'] = trim( $DIPLOMA['Diploma_Config_Field_006_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_006_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_006_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_006_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_006_Type'] ); ?>" id="Field_006_Value" name="Field_006_Value"><?php echo trim( isset( $GRADUATE['Field_006_Value'] ) ? $GRADUATE['Field_006_Value'] : '' ); ?></textarea>
                                        <p class='error Field_006_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_007_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_007_Type'] = trim( $DIPLOMA['Diploma_Config_Field_007_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_007_Text'] = trim( $DIPLOMA['Diploma_Config_Field_007_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_007_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_007_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_007_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_007_Type'] ); ?>" id="Field_007_Value" name="Field_007_Value"><?php echo trim( isset( $GRADUATE['Field_007_Value'] ) ? $GRADUATE['Field_007_Value'] : '' ); ?></textarea>
                                        <p class='error Field_007_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_008_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_008_Type'] = trim( $DIPLOMA['Diploma_Config_Field_008_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_008_Text'] = trim( $DIPLOMA['Diploma_Config_Field_008_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_008_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_008_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_008_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_008_Type'] ); ?>" id="Field_008_Value" name="Field_008_Value"><?php echo trim( isset( $GRADUATE['Field_008_Value'] ) ? $GRADUATE['Field_008_Value'] : '' ); ?></textarea>
                                        <p class='error Field_008_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_009_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_009_Type'] = trim( $DIPLOMA['Diploma_Config_Field_009_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_009_Text'] = trim( $DIPLOMA['Diploma_Config_Field_009_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_009_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_009_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_009_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_009_Type'] ); ?>" id="Field_009_Value" name="Field_009_Value"><?php echo trim( isset( $GRADUATE['Field_009_Value'] ) ? $GRADUATE['Field_009_Value'] : '' ); ?></textarea>
                                        <p class='error Field_009_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_010_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_010_Type'] = trim( $DIPLOMA['Diploma_Config_Field_010_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_010_Text'] = trim( $DIPLOMA['Diploma_Config_Field_010_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_010_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_010_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_010_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_010_Type'] ); ?>" id="Field_010_Value" name="Field_010_Value"><?php echo trim( isset( $GRADUATE['Field_010_Value'] ) ? $GRADUATE['Field_010_Value'] : '' ); ?></textarea>
                                        <p class='error Field_010_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_011_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_011_Type'] = trim( $DIPLOMA['Diploma_Config_Field_011_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_011_Text'] = trim( $DIPLOMA['Diploma_Config_Field_011_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_011_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_011_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_011_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_011_Type'] ); ?>" id="Field_011_Value" name="Field_011_Value"><?php echo trim( isset( $GRADUATE['Field_011_Value'] ) ? $GRADUATE['Field_011_Value'] : '' ); ?></textarea>
                                        <p class='error Field_011_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_012_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_012_Type'] = trim( $DIPLOMA['Diploma_Config_Field_012_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_012_Text'] = trim( $DIPLOMA['Diploma_Config_Field_012_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_012_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_012_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_012_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_012_Type'] ); ?>" id="Field_012_Value" name="Field_012_Value"><?php echo trim( isset( $GRADUATE['Field_012_Value'] ) ? $GRADUATE['Field_012_Value'] : '' ); ?></textarea>
                                        <p class='error Field_012_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_013_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_013_Type'] = trim( $DIPLOMA['Diploma_Config_Field_013_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_013_Text'] = trim( $DIPLOMA['Diploma_Config_Field_013_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_013_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_013_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_013_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_013_Type'] ); ?>" id="Field_013_Value" name="Field_013_Value"><?php echo trim( isset( $GRADUATE['Field_013_Value'] ) ? $GRADUATE['Field_013_Value'] : '' ); ?></textarea>
                                        <p class='error Field_013_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_014_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_014_Type'] = trim( $DIPLOMA['Diploma_Config_Field_014_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_014_Text'] = trim( $DIPLOMA['Diploma_Config_Field_014_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_014_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_014_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_014_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_014_Type'] ); ?>" id="Field_014_Value" name="Field_014_Value"><?php echo trim( isset( $GRADUATE['Field_014_Value'] ) ? $GRADUATE['Field_014_Value'] : '' ); ?></textarea>
                                        <p class='error Field_014_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_015_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_015_Type'] = trim( $DIPLOMA['Diploma_Config_Field_015_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_015_Text'] = trim( $DIPLOMA['Diploma_Config_Field_015_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_015_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_015_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_015_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_015_Type'] ); ?>" id="Field_015_Value" name="Field_015_Value"><?php echo trim( isset( $GRADUATE['Field_015_Value'] ) ? $GRADUATE['Field_015_Value'] : '' ); ?></textarea>
                                        <p class='error Field_015_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_016_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_016_Type'] = trim( $DIPLOMA['Diploma_Config_Field_016_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_016_Text'] = trim( $DIPLOMA['Diploma_Config_Field_016_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_016_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_016_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_016_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_016_Type'] ); ?>" id="Field_016_Value" name="Field_016_Value"><?php echo trim( isset( $GRADUATE['Field_016_Value'] ) ? $GRADUATE['Field_016_Value'] : '' ); ?></textarea>
                                        <p class='error Field_016_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_017_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_017_Type'] = trim( $DIPLOMA['Diploma_Config_Field_017_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_017_Text'] = trim( $DIPLOMA['Diploma_Config_Field_017_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_017_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_017_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_017_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_017_Type'] ); ?>" id="Field_017_Value" name="Field_017_Value"><?php echo trim( isset( $GRADUATE['Field_017_Value'] ) ? $GRADUATE['Field_017_Value'] : '' ); ?></textarea>
                                        <p class='error Field_017_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_018_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_018_Type'] = trim( $DIPLOMA['Diploma_Config_Field_018_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_018_Text'] = trim( $DIPLOMA['Diploma_Config_Field_018_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_018_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_018_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_018_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_018_Type'] ); ?>" id="Field_018_Value" name="Field_018_Value"><?php echo trim( isset( $GRADUATE['Field_018_Value'] ) ? $GRADUATE['Field_018_Value'] : '' ); ?></textarea>
                                        <p class='error Field_018_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_019_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_019_Type'] = trim( $DIPLOMA['Diploma_Config_Field_019_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_019_Text'] = trim( $DIPLOMA['Diploma_Config_Field_019_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_019_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_019_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_019_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_019_Type'] ); ?>" id="Field_019_Value" name="Field_019_Value"><?php echo trim( isset( $GRADUATE['Field_019_Value'] ) ? $GRADUATE['Field_019_Value'] : '' ); ?></textarea>
                                        <p class='error Field_019_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_020_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_020_Type'] = trim( $DIPLOMA['Diploma_Config_Field_020_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_020_Text'] = trim( $DIPLOMA['Diploma_Config_Field_020_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_020_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_020_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_020_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_020_Type'] ); ?>" id="Field_020_Value" name="Field_020_Value"><?php echo trim( isset( $GRADUATE['Field_020_Value'] ) ? $GRADUATE['Field_020_Value'] : '' ); ?></textarea>
                                        <p class='error Field_020_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_021_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_021_Type'] = trim( $DIPLOMA['Diploma_Config_Field_021_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_021_Text'] = trim( $DIPLOMA['Diploma_Config_Field_021_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_021_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_021_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_021_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_021_Type'] ); ?>" id="Field_021_Value" name="Field_021_Value"><?php echo trim( isset( $GRADUATE['Field_021_Value'] ) ? $GRADUATE['Field_021_Value'] : '' ); ?></textarea>
                                        <p class='error Field_021_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_022_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_022_Type'] = trim( $DIPLOMA['Diploma_Config_Field_022_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_022_Text'] = trim( $DIPLOMA['Diploma_Config_Field_022_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_022_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_022_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_022_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_022_Type'] ); ?>" id="Field_022_Value" name="Field_022_Value"><?php echo trim( isset( $GRADUATE['Field_022_Value'] ) ? $GRADUATE['Field_022_Value'] : '' ); ?></textarea>
                                        <p class='error Field_022_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_023_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_023_Type'] = trim( $DIPLOMA['Diploma_Config_Field_023_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_023_Text'] = trim( $DIPLOMA['Diploma_Config_Field_023_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_023_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_023_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_023_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_023_Type'] ); ?>" id="Field_023_Value" name="Field_023_Value"><?php echo trim( isset( $GRADUATE['Field_023_Value'] ) ? $GRADUATE['Field_023_Value'] : '' ); ?></textarea>
                                        <p class='error Field_023_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_024_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_024_Type'] = trim( $DIPLOMA['Diploma_Config_Field_024_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_024_Text'] = trim( $DIPLOMA['Diploma_Config_Field_024_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_024_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_024_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_024_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_024_Type'] ); ?>" id="Field_024_Value" name="Field_024_Value"><?php echo trim( isset( $GRADUATE['Field_024_Value'] ) ? $GRADUATE['Field_024_Value'] : '' ); ?></textarea>
                                        <p class='error Field_024_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_025_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_025_Type'] = trim( $DIPLOMA['Diploma_Config_Field_025_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_025_Text'] = trim( $DIPLOMA['Diploma_Config_Field_025_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_025_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_025_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_025_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_025_Type'] ); ?>" id="Field_025_Value" name="Field_025_Value"><?php echo trim( isset( $GRADUATE['Field_025_Value'] ) ? $GRADUATE['Field_025_Value'] : '' ); ?></textarea>
                                        <p class='error Field_025_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_026_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_026_Type'] = trim( $DIPLOMA['Diploma_Config_Field_026_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_026_Text'] = trim( $DIPLOMA['Diploma_Config_Field_026_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_026_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_026_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_026_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_026_Type'] ); ?>" id="Field_026_Value" name="Field_026_Value"><?php echo trim( isset( $GRADUATE['Field_026_Value'] ) ? $GRADUATE['Field_026_Value'] : '' ); ?></textarea>
                                        <p class='error Field_026_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_027_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_027_Type'] = trim( $DIPLOMA['Diploma_Config_Field_027_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_027_Text'] = trim( $DIPLOMA['Diploma_Config_Field_027_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_027_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_027_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_027_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_027_Type'] ); ?>" id="Field_027_Value" name="Field_027_Value"><?php echo trim( isset( $GRADUATE['Field_027_Value'] ) ? $GRADUATE['Field_027_Value'] : '' ); ?></textarea>
                                        <p class='error Field_027_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_028_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_028_Type'] = trim( $DIPLOMA['Diploma_Config_Field_028_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_028_Text'] = trim( $DIPLOMA['Diploma_Config_Field_028_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_028_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_028_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_028_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_028_Type'] ); ?>" id="Field_028_Value" name="Field_028_Value"><?php echo trim( isset( $GRADUATE['Field_028_Value'] ) ? $GRADUATE['Field_028_Value'] : '' ); ?></textarea>
                                        <p class='error Field_028_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_029_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_029_Type'] = trim( $DIPLOMA['Diploma_Config_Field_029_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_029_Text'] = trim( $DIPLOMA['Diploma_Config_Field_029_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_029_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_029_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_029_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_029_Type'] ); ?>" id="Field_029_Value" name="Field_029_Value"><?php echo trim( isset( $GRADUATE['Field_029_Value'] ) ? $GRADUATE['Field_029_Value'] : '' ); ?></textarea>
                                        <p class='error Field_029_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                    <?php if(   ( isset( $DIPLOMA['Diploma_Config_Field_030_Type'] ) && in_array( ( $DIPLOMA['Diploma_Config_Field_030_Type'] = trim( $DIPLOMA['Diploma_Config_Field_030_Type'] ) ), $diplomas_design_field_type ) ) && 
                                                ( $DIPLOMA['Diploma_Config_Field_030_Text'] = trim( $DIPLOMA['Diploma_Config_Field_030_Text'] ) ) 
                                            ) : ?>
                                    <li>
                                        <p><label for="Field_030_Value" title="<?php echo( $DIPLOMA['Diploma_Config_Field_030_Type'] ); ?>"><?php echo( $DIPLOMA['Diploma_Config_Field_030_Text'] ); ?>:</label></p>
                                        <textarea role="edit" type="<?php echo( $DIPLOMA['Diploma_Config_Field_030_Type'] ); ?>" id="Field_030_Value" name="Field_030_Value"><?php echo trim( isset( $GRADUATE['Field_030_Value'] ) ? $GRADUATE['Field_010_Value'] : '' ); ?></textarea>
                                        <p class='error Field_010_Value small'></p>
                                    </li>
                                    <?php endif; ?>
                                </ol>
                            </td>
                           <!--  <tr>
                                <td><label class='text-right' for="<?php echo $ddfd; ?>"><i><?php echo $DDFD['text']; ?></i>:</label></td>
                                <td colspan="3"><input type="<?php echo ( in_array( $DDFD['type'], array( 'numeric-integer', 'numeric-decimal' ) ) ? 'number' : ( $DDFD['type'] == 'datetime' ? 'text' : 'text' ) ); ?>" <?php if( $DDFD['type'] == 'datetime' ){ echo ' readonly="readonly" '; } else if( in_array( $DDFD['type'], array( 'numeric-integer', 'numeric-decimal' ) ) ) { echo ' min="0" step="' . ( $DDFD['type'] == 'numeric-integer' ? 1 : 0.01 ) . '" ';  } ?> role="edit" id="<?php echo $ddfd; ?>" field-type="<?php echo $DDFD['type']; ?>" <?php echo ( $DDFD['required'] ? 'required="required"' : '' ); ?> name="<?php echo $ddfd; ?>" value="<?php echo isset( $GRADUATE[$ddfd] ) ? trim( $GRADUATE[$ddfd] ) : ''; ?>" class="span8" /></td>
                                <td colspan="1" class="muted"><small><?php echo $DDFD['type']; ?></small></td>
                                <td colspan="1" class="error <?php echo $ddfd; ?>"></td>
                            </tr> -->

                        </tr>
                        <tr>
                            <td class='text-right'>Seria și Nr. Diplomei:</td>
                            <td class='text-right' colspan='3'>
                                <?php if (  ( isset( $GRADUATE['status'] ) && ( $GRADUATE['status'] == 'printed' || $GRADUATE['status'] == 'updated') && ( $GRADUATE['Diploma_Code_Value'] != $SerieNrProba ) ) ) : ?>
                                    <input role="edit" required="required" type="text" name="Diploma_Code_Value" id="Diploma_Code_Value" maxlength="300" value="<?php echo ( $GRADUATE['Diploma_Code_Value'] ); ?>" readonly />
                                <?php else : ?>
                                    <select role="edit" required="required" name="Diploma_Code_Value" id="Diploma_Code_Value">
                                        <option></option>
                                        <option value="<?php echo $SerieNrProba; ?>"<?php echo ( isset( $GRADUATE['Diploma_Code_Value'] ) && ( $GRADUATE['Diploma_Code_Value'] == $SerieNrProba ) ? ' selected' : '' ); ?>><?php echo $SerieNrProba; ?></option>
                                        <optgroup label="Serii și Nr. Înregistrate">
                                        <?php if( isset( $SERIIsiNR ) && !empty( $SERIIsiNR ) ) : ?>
                                            <?php foreach( $SERIIsiNR as $SrNr ) : ?>
                                                <option value="<?php echo( $SrNr->Diploma_Code_Value ); ?>"><?php echo( $SrNr->Diploma_Code_Value ); ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        </optgroup>
                                    </select>
                                <?php endif; ?>
                            </td>
                            <td colspan="2" class="error Diploma_Code_Value"></td>
                        </tr>
                        <tr>
                            <td class='text-right'>Statusul Diplomei:</td>
                            <td class='text-right' colspan='3'>
                                <select role="edit" required="required" name="status" id="status">
                                    <!-- <option></option> -->
                                    <option value="updated" <?php echo ( isset( $GRADUATE['status'] ) && ( ( $GRADUATE['status'] = trim( $GRADUATE['status'] ) ) == 'updated' ) ? 'selected' : '' ); ?>>updated</option>
                                    <option value="printed" <?php echo ( isset( $GRADUATE['status'] ) && ( $GRADUATE['status']  == 'printed' ) ? 'selected' : '' ); ?>>printed</option>
                                    <?php if ( isset( $GRADUATE['status'] ) && ( $GRADUATE['status'] == 'printed' ) ) : ?>
                                    <option value="cancel" <?php echo ( isset( $GRADUATE['status'] ) && ( ( $GRADUATE['status'] = trim( $GRADUATE['status'] ) ) == 'cancel' ) ? 'selected' : '' ); ?>>cancel</option>
                                    <?php else: ?>
                                    <?php endif; ?>
                                </select>
                            </td>
                            <td colspan="2" class="error status"></td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td colspan="5">
                                <button id="ButtonEditDiploma" type="button" class="btn btn-primary">
                                    <i class="icon icon-white icon-folder-close"></i> 
                                    <?php echo lang('university_graduates_save'); ?>
                                </button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            
            </form>
        </fieldset>
        <script type="text/javascript">
            $( document ).ready( function() {
                $( 'input[field-type="datetime"]' )
                    .datepicker( { 
                        changeMonth:    true, 
                        changeYear:     true, 
                        dateFormat:     'yy-mm-dd',
                        onSelect:       function() 
                                        {
                                            var $M = { '01': "Ianuarie", 
                                                       '02': "Februarie", 
                                                       '03': "Martie", 
                                                       '04': "Aprilie", 
                                                       '05': "Mai", 
                                                       '06': "Iunie", 
                                                       '07': "Iulie", 
                                                       '08': "August", 
                                                       '09': "Septembrie", 
                                                       '10': "Octombrie", 
                                                       '11': "Noiembrie", 
                                                       '12': "Decembrie" };
                                            var $D = ( $( this ).val() ).split( '-' ); 
                                            console.log( $D );
                                            if( ( typeof $( '#Graduate_014_ZiNastere' ) == 'object' ) && ( typeof $D[2] != 'undefined' ) ) { $( '#Graduate_014_ZiNastere' ).attr( 'value', $D[2] ).val( $D[2] ); }
                                            if( ( typeof $( '#Graduate_015_LunaNastere' ) == 'object' ) && ( typeof $D[1] != 'undefined' ) ) { $( '#Graduate_015_LunaNastere' ).attr( 'value', $D[1] ).val( $D[1] ); }
                                            if( ( typeof $( '#Graduate_016_AnNastere' ) == 'object' ) && ( typeof $D[0] != 'undefined' ) ) { $( '#Graduate_016_AnNastere' ).attr( 'value', $D[0] ).val( $D[0] ); }
                                            if( ( typeof $( '#Graduate_017_LunaNastereInLitere' ) == 'object' ) && ( typeof $D[1] != 'undefined' ) && ( typeof $M[$D[1]] != 'undefined' ) ) { $( '#Graduate_017_LunaNastereInLitere' ).attr( 'value', $M[$D[1]] ).val( $M[$D[1]] ); }
                                        }
                    } );
                $( "#datepicker" ).datepicker( "option", "dateFormat", $( this ).val() );
                $('[role="edit"]').bind( "propertychange change click keyup input paste", function( event ) {
                    var field_key = $.trim( $( this ).attr( 'id' ) );
                    var error_key = '.error.'+field_key;
                    if( $( this ).prop( 'required' ) && !$( this ).val() )
                    {
                        if( typeof $( error_key ) != 'undefined' )
                        {
                            $( error_key ).empty('').html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                        }
                    } 
                    else 
                    {
                        if( typeof $( error_key ) != 'undefined' ) 
                        {
                            $( error_key ).empty('').html( '' );
                        }
                    }
                } );
                $( '#Diploma_Code_Value' ).change( function ( event ) { 
                    var index = ( ( $( '#Diploma_Code_Value' ).val() ).split('-') ).pop();
                    $('#NrInregistrare').val( index ).attr('value',index); //.prop('readonly',true);
                });
                $('#ButtonEditDiploma').click( function ( event ) {
                    var valid = true;
                    var formData = new FormData();
                    $.each( $( '#TableEditDiploma [role="edit"]' ), function () {
                        var field_key = $.trim( $( this ).attr( 'id' ) );
                        var error_key = '.error.'+field_key;
                        formData.append( field_key, $( this ).val() );
                        if( typeof $( error_key ) != 'undefined' ) 
                        {
                            $( error_key ).empty('').html( '' );
                        }
                        if( $( this ).prop( 'required' ) && !$( this ).val() )
                        {
                            valid = false; 
                            if( typeof $( error_key ) != 'undefined' )
                            {
                                $( error_key ).empty('').html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                            }
                        }
                    });
                    //console.log(formData);
                    if ( valid == true ) {
                        $( '#spinner' ).fadeIn();
                        $( 'html, body' ).animate( { scrollTop: $( "#anchor" ).offset().top }, 1000 );
                        document.getElementById("FormEditDiploma").submit();
                        //                        $.ajax({
                        //                            url: '<?php echo site_url( 'admin/content/university_graduates/edit/' ); ?>',
                        //                            type: 'POST',
                        //                            data: formData,
                        //                            contentType: false,
                        //                            cache: false,
                        //                            processData: false,
                        //                            success: function( $i ){
                        //                                if( $i )
                        //                                {
                        //                                    location.reload();
                        //                                } else {
                        //                                    alert( '<?php echo lang( 'bf_ajax_error' ); ?>' );
                        //                                }
                        //                            },
                        //                            error: function(e){
                        //                                alert( 'Error: ' + e.message );
                        //                            }
                        //                        });
                    } else {
                        $( 'html, body' ).animate( { scrollTop: $( "#anchor" ).offset().top }, 500 );
                    }
                });
            } );
        </script>
    <?php endif; ?>
    </body>
</html>