<?php
    $can_graduate_view       = $this->auth->has_permission( 'University_graduates.Content.View' );
    $can_graduate_create     = $this->auth->has_permission( 'University_graduates.Content.Create' );
    $can_graduate_edit       = $this->auth->has_permission( 'University_graduates.Content.Edit' );
    $can_graduate_delete     = $this->auth->has_permission( 'University_graduates.Content.Delete' );
?>
<?php if ( $can_graduate_view ) : ?>
    <style type="text/css">
        .popover{
            min-width: 800px !important;
            max-width: 800px !important;
            width: 600px !important;
        }
        .popover-content {
            height: 400px !important;
            overflow-y: scroll !important;
        }
        #Table_University_Graduates tbody tr td { text-align: center; }
    </style>
    <div class='admin-box'>
        <h3>
            <?php echo lang('university_graduates_list_title'); ?>
        </h3>
        <div id="accordion">
            <div class="panel panel-primary">
                <div class="panel-heading btn btn-default btn-small" data-acc-link="table" style="width: 98%; margin: 1em auto; clear: both;">
                    Graduates Table
                </div>
                <div class="panel-body acc-open" data-acc-content="table">
                    <form id="formDataTable" method="post" action="#">
                        <table class='table table-striped table-responsive' id="Table_University_Graduates" style="width:100%;">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan="1">
                                        <i class="icon icon-list"></i>
                                    </th>
                                    <th class="text-left" colspan="10">
                                        <b>Graduates Index</b>
                                    </th>
                                    <th class="text-center" colspan="2">
                                        <a href="<?php echo site_url( 'admin/content/university_graduates/index' ); ?>" class="btn btn-default btn-small" type="button" title="<?php echo lang('university_graduates_list_title'); ?>" style="margin: 0.1em 0.2em;">
                                            <span class="icon icon-list"></span>
                                            <?php echo lang('university_graduates_list_go_to'); ?>
                                        </a>

                                    </th>
                                </tr>
                                <tr>
                                    <th class="text-center" style="width:3%;">Anul</th>
                                    <th class="text-center" style="width:10%;">Sesiunea</th>
                                    <th class="text-center" style="width:22%;">Absolvent</th>
                                    <th class="text-center" style="width:16%;">Facultate</th>
                                    <th class="text-center" style="width:5%;">Ciclu</th>
                                    <th class="text-center" style="width:5%;">Forma</th>
                                    <th class="text-center" style="width:11%;">Domeniu</th>
                                    <th class="text-center" style="width:15%;">Specializare</th>
                                    <th class="text-center" style="width:3%;">Credite</th>
                                    <th class="text-center" style="width:4%;">Status</th>
                                    <th class="text-center" style="width:7%;">Diploma</th>
                                    <th class="text-center" style="width:3%;">Editare</th>
                                    <th class="text-center" style="width:3%;">Ștergere</th> 
                                </tr>
                            </thead>
                            <tbody>
                            <?php   if( ( isset( $GRADUATES ) && !empty( $GRADUATES ) ) ) :
                                        foreach( $GRADUATES AS $G ) : ?>
                                <tr>
                                    <td><?php echo ( $G->Graduate_003_AnExamenAbsolvire ); ?></td>
                                    <td><?php echo ( $G->Graduate_002_SesiuneExamenAbsolvire ); ?></td>
                                    <td><?php echo ( ( ( $G->Graduate_012_NumeIntreg = trim( $G->Graduate_012_NumeIntreg ) ) ? ( strlen( $G->Graduate_012_NumeIntreg ) > 100 ?  substr( $G->Graduate_012_NumeIntreg, 0, 96 ) . '...' : $G->Graduate_012_NumeIntreg ) : '-' ) ); ?></td>
                                    <td><?php echo ( $G->Graduate_031_Facultate ); ?></td>
                                    <td><?php echo ( $G->Graduate_033_CicluDeStudii ); ?></td>
                                    <td><?php echo ( $G->Graduate_032_FormaInvatamant ); ?></td>
                                    <td><?php echo ( $G->Graduate_026_DenumireDomeniu ); ?></td>
                                    <td><?php echo ( $G->Graduate_027_DenumireSpecializare ); ?></td>
                                    <td><?php echo ( $G->Graduate_001_NrCrediteProgram ); ?></td>
                                    <td><?php echo ( $G->status ); ?></td>
                                    <td><?php echo ( $G->Diploma_Code_Value ); ?></td>
                                    <td>
                                        <a role="Edit-Graduate" class="btn btn-link" 
                                           href="<?php echo site_url( "admin/content/university_graduates/edit/".$G->Gratuate_ID ); ?>" 
                                           title="<?php echo lang('university_graduates_edit'); ?> [<?php echo ( $G->Gratuate_ID ); ?>]"
                                        ><i class="icon icon-pencil"></i></a>
                                    </td>
									<td>
										<button onClick="if( confirm('<?php echo lang('university_graduates_delete_student_confirm'); ?>' ) ){ window.location.href = '<?php echo site_url( "admin/content/university_graduates/delete/".$G->Gratuate_ID ); ?>'; return false; } else { return false; }">
										<i class="icon icon-remove"></i>
										</button>
									</td>									
                                </tr>
                            <?php       endforeach;
                                    endif; ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading btn btn-default btn-small" data-acc-link="search" style="width: 98%; margin: 1em auto; clear: both;">
                    Search Form
                </div>
                <div class="panel-body" data-acc-content="search">
                    <form id="formDataearch" method="post" action="<?php echo site_url('admin/content/university_graduates/index/search'); ?>">
                        <table class='table table-striped table-responsive'>
                            <thead>
                                <tr>
                                    <th class="text-right" style="width:20%;">
                                        <i class="icon icon-search"></i>
                                    </td>
                                    <th class="text-left" style="width:80%;">
                                        SEARCH FORM
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-right" style="width:20%;">
                                        <label for="ABSOLVIRE">
                                            AN EXAMEN ABSOLVIRE:
                                        </label>
                                    </td>
                                    <td class="text-left" style="width:80%;">
                                        <select id="ABSOLVIRE" name="ABSOLVIRE" class="span8">
                                            <option></option>
                                            <?php if( ( $AN = intval( date('Y') ) ) ) : ?>
                                                <?php for( $A = $AN; $A > 1900; $A-- ) : ?>
                                                    <option value="<?php echo $A; ?>"<?php echo ( isset( $_POST['ABSOLVIRE'] ) && ( $_POST['ABSOLVIRE'] == $A ) ? ' selected' : '' ); ?>>
                                                        <?php echo $A; ?>
                                                    </option>
                                                <?php endfor; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" style="width:20%;">
                                        <label for="FACULTATE">
                                            DENUMIRE FACULTATE:
                                        </label>
                                    </td>
                                    <td class="text-left" style="width:80%;">
                                        <select id="FACULTATE" name="FACULTATE" class="span8">
                                            <option></option>
                                            <?php if( isset( $FACULTATI ) && !empty( $FACULTATI ) ) : ?>
                                                <?php foreach( $FACULTATI as $F ) : ?>
                                                    <option value="<?php echo $F; ?>"<?php echo ( isset( $_POST['FACULTATE'] ) && ( $_POST['FACULTATE'] == $F ) ? ' selected' : '' ); ?>><?php echo $F; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" style="width:20%;">
                                        <label for="CICLU">
                                            CICLURI de ÎNVĂȚĂMÎNT:
                                        </label>
                                    </td>
                                    <td class="text-left" style="width:80%;">
                                        <select id="CICLU" name="CICLU" class="span8">
                                            <option></option>
                                            <?php if( isset( $CICLURI ) && !empty( $CICLURI ) ) : ?>
                                                <?php foreach( $CICLURI as $C ) : ?>
                                                    <option value="<?php echo $C; ?>"<?php echo ( isset( $_POST['CICLU'] ) && ( $_POST['CICLU'] == $C ) ? ' selected' : '' ); ?>><?php echo $C; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" style="width:20%;">
                                        <label for="DOMENIU">
                                            DENUMIRE DOMENIU:
                                        </label>
                                    </td>
                                    <td class="text-left" style="width:80%;">
                                        <select id="DOMENIU" name="DOMENIU" class="span8">
                                            <option></option>
                                            <?php if( isset( $DOMENII ) && !empty( $DOMENII ) ) : ?>
                                                <?php foreach( $DOMENII as $D ) : ?>
                                                    <option value="<?php echo $D; ?>"<?php echo ( isset( $_POST['DOMENIU'] ) && ( $_POST['DOMENIU'] == $D ) ? ' selected' : '' ); ?>><?php echo $D; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" style="width:20%;">
                                        <label for="SPECIALIZARE">
                                            DENUMIRE SPECIALIZARE:
                                        </label>
                                    </td>
                                    <td class="text-left" style="width:80%;">
                                        <select id="SPECIALIZARE" name="SPECIALIZARE" class="span8">
                                            <option></option>
                                            <?php if( isset( $SPECIALIZARI ) && !empty( $SPECIALIZARI ) ) : ?>
                                                <?php foreach( $SPECIALIZARI as $S ) : ?>
                                                    <option value="<?php echo $S; ?>"<?php echo ( isset( $_POST['SPECIALIZARE'] ) && ( $_POST['SPECIALIZARE'] == $S ) ? ' selected' : '' ); ?>><?php echo $S; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right" style="width:20%;">
                                        <label for="STATUS">
                                            STATUS DIPLOMA:
                                        </label>
                                    </td>
                                    <td class="text-left" style="width:80%;">
                                        <select id="SPECIALIZARE" name="STATUS" class="span8">
                                            <option></option>
                                            <?php if( isset( $STATUSURI ) && !empty( $STATUSURI ) ) : ?>
                                                <?php foreach( $STATUSURI as $S ) : ?>
                                                    <option value="<?php echo $S; ?>"<?php echo ( isset( $_POST['STATUS'] ) && ( $_POST['STATUS'] == $S ) ? ' selected' : '' ); ?>><?php echo $S; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td class="text-right" style="width:20%;">
                                    </td>
                                    <td class="text-left" style="width:80%;">
                                        <button id="CAUTARE" class="btn btn-default">
                                            SUBMIT SEARCH
                                        </button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            function stripQuotes( s )
            {
                s = $.trim( s )
                if( s )
                {
                    s = s.split('"').join('');
                    s = s.split("'").join("");
                }
                return s;
            }
            ( function($, window, document) {
                var effect = {
                        slideDown: {
                                height: "show", 
                                paddingTop: "show", 
                                marginTop: "show", 
                                paddingBottom: "show", 
                                marginBottom: "show"
                        },
                        slideUp: {
                                height: "hide", 
                                paddingTop: "hide", 
                                marginTop: "hide", 
                                paddingBottom: "hide", 
                                marginBottom: "hide"
                        }
                };
                Accordion = {

                        init: function(el, options) {
                                var that = this;

                                // add css style to link
                                $('[data-acc-link]').css({ 'cursor': 'pointer' });

                                // hide content by default except content has class "acc-open"
                                $('[data-acc-content]').not('.acc-open').css({ 'display': 'none' });

                                // bind click event
                                el.on('click', '[data-acc-link]', function(e) {
                                        e.preventDefault();
                                        that.options = options;
                                        var linkName = $(this).data('acc-link');
                                        var contentObj = $('[data-acc-content='+linkName+']');
                                        that.toggle(contentObj, el);
                                });
                        },

                        open: function(contentObj) {
                                contentObj.animate(effect.slideDown, this.options.duration).addClass('acc-open');
                        },

                        close: function(contentObj) {
                                contentObj.animate(effect.slideUp, this.options.duration).removeClass('acc-open');
                        },


                        toggle: function(contentObj, el) {
                                var that = this;
                                if(!this.options.multiOpen) {
                                        var contentName = contentObj.data('acc-content');
                                        var contentObjs = $(el).find('[data-acc-content]:not([data-acc-content='+contentName+'])');
                                        contentObjs.each(function(i, v) {
                                                that.close($(v));
                                        });
                                }
                                contentObj.hasClass('acc-open') ? this.close(contentObj) : this.open(contentObj);
                        }

                };

                $.fn.accordion = function(options) {
                        options = $.extend({
                                multiOpen: true,
                                duration: 200
                        }, options);

                        return this.each(function() {
                                Accordion.init($(this), options);
                        });
                }

            })(jQuery, window, document);
            $('#accordion').accordion();
            <?php if(isset( $GRADUATES ) && !empty( $GRADUATES )) : ?>
                $('table#Table_University_Graduates').dataTable();
                $( "b[role='Info-Graduate']" ).popover( { container: 'body' } );
                $( "a[role='Edit-Graduate']" )
                    .colorbox( {    iframe:         true, 
                                    width:          "98%", 
                                    height:         "96%", 
                                    scrolling:      true, 
                                    overlayClose:   false,
                                    onClosed:       function()
                                                    { 
                                                        if( typeof $( 'button#Refresh_University_Graduates' ) != 'undefined' )
                                                        {
                                                            $( 'button#Refresh_University_Graduates' ).trigger( 'click' );
                                                        }
                                                    } 
                                } );
            <?php endif; ?>
        });
    </script>

<?php endif; ?>