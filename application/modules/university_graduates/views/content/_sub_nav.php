<?php

$checkSegment = $this->uri->segment(4);
$areaUrl = SITE_AREA . '/content/university_graduates';

?>
<ul id='nav_articles' class='nav nav-pills'>
    <?php if ( $this->auth->has_permission( 'University_graduates.Content.View' ) ) : ?>
        <li id="University_Graduates__List" <?php echo $checkSegment == '' ? ' class="active"' : ''; ?>>
            <a href="<?php echo site_url( $areaUrl ); ?>" class='list' title="<?php echo lang('university_graduates_list_index'); ?>">
                <span class='icon icon-list'></span> <?php echo lang('university_graduates_list'); ?>
            </a>
        </li>
    <?php endif; ?>
    <?php if ( $this->auth->has_permission( 'University_graduates.Content.Create' ) ) : ?>
        <li class="<?php echo $checkSegment == 'form' ? ' active' : ''; ?>">
            <a id="University_Graduates__New" href="<?php echo site_url( $areaUrl . '/create' ); ?>" class='new' title="<?php echo lang('university_graduates_create_new_button'); ?>">
                <span class='icon icon-plus-sign'></span> <?php echo lang('university_graduates_new'); ?>
            </a>
        </li>
        <script type='text/javascript'>
            $(document).ready(function(){
                $("a#University_Graduates__New").colorbox( { iframe:         true, 
                                                            width:          "98%", 
                                                            height:         "96%", 
                                                            scrolling:      true, 
                                                            overlayClose:   false, 
                                                            onClosed: function(){
                                                                if( typeof $( "#Refresh_University_Graduates" ) == 'object' )
                                                                {
                                                                    $( "#Refresh_University_Graduates" ).trigger( 'click' );
                                                                }
                                                            }
                                                        } );
            });
        </script>
    <?php endif; ?>
    <?php if ( $this->auth->has_permission( 'University_graduates.Content.Create' ) ) : ?>
        <li class="<?php echo $checkSegment == 'import' ? ' active' : ''; ?>">
            <a id="University_Graduates__Import" href="<?php echo site_url( $areaUrl . '/import' ); ?>" class='new' title="<?php echo lang('university_graduates_import_button'); ?>">
                <span class='icon icon-list-alt'></span> <?php echo lang('university_graduates_import'); ?>
            </a>
        </li>
    <?php endif; ?>
    <?php if ( $this->auth->has_permission( 'University_graduates.Content.View' ) ) : ?>
        <li>
            <a id="University_Graduates__Help" href="<?php echo site_url( $areaUrl . '/help' ); ?>" class='help' title="<?php echo lang('university_graduates_help_manual'); ?>">
                <span class='icon icon-question-sign'></span> <?php echo lang('university_graduates_help'); ?>
            </a>
        </li>
        <script type='text/javascript'>
            $(document).ready(function(){
                $("a#University_Graduates__Help").colorbox( { width:"75%", height:"75%", overlayClose: false } );
            });
        </script>
    <?php endif; ?>
</ul>