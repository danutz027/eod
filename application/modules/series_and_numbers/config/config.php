<?php defined('BASEPATH') || exit('No direct script access allowed');
$config['module_config']    =   array   (
                                            'description'   => 'Modul de Gestiune de Serii și Numere pentru Diplome',
                                            'name'          => 'Series and Numbers',
                                            'version'       => '0.0.1',
                                            'author'        => 'Victor Afteni / victor.afteni@ontotech.ro',
                                        );