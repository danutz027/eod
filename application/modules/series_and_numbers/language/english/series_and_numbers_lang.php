<?php defined('BASEPATH') || exit('No direct script access allowed');


$lang['series_and_numbers_manage']      = 'Manage Series and Numbers';
$lang['series_and_numbers_help']        = 'Help';
$lang['series_and_numbers_true']        = 'True';
$lang['series_and_numbers_false']       = 'False';
$lang['series_and_numbers_create']      = 'Create';
$lang['series_and_numbers_list']        = 'List';
$lang['series_and_numbers_new']       = 'New';
$lang['series_and_numbers_help_text']     = 'User manual for this module';
$lang['series_and_numbers_no_records']    = 'There are no series_and_numbers in the system.';
$lang['series_and_numbers_create_new']    = 'Create a new Batch for Series and Numbers.';
$lang['series_and_numbers_create_success']  = 'The Batch for Series and Numbers was successfully created.';
$lang['series_and_numbers_create_failure']  = 'There was a problem creating the series_and_numbers: ';
$lang['series_and_numbers_create_new_button'] = 'Create New Batch for Series and Numbers';
$lang['series_and_numbers_invalid_id']    = 'Invalid Record ID.';

$lang['series_and_numbers_delete_success']  = 'record(s) successfully deleted.';
$lang['series_and_numbers_delete_failure']  = 'We could not delete the record: ';
$lang['series_and_numbers_delete_error']    = 'You have not selected any records to delete.';
$lang['series_and_numbers_actions']     = 'Actions';
$lang['series_and_numbers_cancel']      = 'Cancel';
$lang['series_and_numbers_delete_record']   = 'Delete this Record';
$lang['series_and_numbers_delete_confirm']  = 'Are you sure you want to delete this series_and_numbers?';
$lang['series_and_numbers_help_heading']    = 'Help for Series and Numbers';

// Create/Edit Buttons
$lang['series_and_numbers_action_create']               = 'Create Batch for Series and Numbers';
$lang['series_and_numbers_action_title_config_batch']   = 'Config The Batch';
$lang['series_and_numbers_action_title_save_batch']     = 'Save The Batch';
$lang['series_and_numbers_action_cancel']           = 'Cancel';
$lang['series_and_numbers_action_button_generate_batch']   = 'Generate Batch';
$lang['series_and_numbers_action_button_save_batch']       = 'Save Batch';

// Activities
$lang['series_and_numbers_act_create_record'] = 'Created record with ID';
$lang['series_and_numbers_act_help_record'] = 'Updated record with ID';
$lang['series_and_numbers_act_delete_record'] = 'Deleted record with ID';

//Listing Specifics
$lang['series_and_numbers_records_empty']    = 'No records found that match your selection.';
$lang['series_and_numbers_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['series_and_numbers_column_created']  = 'Created';
$lang['series_and_numbers_column_deleted']  = 'Deleted';
$lang['series_and_numbers_column_modified'] = 'Modified';
$lang['series_and_numbers_column_deleted_by'] = 'Deleted By';
$lang['series_and_numbers_column_created_by'] = 'Created By';
$lang['series_and_numbers_column_modified_by'] = 'Modified By';

// Module Details
$lang['series_and_numbers_module_name'] = 'Series and Numbers';
$lang['series_and_numbers_module_description'] = 'Manage Series and Numbers';
$lang['series_and_numbers_area_title'] = 'Series and Numbers';

// Fields
$lang['series_and_numbers__field__Diploma_Design__legend'] = 'Design';
$lang['series_and_numbers__field__Diploma_Design__label__type'] = 'Type';
$lang['series_and_numbers__field__Diploma_Design__label__year'] = 'Year';
$lang['series_and_numbers__field__Diploma_String__legend'] = 'Series';
$lang['series_and_numbers__field__Diploma_String__label__string'] = 'String';
$lang['series_and_numbers__field__Diploma_Number__legend'] = 'Numbers';
$lang['series_and_numbers__field__Diploma_Number__label__Length'] = 'Length';
$lang['series_and_numbers__field__Diploma_Number__label__Minimum'] = 'Minimum';
$lang['series_and_numbers__field__Diploma_Number__label__Maximum'] = 'Maximum';
$lang['series_and_numbers__field__Diploma_Record__legend'] = 'Record';
$lang['series_and_numbers__field__Diploma_Record__label__Begin_Index'] = 'Begin Index';