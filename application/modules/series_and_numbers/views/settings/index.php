<?php
    $can__series_and_numbers__view       = $this->auth->has_permission( 'Series_and_numbers.Settings.View' );
    $can__series_and_numbers__create     = $this->auth->has_permission( 'Series_and_numbers.Settings.Create' );
    $can__series_and_numbers__edit       = $this->auth->has_permission( 'Series_and_numbers.Settings.Edit' );
    $can__series_and_numbers__delete     = $this->auth->has_permission( 'Series_and_numbers.Settings.Delete' );
?>
<style>
    #table_series_and_numbers tbody tr td { text-align: center !important; }
</style>
<h3><?php echo lang('series_and_numbers_manage'); ?><hr /></h3>

<?php if ( $can__series_and_numbers__view ) : ?>
    
    <div class='admin-box'>
        <form id="form" method="post" action="<?php echo site_url( 'admin/settings/series_and_numbers/report/report' ); ?>" target="_blank">
            <table class='table table-striped table-responsive' id="table_series_and_numbers" style="width:100%;">
                <thead>
                    <tr>
                        <th class="text-center" style="width:15%;"><input id="checkbox" type="checkbox" /></th>
                        <th class="text-center" style="width:55%;">Code-String</th>
                        <th class="text-center" style="width:15%;">Code-Status</th>
                        <th class="text-center" style="width:15%;">Diploma-Id</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot>
                    <tr>
                        <td colspan="1">
                            <select id="select" name="select" style="width: 100%; display: none;">
                                <option value="PDF">PDF</option>
                                <option value="CSV">CSV</option>
                            </select>
                        </td>
                        <td colspan="3">
                            <button id="button" type="submit" class="btn btn-default" style="width: 100%; display: none;">
                                Genereaza Raport pentru Înregistrările Selectate
                            </button>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('table#table_series_and_numbers')
                .dataTable( {
                    "destroy":      true,
                    "responsive":   true,
                    "bFilter":      true,
                    "bProcessing":  true,
                    "bServerSide":  true,
                    "sDom":         'T<"clear">lfrtip',
                    "bSort":        true, 
                    "ajax":         '<?php echo site_url( 'admin/settings/series_and_numbers/index/ajax' ); ?>',
                    "searching": true,
                    "sPaginationType": 'full_numbers',
                    "iDisplayLength":   100,
                    "lengthMenu":       [
                                            [ 
                                                100, 
                                                250, 
                                                500,
                                                2147483647
                                            ],
                                            [ 
                                                100, 
                                                250, 
                                                500,
                                                'All'
                                            ]
                                        ],
                    "columnDefs":       [ 
                                            {
                                                "targets":          0,
                                                "visible" :         true,
                                                "searchable":       true,
                                                "orderable":        false,
                                                "render":           function ( data, type, row ) 
                                                                    {
                                                                        return  '<input name="code[]" type="checkbox" value="'+row.Diploma_Code_Value+'" />';
                                                                    }
                                            },
                                            {
                                                "targets":          1,
                                                "visible" :         true,
                                                "searchable":       true,
                                                "orderable":        true,
                                                "render":           function ( data, type, row ) 
                                                                    {
                                                                        var D_C_V = ( row.Diploma_Code_Value ).split('-');
                                                                        return  '<span style="color: grey; font-family: monospace;">'+
                                                                                ( D_C_V.length == 5 
                                                                                ?   '<b style="color: darkblue;">' + D_C_V[0] + '</b>-'+
                                                                                    '<b style="color: darkorange;">' + D_C_V[1] + '</b>-'+
                                                                                    '<b style="color: red;">' + D_C_V[2] + '</b>-'+
                                                                                    '<b style="color: red;">' + D_C_V[3] + '</b>-'+
                                                                                    '<b style="color: darkorchid;">' + D_C_V[4] + '</b>'
                                                                                :   row.Diploma_Code_Value ) + '</span>'; 
                                                                    }
                                            },
                                            {
                                                "targets":          2,
                                                "visible" :         true,
                                                "searchable":       true,
                                                "orderable":        true,
                                                "render":           function ( data, type, row ) 
                                                                    {
                                                                        return  row.Diploma_Code_Status;
                                                                    }
                                            },
                                            {
                                                "targets":          3,
                                                "visible" :         true,
                                                "searchable":       true,
                                                "orderable":        true,
                                                "render":           function ( data, type, row ) 
                                                                    {
                                                                        return  row.Gratuate_ID;
                                                                    }
                                            }
                                        ],
                    "fnDrawCallback":   function()  
                    {
                            $( "input#checkbox[type='checkbox']" )
                                .on(    "click", 
                                        function()
                                        {
                                            var c = $( this ).prop( 'checked' );
                                            if( c ) 
                                            {
                                                $( "button#button" ).fadeIn();
                                                $( "select#select" ).fadeIn();
                                            } else {
                                                $( "button#button" ).fadeOut();
                                                $( "select#select" ).fadeOut();
                                            }
                                            $( "table#table_series_and_numbers tbody tr td:first-child input[type='checkbox']" )
                                                .each( function( index ) {
                                                    $( this ).prop( 'checked', c );
                                                } );
                                        } 
                                   );
                            $( "input[type='checkbox']" )
                                .on(    "click", 
                                        function()
                                        {
                                            var c = $( this ).prop( 'checked' );
                                            if( c ) 
                                            {
                                                $( "button#button" ).fadeIn();
                                                $( "select#select" ).fadeIn();
                                            } else {
                                                c = false;
                                                $( "table#table_series_and_numbers tbody tr td:first-child input[type='checkbox']" ).each( function( index ) { 
                                                    if( $( this ).prop( 'checked' ) ) { c = true; } 
                                                } );
                                                if( c ) 
                                                {
                                                    $( "button#button" ).fadeIn();
                                                    $( "select#select" ).fadeIn();
                                                } else {
                                                    $( "button#button" ).fadeOut();
                                                    $( "select#select" ).fadeOut();
                                                }
                                            }
                                        } 
                                   );
                            $( "button#button" )
                                .on(    "click", 
                                        function()
                                        {
                                            var $a = [];
                                            if( ( $( "table#table_series_and_numbers tbody tr td:first-child input[type='checkbox']:checked" ) ).length )
                                            {
                                                $( "table#table_series_and_numbers tbody tr td:first-child input[type='checkbox']:checked" ).each( function( index ) { 
                                                    $a.push( $( this ).val() )
                                                } );
                                            }
                                            if( $a.length )
                                            {
                                                $( 'form#form' ).submit();
                                            } else {
                                                alert('Lista selectată este goală');
                                            }
                                        } 
                                   );
                    }
                } );

        });
    </script>

<?php endif; ?>