<h3><?php echo lang('series_and_numbers_action_create'); ?><hr /></h3>

<?php if ( $this->auth->has_permission('Series_and_numbers.Settings.Create') ) : ?>

    <div id="div-alert" class='alert alert-error' style="display: none;">
        <a class='close' name="link-alert" id="link-alert"></a>
        <h4 class='alert-heading'></h4>
    </div>

    <div class='admin-box'>

        <table id="table-config" class="table table-responsive table-striped" style="width: 100%; margin: 0 auto;">
            <thead>
                <tr>
                    <th class="text-right">
                        <a name="link-config" id="link-config"></a>
                        <i class="icon icon-cog"></i>  
                    </th>
                    <th class="text-left">
                        <b>
                            <?php echo lang( 'series_and_numbers_action_title_config_batch' ); ?>
                        </b>  
                    </th>
                    <th class="text-left">
                        <span class='help-inline table-config'></span>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <h5><?php echo lang('series_and_numbers__field__Diploma_Design__legend'); ?></h5>
                    </td>
                </tr>
                <tr class="control-group">
                    <td class="text-right">
                        <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'series_and_numbers__field__Diploma_Design__label__type' ), 'Diploma_String', array('class' => 'control-label' ) ); ?>
                    </td>
                    <td class='controls'>
                        <select id='Diploma_Design_Type' required='required' name='Diploma_Design_Type' class="span8">
                            <option></option>
                            <?php if ( isset( $diplomas_design_type ) && !empty( $diplomas_design_type ) ) : ?>
                                <?php foreach ( $diplomas_design_type as $ddt => $DDT ) : ?>

                                        <option value="<?php echo $DDT; ?>">
                                            <?php echo $DDT; ?>
                                        </option>

                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </td>
                    <td class="text-left">
                        <span class='help-inline Diploma_Design_Type'></span>
                    </td>
                </tr>               
                <tr class="control-group">
                    <td class="text-right">
                        <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'series_and_numbers__field__Diploma_Design__label__year' ), 'Diploma_Year', array('class' => 'control-label' ) ); ?>
                    </td>
                    <td class='controls'>
                        <select id='Diploma_Design_Year' required='required' name='Diploma_Design_Year' class="span8">
                            <option></option>
                            <?php  for ( ( $y = intval( date( 'Y' ) ) ); 1900 <= $y; $y-- ) : ?>
                                <option value="<?php echo $y; ?>">
                                    <?php echo $y; ?>
                                </option>
                            <?php endfor; ?>
                        </select>
                    </td>
                    <td class="text-left">
                        <span class='help-inline Diploma_Design_Year'></span>
                    </td>
                </tr>               
                <tr>
                    <td></td>
                    <td colspan="2">
                        <h5><?php echo lang('series_and_numbers__field__Diploma_String__legend'); ?></h5>
                    </td>
                </tr>
                <tr class="control-group">
                    <td class="text-right">
                        <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'series_and_numbers__field__Diploma_String__label__string' ), 'Diploma_String', array('class' => 'control-label' ) ); ?>
                    </td>
                    <td class='controls'>
                        <input id='Diploma_String' type='text' required='required' name='Diploma_String' maxlength='255' value="" class="span8" />
                    </td>
                    <td class="text-left">
                        <span class='help-inline Diploma_String'></span>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <h5><?php echo lang('series_and_numbers__field__Diploma_Number__legend'); ?></h5>
                    </td>
                </tr>
                <tr class="control-group">
                    <td class="text-right">
                        <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'series_and_numbers__field__Diploma_Number__label__Length' ), 'Diploma_Number_Len', array('class' => 'control-label' ) ); ?>
                    </td>
                    <td class='controls'>
                        <input id='Diploma_Number_Len' type='number' required='required' name='Diploma_Number_Len' maxlength='255' value="6" min="1" max="9" step="1" class="span8" />
                    </td>
                    <td class="text-left">
                        <span class='help-inline Diploma_Number_Len'></span>
                    </td>
                </tr>
                <tr class="control-group">
                    <td class="text-right">
                        <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'series_and_numbers__field__Diploma_Number__label__Minimum' ), 'Diploma_Number_Min', array('class' => 'control-label' ) ); ?>
                    </td>
                    <td class='controls'>
                        <input id='Diploma_Number_Min' type='number' required='required' name='Diploma_Number_Min' maxlength='9' value="0" min="0" max="999999999" step="1" class="span8" />
                    </td>
                    <td class="text-left">
                        <span class='help-inline Diploma_Number_Min'></span>
                    </td>
                </tr>
                <tr class="control-group">
                    <td class="text-right">
                        <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'series_and_numbers__field__Diploma_Number__label__Maximum' ), 'Diploma_Number_Max', array('class' => 'control-label' ) ); ?>
                    </td>
                    <td class='controls'>
                        <input id='Diploma_Number_Max' type='number' required='required' name='Diploma_Number_Max' maxlength='9' value="0" min="0" max="999999999" class="span8" />
                    </td>
                    <td class="text-left">
                        <span class='help-inline Diploma_Number_Max'></span>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="2">
                        <h5><?php echo lang('series_and_numbers__field__Diploma_Record__legend'); ?></h5>
                    </td>
                </tr>
                <tr class="control-group">
                    <td class="text-right">
                        <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'series_and_numbers__field__Diploma_Record__label__Begin_Index' ), 'Diploma_Record_BeginIndex', array('class' => 'control-label' ) ); ?>
                    </td>
                    <td class='controls'>
                        <input id='Diploma_Record_BeginIndex' type='number' required='required' name='Diploma_Record_BeginIndex' maxlength='19' value="0" min="1" max="9999999999999999999" class="span8" />
                    </td>
                    <td class="text-left">
                        <span class='help-inline Diploma_Record_BeginIndex'></span>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td style="background-color: #CCC; border-top: 1px solid #444;"></td>
                    <td style="background-color: #CCC; border-top: 1px solid #444;">
                        <button name='button-config' id='button-config' type='button' class='btn btn-primary pull-left'>
                            <span class='icon icon-ok-circle icon-white'></span>&nbsp;<?php echo lang( 'series_and_numbers_action_button_generate_batch' ); ?>
                        </button>
                    </td>
                    <td style="background-color: #CCC; border-top: 1px solid #444;">
                        <a href='<?php echo site_url( 'admin/settings/series_and_numbers/index' ); ?>' class='btn btn-warning pull-right'>
                            <span class='icon icon-refresh icon-white'></span>&nbsp;<?php echo lang( 'series_and_numbers_action_cancel' ); ?>
                        </a>
                    </td>
                </tr>
            </tfoot>
        </table>
        <table id="table-batch"  class="table table-responsive table-striped" style="width: 100%; margin: 1em auto; display: none;">
            <thead>
                <tr>
                    <th class="span1 text-center">
                        <a name="link-save" id="link-generate"></a>
                        <i class="icon icon-list"></i>  
                    </th>
                    <th class="span11 text-left" colspan="2">
                        <b>
                            <?php echo lang( 'series_and_numbers_action_title_save_batch' ); ?>
                        </b>  
                    </th>
                </tr>
                <tr>
                    <th class="span1 text-center" style="background-color: #CCC;">
                        <img id="img-ajax" src="<?php echo base_url ( 'themes/admin/images/ajax-loader-small.gif' ); ?>" style="width: 16px;  height: 16px; border: none; display: none;" />
                    </th>
                    <th class="span8 text-left" style="background-color: #CCC;">Cod</th>
                    <th class="span3 text-center" style="background-color: #CCC;"></th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <td class="span1" style="background-color: #CCC;">
                        <a name="link-save" id="link-save"></a>
                    </td>
                    <td class="span8" style="background-color: #CCC;">
                        <button name='button-save' id='button-save' type='button' class='btn btn-primary pull-left'>
                            <span class='icon icon-ok-sign icon-white'></span>&nbsp;<?php echo lang( 'series_and_numbers_action_button_save_batch' ); ?>
                        </button>
                    </td>
                    <td class="span3" style="background-color: #CCC;">
                        <a href='<?php echo site_url( 'admin/settings/series_and_numbers/index' ); ?>' class='btn btn-warning pull-right'>
                            <span class='icon icon-refresh icon-white'></span>&nbsp;<?php echo lang( 'series_and_numbers_action_cancel' ); ?>
                        </a>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>

    <script type="text/javascript">
        if( window.jQuery )
        {
            $( document ).ready(function(){

                if  ( 
                        ( typeof $( 'div#div-alert' ) != 'undefined' ) && 
                        ( typeof $( 'img#img-ajax' ) != 'undefined' ) && 
                        ( typeof $( 'table#table-config' ) != 'undefined' ) && 
                        ( typeof $( 'button#button-config' ) != 'undefined' ) && 
                        ( typeof $( 'span.help-inline.table-config' ) != 'undefined' ) &&
                        ( typeof $( 'table#table-batch' ) != 'undefined' ) && 
                        ( typeof $( 'button#button-save' ) != 'undefined' )
                    )
                {
                    var $div_alert = $( 'div#div-alert' ); 
                    var $img_ajax = $( 'img#img-ajax' ); 
                    $div_alert.find('h4.alert-heading').empty().html('').parent().hide();
                    $img_ajax.hide();
                    $( 'button#button-save' ).click( function( e ) {
                        var  $input_series_and_numbers = $( 'input[type="checkbox"]' );
                        if( $input_series_and_numbers.length )
                        {
                            var $array__series_and_numbers = [];
                            $.each( $input_series_and_numbers, function() {
                                if( $(this).prop( 'checked' ) )
                                {
                                    $array__series_and_numbers.push( { 'Diploma_Code_Type': $(this).attr( 'role' ), Diploma_Code_Value: $( this ).val() } );
                                }
                            });
                            console.log( $array__series_and_numbers );
                            if( $array__series_and_numbers.length )
                            {
                                var $url___series_and_numbers = "<?php echo site_url( 'admin/settings/series_and_numbers/create' ); ?>";
                                var $json__series_and_numbers = { 'series_and_numbers': $array__series_and_numbers };
                                $div_alert.find('h4.alert-heading').empty().html('').parent().hide();
                                $img_ajax.fadeIn(); 
                                console.log( $array__series_and_numbers );
                                $.post  ( 
                                            $url___series_and_numbers, 
                                            $json__series_and_numbers 
                                        )
                                        .done( function( data ) {
                                            if( data )
                                            {
                                                $JSON = JSON.parse( data );
                                                console.log( 'JSON', $JSON );
                                                if( $JSON.length ) 
                                                {
                                                    $.each( $JSON, function( $i, $object ) {
                                                        if( $object )
                                                        {
                                                            var $code   =   ( Object.keys( $object ) )[0];
                                                            var $value  =   ( Object.values( $object ) )[0];
                                                            if( $code && $value && ( typeof $( 'input[value="'+$code+'"]' ) != 'undefined' ) )
                                                            {
                                                                console.log( $i, 'code', $code, $value );
                                                                $( 'input[value="'+$code+'"]' ).closest('tr').children('td:last-child').html( $value );
                                                            } else {
                                                                console.log( $i, 'No Input' );
                                                                
                                                            }
                                                        } else {
                                                            console.log( $i, 'No Object' );
                                                        }
                                                    });
                                                } else {
                                                    $div_alert
                                                        .children('h4.alert-heading').empty().html( 'Empty Data Array!' )
                                                        .parent().removeAttr('class').addClass('alert').addClass('alert-error').fadeIn();
                                                }
                                            } else {
                                                $div_alert
                                                    .children('h4.alert-heading').empty().html( 'Data Reception Error!' )
                                                    .parent().removeAttr('class').addClass('alert').addClass('alert-error').fadeIn();
                                            }
                                        })
                                        .fail(function() {
                                                $div_alert
                                                    .children('h4.alert-heading').empty().html( 'Data Transmission Error!' )
                                                    .parent().removeAttr('class').addClass('alert').addClass('alert-error').fadeIn();
                                        })
                                        .always(function() {
                                            $img_ajax.fadeOut(); 
                                            $( 'table#table-batch tfoot' ).fadeOut();
                                        });
                            } else {
                                $div_alert
                                    .children('h4.alert-heading').empty().html('Series and Numbers List is Empty')
                                    .parent().removeAttr('class').addClass('alert').addClass('alert-error').fadeIn();
                            }
                        }
                    });
                    $( 'button#button-config' ).click( function( e ) {
                        var VALID = true;
                        var ALARM = $( 'h3#heading-generte' ).children( 'span.help-inline.pull-right' );
                        ALARM.empty().html('');
                        if  ( 
                                ( 
                                    typeof $( 'select#Diploma_Design_Type' )            !=  'undefined' && 
                                    typeof $( 'span.help-inline.Diploma_Design_Type' )  !=  'undefined' 
                                )   
                                &&
                                ( 
                                    typeof $( 'select#Diploma_Design_Year' )            !=  'undefined' && 
                                    typeof $( 'span.help-inline.Diploma_Design_Year' )  !=  'undefined' 
                                )   
                                &&
                                ( 
                                    typeof $( 'input#Diploma_String' )                  !=  'undefined' && 
                                    typeof $( 'span.help-inline.Diploma_String' )       !=  'undefined' 
                                )   
                                &&
                                ( 
                                    typeof $( 'input#Diploma_Number_Len' )              !=  'undefined' && 
                                    typeof $( 'span.help-inline.Diploma_Number_Len' )   !=  'undefined' 
                                )
                                &&
                                ( 
                                    typeof $( 'input#Diploma_Number_Min' )              !=  'undefined' && 
                                    typeof $( 'span.help-inline.Diploma_Number_Min' )   !=  'undefined' 
                                )
                                &&
                                ( 
                                    typeof $( 'input#Diploma_Number_Max' )              !=  'undefined' && 
                                    typeof $( 'span.help-inline.Diploma_Number_Max' )   !=  'undefined' 
                                )
                                &&
                                ( 
                                    typeof $( 'input#Diploma_Record_BeginIndex' )              !=  'undefined' && 
                                    typeof $( 'span.help-inline.Diploma_Record_BeginIndex' )   !=  'undefined' 
                                )
                            )
                        {
                            $( 'span.help-inline.Diploma_Design_Type' ).empty().html( '' );
                            $( 'span.help-inline.Diploma_Design_Year' ).empty().html( '' );
                            $( 'span.help-inline.Diploma_String' ).empty().html( '' );
                            $( 'span.help-inline.Diploma_Number_Len' ).empty().html( '' );
                            $( 'span.help-inline.Diploma_Number_Min' ).empty().html( '' );
                            $( 'span.help-inline.Diploma_Number_Max' ).empty().html( '' );
                            $( 'span.help-inline.Diploma_Record_BeginIndex' ).empty().html( '' );

                            var $Diploma_Design_Type        =   $.trim( $( 'select#Diploma_Design_Type' ).val() );
                            var $Diploma_Design_Year        =   parseInt( $.trim( $( 'select#Diploma_Design_Year' ).val() ) );
                            var $Diploma_String             =   $.trim( $( 'input#Diploma_String' ).val() );
                            var $Diploma_Number_Len         =   parseInt( $( 'input#Diploma_Number_Len' ).val() );
                            var $Diploma_Number_Min         =   parseInt( $( 'input#Diploma_Number_Min' ).val() );
                            var $Diploma_Number_Max         =   parseInt( $( 'input#Diploma_Number_Max' ).val() );
                            var $Diploma_Record_BeginIndex  =   parseInt( $( 'input#Diploma_Record_BeginIndex' ).val() );

                            if( $Diploma_Design_Type )
                            {
                                $( 'span.help-inline.Diploma_Design_Type' ).empty().html( '' );
                                if( $Diploma_Design_Year )
                                {
                                    $( 'span.help-inline.Diploma_Design_Year' ).empty().html( '' );
                                    if( $Diploma_String )
                                    {
                                        $( 'span.help-inline.Diploma_String' ).empty().html( '' );
                                        if( $Diploma_Number_Len )
                                        {
                                            $( 'span.help-inline.Diploma_Number_Len' ).empty().html( '' );
                                            if( $Diploma_Number_Min )
                                            {
                                                $( 'span.help-inline.Diploma_Number_Min' ).empty().html( '' );
                                                if( $Diploma_Number_Max )
                                                {
                                                    $( 'span.help-inline.Diploma_Number_Max' ).empty().html( '' );

                                                    if( $Diploma_Number_Min <= $Diploma_Number_Max  )
                                                    {
                                                        var String__Diploma_Number_Max__Length = ( $Diploma_Number_Max.toString() ).length;
                                                        if( String__Diploma_Number_Max__Length <= $Diploma_Number_Len )
                                                        {
                                                            if( $Diploma_Record_BeginIndex )
                                                            {
                                                                $( 'span.help-inline.Diploma_Record_BeginIndex' ).empty().html( '' );
                                                                function Integer2String( INT, LEN = $Diploma_Number_Len )
                                                                {
                                                                    var PREFIX = '';
                                                                    var STRING = INT.toString();
                                                                    var LENGTH = STRING.length;
                                                                    if( LENGTH < LEN )
                                                                    {
                                                                        for( var i = 0; i < ( LEN - LENGTH ); i++ )
                                                                        {
                                                                            PREFIX = PREFIX + '0';
                                                                        }
                                                                        return PREFIX + STRING;
                                                                    } else {
                                                                        return STRING;
                                                                    }
                                                                }
                                                                $( 'table#table-config' ).fadeOut();
                                                                if  ( typeof $( 'table#table-batch' ) != 'undefined' )
                                                                {
                                                                    var  $table_batch = $( 'table#table-batch' );
                                                                    $table_batch.fadeIn();
                                                                    var  $table_batch_body = $table_batch.children( 'tbody' );
                                                                    $table_batch_body.children( 'tbody' ).empty().html( '' );
                                                                    for( $i = 0; $i <= ( $Diploma_Number_Max - $Diploma_Number_Min ); $i++ )
                                                                    {
                                                                        var $Diploma_Number = $Diploma_Number_Min + $i;
                                                                        var $Diploma_Record = $Diploma_Record_BeginIndex + $i;
                                                                        var $Diploma_Code = ( $Diploma_Design_Year + '-' + $Diploma_Design_Type + '-' + $Diploma_String + '-' + Integer2String( $Diploma_Number ) + '-' + $Diploma_Record ); //.toUpperCase();
                                                                        $table_batch_body
                                                                            .append (
                                                                                        '<tr>'+
                                                                                            '<td class="text-center"><input value="'+$Diploma_Code+'" role="'+$Diploma_Design_Type+'" type="checkbox" checked></td>'+
                                                                                            '<td class="text-left bold">'+$Diploma_Code+'</td>'+
                                                                                            '<td class="text-left"></td>'+
                                                                                        '</tr>'
                                                                                    );
                                                                    }
                                                                }
                                                            } else {
                                                                VALID = false;
                                                                $( 'span.help-inline.Diploma_Record_BeginIndex' ).empty().html( '<?php echo ( lang( 'bf_error_invalid' ) ); ?>' );
                                                                $( 'input#Diploma_Record_BeginIndex' ).focus();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            VALID = false;
                                                            $( 'span.help-inline.Diploma_Number_Len' ).empty().html( '<?php echo ( lang( 'bf_error_invalid' ) ); ?>' );
                                                            $( 'span.help-inline.Diploma_Number_Min' ).empty().html( '<?php echo ( lang( 'bf_error_invalid' ) ); ?>' );
                                                            $( 'span.help-inline.Diploma_Number_Max' ).empty().html( '<?php echo ( lang( 'bf_error_invalid' ) ); ?>' );  
                                                            $( 'input#Diploma_Number_Len' ).focus();
                                                        }
                                                    } else {
                                                        VALID = false;
                                                        $( 'span.help-inline.Diploma_Number_Min' ).empty().html( '<?php echo ( lang( 'bf_error_invalid' ) ); ?>' );
                                                        $( 'span.help-inline.Diploma_Number_Max' ).empty().html( '<?php echo ( lang( 'bf_error_invalid' ) ); ?>' );
                                                        $( 'input#Diploma_Number_Min' ).focus();
                                                    }
                                                }
                                                else
                                                {
                                                    VALID = false;
                                                    $( 'span.help-inline.Diploma_Number_Max' ).empty().html( '<?php echo ( lang( 'bf_error_invalid' ) ); ?>' );
                                                    $( 'input#Diploma_Number_Max' ).focus();
                                                }
                                            }
                                            else
                                            {
                                                VALID = false;
                                                $( 'span.help-inline.Diploma_Number_Min' ).empty().html( '<?php echo ( lang( 'bf_error_invalid' ) ); ?>' );
                                                $( 'input#Diploma_Number_Min' ).focus();
                                            }
                                        }
                                        else
                                        {
                                            VALID = false;
                                            $( 'span.help-inline.Diploma_Number_Len' ).empty().html( '<?php echo ( lang( 'bf_error_invalid' ) ); ?>' );
                                            $( 'input#Diploma_Number_Len' ).focus();
                                        }
                                    }
                                    else
                                    {
                                        VALID = false;
                                        $( 'span.help-inline.Diploma_String' ).empty().html( '<?php echo ( lang( 'bf_error_invalid' ) ); ?>' );
                                        $( 'input#Diploma_String' ).focus();
                                    }
                                }
                                else
                                {
                                    VALID = false;
                                    $( 'span.help-inline.Diploma_Design_Year' ).empty().html( '<?php echo ( lang( 'bf_error_empty' ) ); ?>' );
                                    $( 'select#Diploma_Design_Year' ).focus();
                                }
                            } 
                            else
                            {
                                VALID = false;
                                $( 'span.help-inline.Diploma_Design_Type' ).empty().html( '<?php echo ( lang( 'bf_error_empty' ) ); ?>' );
                                $( 'input#Diploma_Design_Type' ).focus();
                            }
                        } 
                        else 
                        {
                            VALID = false;
                            ALARM.empty().html( '<?php echo ( lang( 'bf_error_fatal' ) ); ?>' );
                        }
                    });
                }
            });
        } 
        else 
        {
            if( typeof ( document.getElementById( 'body_blank' ) ) != 'undefined' )
            {
                document.getElementById( 'desktop' ).innerHTML = "<center><h5 style='color:red;'><?php echo lang( 'bf_ajax_warning' ); ?></h5></center>";
            }
            alert( '<?php echo lang( 'bf_ajax_warning' ); ?>' );
        }
    </script>
<?php endif; ?>