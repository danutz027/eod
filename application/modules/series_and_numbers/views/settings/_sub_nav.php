<?php

$checkSegment = $this->uri->segment(4);
$areaUrl = SITE_AREA . '/settings/series_and_numbers';

?>
<ul id='nav_articles' class='nav nav-pills'>
    <li<?php echo $checkSegment == '' ? ' class="active"' : ''; ?>>
        <a href="<?php echo site_url( $areaUrl ); ?>" class='list' title="List of Series and Numbers">
            <span class='icon icon-list'></span> Index
        </a>
    </li>
    <?php if ( $this->auth->has_permission('Series_and_numbers.Settings.Create') ) : ?>
    <li class="<?php echo $checkSegment == 'create' ? ' active' : ''; ?>">
        <a href="<?php echo site_url( $areaUrl . '/create' ); ?>" class='new' title="Create a new batch of numbers for a series">
            <span class='icon icon-plus-sign'></span> New
        </a>
    </li>
    <?php endif; ?>
    <li>
        <a id="Series_and_Numbers__Help" href="<?php echo site_url( $areaUrl . '/help' ); ?>" class='help' title="User manual for this module">
            <span class='icon icon-question-sign'></span> Help
        </a>
    </li>
        <script type='text/javascript'>
            $(document).ready(function(){
                $("a#Series_and_Numbers__Help").colorbox( { width:"75%", height:"75%", overlayClose: false } );
            });
        </script>
</ul>