<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Settings controller
 */
class Settings extends Admin_Controller
{
    protected $permissionCreate = 'Series_and_numbers.Settings.Create';
    protected $permissionDelete = 'Series_and_numbers.Settings.Delete';
    protected $permissionEdit   = 'Series_and_numbers.Settings.Edit';
    protected $permissionView   = 'Series_and_numbers.Settings.View';

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->auth->restrict($this->permissionView);
        $this->lang->load('series_and_numbers');
        $this->load->model( 'diplomas_table__diplomas_codes__model' );
        $this->load->model( 'diplomas_table__diplomas_graduates__model' );
        $this->load->model( 'diplomas_view__diplomas_codes_2_graduate__model' );
        
        $this->form_validation->set_error_delimiters("<span class='error'>", "</span>");
        Template::set_block('sub_nav', 'settings/_sub_nav');
        Assets::add_module_js('series_and_numbers', 'series_and_numbers.js');
        Template::set( 'diplomas_design_type', $this->config->item('diplomas_design_type') ); 
    }
    private function convert__Romanian_CI_AI__to__UTF8( $string )
    {
        //return str_replace( "â", "â", str_replace( "Â", "Â", str_replace( "Ă", "Ă", str_replace( "ă", "ă", str_replace( "Ş", "Ș", str_replace( "ş", "ș", str_replace( "Ţ", "Ț", str_replace( "ţ", "ț", $string ) ) ) ) ) ) ) );
        return str_replace( "â", "â", str_replace( "Â", "Â", str_replace( "Ă", "Ă", str_replace( "ă", "ă", str_replace( "Ş", "Ș", str_replace( "ş", "ș", str_replace( "Ţ", "Ț", str_replace( "ţ", "ț", $string ) ) ) ) ) ) ) );
    }

    /**
     * Display a list of Series and Numbers data.
     *
     * @return void
     */
    public function index( $ajax = NULL )
    {
        if ( $ajax == 'ajax' )
        {
            $SQL = 'SELECT '.
                        $this->db->dbprefix.$this->diplomas_table__diplomas_codes__model->table_name . '.Diploma_Code_Value, ' . PHP_EOL .
                        $this->db->dbprefix.$this->diplomas_table__diplomas_codes__model->table_name . '.Diploma_Code_Status, ' . PHP_EOL .
                        '( SELECT '
                            . $this->db->dbprefix.$this->diplomas_table__diplomas_graduates__model->table_name . '.' . $this->db->dbprefix.$this->diplomas_table__diplomas_graduates__model->key .  
                            ' FROM ' . $this->db->dbprefix.$this->diplomas_table__diplomas_graduates__model->table_name .
                            ' WHERE ' . $this->db->dbprefix.$this->diplomas_table__diplomas_graduates__model->table_name  . '.Diploma_Code_Value = ' . $this->db->dbprefix.$this->diplomas_table__diplomas_codes__model->table_name . '.Diploma_Code_Value'
                        . ' ) as Gratuate_ID ' . PHP_EOL .

                    ' FROM ' . $this->db->dbprefix . $this->diplomas_table__diplomas_codes__model->table_name . PHP_EOL .

                    ' WHERE     ' . $this->db->dbprefix . $this->diplomas_table__diplomas_codes__model->table_name . '.Diploma_Code_Value != \'\' ' . PHP_EOL;

            if (
                    ( isset( $_GET['search']['value'] ) && ( $_GET['search']['value'] = trim( $_GET['search']['value'] ) ) ) 
               )
            {
                if( isset( $_GET['search']['value'] ) && $_GET['search']['value'] )
                {
                    $sSearchValue   =   explode( ' ', preg_replace( '/([ ]+)/', ' ', trim( urldecode( $_GET['search']['value'] ) ) ) );
                    $sSearchCount   =   sizeof( $sSearchValue );
                    $sSearchIndex   =   0;
                    if( $sSearchCount  )
                    {
                        $SQL .=     ' AND ' . PHP_EOL . 
                                    ' ( ' . PHP_EOL;
                        foreach ( $sSearchValue as $sS ) 
                        {
                            $sSearchIndex++; 
                            if( $sS != '' ) 
                            {
                                $SQL        .= 	' ( ' . PHP_EOL .
                                                    ' ( ' . PHP_EOL .
                                                            ' ( ' . $this->db->dbprefix . $this->diplomas_table__diplomas_codes__model->table_name . '.Diploma_Code_Value LIKE \'%' . $sS . '%\' ) ' . PHP_EOL .
                                                            ' OR ' . PHP_EOL .
                                                            ' ( ' . $this->db->dbprefix . $this->diplomas_table__diplomas_codes__model->table_name . '.Diploma_Code_Status LIKE \'%' . $sS . '%\' ) ' . PHP_EOL .
                                                    ' ) ' . PHP_EOL .
                                                ' ) ' . PHP_EOL;
                                if( $sSearchIndex != $sSearchCount ) 
                                {
                                    $SQL    .= 	' OR ' . PHP_EOL;
                                }
                            }
                        }
                        $SQL .=     ' ) ' . PHP_EOL;
                    }
                }
                $SQL    .=  ' ORDER BY  ' . $this->db->dbprefix . $this->diplomas_table__diplomas_codes__model->table_name . '.Diploma_Code_Value DESC ' . PHP_EOL ;
                $users__diplomas_codes__nrtotal =   count ( $this->db->query( $SQL )->result() );
                $users__diplomas_codes__records =   $this
                                                        ->db
                                                        ->query(    $SQL . 
                                                                    ' OFFSET    ' . (   isset( $_GET['start'] ) && is_int( ( $_GET['start'] = intval( $_GET['start'] ) ) ) 
                                                                                    ?   $_GET['start']
                                                                                    :   0  ) .
                                                                    ' ROWS FETCH NEXT ' .   (   isset( $_GET['length'] ) && is_int( ( $_GET['length'] = intval( $_GET['length'] ) ) ) 
                                                                                            ?   $_GET['length'] 
                                                                                            :   ( $this->settings_lib->item('site.list_limit') ? $this->settings_lib->item('site.list_limit') : 100 ) ) . ' ROWS ONLY '
                                                               )
                                                        ->result();
            } 
            else 
            {
                $users__diplomas_codes__records =   $this
                                                        ->db
                                                        ->query(    
                                                                    $SQL .  
                                                                    ' ORDER BY  ' . $this->db->dbprefix . $this->diplomas_table__diplomas_codes__model->table_name . '.Diploma_Code_Value DESC ' . PHP_EOL .
                                                                    ' OFFSET    ' . (   isset( $_GET['start'] ) && is_int( ( $_GET['start'] = intval( $_GET['start'] ) ) ) 
                                                                                    ?   $_GET['start']
                                                                                    :   0  ) .
                                                                    ' ROWS FETCH NEXT ' .   (   isset( $_GET['length'] ) && is_int( ( $_GET['length'] = intval( $_GET['length'] ) ) ) 
                                                                                            ?   $_GET['length'] 
                                                                                            :   ( $this->settings_lib->item('site.list_limit') ? $this->settings_lib->item('site.list_limit') : 100 ) ) . ' ROWS ONLY '
                                                               )
                                                        ->result();
                //die( $this->diplomas_table__diplomas_codes__model->last_query()  );
                $users__diplomas_codes__nrtotal =   $this->diplomas_table__diplomas_codes__model
                                                        ->select( $this->diplomas_table__diplomas_codes__model->table_name.'.'.$this->diplomas_table__diplomas_codes__model->key )
                                                        ->count_by( $this->diplomas_table__diplomas_codes__model->table_name.'.'.$this->diplomas_table__diplomas_codes__model->key . ' <> \'\' ' );
            }

            if( empty( $users__diplomas_codes__records ) )
            {
                $output =   array(  "sEcho"                     => intval( ( isset( $_GET['sEcho'] ) ? $_GET['sEcho'] : '0' ) ),
                                    "iTotalDisplayRecords"      => 0,
                                    "iTotalRecords"             => intval( ( isset( $_GET['length'] ) ? $_GET['length'] : '10' ) ),
                                    "aaData"                    => array() );
            } 
            else 
            {                            
                $output =   array(  "sEcho"                     => intval( ( isset( $_GET['sEcho'] ) ? $_GET['sEcho'] : '0' ) ),
                                    "iTotalDisplayRecords"      => $users__diplomas_codes__nrtotal,
                                    "iTotalRecords"             => intval( ( isset( $_GET['length'] ) ? $_GET['length'] : '10' ) ),
                                    "aaData"                    => array()
                                );
                foreach (  $users__diplomas_codes__records as $u_s_r ) 
                {
                    $output['aaData'][] =   array(
                                                    'Diploma_Code_Value'    =>  $u_s_r->Diploma_Code_Value, 
                                                    'Diploma_Code_Status'   =>  $u_s_r->Diploma_Code_Status, 
                                                    'Gratuate_ID'           =>  $u_s_r->Gratuate_ID
                                                    
                                                );
                }
            }
            $this->diplomas_table__diplomas_codes__model->close();
            header('Content-Type: application/json');
            die( json_encode( $output ) );
        }   
        else    
        {
            Template::set( 'toolbar_title', get_class( $this ) . ': ' . lang( 'series_and_numbers_manage' ) );
            Template::render();
        }
    }

    public function create()
    {
        $this->auth->restrict($this->permissionCreate);
        if( isset( $_POST['series_and_numbers'] ) && !empty( $_POST['series_and_numbers'] ) )
        {
            $return = array();
            foreach( $_POST['series_and_numbers'] as $sn => $SN )
            {
                if  ( 
                        ( isset( $SN['Diploma_Code_Value'] )  &&  $SN['Diploma_Code_Value'] ) &&
                        ( isset( $SN['Diploma_Code_Type'] ) &&  in_array( $SN['Diploma_Code_Type'], $this->config->item('diplomas_design_type') ) ) 
                    )
                {
                    if( empty( $this->diplomas_table__diplomas_codes__model->find( $SN['Diploma_Code_Value'] ) ) )
                    {
                        $this->diplomas_table__diplomas_codes__model->insert( array( 'Diploma_Code_Value' =>  $SN['Diploma_Code_Value'], 'Diploma_Code_Status' => 'received' ) );
                        $return[] = array( $SN['Diploma_Code_Value'] => 'success' );
                    } else {
                            $return[] = array( $SN['Diploma_Code_Value'] => 'duplicate' );
                    }
                } 
            }
            $this->diplomas_table__diplomas_codes__model->close();
            header('Content-Type: application/json');
            die( json_encode( $return ) );
        }
        else 
        {
            Template::set( 'toolbar_title', get_class( $this ) . ': ' . lang( 'series_and_numbers_action_create' ) );
            Template::render();
        }
    }
    public function help()
    {
        Template::set('toolbar_title', get_class( $this ) . ': ' . lang('series_and_numbers_help_heading'));
        Template::render();
    }
    public function report( $type='report.csv' )
    {
        //print_r( $_POST );
        if( ( isset( $_POST['code'] ) && !empty( $_POST['code'] ) ) &&
            ( 
                ( $records = $this->diplomas_view__diplomas_codes_2_graduate__model->select( 'Gratuate_ID, NrInregistrare, ID_Student, Diploma_Code_Value, Diploma_Code_Status, Diploma_Config_Id, Diploma_Config_Title, ID_AnUniv, Graduate_001_NrCrediteProgram, Graduate_002_SesiuneExamenAbsolvire, Graduate_003_AnExamenAbsolvire, Graduate_004_DenumireProba1, Graduate_005_NrCrediteProba1, Graduate_006_Nota1, Graduate_007_NotaInCifreSiLitere1, Graduate_008_DenumireProba2, Graduate_009_NrCrediteProba2, Graduate_010_Nota2, Graduate_011_NotaInCifreSiLitere2, Graduate_012_NumeIntreg, Graduate_013_DataNastere, Graduate_014_ZiNastere, Graduate_015_LunaNastere, Graduate_016_AnNastere, Graduate_017_LunaNastereInLitere, Graduate_018_MedieFinalaAbsolvire, Graduate_019_DenumireTaraNastere, Graduate_020_DenumireLocalitateNastere, Graduate_021_DenumireJudetNastere, Graduate_022_FormulaAdresare, Graduate_024_FormulaAdresareArticulat, Graduate_025_FormulaAdresareSufixF, Graduate_026_DenumireDomeniu, Graduate_027_DenumireSpecializare, Graduate_028_DenumireProgramDeStudii, Graduate_029_Universitate, Graduate_030_UniversitateArticulat, Graduate_031_Facultate, Graduate_032_FormaInvatamant, Graduate_033_CicluDeStudii, Graduate_034_LimbaPredare, Graduate_035_NumeRector, Graduate_036_NumeSecretarSefUniversitate, Graduate_037_NumeDecan, Graduate_038_NumeSecretarSefFacultate, Graduate_039_DurataStudiilor, Graduate_040_FacultateArticulat, Graduate_041_MedieFinalaAbsolvireCifre, Graduate_042_TitlulObtinut, Graduate_043_FormulaAdresareSufixM, Graduate_044_PrenumeTata, Graduate_045_PrenumeMama, Graduate_046_CNP, Field_001_Value, Field_002_Value, Field_003_Value, Field_004_Value, Field_005_Value, Field_006_Value, Field_007_Value, Field_008_Value, Field_009_Value, Field_010_Value, Field_011_Value, Field_012_Value, Field_013_Value, Field_014_Value, Field_015_Value, Field_016_Value, Field_017_Value, Field_018_Value, Field_019_Value, Field_020_Value, Field_021_Value, Field_022_Value, Field_023_Value, Field_024_Value, Field_025_Value, Field_026_Value, Field_027_Value, Field_028_Value, Field_029_Value, Field_030_Value' )->where_in( 'Diploma_Code_Value', $_POST['code']  )->order_by( $this->diplomas_view__diplomas_codes_2_graduate__model->table_name . ".Diploma_Code_Value" )->find_all() ) 
                && 
                !empty( $records ) 
            ) &&
            ( isset( $_POST['select'] ) && in_array( $_POST['select'], array( 'PDF', 'CSV' ) ) )
          ) 
        {
            //die( print_r( $records ) );
            $t  =  '';
            if( $_POST['select'] == 'PDF' )
            {				
				$s = explode( '-', $records[0]->Diploma_Code_Value );
                $t .=  '<html>' . PHP_EOL; 
                $t .=      '<body>' . PHP_EOL;
				$t .=      '<p style="margin-left: 1.5cm"> '.$records[0]->Graduate_031_Facultate. '</p>'. PHP_EOL;
				$t .=      '<p style="margin-left: 1.5cm"> '.$s[1]. '</p>'. PHP_EOL;
				$t .=      '<p style="margin-left: 1.5cm"> '.$records[0]->Graduate_002_SesiuneExamenAbsolvire. '</p>'. PHP_EOL;
                $t .=          '<table style="margin-left: 1.5cm; margin-right: 1.5cm;" width="100%" border="1" cellspacing="0" cellpadding="14">' . PHP_EOL;
                $t .=              '<thead>' . PHP_EOL;
                $t .=                  '<tr>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Nr. crt.</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Seria și nr. actului de studiu</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Numele, inițiala/inițialele, prenumele titularului</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Locul și data nașterii</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Prenumele părinților</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Anul absolvirii</td>' . PHP_EOL;
				$t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Sesiunea</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Instituția de învățământ la care s-a desfășurat examenul final</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Specializarea</td>' . PHP_EOL;
				$t .=                      '<td rowspan="2" style="font-size: 10px; text-align: center; font-weight: bold;">Forma de învățământ</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Media (nota) la examenul final</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Data eliberării actului de studii</td>' . PHP_EOL;
                $t .=                      '<td colspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Persoana care eliberează actul de studii</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Semnătura titularului/' .PHP_EOL.'împuternicitului</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Seria și nr. actului de identitate</td>' . PHP_EOL;
                $t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Mențiuni</td>' . PHP_EOL;
				$t .=                      '<td rowspan="2" style="font-size: 11px; text-align: center; font-weight: bold;">Nr. crt.</td>' . PHP_EOL;
                $t .=                  '</tr>' . PHP_EOL;
                $t .=                  '<tr>' . PHP_EOL;
                $t .=                      '<td style="font-size: 10px; text-align: center; font-weight: bold;">Numele și prenumele</td>' . PHP_EOL;
                $t .=                      '<td style="font-size: 10px; text-align: center; font-weight: bold;">Semnătura</td>' . PHP_EOL;
                $t .=                  '</tr>' . PHP_EOL;
                $t .=              '</thead>' . PHP_EOL;
                $t .=              '<tbody>' . PHP_EOL;
                foreach( $records as $r )
                {
                    $s = explode( '-', $r->Diploma_Code_Value );

                    $t .=              '<tr>' . PHP_EOL .				
                                                '<td style="font-size: 10px; text-align: center;">'.$s[4].'</td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;">'.$s[2].' '.$s[3].'</td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;">'.trim($r->Graduate_012_NumeIntreg).'</td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;">'.$r->Graduate_013_DataNastere.', '.$r->Graduate_020_DenumireLocalitateNastere.' '.$r->Graduate_021_DenumireJudetNastere.'</td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;">'.$r->Graduate_044_PrenumeTata.' '.$r->Graduate_045_PrenumeMama.'</td>' . PHP_EOL .
                                                '<td style="font-size: 10px; text-align: center;">'.trim($r->Graduate_003_AnExamenAbsolvire).'</td>' . PHP_EOL . 
						'<td style="font-size: 10px; text-align: center;">'.trim($r->Graduate_002_SesiuneExamenAbsolvire).'</td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;">'.trim($r->Graduate_029_Universitate).', '.trim($r->Graduate_031_Facultate).'</td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;">'.trim($r->Graduate_027_DenumireSpecializare).'</td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;">'.trim($r->Graduate_032_FormaInvatamant).'</td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;">'.trim($r->Graduate_018_MedieFinalaAbsolvire).'</td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;"></td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;"></td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;"></td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;"></td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;"></td>' . PHP_EOL . 
                                                '<td style="font-size: 10px; text-align: center;">'.$r->Graduate_046_CNP.'</td>' . PHP_EOL . 
						'<td style="font-size: 10px; text-align: center;">'.$s[4].'</td>' . PHP_EOL .
                                                //'<td style="font-size: 10px; text-align: center;">'.( $r->Diploma_Code_Status == 'cancel' ? 'anulată' : '' ).'</td>' . PHP_EOL . 
                                            '</tr>' . PHP_EOL;
                }
                $t .=              '</tbody>' . PHP_EOL;
                $t .=          '</table>' . PHP_EOL;
                $t .=      '</body>' . PHP_EOL;
                $t .=  '<html>' . PHP_EOL; 
                require_once 'vendor/autoload.php';
                $mpdf = new mPDF( 'utf-8', 'A3-L', 0, '', 10, 10, 10, 10, 10, 10 );
				$mpdf->SetHTMLFooter ('<p style="margin-left: 1.5cm">{PAGENO}</p>', 'O');
                $mpdf->SetDirectionality('ltr');
                $mpdf->useOnlyCoreFonts = true;
                $mpdf->use_kwt = true;
                $mpdf->allow_charset_conversion=true;
                $mpdf->charset_in='UTF-8';
                $mpdf->WriteHTML( $t );
                $mpdf->Output();
            } else {

                // die( print_r( $records ) );
                header( "Content-Type: text/csv" );
                header( "Content-Disposition: attachment; filename=file-".time().".csv" );
                $output = fopen( "php://output", "w" );
                fputcsv ( 
                            $output, 
                            array( 
                                    'Gratuate_ID', 
                                    'NrInregistrare', 
                                    'ID_Student', 
                                    'Diploma_Code_Value', 
                                    'Diploma_Code_Status', 
                                    'Diploma_Config_Id', 
                                    'Diploma_Config_Title', 
                                    'ID_AnUniv', 
                                    'Graduate_001_NrCrediteProgram', 
                                    'Graduate_002_SesiuneExamenAbsolvire', 
                                    'Graduate_003_AnExamenAbsolvire', 
                                    'Graduate_004_DenumireProba1', 
                                    'Graduate_005_NrCrediteProba1', 
                                    'Graduate_006_Nota1', 
                                    'Graduate_007_NotaInCifreSiLitere1', 
                                    'Graduate_008_DenumireProba2', 
                                    'Graduate_009_NrCrediteProba2', 
                                    'Graduate_010_Nota2', 
                                    'Graduate_011_NotaInCifreSiLitere2', 
                                    'Graduate_012_NumeIntreg', 
                                    'Graduate_013_DataNastere', 
                                    'Graduate_014_ZiNastere', 
                                    'Graduate_015_LunaNastere', 
                                    'Graduate_016_AnNastere', 
                                    'Graduate_017_LunaNastereInLitere', 
                                    'Graduate_018_MedieFinalaAbsolvire', 
                                    'Graduate_019_DenumireTaraNastere', 
                                    'Graduate_020_DenumireLocalitateNastere', 
                                    'Graduate_021_DenumireJudetNastere', 
                                    'Graduate_022_FormulaAdresare', 
                                    'Graduate_024_FormulaAdresareArticulat', 
                                    'Graduate_025_FormulaAdresareSufixF', 
                                    'Graduate_026_DenumireDomeniu', 
                                    'Graduate_027_DenumireSpecializare', 
                                    'Graduate_028_DenumireProgramDeStudii', 
                                    'Graduate_029_Universitate', 
                                    'Graduate_030_UniversitateArticulat', 
                                    'Graduate_031_Facultate', 
                                    'Graduate_032_FormaInvatamant', 
                                    'Graduate_033_CicluDeStudii', 
                                    'Graduate_034_LimbaPredare', 
                                    'Graduate_035_NumeRector', 
                                    'Graduate_036_NumeSecretarSefUniversitate', 
                                    'Graduate_037_NumeDecan', 
                                    'Graduate_038_NumeSecretarSefFacultate', 
                                    'Graduate_039_DurataStudiilor', 
                                    'Graduate_040_FacultateArticulat', 
                                    'Graduate_041_MedieFinalaAbsolvireCifre', 
                                    'Graduate_042_TitlulObtinut', 
									'Graduate_043_FormulaAdresareSufixM', 
									'Graduate_044_PrenumeTata', 
									'Graduate_045_PrenumeMama', 
									'Graduate_046_CNP', 
                                    'Field_001_Value', 
                                    'Field_002_Value', 
                                    'Field_003_Value', 
                                    'Field_004_Value', 
                                    'Field_005_Value', 
                                    'Field_006_Value', 
                                    'Field_007_Value', 
                                    'Field_008_Value', 
                                    'Field_009_Value', 
                                    'Field_010_Value', 
                                    'Field_011_Value', 
                                    'Field_012_Value', 
                                    'Field_013_Value', 
                                    'Field_014_Value', 
                                    'Field_015_Value', 
                                    'Field_016_Value', 
                                    'Field_017_Value', 
                                    'Field_018_Value', 
                                    'Field_019_Value', 
                                    'Field_020_Value', 
                                    'Field_021_Value', 
                                    'Field_022_Value', 
                                    'Field_023_Value', 
                                    'Field_024_Value', 
                                    'Field_025_Value', 
                                    'Field_026_Value', 
                                    'Field_027_Value', 
                                    'Field_028_Value', 
                                    'Field_029_Value', 
                                    'Field_030_Value' 
                                ), 
                                ';' 
                        ); 
                foreach( $records as $r )
                {
                    //die( print_r( $r ) );
                    $R =    array   ( 
                                        'Gratuate_ID' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Gratuate_ID ),
                                        'NrInregistrare' => $this->convert__Romanian_CI_AI__to__UTF8( $r->NrInregistrare ),
                                        'ID_Student' => $this->convert__Romanian_CI_AI__to__UTF8( $r->ID_Student ),
                                        'Diploma_Code_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Diploma_Code_Value ),
                                        'Diploma_Code_Status' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Diploma_Code_Status ),
                                        'Diploma_Config_Id' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Diploma_Config_Id ),
                                        'Diploma_Config_Title' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Diploma_Config_Title ),
                                        'ID_AnUniv' => $this->convert__Romanian_CI_AI__to__UTF8( $r->ID_AnUniv ),
                                        'Graduate_001_NrCrediteProgram' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_001_NrCrediteProgram ),
                                        'Graduate_002_SesiuneExamenAbsolvire' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_002_SesiuneExamenAbsolvire ),
                                        'Graduate_003_AnExamenAbsolvire' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_003_AnExamenAbsolvire ),
                                        'Graduate_004_DenumireProba1' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_004_DenumireProba1 ),
                                        'Graduate_005_NrCrediteProba1' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_005_NrCrediteProba1 ),
                                        'Graduate_006_Nota1' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_006_Nota1 ),
                                        'Graduate_007_NotaInCifreSiLitere1' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_007_NotaInCifreSiLitere1 ),
                                        'Graduate_008_DenumireProba2' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_008_DenumireProba2 ),
                                        'Graduate_009_NrCrediteProba2' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_009_NrCrediteProba2 ),
                                        'Graduate_010_Nota2' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_010_Nota2 ),
                                        'Graduate_011_NotaInCifreSiLitere2' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_011_NotaInCifreSiLitere2 ),
                                        'Graduate_012_NumeIntreg' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_012_NumeIntreg ),
                                        'Graduate_013_DataNastere' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_013_DataNastere ),
                                        'Graduate_014_ZiNastere' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_014_ZiNastere ),
                                        'Graduate_015_LunaNastere' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_015_LunaNastere ),
                                        'Graduate_016_AnNastere' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_017_LunaNastereInLitere ),
                                        'Graduate_017_LunaNastereInLitere' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_017_LunaNastereInLitere ),
                                        'Graduate_018_MedieFinalaAbsolvire' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_018_MedieFinalaAbsolvire ),
                                        'Graduate_019_DenumireTaraNastere' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_019_DenumireTaraNastere ),
                                        'Graduate_020_DenumireLocalitateNastere' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_020_DenumireLocalitateNastere ),
                                        'Graduate_021_DenumireJudetNastere' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_021_DenumireJudetNastere ),
                                        'Graduate_022_FormulaAdresare' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_022_FormulaAdresare ),
                                        'Graduate_024_FormulaAdresareArticulat' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_024_FormulaAdresareArticulat ),
                                        'Graduate_025_FormulaAdresareSufixF' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_025_FormulaAdresareSufixF ),
                                        'Graduate_026_DenumireDomeniu' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_026_DenumireDomeniu ),
                                        'Graduate_027_DenumireSpecializare' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_027_DenumireSpecializare ),
                                        'Graduate_028_DenumireProgramDeStudii' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_028_DenumireProgramDeStudii ),
                                        'Graduate_029_Universitate' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_029_Universitate ),
                                        'Graduate_030_UniversitateArticulat' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_030_UniversitateArticulat ),
                                        'Graduate_031_Facultate' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_031_Facultate ),
                                        'Graduate_032_FormaInvatamant' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_032_FormaInvatamant ),
                                        'Graduate_033_CicluDeStudii' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_033_CicluDeStudii ),
                                        'Graduate_034_LimbaPredare' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_034_LimbaPredare ),
                                        'Graduate_035_NumeRector' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_035_NumeRector ),
                                        'Graduate_036_NumeSecretarSefUniversitate' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_036_NumeSecretarSefUniversitate ),
                                        'Graduate_037_NumeDecan' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_037_NumeDecan ),
                                        'Graduate_038_NumeSecretarSefFacultate' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_038_NumeSecretarSefFacultate ),
                                        'Graduate_039_DurataStudiilor' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_039_DurataStudiilor ), 
                                        'Graduate_040_FacultateArticulat' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_040_FacultateArticulat ), 
                                        'Graduate_041_MedieFinalaAbsolvireCifre' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_041_MedieFinalaAbsolvireCifre ), 
                                        'Graduate_042_TitlulObtinut' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_042_TitlulObtinut ), 
										'Graduate_043_FormulaAdresareSufixM' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_043_FormulaAdresareSufixM ), 
										'Graduate_044_PrenumeTata' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_044_PrenumeTata ), 
										'Graduate_045_PrenumeMama' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_045_PrenumeMama ), 
										'Graduate_046_CNP' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Graduate_046_CNP ), 
                                        'Field_001_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_001_Value ),
                                        'Field_002_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_002_Value ),
                                        'Field_003_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_003_Value ),
                                        'Field_004_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_004_Value ),
                                        'Field_005_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_005_Value ),
                                        'Field_006_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_006_Value ),
                                        'Field_007_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_007_Value ),
                                        'Field_008_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_008_Value ),
                                        'Field_009_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_009_Value ),
                                        'Field_010_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_010_Value ),
                                        'Field_011_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_011_Value ),
                                        'Field_012_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_012_Value ),
                                        'Field_013_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_013_Value ),
                                        'Field_014_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_014_Value ),
                                        'Field_015_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_015_Value ),
                                        'Field_016_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_016_Value ),
                                        'Field_017_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_017_Value ),
                                        'Field_018_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_018_Value ),
                                        'Field_019_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_019_Value ),
                                        'Field_020_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_020_Value ),
                                        'Field_021_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_021_Value ),
                                        'Field_022_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_022_Value ),
                                        'Field_023_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_023_Value ),
                                        'Field_024_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_024_Value ),
                                        'Field_025_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_025_Value ),
                                        'Field_026_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_026_Value ),
                                        'Field_027_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_027_Value ),
                                        'Field_028_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_028_Value ),
                                        'Field_029_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_029_Value ),
                                        'Field_030_Value' => $this->convert__Romanian_CI_AI__to__UTF8( $r->Field_030_Value ),
                                    );
                    fputcsv( $output, $R, ';' ); 
                }
                fclose( $output );
            }
            $this->diplomas_view__diplomas_codes_2_graduate__model->close();
        }
        die();
    }
}