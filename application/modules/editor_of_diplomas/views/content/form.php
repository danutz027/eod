<h3>
    <?php   
        echo    (   $title =    (   isset( $DIPLOMA_CONFIG ) && isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && $DIPLOMA_CONFIG['Diploma_Config_Id']
                                    ?   lang( 'editor_of_diplomas_edit_heading' ) //. ' [ ' . $DIPLOMA_CONFIG['Diploma_Config_Id'] . ' ]'
                                    :   lang( 'editor_of_diplomas_create_new' ) ) ); 
    ?>
</h3>
<hr />
<?php if ( $this->auth->has_permission('Editor_of_diplomas.Content.Create') ) : ?>
    <div id="div-alert" class='alert alert-error' style="display: none;">
        <a class='close' name="link-alert" id="link-alert"></a>
        <h4 class='alert-heading'></h4>
    </div>

    <div class='admin-box'>
        <form id="form-config" action="#" onsubmit="javascript: return false;" class="form-horizontal" method="post" accept-charset="utf-8">
            <table id="table-config" class="table table-responsive table-striped" style="width: 100%; margin: 0 auto;" border='0'>
                <thead>
                    <tr>
                        <th class="text-right" style="width: 15% !important;" colspan='2'>
                            <a name="link-config" id="link-config"></a>
                            <img id="img-ajax" class="pull-right" src="<?php echo base_url ( 'themes/admin/images/ajax-loader-small.gif' ); ?>" style="width: 16px;  height: 16px; border: none; display: none;" />
                            <i id="icon-form" class="icon icon-cog pull-right" style="display: block;"></i>  
                        </th>
                        <th class="text-left" style="width: 65% !important;" colspan='2'>
                            <b>
                                <?php echo lang( 'editor_of_diplomas_form_config_title' ); ?>
                            </b>  
                        </th>
                        <th class="text-left" style="width: 20% !important;">
                            <span class='help-inline Diploma_Config_Table'></span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php   if ( isset( $DIPLOMA_CONFIG ) && isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && $DIPLOMA_CONFIG['Diploma_Config_Id']  ) : ?>
                    <tr>
                        <td class="text-right">
                            <i>a</i>
                        </td>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_id' ), 'Diploma_Config_Id', array( 'class' => 'control-label', 'style' => 'float: right !important;' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <input id='Diploma_Config_Id' type='text' required='required' name='Diploma_Config_Id' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Id', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Id'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Id'] : '' ) ); ?>" class="span8" readonly='readonly' />
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Id'></span>
                        </td>
                    </tr>
                    <?php   endif; ?>
                    <tr>
                        <td class="text-right">
                            <i>b</i>
                        </td>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_title' ), 'Diploma_Config_Title', array( 'class' => 'control-label', 'style' => 'float: right !important;' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <input id='Diploma_Config_Title' type='text' required='required' name='Diploma_Config_Title' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Title', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Title'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Title'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Title'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Title'] : '' ) ); ?>" class="span8" />
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Title'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <i>c</i>
                        </td>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_type' ), 'Diploma_Config_Type', array( 'class' => 'control-label', 'style' => 'float: right !important;' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <select id='Diploma_Config_Type' required='required' name='Diploma_Config_Type' class="span8">
                                <option></option>
                                <?php if ( isset( $diplomas_design_type ) && !empty( $diplomas_design_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_type as $dt => $DT ) : ?>

                                            <option value="<?php echo $DT; ?>" <?php echo set_select( 'Diploma_Config_Type', ( $Diploma_Config_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Type'] : '' ) ), ( $DT == $Diploma_Config_Type ? true : false ) ); ?>>
                                                <?php echo $DT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Type'></span>
                        </td>
                    </tr> 
                    <tr>
                        <td class="text-right">
                            <i>d</i>
                        </td>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_form' ), 'Diploma_Config_Form', array( 'class' => 'control-label', 'style' => 'float: right !important;' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <select id='Diploma_Config_Form' required='required' name='Diploma_Config_Form' class="span8">
                                <option></option>
                                <?php if ( isset( $diplomas_design_form ) && !empty( $diplomas_design_form ) ) : ?>
                                    <?php foreach ( $diplomas_design_form as $df => $DF ) : ?>

                                            <option value="<?php echo $DF; ?>" <?php echo set_select( 'Diploma_Config_Form', ( $Diploma_Config_Form = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Form'] ) && $DIPLOMA_CONFIG['Diploma_Config_Form'] ? $DIPLOMA_CONFIG['Diploma_Config_Form'] : '' ) ), ( $DF == $Diploma_Config_Form ? true : false ) ); ?>>
                                                <?php echo $DF; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Form'></span>
                        </td>
                    </tr> 
                
                    <tr>
                        <td class="text-right" colspan='2'>
                            <i class="icon icon-picture pull-right"></i>  
                        </td>
                        <td class="text-left" colspan='3'>
                            <p>
                                <b><?php echo lang( 'editor_of_diplomas_form_config_label_images' ); ?>:</b>
                                <?php echo lang( 'editor_of_diplomas_form_config_label_images__max_size_MB' ); ?>: <b><?php e( $diploma_img_max_size_in_MB ); ?></b> MB,
                                <?php echo lang( 'editor_of_diplomas_form_config_label_images__max_width_PX' ); ?>: <b><?php e( $diploma_img_max_width___PX ); ?></b> PX,
                                <?php echo lang( 'editor_of_diplomas_form_config_label_images__max_height_PX' ); ?>: <b><?php e( $diploma_img_max_height__PX ); ?></b> PX,
                                <?php echo lang( 'editor_of_diplomas_form_config_label_images__resolution_DPI' ); ?>: <b><?php e( $diploma_img_resolution_DPI ); ?></b> DPI;
                            </p>
                        </td>
                    </tr>

                    <tr>
                        <td class="text-right">
                            <i>e</i>
                        </td>
                        <td class="text-right">
                            <?php   $Diploma_Config_Image_Front = set_value( 'Diploma_Config_Image_Front', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] : '' ) );
                                    echo form_label( ( $Diploma_Config_Image_Front ? '' : lang( 'bf_form_label_required' ) ) . lang( 'editor_of_diplomas_form_config_field_image_front' ), 'Diploma_Config_Image_Front', array( 'class' => 'control-label', 'style' => 'float: right !important;' ) ); ?>
                        </td>
                        <td class='controls'>
                            <input id='Diploma_Config_Image_Front' type='file' <?php echo ( $Diploma_Config_Image_Front ? '' : "required='required'" ); ?> name='Diploma_Config_Image_Front' value="" class="span8" />
                        </td>
                        <td>
                            <?php if( $Diploma_Config_Image_Front ) : ?>
                                <a href='<?php echo base_url( $diploma_img_upload_folder_image . $Diploma_Config_Image_Front ); ?>' target='_blank'>
                                    <img src='<?php echo base_url( $diploma_img_upload_folder_thumb . $Diploma_Config_Image_Front ); ?>' border='0' />
                                </a>
                            <?php endif; ?>
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Image_Front'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <i>f</i>
                        </td>
                        <td class="text-right">
                            <?php   $Diploma_Config_Image_Verso = set_value( 'Diploma_Config_Image_Verso', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] : '' ) );
                                    echo form_label( ( $Diploma_Config_Image_Verso ? '' : lang( 'bf_form_label_required' ) ) . lang( 'editor_of_diplomas_form_config_field_image_verso' ), 'Diploma_Config_Image_Verso', array( 'class' => 'control-label', 'style' => 'float: right !important;' ) ); ?>
                        </td>
                        <td class='controls'>
                            <input id='Diploma_Config_Image_Verso' type='file' name='Diploma_Config_Image_Verso' value="" class="span8" />
                        </td>
                        <td>
                            <?php if( $Diploma_Config_Image_Verso ) : ?>
                                <a href='<?php echo base_url( $diploma_img_upload_folder_image . $Diploma_Config_Image_Verso ); ?>' target='_blank'>
                                    <img src='<?php echo base_url( $diploma_img_upload_folder_thumb . $Diploma_Config_Image_Verso ); ?>' border='0' />
                                </a>
                            <?php endif; ?>
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Image_Verso'></span>
                        </td>
                    </tr>
                    <?php if ( isset( $diplomas_design_field_default ) && !empty( $diplomas_design_field_default ) ) : ?>
                        <tr>
                            <td class="text-right" colspan='2'>
                                <i class="icon icon-list pull-right"></i>  
                            </td>
                            <td class="text-left" colspan='3'>
                                <p><b><?php echo lang( 'editor_of_diplomas_form_config_field_default' ); ?></b></p>
                            </td>
                        </tr>
                        <?php foreach ( $diplomas_design_field_default as $ddfd => $DDFD ) : ?>
                            <tr>
                                <td class="text-right small"><small><i><?php echo $ddfd; ?></i></small></td>
                                <td class='controls text-center'><input type='text' name="Diploma_Config_Field_Default_Type[<?php echo $ddfd; ?>]" maxlength='255' value="<?php echo $DDFD['type']; ?>" class="span2" readonly disabled /></td>
                                <td class='controls' colspan='3'><input type='text' name="Diploma_Config_Field_Default_Text[<?php echo $ddfd; ?>]" maxlength='255' value="<?php echo $DDFD['text']; ?>" class="span8" readonly disabled /></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <tr>
                        <td class="text-right" colspan='2'>
                            <i class="icon icon-list pull-right"></i>  
                        </td>
                        <td class="text-left" colspan='3'>
                            <p><b><?php echo lang( 'editor_of_diplomas_form_config_field_more' ); ?></b></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-001</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_001_Type' id="Diploma_Config_Field_001_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_001_Type', ( $Diploma_Config_Field_001_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_001_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_001_Text' type='text' name='Diploma_Config_Field_001_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_001_Text', ( $Diploma_Config_Field_001_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_001'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-002</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_002_Type' id="Diploma_Config_Field_002_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_002_Type', ( $Diploma_Config_Field_002_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_002_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_002_Text' type='text' name='Diploma_Config_Field_002_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_002_Text', ( $Diploma_Config_Field_002_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_002'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-003</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_003_Type' id="Diploma_Config_Field_003_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_003_Type', ( $Diploma_Config_Field_003_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_003_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_003_Text' type='text' name='Diploma_Config_Field_003_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_003_Text', ( $Diploma_Config_Field_003_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_003'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-004</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_004_Type' id="Diploma_Config_Field_004_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_004_Type', ( $Diploma_Config_Field_004_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_004_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_004_Text' type='text' name='Diploma_Config_Field_004_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_004_Text', ( $Diploma_Config_Field_004_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_004'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-005</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_005_Type' id="Diploma_Config_Field_005_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_005_Type', ( $Diploma_Config_Field_005_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_005_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_005_Text' type='text' name='Diploma_Config_Field_005_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_005_Text', ( $Diploma_Config_Field_005_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_005'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-006</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_006_Type' id="Diploma_Config_Field_006_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_006_Type', ( $Diploma_Config_Field_006_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_006_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_006_Text' type='text' name='Diploma_Config_Field_006_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_006_Text', ( $Diploma_Config_Field_006_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_006'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-007</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_007_Type' id="Diploma_Config_Field_007_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_007_Type', ( $Diploma_Config_Field_007_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_007_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_007_Text' type='text' name='Diploma_Config_Field_007_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_007_Text', ( $Diploma_Config_Field_007_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_007'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-008</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_008_Type' id="Diploma_Config_Field_008_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_008_Type', ( $Diploma_Config_Field_008_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_008_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_008_Text' type='text' name='Diploma_Config_Field_008_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_008_Text', ( $Diploma_Config_Field_008_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_008'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-009</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_009_Type' id="Diploma_Config_Field_009_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_009_Type', ( $Diploma_Config_Field_009_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_009_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_009_Text' type='text' name='Diploma_Config_Field_009_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_009_Text', ( $Diploma_Config_Field_009_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_009'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-010</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_010_Type' id="Diploma_Config_Field_010_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_010_Type', ( $Diploma_Config_Field_010_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_010_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_010_Text' type='text' name='Diploma_Config_Field_010_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_010_Text', ( $Diploma_Config_Field_010_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_010'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-011</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_011_Type' id="Diploma_Config_Field_011_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_011_Type', ( $Diploma_Config_Field_011_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_011_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_011_Text' type='text' name='Diploma_Config_Field_011_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_011_Text', ( $Diploma_Config_Field_011_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_011'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-012</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_012_Type' id="Diploma_Config_Field_012_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_012_Type', ( $Diploma_Config_Field_012_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_012_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_012_Text' type='text' name='Diploma_Config_Field_012_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_012_Text', ( $Diploma_Config_Field_012_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_012'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-013</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_013_Type' id="Diploma_Config_Field_013_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_013_Type', ( $Diploma_Config_Field_013_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_013_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_013_Text' type='text' name='Diploma_Config_Field_013_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_013_Text', ( $Diploma_Config_Field_013_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_013'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-014</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_014_Type' id="Diploma_Config_Field_014_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_014_Type', ( $Diploma_Config_Field_014_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_014_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_014_Text' type='text' name='Diploma_Config_Field_014_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_014_Text', ( $Diploma_Config_Field_014_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_014'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-015</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_015_Type' id="Diploma_Config_Field_015_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_015_Type', ( $Diploma_Config_Field_015_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_015_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_015_Text' type='text' name='Diploma_Config_Field_015_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_015_Text', ( $Diploma_Config_Field_015_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_015'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-016</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_016_Type' id="Diploma_Config_Field_016_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_016_Type', ( $Diploma_Config_Field_016_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_016_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_016_Text' type='text' name='Diploma_Config_Field_016_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_016_Text', ( $Diploma_Config_Field_016_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_016'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-017</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_017_Type' id="Diploma_Config_Field_017_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_017_Type', ( $Diploma_Config_Field_017_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_017_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_017_Text' type='text' name='Diploma_Config_Field_017_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_017_Text', ( $Diploma_Config_Field_017_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_017'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-018</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_018_Type' id="Diploma_Config_Field_018_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_018_Type', ( $Diploma_Config_Field_018_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_018_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_018_Text' type='text' name='Diploma_Config_Field_018_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_018_Text', ( $Diploma_Config_Field_018_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_018'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-019</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_019_Type' id="Diploma_Config_Field_019_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_019_Type', ( $Diploma_Config_Field_019_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_019_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_019_Text' type='text' name='Diploma_Config_Field_019_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_019_Text', ( $Diploma_Config_Field_019_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_019'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-020</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_020_Type' id="Diploma_Config_Field_020_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_020_Type', ( $Diploma_Config_Field_020_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_020_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_020_Text' type='text' name='Diploma_Config_Field_020_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_020_Text', ( $Diploma_Config_Field_020_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_020'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-021</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_021_Type' id="Diploma_Config_Field_021_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_021_Type', ( $Diploma_Config_Field_021_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_021_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_021_Text' type='text' name='Diploma_Config_Field_021_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_021_Text', ( $Diploma_Config_Field_021_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_021'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-022</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_022_Type' id="Diploma_Config_Field_022_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_022_Type', ( $Diploma_Config_Field_022_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_022_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_022_Text' type='text' name='Diploma_Config_Field_022_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_022_Text', ( $Diploma_Config_Field_022_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_022'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-023</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_023_Type' id="Diploma_Config_Field_023_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_023_Type', ( $Diploma_Config_Field_023_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_023_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_023_Text' type='text' name='Diploma_Config_Field_023_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_023_Text', ( $Diploma_Config_Field_023_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_023'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-024</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_024_Type' id="Diploma_Config_Field_024_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_024_Type', ( $Diploma_Config_Field_024_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_024_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_024_Text' type='text' name='Diploma_Config_Field_024_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_024_Text', ( $Diploma_Config_Field_024_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_024'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-025</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_025_Type' id="Diploma_Config_Field_025_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_025_Type', ( $Diploma_Config_Field_025_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_025_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_025_Text' type='text' name='Diploma_Config_Field_025_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_025_Text', ( $Diploma_Config_Field_025_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_025'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-026</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_026_Type' id="Diploma_Config_Field_026_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_026_Type', ( $Diploma_Config_Field_026_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_026_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_026_Text' type='text' name='Diploma_Config_Field_026_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_026_Text', ( $Diploma_Config_Field_026_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_026'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-027</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_027_Type' id="Diploma_Config_Field_027_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_027_Type', ( $Diploma_Config_Field_027_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_027_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_027_Text' type='text' name='Diploma_Config_Field_027_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_027_Text', ( $Diploma_Config_Field_027_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_027'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-028</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_028_Type' id="Diploma_Config_Field_028_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_028_Type', ( $Diploma_Config_Field_028_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_028_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_028_Text' type='text' name='Diploma_Config_Field_028_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_028_Text', ( $Diploma_Config_Field_028_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_028'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-029</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_029_Type' id="Diploma_Config_Field_029_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_029_Type', ( $Diploma_Config_Field_029_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_029_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_029_Text' type='text' name='Diploma_Config_Field_029_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_029_Text', ( $Diploma_Config_Field_029_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_029'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-030</i></small></td>
                        <td class='controls text-center'>
                            <select role="field-type" name='Diploma_Config_Field_030_Type' id="Diploma_Config_Field_030_Type" class="form-control span2">
                                <option></option>
                                <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                            <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Config_Field_030_Type', ( $Diploma_Config_Field_030_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] : '' ) ), ( $DDFT == $Diploma_Config_Field_030_Type ? true : false ) ); ?>>
                                                <?php echo $DDFT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td class='controls' colspan='2'>
                            <input role="field-text" id='Diploma_Config_Field_030_Text' type='text' name='Diploma_Config_Field_030_Text' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Field_030_Text', ( $Diploma_Config_Field_030_Text = ( ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'], $diplomas_design_field_type ) ) && ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] : '' ) ) ); ?>" class="span8" <?php echo ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] ) && in_array( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'], $diplomas_design_field_type ) ? '' : 'readonly' ); ?> />
                        </td>
                        <td>
                            <span role="field-help" class='help-inline Diploma_Config_Field_030'></span>
                        </td>
                    </tr>
                    
                </tbody>
                <tfoot>
                    <tr>
                        <td style="background-color: #CCC; border-top: 1px solid #444;" colspan='2'></td>
                        <td style="background-color: #CCC; border-top: 1px solid #444;" colspan='2'>
                            <button name='button-config' id='button-config' type='button' class='btn btn-primary pull-left'>
                                <span class='icon icon-ok-circle icon-white'></span>&nbsp;<?php echo lang( 'editor_of_diplomas_form_config_button_submit' ); ?>
                            </button>
                        </td>
                        <td style="background-color: #CCC; border-top: 1px solid #444;">
                            <a href='<?php echo site_url( 'admin/content/editor_of_diplomas/index' ); ?>' class='btn btn-warning pull-right' onclick="javascript: if( confirm( '<?php echo $title . ' [ ' . lang( 'editor_of_diplomas_form_config_button_cancel' ) . ' ]'; ?>' ) ) { return true; } else { return false; }">
                                <span class='icon icon-refresh icon-white'></span>&nbsp;<?php echo lang( 'editor_of_diplomas_form_config_button_cancel' ); ?>
                            </a>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>
    </div>
    <script type="text/javascript">
        function in_array( needle, haystack ) 
        {
            var length = haystack.length;
            for( var i = 0; i < length; i++ ) 
            {
                if( typeof haystack[i] == 'object' ) 
                {
                    if( array_compare( haystack[i], needle ) ) return true;
                } 
                else 
                {
                    if( haystack[i] == needle ) return true;
                }
            }
            return false;
        }
        var Diplomas_Design_Type =   
            [

                <?php if ( isset( $diplomas_design_type ) && !empty( $diplomas_design_type ) ) : ?>
                    <?php foreach ( $diplomas_design_type as $dt => $DT ) : ?>

                        '<?php echo $DT; ?>', 

                    <?php endforeach; ?>
                <?php endif; ?>

            ];
        var Diplomas_Design_Form =   
            [

                <?php if ( isset( $diplomas_design_form ) && !empty( $diplomas_design_form ) ) : ?>
                    <?php foreach ( $diplomas_design_form as $df => $DF ) : ?>

                        '<?php echo $DF; ?>', 

                    <?php endforeach; ?>
                <?php endif; ?>

            ];
        function is_image( file ) 
        {
            if( file )
            {   var sufix_file = ( ( file.toLowerCase() ).split( '.' ) ).pop();
                var image_type = [ 'jpg', 'jpeg', 'png', 'tif', 'gif', 'bmp' ];   
                return in_array( sufix_file, image_type );
            } else 
                return false;
        }
        
        if( window.jQuery )
        {
            function scroll_to( obj ) 
            {
                if( typeof $( obj ) != 'undefined'  )
                {
                    $('html, body').animate( { 'scrollTop': $( obj ).offset().top }, 2000 );
                }
            }

            $(document).ready(function(){

                if  ( 
                        ( typeof $( 'div#div-alert' ) != 'undefined' ) && 
                        ( typeof $( 'form#form-config' ) != 'undefined' ) && 
                        ( typeof $( 'a#link-config' ) != 'undefined' ) && 
                        ( typeof $( 'a#link-alert' ) != 'undefined' ) && 
                        ( typeof $( 'img#img-ajax' ) != 'undefined' ) && 
                        ( typeof $( 'i#icon-form' ) != 'undefined' ) && 
                        ( typeof $( 'button#button-config' ) != 'undefined' ) && 
                        ( typeof $( 'span.help-inline.Diploma_Config_Table' ) != 'undefined' ) &&
                        ( typeof $( 'input#Diploma_Config_Title' ) != 'undefined' ) &&
                        ( typeof $( 'span.help-inline.Diploma_Config_Title' ) != 'undefined' ) &&
                        ( typeof $( 'select#Diploma_Config_Type' ) != 'undefined' ) &&
                        ( typeof $( 'span.help-inline.Diploma_Config_Type' ) != 'undefined' ) &&
                        ( typeof $( 'select#Diploma_Config_Form' ) != 'undefined' ) &&
                        ( typeof $( 'span.help-inline.Diploma_Config_Form' ) != 'undefined' ) &&
                        ( typeof $( 'input#Diploma_Config_Image_Front' ) != 'undefined' ) &&
                        ( typeof $( 'span.help-inline.Diploma_Config_Image_Front' ) != 'undefined' ) &&
                        ( typeof $( 'input#Diploma_Config_Image_Verso' ) != 'undefined' ) &&
                        ( typeof $( 'span.help-inline.Diploma_Config_Image_Verso' ) != 'undefined' ) &&
                        ( typeof $( 'select[role="field-type"]' ) != 'undefined' ) &&
                        ( typeof $( 'input[role="field-text"]' ) != 'undefined' ) &&
                        ( typeof $( 'span[role="field-help"]' ) != 'undefined' )
                    )
                {
                    var $Div_Alert = $( 'div#div-alert' ); 
                    var $Img__Ajax = $( 'img#img-ajax' ); $Img__Ajax.hide();
                    var $Icon_Form = $( 'i#icon-form' ); $Icon_Form.show();
                    var $Form_Config = $( 'form#form-config' ); 
                    var $Link_Config = $( 'a#link-config' );
                    var $Link_Alert = $( 'a#link-alert' );
                    var $Button_Config = $( 'button#button-config' ); 
                    var $Diploma_Config_Title = $( 'input#Diploma_Config_Title' ); 
                    var $Diploma_Config_Type = $( 'select#Diploma_Config_Type' ); 
                    var $Diploma_Config_Form = $( 'select#Diploma_Config_Form' ); 
                    var $Diploma_Config_Image_Front = $( 'input#Diploma_Config_Image_Front' ); 
                    var $Diploma_Config_Image_Verso = $( 'input#Diploma_Config_Image_Verso' ); 
                    $Div_Alert.find('h4.alert-heading').empty().html('').parent().hide();
                    $Img__Ajax.hide();
                    $( 'select[role="field-type"]' ).change( function() {
                        var $select_type = $( this );
                        var $input__text = $select_type.closest( 'tr' ).find( 'input[role="field-text"]' );
                        var $span___help = $select_type.closest( 'tr' ).find( 'span[role="field-help"]' );
                        if( $select_type.val() )
                        {
                            $input__text.prop( 'readonly', false ).focus();
                        } else {
                            $input__text.prop( 'readonly', false ).attr( 'value', '' ).val( '' ).prop( 'readonly', true );
                        }
                        $span___help.empty().html( '' );
                    });
                    $( 'input[role="field-text"]' ).focusout( function() {
                        var $input__text = $( this );
                        var $select_type = $input__text.closest( 'tr' ).find( 'select[role="field-type"]' );
                        var $span___help = $input__text.closest( 'tr' ).find( 'span[role="field-help"]' );
                        if( $select_type.val() )
                        {
                            if( $input__text.val() )
                            {
                                $span___help.empty().html( '' );
                                
                            } else {
                                
                                $span___help.empty().html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                            }
                        } else {
                            $input__text.prop( 'readonly', false ).attr( 'value', '' ).val( '' ).prop( 'readonly', true );
                            $span___help.empty().html( '' );
                        }
                    });
                    $Button_Config.click( function( e ) {
                        //$( 'span.help-inline' ).empty().html('');
                        var valid                       = true;
                        var Diploma_Config_Title        = $.trim( $Diploma_Config_Title.val() );
                        var Diploma_Config_Type         = $.trim( $Diploma_Config_Type.val() );
                        var Diploma_Config_Form         = $.trim( $Diploma_Config_Form.val() );
                        var Diploma_Config_Image_Front  = $Diploma_Config_Image_Front.val();
                        var Diploma_Config_Image_Verso  = $Diploma_Config_Image_Verso.val();
                        if( !Diploma_Config_Title )
                        {   valid = false;
                            $( 'span.help-inline.Diploma_Config_Title' ).empty().html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                        } else {
                            $( 'span.help-inline.Diploma_Config_Title' ).empty().html( '' );
                        }
                        if( !Diploma_Config_Type )
                        {
                            valid = false;
                            $( 'span.help-inline.Diploma_Config_Type' ).empty().html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                        } else {
                            if( in_array( Diploma_Config_Type, Diplomas_Design_Type ) )
                            {   
                                $( 'span.help-inline.Diploma_Config_Type' ).empty().html( '' );
                            } else {
                                valid = false;
                                $( 'span.help-inline.Diploma_Config_Type' ).empty().html( '<?php echo lang( 'bf_error_invalid' ); ?>' );
                            }
                        }
                        if( !Diploma_Config_Form )
                        {
                            valid = false;
                            $( 'span.help-inline.Diploma_Config_Form' ).empty().html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                        } else {
                            if( in_array( Diploma_Config_Form, Diplomas_Design_Form ) )
                            {   
                                $( 'span.help-inline.Diploma_Config_Form' ).empty().html( '' );
                            } else {
                                valid = false;
                                $( 'span.help-inline.Diploma_Config_Form' ).empty().html( '<?php echo lang( 'bf_error_invalid' ); ?>' );
                            }
                        }
                        if( $Diploma_Config_Image_Front.prop( 'required' ) && ( Diploma_Config_Image_Front == '' ) )
                        {   valid = false;
                            $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                        } else {
                            if( ( Diploma_Config_Image_Front != '' ) && 
                                is_image( Diploma_Config_Image_Front ) && 
                                ( ( $Diploma_Config_Image_Front )[0].files.length == 1 ) ) 
                            {   
                                $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '' );
                            } else {
                                if( $Diploma_Config_Image_Front.prop( 'required' ) )
                                {   valid = false;
                                    $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '<?php echo lang( 'bf_error_invalid' ); ?>' );
                                } else {
                                    $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '' );
                                }
                            }
                        }
                        if( Diploma_Config_Image_Verso )
                        {
                            if( is_image( Diploma_Config_Image_Verso ) && ( ( $Diploma_Config_Image_Verso )[0].files.length == 1 ) )
                            {   
                                $( 'span.help-inline.Diploma_Config_Image_Verso' ).empty().html( '' );
                            } else {
                                valid = false;
                                $( 'span.help-inline.Diploma_Config_Image_Verso' ).empty().html( '<?php echo lang( 'bf_error_invalid' ); ?>' );
                            }
                        }
                        $( 'select[role="field-type"]' ).each(function() {

                            var $select_type = $( this );
                            var $input__text = $select_type.closest( 'tr' ).find( 'input[role="field-text"]' );
                            var $span___help = $select_type.closest( 'tr' ).find( 'span[role="field-help"]' );

                            if( $select_type.val() )
                            {
                                if( $input__text.val() )
                                {
                                    $span___help.empty().html( '' );
                                } else {
                                    valid = false;
                                    $span___help.empty().html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                                }
                            } else {
                                $input__text.prop( 'readonly', false ).attr( 'value', '' ).val( '' ).prop( 'readonly', true );
                                $span___help.empty().html( '' );
                            }
                        });
                        if( valid == true )
                        {
                            $( 'span.help-inline.Diploma_Config_Table' ).empty().html( '' );
                            var $ConfigDATA = new FormData( $Form_Config[0] );
                            var $Config_URL = '<?php echo site_url( 'admin/content/editor_of_diplomas/form' ); ?>';
                            $Icon_Form.fadeOut();
                            $Img__Ajax.fadeIn();
                            $.ajax( 
                                    {
                                        url:            $Config_URL,
                                        enctype:        'multipart/form-data',
                                        type:           'POST',
                                        data:           $ConfigDATA,
                                        contentType:    false,
                                        cache:          false,
                                        processData:    false,
                                        success:        function( ID )
                                                        {
                                                            ID = parseInt( ID );
                                                            $Img__Ajax.fadeOut();
                                                            $Icon_Form.fadeIn();
                                                            if( ID != 'NaN' )
                                                            {
                                                                $Div_Alert
                                                                    .removeAttr('class').addClass('alert').addClass('alert-success')
                                                                    .find('h4.alert-heading').empty().html('<?php echo lang( 'bf_ajax_success' ); ?>')
                                                                    .parent().fadeIn();
                                                                $( 'html, body' ).animate( { scrollTop: $Link_Alert.offset().top }, 1000 );
                                                                setTimeout( function(){
                                                                    $( location ).attr( 'href', '<?php echo site_url( 'admin/content/editor_of_diplomas/config' ); ?>/' + ID );
                                                                }, 3000 );
                                                            } else {
                                                                $Div_Alert
                                                                    .removeAttr('class').addClass('alert').addClass('alert-warning')
                                                                    .find('h4.alert-heading').empty().html('<?php echo lang( 'bf_ajax_error' ); ?>')
                                                                    .parent().fadeIn();
                                                                $( 'html, body' ).animate( { scrollTop: $Link_Alert.offset().top }, 1000 );
                                                            
                                                            }
                                                        },
                                        error:          function( err )
                                                        {
                                                            $Div_Alert
                                                                .removeAttr('class').addClass('alert').addClass('alert-error')
                                                                .find('h4.alert-heading').empty().html('<?php echo lang( 'bf_ajax_error' ); ?>')
                                                                .parent().fadeIn();
                                                            setTimeout( function(){
                                                                scroll_to( 'link-alert' );
                                                            }, 2000 );
                                                        }
                                    } 
                                );
                            
                        } else {
                            $( 'span.help-inline.Diploma_Config_Table' ).empty().html( '<?php echo lang( 'bf_errors' ); ?>' );
                            $( 'html, body' ).animate( { scrollTop: $Link_Config.offset().top }, 1000 );
                        }
                    });
                }
            });
        } 
        else 
        {
            if( typeof ( document.getElementById( 'body_blank' ) ) != 'undefined' )
            {
                document.getElementById( 'desktop' ).innerHTML = "<center><h5 style='color:red;'><?php echo lang( 'bf_ajax_warning' ); ?></h5></center>";
            }
            alert( '<?php echo lang( 'bf_ajax_warning' ); ?>' );
        }
    </script>
<?php endif; ?>
