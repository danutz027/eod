<?php //prototipDEFAULT
    $STRINGS    =   isset( $prototipSTRINGS )   && $prototipSTRINGS     ? $prototipSTRINGS : 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium totam rem aperiam eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam quis nostrum exercitationem ullam corporis suscipit laboriosam nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.';
    $NUMBERS    =   isset( $prototipNUMBERS )   && $prototipNUMBERS     ? $prototipNUMBERS : '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567';
    $DATETIME   =   isset( $prototipDATETIME )  && $prototipDATETIME    ? $prototipDATETIME : time();
    $DEFAULT    =   isset( $prototipDEFAULT )  && !empty( $prototipDEFAULT )    
                    ?   $prototipDEFAULT 
                    :   array(
                                'Diploma_Field_Type'            => 'alfanumeric-text',
                                'Diploma_Field_Coord_Y'         => 0,
                                'Diploma_Field_Coord_X'         => 0,
                                'Diploma_Field_Area_Width'      => 100,
                                'Diploma_Field_Area_Height'     => 14,
                                'Diploma_Field_Font_Size'       => 12,
                                'Diploma_Field_Font_Family'     => 'Arial',
                                'Diploma_Field_Font_Style'      => 'normal',
                                'Diploma_Field_Font_Weight'     => 'normal',
                                'Diploma_Field_Line_Height'     => 14,
                                'Diploma_Field_Text_Transform'  => 'none',
                                'Diploma_Field_Text_Align'      => 'left',
                                'Diploma_Field_Indent_Width'    => 0,
                                'Diploma_Field_Format_Prefix'   => 11,
                                'Diploma_Field_Format_Sufix'    => 0,
                                'Diploma_Field_Format_DateTime' => $DATETIME,
                                'Diploma_Field_Status'          => 'live',
                            );
    $PICTURE    =   ( isset( $Diploma_Config_Face ) && in_array( $Diploma_Config_Face, array( 'front', 'verso' ) ) 
                    ?   (   (
                                ( 
                                    ( $Diploma_Config_Face == 'front' ) && 
                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) && $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] )
                                )
                                ?   $DIPLOMA_CONFIG['Diploma_Config_Image_Front']
                                :   (   ( 
                                            ( $Diploma_Config_Face == 'verso' ) && 
                                            ( isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ) && $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] )
                                        )
                                    ?   $DIPLOMA_CONFIG['Diploma_Config_Image_Verso']
                                    :   '' ) ) )
                    :   ''  );
    $Diploma_Field_MyType   = 'alfanumeric-text';
    $Diploma_Field_MyText   = 'Lorem ipsum';
    $Diploma_Field_Format_IntPrefix = 0;
    $Diploma_Field_Format_Int_Sufix = 0;
    $Diploma_Field_Format_Date_Time = '';
    if  (
            ( isset( $DIPLOMA_CONFIG_FIELD ) && !empty( $DIPLOMA_CONFIG_FIELD ) ) && 
            ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) 
        )
    {
        if  (     
                ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) &&  in_array( ( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) ), $diplomas_design_field_type ) ) 
                &&
                ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Format_Value'] ) &&  ( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Format_Value'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Format_Value'] ) ) ) 
            )
        {
            $Diploma_Field_MyType = $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'];
            switch( $Diploma_Field_MyType ) 
            {
                case 'alfanumeric-text':
                    $Diploma_Field_Format_IntPrefix = intval( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Format_Value'] );
                    $Diploma_Field_MyText = substr ( $STRINGS, 0, $Diploma_Field_Format_IntPrefix );
                    break;
                case 'alfanumeric-html':
                    $Diploma_Field_Format_IntPrefix = intval( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Format_Value'] );
                    $Diploma_Field_MyText = substr ( $STRINGS, 0, $Diploma_Field_Format_IntPrefix );
                    break;
                case 'numeric-integer':
                    $Diploma_Field_Format_IntPrefix = intval( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Format_Value'] );
                    $Diploma_Field_MyText = substr ( $NUMBERS, 0, $Diploma_Field_Format_IntPrefix );
                    break;
                case 'numeric-decimal':
                    $Diploma_Field_Format_Array     =   explode( ',', $DIPLOMA_CONFIG_FIELD['Diploma_Field_Format_Value'] );
                    $Diploma_Field_Format_IntPrefix =   ( isset( $Diploma_Field_Format_Array[0] ) && is_int( ( $Diploma_Field_Format_Array[0] = intval( $Diploma_Field_Format_Array[0] ) ) )
                                                        ? $Diploma_Field_Format_Array[0]
                                                        : 0 );
                    $Diploma_Field_Format_Int_Sufix =   ( isset( $Diploma_Field_Format_Array[1] ) && is_int( ( $Diploma_Field_Format_Array[1] = intval( $Diploma_Field_Format_Array[1] ) ) )
                                                        ? $Diploma_Field_Format_Array[1]
                                                        : 0 );
                    $Diploma_Field_MyText           =   substr ( $NUMBERS, 0, $Diploma_Field_Format_IntPrefix ) . '.' . substr ( $NUMBERS, 0, $Diploma_Field_Format_Int_Sufix );
                    break;
                case 'datetime':
                    $Diploma_Field_Format_Date_Time = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Format_Value'] );
                    break;
                default:
                    echo substr ( $STRINGS, 0, $Diploma_Field_Format_Prefix );
            }
        } else {
            $DIPLOMA_CONFIG_FIELD = array();
        }
    } else {
        $DIPLOMA_CONFIG_FIELD = array();
    }
?>
<?php if( $PICTURE ) : ?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">

        <style type="text/css">
            .ruler 
            {
                background: rgba(255,255,255,1);
                color: #444;
                font-family: source code pro, "Arial Narrow", "Helvetica Neue", Helvetica, Arial, Veranda, sans-serif;
                font-size: 12px;
                line-height: 14px;
                overflow: hidden;
            }
            .ruler > div 
            {
                background: #444;
            }
            .hRule 
            {
                position: fixed;
                width: 100%;
                height: 18px;
                left: 0px;
                top: 0px;
                border-bottom: 1px solid;
                z-index: 9;
            }
            .vRule {
                position: absolute;

                min-height: 100%;
                width: 18px;
                left: 0px;
                top: 0px;

                border-right: 1px solid;
                z-index: 9;

            }
            .corner {
                position: fixed;
                top: 0px;
                left: 0px;
                width: 18px;
                height: 18px;
                border-right: 1px solid;
                border-bottom: 1px solid;
                z-index: 10;
            }
            .hRule .tickLabel {
                position: absolute;
                top: 0px;
                width: 1px;
                height: 100%;
                text-indent: 1px;
            }
            .hRule .tickMajor {
                position: absolute;
                bottom: 0px;
                width: 1px;
                height: 6px;
            }
            .hRule .tickMinor {
                position: absolute;
                bottom: 0px;
                width: 1px;
                height: 4px;
            }
            .vRule .tickLabel {
                position: absolute;
                right: 0px;
                height: 1px;
                width: 100%;
                text-indent: 1px;
            }
            .vRule .tickLabel span {
                display: block;
                position: absolute;

                top: 1px;
                right: 0px;
                margin-right: 18px;

                -webkit-transform: rotate(-90deg);
              -moz-transform: rotate(-90deg);
              -ms-transform: rotate(-90deg);
              -o-transform: rotate(-90deg);
              transform: rotate(-90deg);

                -webkit-transform-origin: top right;
              -moz-transform-origin: top right;
              -ms-transform-origin: top right;
              -o-transform-origin: top right;
              transform-origin: top right;

            }
            .vRule .tickMajor {
                position: absolute;
                right: 0px;
                height: 1px;
                width: 6px;
            }
            .vRule .tickMinor {
                position: absolute;
                right: 0px;
                height: 1px;
                width: 4px;
            }
            .vMouse {
                display: block;
                position: fixed;
                width: 100%;
                height: 0px;

                left: 0;
                background: rgba(0,0,0,0);
                border-bottom: 1px dotted;
                z-index: 11;
            }
            .hMouse {
                display: block;
                position: fixed;
                height: 100%;
                width: 0px;
                top: 0;

                background: rgba(0,0,0,0);
                border-left: 1px dotted;
                z-index: 11;
            }
            .mousePosBox {
                height: 16px;
                background: rgba(0,0,0,0.25);
                color: #fff;
                font-family: source code pro;
                font-size: 12px;
                line-height: 16px;
                border: 1px solid rgba(0,0,0,0.5);
                position: fixed;
                left: -50%;
                top: -50%;
                padding: 0 2px;
            }
        </style>
        <style type="text/css">
            body {
                padding: 0;
                overflow: hidden;
            }
            #wrapper {
                min-height: 100%;
                height: 100%;
                width: 100%;
                position: absolute;
                top: 0px;
                left: 0;
                display: inline-block;
            }
            #main-wrapper {
                height: 100%;
                overflow-y: auto;
                overflow-x: auto;
                padding: 19px 0 0 19px !important;
            }
            #main {
                position: relative;
                height: 100%;
                overflow-y: auto;
                padding: 0;
            }
            #table {
                top: 2em;
                right: 1em;
                height: auto;
                width: 20em;
                padding: 0;
                position: fixed;
                border: 1px solid gray !important;
            }
            #table.table-striped>tbody>tr>td,
            #table.table-striped>tfoot>tr>td{
                font-size: 12px !important;
                padding: 4px !important;
            }
            #table>tbody>tr>td>select, 
            #table>tbody>tr>td>input { 
                height: 24px !important;
                font-size: 12px !important;
                width: 12em !important;
            }
            #table.table-striped>tbody>tr:nth-child(even)>td,
            #table.table-striped>tfoot>tr:last-child>td
            {
                background-color: #ffffff;
            }
            #table.table-striped>thead>tr>th,
            #table.table-striped>tfoot>tr:first-child>td
            {
                background-color: #cccccc;
                color: #ffffff;
            }
            table[table='format'] tr td { 
                border-top: none !important;
                border-left: none !important;
            }

            @media (max-width: 992px) {
                body {
                    padding-top: 0px;
                }
            }
            @media (min-width: 992px) {
                #main-wrapper {
                    float:left;
                }
                #sidebar-wrapper {
                    float:right;
                }
            }
            @media (max-width: 992px) {
                #main-wrapper {
                    padding-top: 0px;
                }
            }
            @media (max-width: 992px) {
                #sidebar-wrapper {
                    position: static;
                    height:auto;
                    max-height: 300px;
                    border-right:0;
                }
            }
        </style>
        <style>
            #box {
                margin: 0;
                margin-top: 0;
                width: 100px;
                height: 10px;
                background-color: #54cfb3;
                padding: 0;
            }

            .opac {
                opacity: 0.5;
            }

            .move-cursor {
                cursor: move;
            }

            .grab-cursor {
                cursor: grab;
                cursor: -webkit-grab;
            }
        </style>
        <script>
            /*
             * Date Format 1.2.3
             * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
             * MIT license
             *
             * Includes enhancements by Scott Trenda <scott.trenda.net>
             * and Kris Kowal <cixar.com/~kris.kowal/>
             *
             * Accepts a date, a mask, or a date and a mask.
             * Returns a formatted version of the given date.
             * The date defaults to the current date/time.
             * The mask defaults to dateFormat.masks.default.
             */

            var dateFormat = function () {
                    var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
                            timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
                            timezoneClip = /[^-+\dA-Z]/g,
                            pad = function (val, len) {
                                    val = String(val);
                                    len = len || 2;
                                    while (val.length < len) val = "0" + val;
                                    return val;
                            };

                    // Regexes and supporting functions are cached through closure
                    return function (date, mask, utc) {
                            var dF = dateFormat;

                            // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
                            if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
                                    mask = date;
                                    date = undefined;
                            }

                            // Passing date through Date applies Date.parse, if necessary
                            date = date ? new Date(date) : new Date;
                            if (isNaN(date)) throw SyntaxError("invalid date");

                            mask = String(dF.masks[mask] || mask || dF.masks["default"]);

                            // Allow setting the utc argument via the mask
                            if (mask.slice(0, 4) == "UTC:") {
                                    mask = mask.slice(4);
                                    utc = true;
                            }

                            var	_ = utc ? "getUTC" : "get",
                                    d = date[_ + "Date"](),
                                    D = date[_ + "Day"](),
                                    m = date[_ + "Month"](),
                                    y = date[_ + "FullYear"](),
                                    H = date[_ + "Hours"](),
                                    M = date[_ + "Minutes"](),
                                    s = date[_ + "Seconds"](),
                                    L = date[_ + "Milliseconds"](),
                                    o = utc ? 0 : date.getTimezoneOffset(),
                                    flags = {
                                            d:    d,
                                            dd:   pad(d),
                                            ddd:  dF.i18n.dayNames[D],
                                            dddd: dF.i18n.dayNames[D + 7],
                                            m:    m + 1,
                                            mm:   pad(m + 1),
                                            mmm:  dF.i18n.monthNames[m],
                                            mmmm: dF.i18n.monthNames[m + 12],
                                            yy:   String(y).slice(2),
                                            yyyy: y,
                                            h:    H % 12 || 12,
                                            hh:   pad(H % 12 || 12),
                                            H:    H,
                                            HH:   pad(H),
                                            M:    M,
                                            MM:   pad(M),
                                            s:    s,
                                            ss:   pad(s),
                                            l:    pad(L, 3),
                                            L:    pad(L > 99 ? Math.round(L / 10) : L),
                                            t:    H < 12 ? "a"  : "p",
                                            tt:   H < 12 ? "am" : "pm",
                                            T:    H < 12 ? "A"  : "P",
                                            TT:   H < 12 ? "AM" : "PM",
                                            Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                                            o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                                            S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
                                    };

                            return mask.replace(token, function ($0) {
                                    return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
                            });
                    };
            }();

            // Some common format strings
            dateFormat.masks = {
                    "default":      "ddd mmm dd yyyy HH:MM:ss",
                    shortDate:      "m/d/yy",
                    mediumDate:     "mmm d, yyyy",
                    longDate:       "mmmm d, yyyy",
                    fullDate:       "dddd, mmmm d, yyyy",
                    shortTime:      "h:MM TT",
                    mediumTime:     "h:MM:ss TT",
                    longTime:       "h:MM:ss TT Z",
                    isoDate:        "yyyy-mm-dd",
                    isoTime:        "HH:MM:ss",
                    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
                    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
            };

            // Internationalization strings
            dateFormat.i18n = {
                    dayNames: [
                            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
                            "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
                    ],
                    monthNames: [
                            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
                            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
                    ]
            };

            // For convenience...
            Date.prototype.format = function (mask, utc) {
                    return dateFormat(this, mask, utc);
            };
        </script>
        
        <script src="<?php echo base_url( 'themes/admin/js/jquery-1.7.2.min.js' ); ?>"></script>
        <script src="<?php echo base_url( 'themes/admin/js/jquery-ui-1.8.13.min.js' ); ?>"></script>
        <script src="<?php echo base_url( 'themes/admin/js/modernizr-2.6.2.min.js' ); ?>"></script>
        <script src="<?php echo base_url( 'themes/admin/js/jquery.ruler.js' ); ?>"></script>
        <link rel="stylesheet" href="<?php echo base_url( 'themes/admin/css/jquery-ui/jquery-ui.min.css' ); ?>" />
        <link rel="stylesheet" href="<?php echo base_url( 'themes/admin/css/bootstrap.min.css' ); ?>" />
        <link rel="stylesheet" href="<?php echo base_url( 'themes/admin/css/bootstrap-responsive.min.css' ); ?>" />
        
    </head>
    <body id="ruler" style="background: #fff; width: 98% !important; margin: 0 auto; padding: 0; min-height: 1200px; height: auto !important;">
        
        <div id="wrapper" style="background: #fff; width: 100% !important; margin: 0; min-height: 1200px; height: auto !important;">
            <div id="main-wrapper" style="background: #fff; width: 100% !important; margin: 0; min-height: 1200px; height: auto !important;">
                <div id="main" style="background-color: transparent !important; background-image: url( '<?php echo base_url( $diploma_img_upload_folder_image . $PICTURE ); ?>' ) !important; background-repeat: no-repeat; background-position-x: 0; background-position-y: 0; width: 100% !important; margin: 0; min-height: 1200px; height: auto !important;">
                    <div id="box" style="position: relative; 
                                        top: <?php echo ( $Diploma_Field_Coord_Y = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_Y'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Coord_Y'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_Y'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_Y'] : $DEFAULT['Diploma_Field_Coord_Y'] ) ); ?>px; 
                                        left: <?php echo ( $Diploma_Field_Coord_X = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_X'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Coord_X'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_X'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_X'] : $DEFAULT['Diploma_Field_Coord_X'] ) ); ?>px; 
                                        width: <?php echo ( $Diploma_Field_Area_Width = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Width'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Area_Width'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Width'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Width'] : $DEFAULT['Diploma_Field_Area_Width'] ) ); ?>px; 
                                        height: <?php echo ( $Diploma_Field_Area_Height = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Height'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Area_Height'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Height'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Height'] : $DEFAULT['Diploma_Field_Area_Height'] ) ); ?>px; 
                                        font-family: <?php echo ( $Diploma_Field_Font_Family = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Font_Family'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] : $DEFAULT['Diploma_Field_Font_Family'] ) ); ?>; 
                                        font-size: <?php echo ( $Diploma_Field_Font_Size = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Size'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Font_Size'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Size'] ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Size'] : $DEFAULT['Diploma_Field_Font_Size'] ) ); ?>; 
                                        font-style: <?php echo ( $Diploma_Field_Font_Style = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Style'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Font_Style'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Style'] ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Style'] : $DEFAULT['Diploma_Field_Font_Style'] ) ); ?>; 
                                        font-weight: <?php echo ( $Diploma_Field_Font_Weight = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Font_Weight'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] : $DEFAULT['Diploma_Field_Font_Weight'] ) ); ?>; 
                                        line-height: <?php echo ( $Diploma_Field_Line_Height = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Line_Height'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Line_Height'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Line_Height'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Line_Height'] : $DEFAULT['Diploma_Field_Line_Height'] ) ); ?>px;
                                        text-transform: <?php echo ( $Diploma_Field_Text_Transform = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Text_Transform'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] : $DEFAULT['Diploma_Field_Text_Transform'] ) ); ?>;
                                        text-align: <?php echo ( $Diploma_Field_Text_Align = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Text_Align'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] : $DEFAULT['Diploma_Field_Text_Align'] ) ); ?>;">
                        <span class="indent" style="background-color: yellow !important; display: inline-block !important; float: left; border: none; height: <?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Line_Height'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Indent_Width'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Line_Height'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Line_Height'] : $DEFAULT['Diploma_Field_Line_Height'] ); ?>px; width: <?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Indent_Width'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Indent_Width'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Indent_Width'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Indent_Width'] : $DEFAULT['Diploma_Field_Indent_Width'] ); ?>px;"></span>
                        <span class="mytext"><?php echo $Diploma_Field_MyText; ?></span>
                    </div>
                </div>
            </div>

            <form action="#" method="post" class="form-horizontal">
                <input type="hidden" id="Diploma_Config_Id"     value="<?php echo set_value( 'Diploma_Config_Id', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Id'] = intval( trim( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Id'] : '' ) ); ?>" />
                <input type="hidden" id="Diploma_Config_Face"   value="<?php echo set_value( 'Diploma_Config_Face', ( isset( $Diploma_Config_Face ) ? $Diploma_Config_Face : 'front' ) ); ?>" />
                <input type="hidden" id="Diploma_Field_Id"      value="<?php echo set_value( 'Diploma_Field_Id', ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Id'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Id'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Id'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Id'] : '' ) ); ?>" />
                <table id="table" class="table table-responsive table-striped" border="0">
                    <thead>
                        <tr>
                            <th class="text-right" style='width: 30%; padding: 0.5em; font-weight: bold; cursor: move; cursor: grab; cursor: -moz-grab; cursor: -webkit-grab; font-size: small;'>
                                <small>
                                    Side&nbsp;Bar:
                                </small>
                            </th>
                            <th class="text-left" style='width: 55%; padding: 0.5em; font-weight: bold; cursor: move; cursor: grab; cursor: -moz-grab; cursor: -webkit-grab; font-size: small;'>
                                <small>
                                    Field&nbsp;Attributes
                                </small>
                            </th>
                            <th class="text-left" style='width: 15%; padding: 0.5em; font-weight: normal; cursor: move; cursor: grab; cursor: -moz-grab; cursor: -webkit-grab; font-size: smaller;'></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-right">
                                <small>
                                    Field-Name:
                                </small>
                            </td>
                            <td class="text-left controls" colspan="2">
                                <select id="Diploma_Field_Name" class="form-control">
                                <?php if ( isset( $diplomas_design_field_default ) && !empty( $diplomas_design_field_default ) ) : ?>
                                    <optgroup label="<?php echo lang( 'editor_of_diplomas_form_config_field_default' ); ?>">
                                    <?php foreach ( $diplomas_design_field_default as $ddfd => $DDFD ) : ?>

                                            <option value="<?php echo $ddfd; ?>" title="<?php echo $DDFD['type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $ddfd == $Diploma_Field_Name ? true : false ) ); ?>>
                                                <?php echo $DDFD['text']; ?>
                                            </option>

                                    <?php endforeach; ?>
                                    </optgroup>
                                <?php endif; ?>
                                <?php if ( 1 > 0 ) : ?>
                                    <optgroup label="<?php echo lang( 'editor_of_diplomas_form_config_field_more' ); ?>">
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_001_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_001_Value' ? true : false ) ); ?>>
                                                [001] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_002_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_002_Value' ? true : false ) ); ?>>
                                                [002] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_003_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_003_Value' ? true : false ) ); ?>>
                                                [003] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_004_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_004_Value' ? true : false ) ); ?>>
                                                [004] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_005_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_005_Value' ? true : false ) ); ?>>
                                                [005] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_006_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_006_Value' ? true : false ) ); ?>>
                                                [006] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_007_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_007_Value' ? true : false ) ); ?>>
                                                [007] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_008_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_008_Value' ? true : false ) ); ?>>
                                                [008] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_009_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_009_Value' ? true : false ) ); ?>>
                                                [009] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_010_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_0101=_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_010_Value' ? true : false ) ); ?>>
                                                [010] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_011_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_011_Value' ? true : false ) ); ?>>
                                                [011] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_012_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_012_Value' ? true : false ) ); ?>>
                                                [012] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_013_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_013_Value' ? true : false ) ); ?>>
                                                [013] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_014_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_014_Value' ? true : false ) ); ?>>
                                                [014] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_015_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_015_Value' ? true : false ) ); ?>>
                                                [015] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_016_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_016_Value' ? true : false ) ); ?>>
                                                [016] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_017_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_017_Value' ? true : false ) ); ?>>
                                                [017] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_018_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_018_Value' ? true : false ) ); ?>>
                                                [018] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_019_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_019_Value' ? true : false ) ); ?>>
                                                [019] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_020_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_020_Value' ? true : false ) ); ?>>
                                                [020] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text']; ?>
                                            </option>
                                        <?php endif; ?>

                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_021_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_021_Value' ? true : false ) ); ?>>
                                                [021] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_022_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_022_Value' ? true : false ) ); ?>>
                                                [022] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_023_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_023_Value' ? true : false ) ); ?>>
                                                [023] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_024_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_024_Value' ? true : false ) ); ?>>
                                                [024] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_025_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_025_Value' ? true : false ) ); ?>>
                                                [025] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_026_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_026_Value' ? true : false ) ); ?>>
                                                [026] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_027_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_027_Value' ? true : false ) ); ?>>
                                                [027] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_028_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_028_Value' ? true : false ) ); ?>>
                                                [028] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_029_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_029_Value' ? true : false ) ); ?>>
                                                [029] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                        <?php if ( 
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] ) ) ) 
                                                ) : ?>
                                            <option value="Field_030_Value" title="<?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type']; ?>" <?php echo set_select( 'Diploma_Field_Name', ( $Diploma_Field_Name = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Name'] : '' ) ), ( $Diploma_Field_Name == 'Field_030_Value' ? true : false ) ); ?>>
                                                [030] <?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text']; ?>
                                            </option>
                                        <?php endif; ?>
                                    </optgroup>
                                <?php endif; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Field-Type:
                                </small>
                            </td>
                            <td class="text-left controls" colspan="2">
                                <select id="Diploma_Field_Type" class="form-control">
                                    <?php if ( isset( $diplomas_design_field_type ) && !empty( $diplomas_design_field_type ) ) : ?>
                                        <?php foreach ( $diplomas_design_field_type as $ddft => $DDFT ) : ?>

                                                <option value="<?php echo $DDFT; ?>" <?php echo set_select( 'Diploma_Field_Type', ( $Diploma_Field_Type = ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) && $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] : '' ) ), ( $DDFT == $Diploma_Field_Type ? true : false ) ); ?>>
                                                    <?php echo $DDFT; ?>
                                                </option>

                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <option value="alfanumeric-text"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Type'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) ) == 'alfanumeric-text' ) ? ' selected' : '' ); ?>>alfanumeric-text</option>
                                        <option value="alfanumeric-html"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Type'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) ) == 'alfanumeric-html' ) ? ' selected' : '' ); ?>>alfanumeric-html</option>
                                        <option value="numeric-integer"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Type'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) ) == 'numeric-integer' ) ? ' selected' : '' ); ?>>numeric-integer</option>
                                        <option value="numeric-decimal"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Type'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) ) == 'numeric-decimal' ) ? ' selected' : '' ); ?>>numeric-decimal</option>
                                        <option value="datetime"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Type'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Type'] ) ) == 'datetime' ) ? ' selected' : '' ); ?>>datetime</option>
                                    <?php endif; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Position-Top:
                                </small>
                            </td>
                            <td class="text-left controls">
                                <input id="Diploma_Field_Coord_Y" type="number" min="0" max="1200" step="1" value="<?php echo set_value( 'Diploma_Field_Coord_Y', ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_Y'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Coord_Y'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_Y'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_Y'] : $DEFAULT['Diploma_Field_Coord_Y'] ) ); ?>" class="form-control text-right" />
                            </td>
                            <td class="text-center small"><small>px</small></td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Position-Left:
                                </small>
                            </td>
                            <td class="text-left controls">
                                <input id="Diploma_Field_Coord_X" type="number" min="0" max="1000" step="1" value="<?php echo set_value( 'Diploma_Field_Coord_X', ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_X'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Coord_X'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_X'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Coord_X'] : $DEFAULT['Diploma_Field_Coord_X'] ) ); ?>" class="form-control text-right" />
                            </td>
                            <td class="text-center small"><small>px</small></td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Area-Width:
                                </small>
                            </td>
                            <td class="text-left controls">
                                <input id="Diploma_Field_Area_Width" type="number" min="0" max="1000" step="1" value="<?php echo set_value( 'Diploma_Field_Area_Width', ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Width'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Area_Width'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Width'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Width'] : $DEFAULT['Diploma_Field_Area_Width'] ) ); ?>" class="form-control text-right" />
                            </td>
                            <td class="text-center small"><small>px</small></td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Area-Height:
                                </small>
                            </td>
                            <td class="text-left controls">
                                <input id="Diploma_Field_Area_Height" type="number" min="0" max="1000" step="1" value="<?php echo set_value( 'Diploma_Field_Area_Height', ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Height'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Area_Height'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Height'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Area_Height'] : $DEFAULT['Diploma_Field_Area_Height'] ) ); ?>" class="form-control text-right" />
                            </td>
                            <td class="text-center small"><small>px</small></td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Font-Family:
                                </small>
                            </td>
                            <td class="text-left controls" colspan="2">
                                <select id="Diploma_Field_Font_Family" class="form-control">
                                    <option value="Arial"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Family'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) ) == 'Arial' ) ? ' selected' : '' ); ?>>Arial</option>
                                    <option value="Courier"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Family'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) ) == 'Courier' ) ? ' selected' : '' ); ?>>Courier</option>
                                    <option value="Tahoma"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Family'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) ) == 'Tahoma' ) ? ' selected' : '' ); ?>>Tahoma</option>
                                    <option value="Times"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Family'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) ) == 'Times' ) ? ' selected' : '' ); ?>>Times</option>
                                    <option value="Verdana"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Family'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Family'] ) ) == 'Verdana' ) ? ' selected' : '' ); ?>>Verdana</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Font-Size:
                                </small>
                            </td>
                            <td class="text-left controls">
                                <input id="Diploma_Field_Font_Size" type="number" min="0" max="100" step="0.01" value="<?php echo set_value( 'Diploma_Field_Font_Size', ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Size'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Font_Size'] = number_format( floatval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Size'] ) ), 2 ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Size'] : $DEFAULT['Diploma_Field_Font_Size'] ) ); ?>" class="form-control text-right" />
                            </td>
                            <td class="text-center small"><small>px</small></td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Font-Style:
                                </small>
                            </td>
                            <td class="text-left controls" colspan="2">
                                <select id="Diploma_Field_Font_Style" class="form-control">
                                    <option value="normal"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Style'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Style'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Style'] ) ) == 'normal' ) ? ' selected' : '' ); ?>>normal</option>
                                    <option value="italic"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Style'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Style'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Style'] ) ) == 'italic' ) ? ' selected' : '' ); ?>>italic</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Font-Weight:
                                </small>
                            </td>
                            <td class="text-left controls" colspan="2">
                                <select id="Diploma_Field_Font_Weight" class="form-control">
                                    <option value="normal"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Weight'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] ) ) == 'normal' ) ? ' selected' : '' ); ?>>normal</option>
                                    <option value="bold"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Weight'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] ) ) == 'bold' ) ? ' selected' : '' ); ?>>bold</option>
                                    <option value="bolder"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Weight'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] ) ) == 'bolder' ) ? ' selected' : '' ); ?>>bolder</option>
                                    <option value="lighter"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Font_Weight'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Font_Weight'] ) ) == 'lighter' ) ? ' selected' : '' ); ?>>lighter</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Line-Height:
                                </small>
                            </td>
                            <td class="text-left controls">
                                <input id="Diploma_Field_Line_Height" type="number" min="0" max="200" step="1" value="<?php echo set_value( 'Diploma_Field_Line_Height', ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Line_Height'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Line_Height'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Line_Height'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Line_Height'] : $DEFAULT['Diploma_Field_Line_Height']) ); ?>" class="form-control text-right" />
                            </td>
                            <td class="text-center small"><small>px</small></td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Text-Transform:
                                </small>
                            </td>
                            <td class="text-left controls" colspan="2">
                                <select id="Diploma_Field_Text_Transform" class="form-control">
                                    <option value="none"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Text_Transform'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] ) ) == 'none' ) ? ' selected' : '' ); ?>>none</option>
                                    <option value="capitalize"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Text_Transform'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] ) ) == 'capitalize' ) ? ' selected' : '' ); ?>>capitalize</option>
                                    <option value="uppercase"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Text_Transform'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] ) ) == 'uppercase' ) ? ' selected' : '' ); ?>>uppercase</option>
                                    <option value="lowercase"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Text_Transform'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Transform'] ) ) == 'lowercase' ) ? ' selected' : '' ); ?>>lowercase</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Text-Align:
                                </small>
                            </td>
                            <td class="text-left controls" colspan="2">
                                <select id="Diploma_Field_Text_Align" class="form-control">
                                    <option value="left"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Text_Align'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] ) ) == 'left' ) ? ' selected' : '' ); ?>>left</option>
                                    <option value="center"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Text_Align'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] ) ) == 'center' ) ? ' selected' : '' ); ?>>center</option>
                                    <option value="right"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Text_Align'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] ) ) == 'right' ) ? ' selected' : '' ); ?>>right</option>
                                    <option value="justify"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Text_Align'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Text_Align'] ) ) == 'justify' ) ? ' selected' : '' ); ?>>justify</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                    Ident-Width:
                                </small>
                            </td>
                            <td class="text-left controls">
                                <input id="Diploma_Field_Indent_Width" type="number" min="0" max="1000" step="1" value="<?php echo set_value( 'Diploma_Field_Indent_Width', ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Indent_Width'] ) && ( $DIPLOMA_CONFIG['Diploma_Field_Indent_Width'] = intval( trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Indent_Width'] ) ) ) ? $DIPLOMA_CONFIG_FIELD['Diploma_Field_Indent_Width'] : $DEFAULT['Diploma_Field_Indent_Width'] ) ); ?>" class="form-control text-right" />
                            </td>
                            <td class="text-center small"><small>px</small></td>
                        </tr>
                        <tr>
                            <td class="text-right small">
                                <small>
                                   Format-Value:
                                </small>
                            </td>
                            <td class="text-left controls small" colspan="2">
                                <table table="format" class="table-responsive" style="width: 12em !important;">
                                    <thead>
                                        <tr format="prefix" style="display: <?php echo ( $format_prefix = ( isset( $Diploma_Field_MyType ) && in_array( $Diploma_Field_MyType, array( 'alfanumeric-text', 'alfanumeric-html', 'numeric-integer', 'numeric-decimal' ) ) ? '' : 'none' ) ); ?>;">
                                            <td class="text-right small">
                                                <span style="font-size: 0.5em !important;">
                                                    Prefix:
                                                </span>
                                            </td>
                                            <td class="text-left controls">
                                                <input style="width: 7em !important;" id="Diploma_Field_Format_Prefix" type="number" min="0" max="1000" step="1" value="<?php echo set_value( 'Diploma_Field_Format_Prefix', ( isset( $Diploma_Field_Format_IntPrefix ) && ( $Diploma_Field_Format_IntPrefix = intval( trim( $Diploma_Field_Format_IntPrefix ) ) ) ? $Diploma_Field_Format_IntPrefix : $DEFAULT['Diploma_Field_Format_Prefix'] ) ); ?>" class="form-control text-right" <?php echo ( $format_prefix == 'none' ? 'readonly' : '' ); ?> />
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr format="sufix" style="display: <?php echo ( $format_sufix = ( ( isset( $Diploma_Field_MyType ) && ( $Diploma_Field_MyType == 'numeric-decimal' ) ? '' : 'none' ) ) ); ?>;">
                                            <td class="text-right small">
                                                <span style="font-size: 0.5em !important;">
                                                   Sufix:
                                                </span>
                                            </td>
                                            <td class="text-left controls">
                                                <input style="width: 7em !important;" id="Diploma_Field_Format_Sufix" type="number" min="0" max="1000" step="1" value="<?php echo set_value( 'Diploma_Field_Format_Sufix', ( isset( $Diploma_Field_Format_Int_Sufix ) && ( $Diploma_Field_Format_Int_Sufix = intval( trim( $Diploma_Field_Format_Int_Sufix ) ) ) ? $Diploma_Field_Format_Int_Sufix : 0 ) ); ?>" class="form-control text-right" <?php echo ( $format_sufix == 'none' ? 'readonly' : '' ); ?> />
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr format="datetime" style="display: <?php echo ( $format_datetime = ( ( isset( $Diploma_Field_MyType ) && ( $Diploma_Field_MyType == 'datetime' ) ? '' : 'none' ) ) ); ?>;">
                                            <td class="text-right small">
                                                <span style="font-size: 0.5em !important;">
                                                   DateTime:
                                                </span>
                                            </td>
                                            <td class="text-left controls">
                                                <select style="width: 7em !important;" id="Diploma_Field_Format_DateTime" class="form-control">
                                                    <?php if ( $format_datetime != 'none' ) : ?>
                                                    <option value="Y-m-d"<?php echo ( isset( $Diploma_Field_Format_Date_Time ) && ( ( $Diploma_Field_Format_Date_Time = trim( $Diploma_Field_Format_Date_Time ) ) == 'Y-m-d' ) ? ' selected' : '' ); ?>>Y-m-d</option>
                                                    <option value="Y-m-d H:i:s"<?php echo ( isset( $Diploma_Field_Format_Date_Time ) && ( ( $Diploma_Field_Format_Date_Time = trim( $Diploma_Field_Format_Date_Time ) ) == 'Y-m-d H:i:s' ) ? ' selected' : '' ); ?>>Y-m-d H:i:s</option>
                                                    <?php endif; ?>
                                                </select>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right small">
                                <small>
                                    Field-Status:
                                </small>
                            </td>
                            <td class="text-left controls" colspan="2">
                                <select id="Diploma_Field_Status" class="form-control">
                                    <option value="live"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Status'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Status'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Status'] ) ) == 'live' ) ? ' selected' : '' ); ?>>live</option>
                                    <option value="draft"<?php echo ( isset( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Status'] ) && ( ( $DIPLOMA_CONFIG['Diploma_Field_Status'] = trim( $DIPLOMA_CONFIG_FIELD['Diploma_Field_Status'] ) ) == 'draft' ) ? ' selected' : '' ); ?>>draft</option>
                                </select>
                            </td>
                        </tr>
                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <td class="text-right">
                                <img id="img" src="<?php echo base_url ( 'themes/admin/images/ajax-loader-small.gif' ); ?>" style="width: 16px;  height: 16px; border: none; display: none; float: right;" />
                                <small>
                                    Control:
                                </small>
                            </td>
                            <td class="text-left controls" colspan="2">
                                <button class="btn btn-primary" type="button" id="button">
                                    <i class="icon icon-white icon-check"></i> Save Field Attributes
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <small>
                                    Legenda:
                                </small>
                            </td>
                            <td class="text-left small" colspan="2">
                                <ul>
                                    <li>
                                        <small>
                                            1 mm = 3.779528 px
                                        </small>
                                    </li>
                                    <li>
                                        <small>
                                            1 inch = 72px
                                        </small>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </form>
        </div>
        <script>
            $( function() {
                var STRINGS = '<?php e( $STRINGS ); ?>';
                var NUMBERS = '<?php e( $NUMBERS ); ?>';
                var $RUL    = $( "#ruler" );
                var $TAB    = $('#table');
                var $MSG    = $('#table thead tr th:last-child');
                var $IMG    = $('#img');
                var $BTN    = $('#button');
                var $BOX    = $( "#box" )
                var $IND    = $BOX.find( "span.indent" );
                var $TXT    = $BOX.find( "span.mytext" );
                function OnDraggable( BOX ) 
                {
                    var TOP = BOX.css( 'top' ); TOP = ( parseFloat( TOP.substring( 0, TOP.length - 2 ) ) ).toFixed(4);
                    var LEFT = BOX.css( 'left' ); LEFT = ( parseFloat( LEFT.substring( 0, LEFT.length - 2 ) ) ).toFixed(4);
                    $( 'input#Diploma_Field_Coord_Y' ).attr( 'value', TOP ).val( TOP );
                    $( 'input#Diploma_Field_Coord_X' ).attr( 'value', LEFT ).val( LEFT );
                }
                function OnResizable( BOX ) 
                {
                    var WIDTH = BOX.css( 'width' ); WIDTH = ( parseFloat( WIDTH.substring( 0, WIDTH.length - 2 ) ) ).toFixed(4);
                    var HEIGHT = BOX.css( 'height' ); HEIGHT = ( parseFloat( HEIGHT.substring( 0, HEIGHT.length - 2 ) ) ).toFixed(4);
                    $( 'input#Diploma_Field_Area_Width' ).attr( 'value', WIDTH ).val( WIDTH );
                    $( 'input#Diploma_Field_Area_Height' ).attr( 'value', HEIGHT ).val( HEIGHT );
                }
                $( '#Diploma_Field_Name' ).change( function() {
                    var $Diploma_Field_Name =  $( this );
                    var $Diploma_Field_Type = $( '#Diploma_Field_Type' );
                    var value = $.trim( $Diploma_Field_Name.val() );
                    var title = $.trim( $Diploma_Field_Name.find( 'option[value="'+value+'"]' ).attr('title') );
                    $Diploma_Field_Type.find( 'option[value="'+title+'"]' ).prop( 'selected', true ).trigger('change');
                });
                $( '#Diploma_Field_Type' ).change( function() {
                    var $Diploma_Field_Type             =   $( this );
                    var $Diploma_Field_Format_Prefix    =   $( '#Diploma_Field_Format_Prefix' );
                    var $Diploma_Field_Format_Sufix     =   $( '#Diploma_Field_Format_Sufix' );
                    var $Diploma_Field_Format_DateTime  =   $( '#Diploma_Field_Format_DateTime' );
                    var Diploma_Field_Type              =   $Diploma_Field_Type.val();

                    switch( Diploma_Field_Type ) 
                    {
                        case 'alfanumeric-text':
                        case 'alfanumeric-html':
                            $Diploma_Field_Format_Prefix.attr( 'value', <?php echo $DEFAULT['Diploma_Field_Format_Prefix']; ?> ).val( <?php echo $DEFAULT['Diploma_Field_Format_Prefix']; ?> ).prop( 'readonly', false ).closest( 'tr' ).show();
                            $Diploma_Field_Format_Sufix.attr( 'value', 0 ).val( 0 ).prop( 'readonly', true ).closest( 'tr' ).hide();
                            $Diploma_Field_Format_DateTime.empty().html( '' ).closest( 'tr' ).hide();
                            var Diploma_Field_Format_Prefix = parseInt( $Diploma_Field_Format_Prefix.val() );
                            if( Diploma_Field_Format_Prefix != 'NaN' )
                            {
                                $TXT.html( STRINGS.substr( 0, Diploma_Field_Format_Prefix ) );
                            } else {
                                alert( 'ERROR' );
                            }
                            break;
                        case 'numeric-integer':
                            $Diploma_Field_Format_Prefix.attr( 'value', <?php echo $DEFAULT['Diploma_Field_Format_Prefix']; ?> ).val( <?php echo $DEFAULT['Diploma_Field_Format_Prefix']; ?> ).prop( 'readonly', false ).closest( 'tr' ).show();
                            $Diploma_Field_Format_Sufix.attr( 'value', 0 ).val( 0 ).prop( 'readonly', true ).closest( 'tr' ).hide();
                            $Diploma_Field_Format_DateTime.empty().html( '' ).closest( 'tr' ).hide();
                            var Diploma_Field_Format_Prefix = parseInt( $Diploma_Field_Format_Prefix.val() );
                            if( Diploma_Field_Format_Prefix != 'NaN' )
                            {
                                $TXT.html( NUMBERS.substr( 0, Diploma_Field_Format_Prefix ) );
                            } else {
                                alert( 'ERROR' );
                            }
                            break;
                        case 'numeric-decimal':
                            $Diploma_Field_Format_Prefix.attr( 'value', 2 ).val( 2 ).prop( 'readonly', false ).closest( 'tr' ).show();
                            $Diploma_Field_Format_Sufix.attr( 'value', 2 ).val( 2 ).prop( 'readonly', false ).closest( 'tr' ).show();
                            $Diploma_Field_Format_DateTime.empty().html( '' ).closest( 'tr' ).hide();
                            var Diploma_Field_Format_Prefix = parseInt( $Diploma_Field_Format_Prefix.val() );
                            var Diploma_Field_Format_Sufix = parseInt( $Diploma_Field_Format_Sufix.val() );
                            if( ( Diploma_Field_Format_Prefix != 'NaN' ) && ( Diploma_Field_Format_Sufix != 'NaN' ) )
                            {
                                $TXT.html( ( NUMBERS.substr( 0, Diploma_Field_Format_Prefix ) ) + '.' + ( NUMBERS.substr( 0, Diploma_Field_Format_Sufix ) ) );
                            } else {
                                alert( 'ERROR' );
                            }
                            break;
                        case 'datetime':
                            $Diploma_Field_Format_Prefix.attr( 'value', 2 ).val( 0 ).prop( 'readonly', true ).closest( 'tr' ).hide();
                            $Diploma_Field_Format_Sufix.attr( 'value', 2 ).val( 0 ).prop( 'readonly', true ).closest( 'tr' ).hide();
                            $Diploma_Field_Format_DateTime
                                    .empty().html( '' )
                                    .append( '<option value="yyyy-mm-dd">yyyy-mm-dd</option>' )
                                    .append( '<option value="yyyy-mm-dd h:MM:ss">yyyy-mm-dd h:MM:ss</option>' )
                                    .closest( 'tr' ).show();
                            var Diploma_Field_Format_DateTime = $Diploma_Field_Format_DateTime.val()
                            if( Diploma_Field_Format_DateTime )
                            {
                                var n_o_w = new Date(  );
                                var today = dateFormat( n_o_w, Diploma_Field_Format_DateTime );
                                alert( today );
                                $TXT.html( today );
                            } else {
                                alert( 'ERROR' );
                            }
                            break;
                        default:
                            $Diploma_Field_Type.find( 'option[value="alfanumeric-text"]' ).prop( 'selected', true );
                            $Diploma_Field_Format_Prefix.attr( 'value', <?php echo $DEFAULT['Diploma_Field_Format_Prefix']; ?> ).val( <?php echo $DEFAULT['Diploma_Field_Format_Prefix']; ?> ).prop( 'readonly', false );
                            $Diploma_Field_Format_Sufix.attr( 'value', 0 ).val( 0 ).prop( 'readonly', true );
                            $TXT.html( STRINGS.substr( 0, parseInt( $Diploma_Field_Format_Prefix.val() ) ) );
                    }
                });
                $( '#Diploma_Field_Coord_Y' ).bind( 'change keyup input paste', function() {
                    $BOX.css( 'top', $( this ).val() + 'px' );
                });
                $( '#Diploma_Field_Coord_X' ).bind( 'change keyup input paste', function() {
                    $BOX.css( 'left', $( this ).val() + 'px' );
                });
                $( '#Diploma_Field_Area_Width' ).bind( 'change keyup input paste', function() {
                    $BOX.css( 'width', $( this ).val() + 'px' );
                });
                $( '#Diploma_Field_Area_Height' ).bind( 'change keyup input paste', function() {
                    $BOX.css( 'height', $( this ).val() + 'px' );
                });
                $( '#Diploma_Field_Font_Family' ).change( function() {
                    $BOX.css( 'font-family', $( this ).val() );
                });
                $( '#Diploma_Field_Font_Size' ).bind( 'change keyup input paste', function() {
                    $BOX.css( 'font-size', $( this ).val() + 'px' );
                });
                $( '#Diploma_Field_Font_Style' ).change( function() {
                    $BOX.css( 'font-style', $( this ).val() );
                });
                $( '#Diploma_Field_Font_Weight' ).change( function() {
                    $BOX.css( 'font-weight', $( this ).val() );
                });
                $( '#Diploma_Field_Text_Transform' ).change( function() {
                    $BOX.css( 'text-transform', $( this ).val() );
                });
                $( '#Diploma_Field_Text_Align' ).change( function() {
                    $BOX.css( 'text-align', $( this ).val() );
                });
                $( '#Diploma_Field_Line_Height' ).bind( 'change keyup input paste', function() {
                    $BOX.css( 'line-height', $( this ).val() + 'px' );
                    $IND.css( 'height', $( this ).val() + 'px' ).css( 'width', $( '#Diploma_Field_Indent_Width' ).val() + 'px' );
                });
                $( '#Diploma_Field_Indent_Width' ).bind( 'change keyup input paste', function() {
                    $IND.css( 'height', $( '#Diploma_Field_Line_Height' ).val() + 'px' ).css( 'width', $( this ).val() + 'px' );
                });
                $( '#Diploma_Field_Format_Prefix' ).bind( 'change keyup input paste', function() {
                    var $Diploma_Field_Type             =   $( '#Diploma_Field_Type' );
                    var $Diploma_Field_Format_Prefix    =   $( '#Diploma_Field_Format_Prefix' );
                    var $Diploma_Field_Format_Sufix     =   $( '#Diploma_Field_Format_Sufix' );

                    switch( $Diploma_Field_Type.val() ) 
                    {
                        case 'alfanumeric-text':
                            $Diploma_Field_Format_Sufix.attr( 'value', 0 ).val( 0 ).prop( 'readonly', true );
                            var Diploma_Field_Format_Prefix = parseInt( $Diploma_Field_Format_Prefix.val() );
                            if( Diploma_Field_Format_Prefix != 'NaN' )
                            {
                                $TXT.html( STRINGS.substr( 0, Diploma_Field_Format_Prefix ) );
                            } else {
                                alert( 'ERROR' );
                            }
                            break;
                        case 'numeric-integer':
                            $Diploma_Field_Format_Sufix.attr( 'value', 0 ).val( 0 ).prop( 'readonly', true );
                            var Diploma_Field_Format_Prefix = parseInt( $Diploma_Field_Format_Prefix.val() );
                            if( Diploma_Field_Format_Prefix != 'NaN' )
                            {
                                $TXT.html( NUMBERS.substr( 0, Diploma_Field_Format_Prefix ) );
                            } else {
                                alert( 'ERROR' );
                            }
                            break;
                        case 'numeric-decimal':
                            var Diploma_Field_Format_Prefix = parseInt( $Diploma_Field_Format_Prefix.val() );
                            var Diploma_Field_Format_Sufix  = parseInt( $Diploma_Field_Format_Sufix.val() );
                            if( ( Diploma_Field_Format_Prefix != 'NaN' ) && ( Diploma_Field_Format_Sufix != 'NaN' ) )
                            {
                                $TXT.html( ( NUMBERS.substr( 0, Diploma_Field_Format_Prefix ) ) + '.' + ( NUMBERS.substr( 0, Diploma_Field_Format_Sufix ) ) );
                            } else {
                                alert( 'ERROR' );
                            }
                            break;
                        default:
                            $Diploma_Field_Type.find( 'option[value="alfanumeric-text"]' ).prop( 'selected', true );
                            $Diploma_Field_Format_Prefix.prop( 'readonly', false );
                            $Diploma_Field_Format_Sufix.attr( 'value', 0 ).val( 0 ).prop( 'readonly', true );
                            $TXT.html( STRINGS.substr( 0, parseInt( $Diploma_Field_Format_Prefix.val() ) ) );
                    }
                });
                $( '#Diploma_Field_Format_Sufix' ).bind( 'change keyup input paste', function() {
                    var $Diploma_Field_Type             =   $( '#Diploma_Field_Type' );
                    var $Diploma_Field_Format_Prefix    =   $( '#Diploma_Field_Format_Prefix' );
                    var $Diploma_Field_Format_Sufix     =   $( '#Diploma_Field_Format_Sufix' );

                    switch( $Diploma_Field_Type.val() ) 
                    {
                        case 'numeric-decimal':
                            var Diploma_Field_Format_Prefix = parseInt( $Diploma_Field_Format_Prefix.val() );
                            var Diploma_Field_Format_Sufix  = parseInt( $Diploma_Field_Format_Sufix.val() );
                            if( ( Diploma_Field_Format_Prefix != 'NaN' ) && ( Diploma_Field_Format_Sufix != 'NaN' ) )
                            {
                                $TXT.html( ( NUMBERS.substr( 0, Diploma_Field_Format_Prefix ) ) + '.' + ( NUMBERS.substr( 0, Diploma_Field_Format_Sufix ) ) );
                            } else {
                                alert( 'ERROR' );
                            }
                            break;
                        default:
                            $Diploma_Field_Type.find( 'option[value="numeric-decimal"]' ).prop( 'selected', true );
                            $Diploma_Field_Format_Prefix.prop( 'readonly', false );
                            $Diploma_Field_Format_Sufix.prop( 'readonly', false );
                            var Diploma_Field_Format_Prefix = parseInt( $Diploma_Field_Format_Prefix.val() );
                            var Diploma_Field_Format_Sufix  = parseInt( $Diploma_Field_Format_Sufix.val() );
                            $TXT.html( ( NUMBERS.substr( 0, Diploma_Field_Format_Prefix ) ) + '.' + ( NUMBERS.substr( 0, Diploma_Field_Format_Sufix ) ) );
                    }
                });
                $( '#Diploma_Field_Format_DateTime' ).change( function() {
                    var Diploma_Field_Type = $( '#Diploma_Field_Type' ).val();
                    var Diploma_Field_Format_DateTime = $( this ).val();
                    if( ( Diploma_Field_Type == 'datetime' ) && Diploma_Field_Format_DateTime )
                    {
                        var n_o_w = new Date(  );
                        var today = dateFormat( n_o_w, Diploma_Field_Format_DateTime );
                        alert( today );
                        $BOX.html( today );
                    } else {
                        alert( 'ERROR' );
                    }
                });
                var grid_size = 1;
                $RUL.ruler(); 
                $TAB.draggable();
                $BOX.draggable  ({ 
                                    grid: [ grid_size, grid_size ],
                                    start: function() {
                                        OnDraggable( $( this ) );
                                    },
                                    drag: function() {
                                        OnDraggable( $( this ) );
                                    },
                                    stop: function() {
                                        OnDraggable( $( this ) );
                                    }
                                })
                    .resizable  ({ 
                                    grid: grid_size * 1, 

                                    maxHeight: '100%',
                                    maxWidth: '100%',
                                    minHeight: 10,
                                    minWidth: 10,
                                    //aspectRatio: 1 / 1
                                    alsoResize: "#box",
                                    start: function() {
                                        OnResizable( $( this ) );
                                    },
                                    drag: function() {
                                        OnResizable( $( this ) );
                                    },
                                    stop: function() {
                                        OnResizable( $( this ) );
                                    }
                                })
                    .on( "mouseover", function(){
                        $( this ).addClass("move-cursor")
                    })
                    .on( "mousedown", function(){
                        $( this )
                            .removeClass( "move-cursor" )
                            .addClass("grab-cursor")
                            .addClass("opac");
                    })
                    .on( "mouseup", function(){
                        $( this )
                        .removeClass("grab-cursor")
                        .removeClass("opac")
                        .addClass("move-cursor");
                    });
                $BTN.on( "click", function(){
                    $MSG.empty().html('');
                    $IMG.fadeIn();
                                
                    $.post  ( 
                                "<?php echo site_url( 'admin/content/editor_of_diplomas/field/ajax' ); ?>", 
                                {
                                    'Diploma_Config_Id':            $( '#Diploma_Config_Id' ).val(),
                                    'Diploma_Config_Face':          $( '#Diploma_Config_Face' ).val(),
                                    'Diploma_Field_Id':             $( '#Diploma_Field_Id' ).val(),
                                    'Diploma_Field_Name':           $( '#Diploma_Field_Name' ).val(),
                                    'Diploma_Field_Type':           $( '#Diploma_Field_Type' ).val(),
                                    'Diploma_Field_Coord_Y':        $( '#Diploma_Field_Coord_Y' ).val(),
                                    'Diploma_Field_Coord_X':        $( '#Diploma_Field_Coord_X' ).val(),
                                    'Diploma_Field_Area_Width':     $( '#Diploma_Field_Area_Width' ).val(),
                                    'Diploma_Field_Area_Height':    $( '#Diploma_Field_Area_Height' ).val(),
                                    'Diploma_Field_Font_Family':    $( '#Diploma_Field_Font_Family' ).val(),
                                    'Diploma_Field_Font_Size':      $( '#Diploma_Field_Font_Size' ).val(),
                                    'Diploma_Field_Font_Style':     $( '#Diploma_Field_Font_Style' ).val(),
                                    'Diploma_Field_Font_Weight':    $( '#Diploma_Field_Font_Weight' ).val(),
                                    'Diploma_Field_Line_Height':    $( '#Diploma_Field_Line_Height' ).val(),
                                    'Diploma_Field_Text_Transform': $( '#Diploma_Field_Text_Transform' ).val(),
                                    'Diploma_Field_Text_Align':     $( '#Diploma_Field_Text_Align' ).val(),
                                    'Diploma_Field_Indent_Width':   $( '#Diploma_Field_Indent_Width' ).val(),
                                    'Diploma_Field_Format_Value':   (   ( ( $( '#Diploma_Field_Type' ).val() == 'alfanumeric-text' ) || ( $( '#Diploma_Field_Type' ).val() == 'alfanumeric-html' ) )
                                                                        ?   $( '#Diploma_Field_Format_Prefix' ).val()
                                                                        :   (   ( $( '#Diploma_Field_Type' ).val() == 'numeric-integer' )
                                                                                ?   $( '#Diploma_Field_Format_Prefix' ).val()
                                                                                :   (   ( $( '#Diploma_Field_Type' ).val() == 'numeric-decimal' )
                                                                                        ?   $( '#Diploma_Field_Format_Prefix' ).val() + ',' + $( '#Diploma_Field_Format_Sufix' ).val()
                                                                                        :   (   ( $( '#Diploma_Field_Type' ).val() == 'datetime' )
                                                                                                ?   $( '#Diploma_Field_Format_DateTime' ).val()
                                                                                                :   ''
                                                                                            )
                                                                                    )
                                                                            )
                                                                    ),
                                    'Diploma_Field_Status':         $( '#Diploma_Field_Status' ).val(),
                                } 
                            )
                            .done( function( data ) {
                                if( data )
                                {
                                    data = parseInt( data );
                                    if( data != 'NaN' )
                                    {
                                        if( data > 0 )
                                        {
                                            $( '#Diploma_Field_Id' ).attr( 'value', data ).val( data );
                                            $MSG.empty().html( '<small style="color: green;">success</small>' );
                                        } else {
                                            $MSG.empty().html( '<small style="color: red;">no data</small>');
                                        }
                                    } else {
                                        $MSG.empty().html( '<small style="color: red;">error</small>');
                                    }
                                } else {
                                    $MSG.empty().html( '<small style="color: red;">warning</small>' );
                                }
                            })
                            .fail(function() {
                                    $MSG.empty().html( '<small style="color: red;">fail</small>' );
                            })
                            .always(function() {
                                $IMG.fadeOut();
                            });
                    });
            });
        </script>
    </body>
</html>
<?php endif; ?>