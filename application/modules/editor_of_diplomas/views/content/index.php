<h3><?php echo lang( 'editor_of_diplomas_list_index' ); ?><hr /></h3>
<?php
    $total          = 0;
    $can_view       = $this->auth->has_permission( 'Editor_of_diplomas.Content.View' );
    $can_create     = $this->auth->has_permission( 'Editor_of_diplomas.Content.Create' );
    $can_edit       = $this->auth->has_permission( 'Editor_of_diplomas.Content.Edit' );
    $can_delete     = $this->auth->has_permission( 'Editor_of_diplomas.Content.Delete' );
    $has_records    = isset( $DIPLOMAS_CONFIG ) && !empty( $DIPLOMAS_CONFIG  );
    //die( print_r( $DIPLOMAS_CONFIG ) );
?>
<?php if ( $can_view ) : ?>
        <?php echo form_open( $this->uri->uri_string() ); ?>
                <table class='table table-striped' id="table_diplomas_config">
                    <thead>
                        <tr>
                            <th class="text-center" width="5%"><?php echo lang('editor_of_diplomas_form_config_field_id'); ?></th>
                            <th class="text-left" width="55%"><?php echo lang('editor_of_diplomas_form_config_field_title'); ?></th>
                            <th class="text-center" width="10%"><?php echo lang('editor_of_diplomas_form_config_field_type'); ?></th>
                            <th class="text-center" width="10%"><?php echo lang('editor_of_diplomas_form_config_field_image_front'); ?></th>
                            <th class="text-center" width="10%"><?php echo lang('editor_of_diplomas_form_config_field_image_verso'); ?></th>
                            <th class="text-center" width="10%"><?php echo lang('editor_of_diplomas_form_config_label_fields_count'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if ( $has_records ) : ?>
                        <?php foreach ( $DIPLOMAS_CONFIG as $DC ) : ?>
                            <?php $DC = ( array ) $DC; ?>
                            <tr>
                                <td class="text-right">
                                    <label id='Diploma_Config_Id' class="label" style="margin-top: 0.5em !important;">
                                        <?php echo $DC['Diploma_Config_Id']; ?>
                                    </label>
                                </td>
                                <td class="text-left">
                                    <a class="btn btn-info" href="<?php echo site_url( 'admin/content/editor_of_diplomas/config/' . $DC['Diploma_Config_Id'] ); ?>">
                                        <i class="icon icon-white icon-edit"></i>&nbsp;<b><?php echo $DC['Diploma_Config_Title']; ?></b>
                                    </a>
                                </td>
                                <td class="text-center"><label class="label label-success"><?php echo $DC['Diploma_Config_Type']; ?></label></td>
                                <td class="text-center">
                                    <?php if( $DC['Diploma_Config_Image_Front'] ) : ?>
                                    <a href='<?php echo base_url( $diploma_img_upload_folder_image . $DC['Diploma_Config_Image_Front'] ); ?>' target='_blank'>
                                        <img src='<?php echo base_url( $diploma_img_upload_folder_thumb . $DC['Diploma_Config_Image_Front'] ); ?>' style='border: none; height: 4em; width: auto;' />
                                    </a>
                                    <?php endif; ?>
                                </td>
                                <td class="text-center">
                                    <?php if( $DC['Diploma_Config_Image_Verso'] ) : ?>
                                    <a href='<?php echo base_url( $diploma_img_upload_folder_image . $DC['Diploma_Config_Image_Verso'] ); ?>' target='_blank'>
                                        <img src='<?php echo base_url( $diploma_img_upload_folder_thumb . $DC['Diploma_Config_Image_Verso'] ); ?>' style='border: none; height: 4em; width: auto;' />
                                    </a>
                                    <?php endif; ?>
                                </td>
                                <td class="text-right">
                                    <label class="label">
                                        <?php echo $DC['Diploma_Config_Fields_Count']; ?>
                                    </label>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                            <tr>
                                <td class="text-center" colspan="6">
                                    Lista este goală
                                </td>
                            </tr>
                    <?php endif; ?>
                    </tbody>
                    <?php if ( $total ) : ?>
                    <tfoot>
                        <tr>
                            <td class="text-right" colspan="4"><?php echo lang('curriculum__index__datatable__footer__label___curriculum_total'); ?></td>
                            <td class="text-right"><?php echo $total; ?></td>
                        </tr>
                    </tfoot>
                    <?php endif; ?>
                </table>
        <?php echo form_close(); ?>

        <?php if ( $has_records ) : ?>
        <script type="text/javascript">
            $( document ).ready(function(){
                if( typeof $( 'table#table_diplomas_config' ) == 'object' )
                { 
                    $( 'table#table_diplomas_config' ).dataTable( { "ordering": false } );
                }
            });
        </script>
        <?php endif; ?>

<?php endif; ?>