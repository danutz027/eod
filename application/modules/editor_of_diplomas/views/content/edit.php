<h3>
    <?php   echo    (   isset( $DIPLOMA_CONFIG ) && !empty( $DIPLOMA_CONFIG )
                    ?   lang( 'editor_of_diplomas_edit_heading' )
                    :   lang( 'editor_of_diplomas_create_new' ) ); 
    ?><hr />
</h3>
<?php if ( $this->auth->has_permission('Editor_of_diplomas.Content.Create') ) : ?>
    <div id="div-alert" class='alert alert-error' style="display: none;">
        <a class='close' name="link-alert" id="link-alert"></a>
        <h4 class='alert-heading'></h4>
    </div>

    <div class='admin-box'>
        <form id="form-config" action="#" onsubmit="javascript: return false;" class="form-horizontal" method="post" accept-charset="utf-8">
            <table id="table-config" class="table table-responsive table-striped" style="width: 100%; margin: 0 auto;">
                <thead>
                    <tr>
                        <th class="text-right" style="width: 10% !important;">
                            <a name="link-config" id="link-config"></a>
                            <img id="img-ajax" class="pull-right" src="<?php echo base_url ( 'themes/admin/images/ajax-loader-small.gif' ); ?>" style="width: 16px;  height: 16px; border: none; display: none;" />
                            <i id="icon-form" class="icon icon-cog pull-right" style="display: block;"></i>  
                        </th>
                        <th class="text-left" style="width: 70% !important;" colspan='2'>
                            <b>
                                <?php echo lang( 'editor_of_diplomas_form_config_title' ); ?>
                            </b>  
                        </th>
                        <th class="text-left" style="width: 20% !important;">
                            <span class='help-inline Diploma_Config_Table'></span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php   if ( isset( $DIPLOMA_CONFIG ) && !empty( $DIPLOMA_CONFIG ) ) : ?>
                    <tr>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_id' ), 'Diploma_Config_Title', array('class' => 'control-label' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <input id='Diploma_Config_Id' type='text' required='required' name='Diploma_Config_Id' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Title', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Title'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Title'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Title'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Title'] : '' ) ); ?>" class="span8" readonly='readonly' />
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Title'></span>
                        </td>
                    </tr>
                    <?php   endif; ?>
                    <tr>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_title' ), 'Diploma_Config_Title', array('class' => 'control-label' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <input id='Diploma_Config_Title' type='text' required='required' name='Diploma_Config_Title' maxlength='255' value="<?php echo set_value( 'Diploma_Config_Title', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Title'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Title'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Title'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Title'] : '' ) ); ?>" class="span8" />
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Title'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_type' ), 'Diploma_Config_String', array('class' => 'control-label' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <select id='Diploma_Config_Type' required='required' name='Diploma_Config_Type' class="span8">
                                <option></option>
                                <?php if ( isset( $diplomas_design_type ) && !empty( $diplomas_design_type ) ) : ?>
                                    <?php foreach ( $diplomas_design_type as $dt => $DT ) : ?>

                                            <option value="<?php echo $DT; ?>" <?php echo set_select( 'Diploma_Config_Type', ( $Diploma_Config_Type = ( isset( $DIPLOMA_CONFIG['Diploma_Config_Type'] ) && $DIPLOMA_CONFIG['Diploma_Config_Type'] ? $DIPLOMA_CONFIG['Diploma_Config_Type'] : '' ) ), 
                                                                                ( $DT == $Diploma_Config_Type ? true : false ) ); ?>>
                                                <?php echo $DT; ?>
                                            </option>

                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Type'></span>
                        </td>
                    </tr> 
                    <tr>
                        <td class="text-right">
                            <p><b><?php echo lang( 'editor_of_diplomas_form_config_label_images' ); ?></b></p>
                        </td>
                        <td colspan='3'>
                            <ul class="list list-inline list-unstyled">
                                <li><?php echo lang( 'editor_of_diplomas_form_config_label_images__max_size_MB' ); ?>: <b><?php e( $diploma_img_max_size_in_MB ); ?></b> MB</li>
                                <li><?php echo lang( 'editor_of_diplomas_form_config_label_images__max_width_PX' ); ?>: <b><?php e( $diploma_img_max_width___PX ); ?></b> PX</li>
                                <li><?php echo lang( 'editor_of_diplomas_form_config_label_images__max_height_PX' ); ?>: <b><?php e( $diploma_img_max_height__PX ); ?></b> PX</li>
                                <li><?php echo lang( 'editor_of_diplomas_form_config_label_images__resolution_DPI' ); ?>: <b><?php e( $diploma_img_resolution_DPI ); ?></b> DPI</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <?php   $Diploma_Config_Image_Front = set_value( 'Diploma_Config_Image_Front', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] : '' ) );
                                    echo form_label( ( $Diploma_Config_Image_Front ? '' : lang( 'bf_form_label_required' ) ) . lang( 'editor_of_diplomas_form_config_field_image_front' ), 'Diploma_Config_Image_Front', array('class' => 'control-label' ) ); ?>
                        </td>
                        <td class='controls'>
                            <input id='Diploma_Config_Image_Front' type='file' <?php echo ( $Diploma_Config_Image_Front ? '' : "required='required'" ); ?> name='Diploma_Config_Image_Front' value="" class="span8" />
                        </td>
                        <td>
                            <?php if( $Diploma_Config_Image_Front ) : ?>
                                <a href='<?php echo base_url( $diploma_img_upload_folder_image . $Diploma_Config_Image_Front ); ?>' target='_blank'>
                                    <img src='<?php echo base_url( $diploma_img_upload_folder_thumb . $Diploma_Config_Image_Front ); ?>' border='0' />
                                </a>
                            <?php endif; ?>
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Image_Front'></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <?php   $Diploma_Config_Image_Verso = set_value( 'Diploma_Config_Image_Verso', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] : '' ) );
                                    echo form_label( ( $Diploma_Config_Image_Verso ? '' : lang( 'bf_form_label_required' ) ) . lang( 'editor_of_diplomas_form_config_field_image_verso' ), 'Diploma_Config_Image_Verso', array('class' => 'control-label' ) ); ?>
                        </td>
                        <td class='controls'>
                            <input id='Diploma_Config_Image_Verso' type='file' <?php echo ( $Diploma_Config_Image_Verso ? '' : "required='required'" ); ?> name='Diploma_Config_Image_Verso' value="" class="span8" />
                        </td>
                        <td>
                            <?php if( $Diploma_Config_Image_Verso ) : ?>
                                <a href='<?php echo base_url( $diploma_img_upload_folder_image . $Diploma_Config_Image_Verso ); ?>' target='_blank'>
                                    <img src='<?php echo base_url( $diploma_img_upload_folder_thumb . $Diploma_Config_Image_Verso ); ?>' border='0' />
                                </a>
                            <?php endif; ?>
                        </td>
                        <td>
                            <span class='help-inline Diploma_Config_Image_Verso'></span>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td style="background-color: #CCC; border-top: 1px solid #444;"></td>
                        <td style="background-color: #CCC; border-top: 1px solid #444;" colspan='2'>
                            <button name='button-config' id='button-config' type='button' class='btn btn-primary pull-left'>
                                <span class='icon icon-ok-circle icon-white'></span>&nbsp;<?php echo lang( 'editor_of_diplomas_form_config_button_submit' ); ?>
                            </button>
                        </td>
                        <td style="background-color: #CCC; border-top: 1px solid #444;">
                            <a href='<?php echo site_url( 'admin/content/editor_of_diplomas/index' ); ?>' class='btn btn-warning pull-right'>
                                <span class='icon icon-refresh icon-white'></span>&nbsp;<?php echo lang( 'editor_of_diplomas_form_config_button_cancel' ); ?>
                            </a>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>
    </div>
    <script type="text/javascript">
        function in_array( needle, haystack ) 
        {
            var length = haystack.length;
            for( var i = 0; i < length; i++ ) 
            {
                if( typeof haystack[i] == 'object' ) 
                {
                    if( array_compare( haystack[i], needle ) ) return true;
                } 
                else 
                {
                    if( haystack[i] == needle ) return true;
                }
            }
            return false;
        }
        var Diplomas_Design_Type =   
            [

                <?php if ( isset( $diplomas_design_type ) && !empty( $diplomas_design_type ) ) : ?>
                    <?php foreach ( $diplomas_design_type as $dt => $DT ) : ?>

                        '<?php echo $DT; ?>', 

                    <?php endforeach; ?>
                <?php endif; ?>

            ];
        function is_image( file ) 
        {
            if( file )
            {   var sufix_file = ( ( file.toLowerCase() ).split( '.' ) ).pop();
                var image_type = [ 'jpg', 'jpeg', 'png', 'tif', 'gif', 'bmp' ];   
                return in_array( sufix_file, image_type );
            } else 
                return false;
        }
        
        if( window.jQuery )
        {
            function scroll_to( obj ) 
            {
                if( typeof $( obj ) != 'undefined'  )
                {
                    $('html, body').animate( { 'scrollTop': $( obj ).offset().top }, 2000 );
                }
            }

            $(document).ready(function(){

                if  ( 
                        ( typeof $( 'div#div-alert' ) != 'undefined' ) && 
                        ( typeof $( 'form#form-config' ) != 'undefined' ) && 
                        ( typeof $( 'img#img-ajax' ) != 'undefined' ) && 
                        ( typeof $( 'i#icon-form' ) != 'undefined' ) && 
                        ( typeof $( 'button#button-config' ) != 'undefined' ) && 
                        ( typeof $( 'span.help-inline.Diploma_Config_Table' ) != 'undefined' ) &&
                        ( typeof $( 'input#Diploma_Config_Title' ) != 'undefined' ) &&
                        ( typeof $( 'span.help-inline.Diploma_Config_Title' ) != 'undefined' ) &&
                        ( typeof $( 'select#Diploma_Config_Type' ) != 'undefined' ) &&
                        ( typeof $( 'span.help-inline.Diploma_Config_Title' ) != 'undefined' ) &&
                        ( typeof $( 'input#Diploma_Config_Image_Front' ) != 'undefined' ) &&
                        ( typeof $( 'span.help-inline.Diploma_Config_Image_Front' ) != 'undefined' ) &&
                        ( typeof $( 'input#Diploma_Config_Image_Verso' ) != 'undefined' ) &&
                        ( typeof $( 'span.help-inline.Diploma_Config_Image_Verso' ) != 'undefined' )
                    )
                {
                    var $Div_Alert = $( 'div#div-alert' ); 
                    var $Img__Ajax = $( 'img#img-ajax' ); $Img__Ajax.hide();
                    var $Icon_Form = $( 'i#icon-form' ); $Icon_Form.show();
                    var $Form_Config = $( 'form#form-config' ); 
                    var $Button_Config = $( 'button#button-config' ); 
                    var $Diploma_Config_Title = $( 'input#Diploma_Config_Title' ); 
                    var $Diploma_Config_Type = $( 'select#Diploma_Config_Type' ); 
                    var $Diploma_Config_Image_Front = $( 'input#Diploma_Config_Image_Front' ); 
                    var $Diploma_Config_Image_Verso = $( 'input#Diploma_Config_Image_Verso' ); 
                    $Div_Alert.find('h4.alert-heading').empty().html('').parent().hide();
                    $Img__Ajax.hide();
                    $Button_Config.click( function( e ) {
                        //$( 'span.help-inline' ).empty().html('');
                        var valid                       = true;
                        var Diploma_Config_Title        = $.trim( $Diploma_Config_Title.val() );
                        var Diploma_Config_Type         = $.trim( $Diploma_Config_Type.val() );
                        var Diploma_Config_Image_Front  = $Diploma_Config_Image_Front.val();
                        var Diploma_Config_Image_Verso  = $Diploma_Config_Image_Verso.val();
                        if( !Diploma_Config_Title )
                        {   valid = false;
                            $( 'span.help-inline.Diploma_Config_Title' ).empty().html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                        } else {
                            $( 'span.help-inline.Diploma_Config_Title' ).empty().html( '' );
                        }
                        if( !Diploma_Config_Type )
                        {
                            valid = false;
                            $( 'span.help-inline.Diploma_Config_Type' ).empty().html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                        } else {
                            if( in_array( Diploma_Config_Type, Diplomas_Design_Type ) )
                            {   
                                $( 'span.help-inline.Diploma_Config_Type' ).empty().html( '' );
                            } else {
                                valid = false;
                                $( 'span.help-inline.Diploma_Config_Type' ).empty().html( '<?php echo lang( 'bf_error_invalid' ); ?>' );
                            }
                        }
                        if( $Diploma_Config_Image_Front.prop( 'required' ) && ( Diploma_Config_Image_Front == '' ) )
                        {   valid = false;
                            $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                        } else {
                            if( ( Diploma_Config_Image_Front != '' ) && 
                                is_image( Diploma_Config_Image_Front ) && 
                                ( ( $Diploma_Config_Image_Front )[0].files.length == 1 ) ) 
                            {   
                                //                                if( ( $Diploma_Config_Image_Front.size / ( 1024 * 1024 ) ) > <?php e( $diploma_img_max_size_in_MB ); ?> )
                                //                                {   valid = false;
                                //                                    $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '<?php echo lang( 'bf_error_file_size_is_to_lage' ); ?>' );
                                //                                } else {
                                //
                                //                                    var $bmp = new Bitmap("winter.jpg");
                                //                                    var FrontHrRes = $bmp.HorizontalResolution;
                                //                                    var FrontVrRes = $bmp.VerticalResolution;
                                //                                    if( ( $bmp.HorizontalResolution == <?php e( $diploma_img_resolution_DPI ); ?> ) 
                                //                                        && 
                                //                                        ( $bmp.VerticalResolution == <?php e( $diploma_img_resolution_DPI ); ?> ) )
                                //                                    {
                                //                                        if( ( $bmp.Width <= <?php e( $diploma_img_resolution_DPI ); ?> ) 
                                //                                            && 
                                //                                            ( $bmp.Height <= <?php e( $diploma_img_resolution_DPI ); ?> ) )
                                //                                        {
                                //                                            $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '' );
                                //                                        } else {
                                //                                            valid = false;
                                //                                            $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '<?php echo lang( 'bf_error_invalid' ); ?> ( Width / Height )' );
                                //                                        }
                                //                                    } else {
                                //                                        valid = false;
                                //                                        $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '<?php echo lang( 'bf_error_invalid' ); ?> ( Size of Resolution )' );
                                //                                    }
                                //                                }
                                $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '' );
                            } else {
                                if( $Diploma_Config_Image_Front.prop( 'required' ) )
                                {   valid = false;
                                    $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '<?php echo lang( 'bf_error_invalid' ); ?>' );
                                } else {
                                    $( 'span.help-inline.Diploma_Config_Image_Front' ).empty().html( '' );
                                }
                            }
                        }
                        if( !Diploma_Config_Image_Verso )
                        {   valid = false;
                            $( 'span.help-inline.Diploma_Config_Image_Verso' ).empty().html( '<?php echo lang( 'bf_error_empty' ); ?>' );
                        } else {
                            if( is_image( Diploma_Config_Image_Verso ) && ( ( $Diploma_Config_Image_Verso )[0].files.length == 1 ) )
                            {   
                                //                                if( ( $Diploma_Config_Image_Verso.size / ( 1024 * 1024 ) ) > <?php e( $diploma_img_max_size_in_MB ); ?> )
                                //                                {   valid = false;
                                //                                    $( 'span.help-inline.Diploma_Config_Image_Verso' ).empty().html( '<?php echo lang( 'bf_error_file_size_is_to_lage' ); ?>' );
                                //                                } else {
                                //                                    $( 'span.help-inline.Diploma_Config_Image_Verso' ).empty().html( '' );
                                //                                }
                                $( 'span.help-inline.Diploma_Config_Image_Verso' ).empty().html( '' );
                            } else {
                                valid = false;
                                $( 'span.help-inline.Diploma_Config_Image_Verso' ).empty().html( '<?php echo lang( 'bf_error_invalid' ); ?>' );
                            }
                        }
                        if( valid == true )
                        {
                            $( 'span.help-inline.Diploma_Config_Table' ).empty().html( '' );
                            var $ConfigDATA = new FormData( $Form_Config[0] );
                            var $Config_URL = '<?php echo site_url( 'admin/content/editor_of_diplomas/form' ); ?>';
                            $Icon_Form.fadeOut();
                            $Img__Ajax.fadeIn();
                            $.ajax( 
                                    {
                                        url:            $Config_URL,
                                        enctype:        'multipart/form-data',
                                        type:           'POST',
                                        data:           $ConfigDATA,
                                        contentType:    false,
                                        cache:          false,
                                        processData:    false,
                                        success:        function( ID )
                                                        {
                                                            if( ID )
                                                            {
                                                                $Img__Ajax.fadeOut();
                                                                $Icon_Form.fadeIn();
                                                                $Div_Alert
                                                                    .removeAttr('class').addClass('alert').addClass('alert-success')
                                                                    .find('h4.alert-heading').empty().html('<?php echo lang( 'bf_ajax_success' ); ?>')
                                                                    .parent().fadeIn();
                                                                setTimeout( function(){
                                                                    scroll_to( 'link-alert' );
                                                                    setTimeout( function(){
                                                                        $( location ).attr( 'href', '<?php echo site_url( 'admin/content/editor_of_diplomas/edit' ); ?>/' + ID);
                                                                    }, 3000 );
                                                                }, 2000 );
                                                            }
                                                        },
                                        error:          function( err )
                                                        {
                                                            $Div_Alert
                                                                .removeAttr('class').addClass('alert').addClass('alert-error')
                                                                .find('h4.alert-heading').empty().html('<?php echo lang( 'bf_ajax_error' ); ?>')
                                                                .parent().fadeIn();
                                                            setTimeout( function(){
                                                                scroll_to( 'link-alert' );
                                                            }, 2000 );
                                                        }
                                    } 
                                );
                            
                        } else {
                            $( 'span.help-inline.Diploma_Config_Table' ).empty().html( '<?php echo lang( 'bf_errors' ); ?>' );
                        }
                    });
                }
            });
        } 
        else 
        {
            if( typeof ( document.getElementById( 'body_blank' ) ) != 'undefined' )
            {
                document.getElementById( 'desktop' ).innerHTML = "<center><h5 style='color:red;'><?php echo lang( 'bf_ajax_warning' ); ?></h5></center>";
            }
            alert( '<?php echo lang( 'bf_ajax_warning' ); ?>' );
        }
    </script>
<?php endif; ?>
