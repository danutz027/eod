<?php

$checkSegment = $this->uri->segment(4);
$areaUrl = SITE_AREA . '/content/editor_of_diplomas';

?>
<ul id='nav_articles' class='nav nav-pills'>
    <?php if ( $this->auth->has_permission( 'Editor_of_diplomas.Content.View' ) ) : ?>
        <li id="Editor_of_Diplomas__List" <?php echo $checkSegment == '' ? ' class="active"' : ''; ?>>
            <a href="<?php echo site_url( $areaUrl ); ?>" class='list' title="<?php echo lang('editor_of_diplomas_list_index'); ?>">
                <span class='icon icon-list'></span> <?php echo lang('editor_of_diplomas_list'); ?>
            </a>
        </li>
    <?php endif; ?>
    <?php if ( $this->auth->has_permission( 'Editor_of_diplomas.Content.Create' ) ) : ?>
        <li class="<?php echo $checkSegment == 'form' ? ' active' : ''; ?>">
            <a id="Editor_of_Diplomas__New" href="<?php echo site_url( $areaUrl . '/form' ); ?>" class='new' title="<?php echo lang('editor_of_diplomas_create_new_button'); ?>">
                <span class='icon icon-plus-sign'></span> <?php echo lang('editor_of_diplomas_new'); ?>
            </a>
        </li>
    <?php endif; ?>
    <?php if ( $this->auth->has_permission( 'Editor_of_diplomas.Content.View' ) ) : ?>
        <li>
            <a id="Editor_of_Diplomas__Help" href="<?php echo site_url( $areaUrl . '/help' ); ?>" class='help' title="<?php echo lang('editor_of_diplomas_help_manual'); ?>">
                <span class='icon icon-question-sign'></span> <?php echo lang('editor_of_diplomas_help'); ?>
            </a>
        </li>
        <script type='text/javascript'>
            $(document).ready(function(){
                $("a#Editor_of_Diplomas__Help").colorbox( { width:"75%", height:"75%", overlayClose: false } );
            });
        </script>
    <?php endif; ?>
</ul>