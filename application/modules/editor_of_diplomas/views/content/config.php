<?php $fields = array(); ?>
<h3><?php echo lang( 'editor_of_diplomas_config_heading' ); ?><hr /></h3>

<?php if ( $this->auth->has_permission( 'Editor_of_diplomas.Content.View' ) && ( isset( $DIPLOMA_CONFIG ) && !empty( $DIPLOMA_CONFIG ) ) ) : ?>

    <div id="div-alert" class='alert alert-error' style="display: none;">
        <a class='close' name="link-alert" id="link-alert"></a>
        <h4 class='alert-heading'></h4>
    </div>

    <div class='admin-box'>
        <form id="form-config" action="#" onsubmit="javascript: return false;" class="form-horizontal" method="post" accept-charset="utf-8">
            <table id="table-config" class="table table-responsive table-striped" style="width: 100%; margin: 0 auto;">
                <thead>
                    <tr>
                        <th class="text-right" style="width: 10% !important;">
                            <a name="link-config" id="link-config"></a>
                            <img id="img-ajax" class="pull-right" src="<?php echo base_url ( 'themes/admin/images/ajax-loader-small.gif' ); ?>" style="width: 16px;  height: 16px; border: none; display: none;" />
                            <i id="icon-form" class="icon icon-cog pull-right" style="display: block;"></i>  
                        </th>
                        <th class="text-left" style="width: 10% !important; white-space: nowrap;">
                            <b>
                                <?php echo lang( 'editor_of_diplomas_form_config_label_diploma_template' ); ?>
                            </b>  
                        </th>
                        <th class="text-right" style="position:  relative; right: 20%; width: 70% !important; white-space: nowrap;">
                            <a class="btn btn-primary" id="Editor_of_Diploma__Clone" href="<?php echo site_url( 'admin/content/editor_of_diplomas/form/' . ( isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Id'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Id'] . '_' . lang('editor_of_diplomas_clone') : '' )  ); ?>" class='new' onclick="return confirm('<?php echo lang('editor_of_diplomas_clone_confirm'); ?>');" title="<?php echo lang('editor_of_diplomas_clone_text'); ?>">
                                <span class='icon icon-white icon-plus-sign'></span> <?php echo lang('editor_of_diplomas_clone'); ?>
                            </a>
                        </th>
                        <th class="text-right" style="position:  relative; right: 20%; width: 70% !important; white-space: nowrap;">
                            <a class="btn btn-primary" id="Editor_of_Diploma__Edit" href="<?php echo site_url( 'admin/content/editor_of_diplomas/form/' . ( isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Id'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Id'] : '' )  ); ?>" class='new' title="<?php echo lang('editor_of_diplomas_edit_text'); ?>">
                                <span class='icon icon-white icon-pencil'></span> <?php echo lang('editor_of_diplomas_edit'); ?>
                            </a>
                        </th>
                        <th class="text-right" style="position:  relative; right: 20%; width: 70% !important; white-space: nowrap;">
                            <a class="btn btn-primary" id="Editor_of_Diploma__Delete" href="<?php echo site_url( 'admin/content/editor_of_diplomas/delete/' . ( isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Id'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Id'] : '' )  ); ?>" class='new' onclick="return confirm('<?php echo lang('editor_of_diplomas_delete_template_confirm'); ?>');" title="<?php echo lang('editor_of_diplomas_delete_text'); ?>">
                                <span class='icon icon-white icon-remove'></span> <?php echo lang('editor_of_diplomas_delete'); ?>
                            </a>
                        </th>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_id' ), 'Diploma_Config_Title', array('class' => 'control-label' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <label id='Diploma_Config_Id' class="label" title="<?php echo lang( 'editor_of_diplomas_form_config_field_id' ); ?>" style="margin-top: 0.5em !important;">
                                <?php echo set_value( 'Diploma_Config_Id', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Id'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Id'] : '' ) ); ?>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_title' ), 'Diploma_Config_Title', array('class' => 'control-label' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <label id='Diploma_Config_Title' class="label label-info" title="<?php echo lang( 'editor_of_diplomas_form_config_field_title' ); ?>" style="margin-top: 0.25em !important; font-size: larger; font-weight: bolder; padding: 0.25em !important;">
                                <?php echo set_value( 'Diploma_Config_Title', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Title'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Title'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Title'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Title'] : '' ) ); ?>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_type' ), 'Diploma_Config_Type', array( 'class' => 'control-label' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <label id='Diploma_Config_Type' class="label label-success" title="<?php echo lang( 'editor_of_diplomas_form_config_field_type' ); ?>" style="margin-top: 0.5em !important;">
                                <?php echo set_value( 'Diploma_Config_Type', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Type'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Type'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Type'] : '' ) ); ?>
                            </label>
                        </td>
                    </tr> 
                    <tr>
                        <td class="text-right">
                            <?php echo form_label(  lang( 'bf_form_label_required' ) . lang( 'editor_of_diplomas_form_config_field_form' ), 'Diploma_Config_Form', array( 'class' => 'control-label' ) ); ?>
                        </td>
                        <td class='controls' colspan='2'>
                            <label id='Diploma_Config_Form' class="label label-success" title="<?php echo lang( 'editor_of_diplomas_form_config_field_form' ); ?>" style="margin-top: 0.5em !important;">
                                <?php echo set_value( 'Diploma_Config_Form', ( isset( $DIPLOMA_CONFIG['Diploma_Config_Form'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Form'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Form'] ) ) ? $DIPLOMA_CONFIG['Diploma_Config_Form'] : '' ) ); ?>
                            </label>
                        </td>
                    </tr>
                </thead>
                <tfoot role="fields">
                <?php if ( isset( $diplomas_design_field_default ) && !empty( $diplomas_design_field_default ) ) : ?>
                    <tr>
                        <td class="text-right">
                            <i class="icon icon-list"></i>  
                        </td>
                        <td class="text-left" colspan='2'>
                            <b><?php echo lang( 'editor_of_diplomas_form_config_field_default' ); ?></b>
                        </td>
                    </tr>
                    <?php 
                        foreach ( $diplomas_design_field_default as $ddfd => $DDFD ) : 
                            $fields[$ddfd] = array('type' => $DDFD['type'], 'text' => $DDFD['text'] );
                    ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo $ddfd; ?></i></small></td>
                        <td><?php echo $DDFD['type']; ?></td>
                        <td><?php echo $DDFD['text']; ?></td>
                    </tr>

                    <?php endforeach; ?>

                <?php endif; ?>
                <?php if ( 1 > 0 ) : ?>

                    <tr>
                        <td class="text-right">
                            <i class="icon icon-list"></i>  
                        </td>
                        <td class="text-left" colspan='2'>
                            <b><?php echo lang( 'editor_of_diplomas_form_config_field_more' ); ?></b>
                        </td>
                    </tr>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] ) ) )
                                && ( $fields['Field_001_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text'] ) )
                            ) : 
                    ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-001</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_001_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_001_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if (
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] ) ) )
                                && ( $fields['Field_002_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-002</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_002_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_002_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] ) ) ) 
                                && ( $fields['Field_003_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-003</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_003_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_003_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] ) ) ) 
                                && ( $fields['Field_004_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-004</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_004_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_004_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] ) ) ) 
                                && ( $fields['Field_005_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-005</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_005_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_005_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] ) ) ) 
                                && ( $fields['Field_006_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-006</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_006_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_006_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] ) ) ) 
                                && ( $fields['Field_007_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-007</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_007_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_007_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] ) ) ) 
                                && ( $fields['Field_008_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-008</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_008_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_008_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] ) ) ) 
                                && ( $fields['Field_009_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-009</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_009_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_009_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] ) ) ) 
                                && ( $fields['Field_010_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-010</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_010_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_010_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] ) ) ) 
                                && ( $fields['Field_011_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-011</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_011_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_011_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] ) ) ) 
                                && ( $fields['Field_012_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-012</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_012_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_012_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] ) ) ) 
                                && ( $fields['Field_013_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-013</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_013_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_013_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] ) ) ) 
                                && ( $fields['Field_014_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-014</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_014_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_014_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] ) ) ) 
                                && ( $fields['Field_015_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-015</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_015_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_015_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] ) ) ) 
                                && ( $fields['Field_016_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-016</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_016_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_016_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] ) ) )
                                && ( $fields['Field_017_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text'] ) ) 
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-017</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_017_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_017_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] ) ) )
                                && ( $fields['Field_018_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text'] ) ) 
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-018</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_018_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_018_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] ) ) ) 
                                && ( $fields['Field_019_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-019</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_019_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_019_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] ) ) ) 
                                && ( $fields['Field_020_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-020</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_020_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_020_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] ) ) ) 
                                && ( $fields['Field_021_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-021</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_021_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_021_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] ) ) ) 
                                && ( $fields['Field_022_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-022</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_022_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_022_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] ) ) ) 
                                && ( $fields['Field_023_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-023</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_023_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_023_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] ) ) ) 
                                && ( $fields['Field_024_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-024</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_024_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_024_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] ) ) ) 
                                && ( $fields['Field_025_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-025</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_025_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_025_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] ) ) ) 
                                && ( $fields['Field_026_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-026</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_026_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_026_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] ) ) ) 
                                && ( $fields['Field_027_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-027</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_027_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_027_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] ) ) ) 
                                && ( $fields['Field_028_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-028</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_028_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_028_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] ) ) ) 
                                && ( $fields['Field_029_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-029</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_029_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_029_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                    <?php if ( 
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] ) && in_array( ( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'] ) ), $diplomas_design_field_type ) ) &&  
                                ( isset( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] = trim( $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] ) ) ) 
                                && ( $fields['Field_030_Value'] = array( 'type' => $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type'], 'text' => $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text'] ) )
                            ) : ?>

                    <tr>
                        <td class="text-right small"><small><i><?php echo lang( 'editor_of_diplomas_form_config_field_additional' ); ?>-030</i></small></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_030_Type']; ?></td>
                        <td><?php echo $DIPLOMA_CONFIG['Diploma_Config_Field_030_Text']; ?></td>
                    </tr>

                    <?php endif; ?>
                </tfoot>
                <?php endif; ?>
                <tbody role="image">
                    <tr>
                        <td class="text-right">
                            <i class="icon icon-list-alt pull-right" style="display: block;"></i>  
                        </td>
                        <td class='controls' colspan='2'>
                            <b>
                                <?php echo lang( 'editor_of_diplomas_form_config_label_diploma_fields' ); ?>
                            </b> 
                        </td>
                    </tr> 

                    <tr>
                        <td class="text-right">
                            <label class="control-label">
                                <?php echo lang( 'editor_of_diplomas_form_config_label_config_fields_4_front' ); ?>
                                <?php echo form_label( ( isset( $DIPLOMA_FIELDS_FRONT ) && !empty( $DIPLOMA_FIELDS_FRONT ) ) ? sizeof( $DIPLOMA_FIELDS_FRONT ) : 0, '', array( 'id' => 'Editor_of_Diploma__COUNTER__FRONT', 'class' => 'badge', 'title' => lang( 'editor_of_diplomas_form_config_title_fields_count_front' ) ) ); ?>
                            </label>
                        <?php if( isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) ) : ?>
                            <?php list( $Diploma_Config_Image_Front__Width, $Diploma_Config_Image_Front__Height ) = getimagesize( $diploma_img_upload_folder_image . $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ); ?>
                            <p>
                                <a href='<?php echo base_url( $diploma_img_upload_folder_image . $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ); ?>' title="<?php echo lang( 'editor_of_diplomas_form_config_field_image_front' ); ?>" target='_blank'>
                                    <img src='<?php echo base_url( $diploma_img_upload_folder_thumb . $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ); ?>' style="width: 12em; height: auto; border: none;" />
                                </a>
                            </p>
                            <ul class="text-left" style="font-family: monospace !important;">
                                <li class="small"><?php echo $DIPLOMA_CONFIG['Diploma_Config_Image_Front']; ?></li>
                                <li class="small">width = <?php echo $Diploma_Config_Image_Front__Width; ?>px</li>
                                <li class="small">height = <?php echo $Diploma_Config_Image_Front__Height; ?>px</li>
                            </ul>
                        <?php endif; ?>
                        </td>
                        <td class="text-left" colspan='2'>

                            <table class="table table-responsive" style="border: 1px solid #444 !important;">
                                <thead>
                                    <tr>
                                        <th style="width: 15%; text-align: center;" colspan="17">
                                            Attributes
                                        </th>                                        
                                        <th style="width: 2.5%; text-align: center;" colspan="1">
                                            <a id="Editor_of_Diploma__ADD_FIELD__FRONT" 
                                                class="btn btn-primary" 
                                                href="<?php echo site_url( 'admin/content/editor_of_diplomas/field/insert/' . $DIPLOMA_CONFIG['Diploma_Config_Id'] ); ?>/front" 
                                                title="<?php echo lang( 'editor_of_diplomas_form_config_label_config_add_field_2_front' ) . ' [' . ( isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && $DIPLOMA_CONFIG['Diploma_Config_Id'] ? $DIPLOMA_CONFIG['Diploma_Config_Id'] : '' ) . ']'; ?>">
                                                 <i class="icon icon-white icon-plus-sign"></i>
                                            </a>
                                        </th>
                                        <th style="width: 2.5%; text-align: center;" colspan="1">
                                            <a target="_blank" 
                                                class="btn btn-default" 
                                                href="<?php echo site_url( 'admin/content/editor_of_diplomas/pdf/' . $DIPLOMA_CONFIG['Diploma_Config_Id'] ); ?>/front" 
                                                title="<?php echo lang( 'editor_of_diplomas_form_config_label_config_print_2_front' ) . ' [' . ( isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && $DIPLOMA_CONFIG['Diploma_Config_Id'] ? $DIPLOMA_CONFIG['Diploma_Config_Id'] : '' ) . ']'; ?>">
                                                 <i class="icon icon-print"></i>
                                            </a>
                                        </th>
                                        
                                    </tr>
                                    <tr>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Field<br />Id</th>
                                        <th style="width: 10%; text-align: center; font-size: smaller;">Field<br />Name</th>
                                        <th style="width: 10%; text-align: center; font-size: smaller;">Field<br />Type</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Field<br />Top</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Field<br />Left</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Area<br />Width</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Area<br />Height</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Font<br />Family</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Font<br />Size</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Font<br />Style</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Font<br />Weight</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Line<br />Height</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Text<br />Transform</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Text<br />Align</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Indent<br />Width</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Format<br />Value</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Field<br />Status</th>
                                        <th style="width: 2.5%; text-align: center;">Edit</th>
                                        <th style="width: 2.5%; text-align: center;">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                        <?php if( 
                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) ) && 
                                    ( isset( $DIPLOMA_FIELDS_FRONT ) && !empty( $DIPLOMA_FIELDS_FRONT ) )
                                ) : ?>

                            <?php foreach ( $DIPLOMA_FIELDS_FRONT as $dff => $DFF ) : ?>

                                <tr>
                                    <td class="text-right"><?php echo $DFF->Diploma_Field_Id; ?></td>
                                    <td class="text-center"><a role="Field-Info" class="btn-link" href="#" onclick="return false;" data-html="true" data-toggle="popover" data-trigger="focus" title="<b><?php echo $DFF->Diploma_Field_Name; ?></b>" data-content="<table class='table table-responsive table-striped'><tr><td>Type</td><td><?php echo isset( $fields[$DFF->Diploma_Field_Name] ) ? $fields[$DFF->Diploma_Field_Name]['type'] : ''; ?></td></tr><tr><td>Text:</td><td><?php echo isset( $fields[$DFF->Diploma_Field_Name] ) ? $fields[$DFF->Diploma_Field_Name]['text'] : ''; ?></td></tr></table>"><?php echo $DFF->Diploma_Field_Name; ?></a></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Type; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Coord_Y; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Coord_X; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Area_Width; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Area_Height; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Font_Family; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Font_Size; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Font_Style; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Font_Weight; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Line_Height; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Text_Transform; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Text_Align; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Indent_Width; ?></td>
                                    <td class="text-center"><?php echo $DFF->Diploma_Field_Format_Value; ?></td>
                                    <td class="text-center" style="color: <?php echo $DFF->Diploma_Field_Status == 'live' ? 'green' : 'pink'; ?>;"><?php echo $DFF->Diploma_Field_Status; ?></td>
                                    
                                    <td class="text-center">
                                        <a class="EditFront btn btn-info" 
                                           href="<?php echo site_url( 'admin/content/editor_of_diplomas/field/update/' . $DFF->Diploma_Config_Id . '/' . $DFF->Diploma_Config_Face . '/' . $DFF->Diploma_Field_Id ); ?>" 
                                           title="<?php echo lang( 'editor_of_diplomas_form_config_label_config_edit_field_2_front' ) . ' [' . $DFF->Diploma_Config_Id . '] {' . $DFF->Diploma_Field_Id . '}'; ?>">
                                            <i class="icon icon-white icon-edit"></i>
                                        </a>
                                    </td>
                                    
                                    <td class="text-center">
                                        <button class="btn btn-warning" onClick="javascript: if( confirm( 'Delete [<?php echo $DFF->Diploma_Config_Id; ?>] {<?php echo $DFF->Diploma_Field_Id; ?>}' ) ){ window.location.href = '<?php echo site_url( 'admin/content/editor_of_diplomas/field/delete/' . $DFF->Diploma_Config_Id . '/' . $DFF->Diploma_Config_Face . '/' . $DFF->Diploma_Field_Id ); ?>'; } else { return false; }">
                                            <i class="icon icon-white icon-remove"></i>
                                        </button>
                                    </td>
                                </tr>

                            <?php endforeach; ?>


                        <?php endif; ?>
                                </tbody>
                                <tfoot></tfoot>

                            </table>

                        </td>
                    </tr>

                    <tr>
                        <td class="text-right">
                            <label class="control-label">
                                <?php echo lang( 'editor_of_diplomas_form_config_label_config_fields_4_verso' ); ?>
                                <?php echo form_label( ( isset( $DIPLOMA_FIELDS_VERSO ) && !empty( $DIPLOMA_FIELDS_VERSO ) ) ? sizeof( $DIPLOMA_FIELDS_VERSO ) : 0, '', array( 'id' => 'Editor_of_Diploma__COUNTER__VERSO', 'class' => 'badge', 'title' => lang( 'editor_of_diplomas_form_config_title_fields_count_front' ) ) ); ?>
                            </label>
                            <?php if( isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ) ) : ?>
                                <?php list( $Diploma_Config_Image_Verso__Width, $Diploma_Config_Image_Verso__Height ) = getimagesize( $diploma_img_upload_folder_image . $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ); ?>
                                <p>
                                    <a href='<?php echo base_url( $diploma_img_upload_folder_image . $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ); ?>' title="<?php echo lang( 'editor_of_diplomas_form_config_field_image_verso' ); ?>" target='_blank'>
                                        <img src='<?php echo base_url( $diploma_img_upload_folder_thumb . $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ); ?>' style="width: 12em; height: auto; border: none;" />
                                    </a>
                                </p>
                                <ul class="text-left" style="font-family: monospace !important;">
                                    <li class="small"><?php echo $DIPLOMA_CONFIG['Diploma_Config_Image_Verso']; ?></li>
                                    <li class="small">width = <?php echo $Diploma_Config_Image_Verso__Width; ?>px</li>
                                    <li class="small">height = <?php echo $Diploma_Config_Image_Verso__Height; ?>px</li>
                                </ul>
                            <?php endif; ?>
                        </td>
                        <td class="text-left" colspan='2'>

                            <table class="table table-responsive" style="border: 1px solid #444 !important;">
                                <thead>
                                    <tr>
                                        <th style="width: 15%; text-align: center;" colspan="17">
                                            Attributes
                                        </th>                                        
                                        <th style="width: 5%; text-align: center;" colspan="1">
                                            <a id="Editor_of_Diploma__ADD_FIELD__VERSO" 
                                                class="btn btn-primary" 
                                                href="<?php echo site_url( 'admin/content/editor_of_diplomas/field/insert/' . $DIPLOMA_CONFIG['Diploma_Config_Id'] ); ?>/verso" 
                                                title="<?php echo lang( 'editor_of_diplomas_form_config_label_config_add_field_2_verso' ) . ' [' . ( isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && $DIPLOMA_CONFIG['Diploma_Config_Id'] ? $DIPLOMA_CONFIG['Diploma_Config_Id'] : '' ) . ']'; ?>">
                                                 <i class="icon icon-white icon-plus-sign"></i>
                                            </a>
                                        </th>
                                        <th style="width: 2.5%; text-align: center;" colspan="1">
                                            <a target="_blank" 
                                                class="btn btn-default" 
                                                href="<?php echo site_url( 'admin/content/editor_of_diplomas/pdf/' . $DIPLOMA_CONFIG['Diploma_Config_Id'] ); ?>/verso" 
                                                title="<?php echo lang( 'editor_of_diplomas_form_config_label_config_print_2_verso' ) . ' [' . ( isset( $DIPLOMA_CONFIG['Diploma_Config_Id'] ) && $DIPLOMA_CONFIG['Diploma_Config_Id'] ? $DIPLOMA_CONFIG['Diploma_Config_Id'] : '' ) . ']'; ?>">
                                                 <i class="icon icon-print"></i>
                                            </a>
                                        </th>
                                    </tr>
                                    <tr>
                                    <tr>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Field<br />Id</th>
                                        <th style="width: 10%; text-align: center; font-size: smaller;">Field<br />Name</th>
                                        <th style="width: 10%; text-align: center; font-size: smaller;">Field<br />Type</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Field<br />Top</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Field<br />Left</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Area<br />Width</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Area<br />Height</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Font<br />Family</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Font<br />Size</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Font<br />Style</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Font<br />Weight</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Line<br />Height</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Text<br />Transform</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Text<br />Align</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Indent<br />Width</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Format<br />Value</th>
                                        <th style="width: 5%; text-align: center; font-size: smaller;">Field<br />Status</th>
                                        <th style="width: 2.5%; text-align: center;">Edit</th>
                                        <th style="width: 2.5%; text-align: center;">Delete</th>
                                    </tr>
                                    </tr>
                                </thead>
                                <tbody>
                        <?php if( 
                                    ( isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) && ( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) ) && 
                                    ( isset( $DIPLOMA_FIELDS_VERSO ) && !empty( $DIPLOMA_FIELDS_VERSO ) )
                                ) : ?>

                            <?php foreach ( $DIPLOMA_FIELDS_VERSO as $dfv => $DFV ) : ?>

                                <tr>
                                    <td class="text-right"><?php echo $DFV->Diploma_Field_Id; ?></td>
                                    <td class="text-center"><a role="Field-Info" href="#" onclick="return false;" class="btn-link" data-html="true" data-toggle="popover" data-trigger="focus" title="<b><?php echo $DFV->Diploma_Field_Name; ?></b>" data-content="<table class='table table-responsive table-striped'><tr><td>Type</td><td><?php echo isset( $fields[$DFV->Diploma_Field_Name] ) ? $fields[$DFV->Diploma_Field_Name]['type'] : ''; ?></td></tr><tr><td>Text:</td><td><?php echo isset( $fields[$DFV->Diploma_Field_Name] ) ? $fields[$DFV->Diploma_Field_Name]['text'] : ''; ?></td></tr></table>"><?php echo $DFV->Diploma_Field_Name; ?></a></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Type; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Coord_Y; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Coord_X; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Area_Width; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Area_Height; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Font_Family; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Font_Size; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Font_Style; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Font_Weight; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Line_Height; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Text_Transform; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Text_Align; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Indent_Width; ?></td>
                                    <td class="text-center"><?php echo $DFV->Diploma_Field_Format_Value; ?></td>
                                    <td class="text-center" style="color: <?php echo $DFV->Diploma_Field_Status == 'live' ? 'green' : 'pink'; ?>;"><?php echo $DFV->Diploma_Field_Status; ?></td>
                                    
                                    <td class="text-center">
                                        <a class="EditFront btn btn-info" 
                                           href="<?php echo site_url( 'admin/content/editor_of_diplomas/field/update/' . $DFV->Diploma_Config_Id . '/' . $DFV->Diploma_Config_Face . '/' . $DFV->Diploma_Field_Id ); ?>" 
                                           title="<?php echo lang( 'editor_of_diplomas_form_config_label_config_edit_field_2_verso' ) . ' [' . $DFV->Diploma_Config_Id . '] {' . $DFV->Diploma_Field_Id . '}'; ?>">
                                            <i class="icon icon-white icon-edit"></i>
                                        </a>
                                    </td>
                                    
                                    <td class="text-center">
                                        <button class="btn btn-warning" onClick="javascript: if( confirm( 'Delete [<?php echo $DFV->Diploma_Config_Id; ?>] {<?php echo $DFV->Diploma_Field_Id; ?>}' ) ){ window.location.href = '<?php echo site_url( 'admin/content/editor_of_diplomas/field/delete/' . $DFV->Diploma_Config_Id . '/' . $DFV->Diploma_Config_Face . '/' . $DFV->Diploma_Field_Id ); ?>'; } else { return false; }">
                                            <i class="icon icon-white icon-remove"></i>
                                        </button>
                                    </td>
                                </tr>

                            <?php endforeach; ?>


                        <?php endif; ?>
                                </tbody>
                                <tfoot></tfoot>

                            </table>

                        </td>
                    </tr>
                </tbody>

            </table>
        </form>
    </div>
    <script type='text/javascript'>
        $( document ).ready( function() {
            $( 'a.EditFront' )
                .colorbox   ( 
                                { 
                                    'rel':          'EditFront',
                                    'iframe':       true, 
                                    'scrolling':    true, 
                                    'width':        "98%", 
                                    'height':       "98%", 
                                    'onClosed':     function () { 
//                                                                    if( typeof $( 'button#Editor_of_Diploma__REFRESH__FRONT' ) == 'object' ) 
//                                                                    {                                                            
//                                                                        $( 'button#Editor_of_Diploma__REFRESH__FRONT' ).trigger( 'click' ); 
//                                                                    } 
//                                                                    else 
                                                                        location.reload( true ); 
                                                                }, 
                                    'overlayClose': false 
                                }
                            );
            if( typeof $( "a#Editor_of_Diploma__ADD_FIELD__FRONT" ) == 'object' ) 
            {
                $( "a#Editor_of_Diploma__ADD_FIELD__FRONT" )
                    .colorbox   ( 
                                    { 
                                        'iframe':       true, 
                                        'scrolling':    true, 
                                        'width':        "98%", 
                                        'height':       "98%", 
                                        'onClosed':     function () { 
//                                                                        if( typeof $( 'button#Editor_of_Diploma__REFRESH__FRONT' ) == 'object' ) 
//                                                                        {                                                            
//                                                                            $( 'button#Editor_of_Diploma__REFRESH__FRONT' ).trigger( 'click' ); 
//                                                                        } 
//                                                                        else 
                                                                            location.reload( true ); 
                                                                    }, 
                                        'overlayClose': false 
                                    }
                                );
            }
            if( typeof $( "a[role='Field-Info']" ) != 'undefined' ) 
            {
                $( "a[role='Field-Info']" ).popover( { container: 'body' } );
            }
            if( typeof $( "a#Editor_of_Diploma__ADD_FIELD__VERSO" ) == 'object' ) 
            {
                $( "a#Editor_of_Diploma__ADD_FIELD__VERSO" )
                    .colorbox   ( 
                                    { 
                                        'iframe':       true, 
                                        'scrolling':    true, 
                                        'width':        "98%", 
                                        'height':       "98%", 
                                        'onClosed':     function () { 
//                                                                        if( typeof $( 'button#Editor_of_Diploma__REFRESH__VERSO' ) == 'object' ) 
//                                                                        {                                                            
//                                                                            $( 'button#Editor_of_Diploma__REFRESH__VERSO' ).trigger( 'click' ); 
//                                                                        } 
//                                                                        else 
                                                                        location.reload( true ); 
                                                                    }, 
                                        'overlayClose': false 
                                    }
                                );
            }

        } );
    </script>
<?php endif; ?>