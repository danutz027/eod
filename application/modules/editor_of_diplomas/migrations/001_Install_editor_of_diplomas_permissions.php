<?php defined('BASEPATH') || exit('No direct script access allowed');

class Migration_Install_editor_of_diplomas_permissions extends Migration
{
	/**
	 * @var array Permissions to Migrate
	 */
	private $permissionValues = array(
		array(
			'name' => 'Editor_of_diplomas.Content.View',
			'description' => 'View Editor_of_diplomas Content',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Content.Create',
			'description' => 'Create Editor_of_diplomas Content',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Content.Edit',
			'description' => 'Edit Editor_of_diplomas Content',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Content.Delete',
			'description' => 'Delete Editor_of_diplomas Content',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Reports.View',
			'description' => 'View Editor_of_diplomas Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Reports.Create',
			'description' => 'Create Editor_of_diplomas Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Reports.Edit',
			'description' => 'Edit Editor_of_diplomas Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Reports.Delete',
			'description' => 'Delete Editor_of_diplomas Reports',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Settings.View',
			'description' => 'View Editor_of_diplomas Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Settings.Create',
			'description' => 'Create Editor_of_diplomas Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Settings.Edit',
			'description' => 'Edit Editor_of_diplomas Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Settings.Delete',
			'description' => 'Delete Editor_of_diplomas Settings',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Developer.View',
			'description' => 'View Editor_of_diplomas Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Developer.Create',
			'description' => 'Create Editor_of_diplomas Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Developer.Edit',
			'description' => 'Edit Editor_of_diplomas Developer',
			'status' => 'active',
		),
		array(
			'name' => 'Editor_of_diplomas.Developer.Delete',
			'description' => 'Delete Editor_of_diplomas Developer',
			'status' => 'active',
		),
    );

    /**
     * @var string The name of the permission key in the role_permissions table
     */
    private $permissionKey = 'permission_id';

    /**
     * @var string The name of the permission name field in the permissions table
     */
    private $permissionNameField = 'name';

	/**
	 * @var string The name of the role/permissions ref table
	 */
	private $rolePermissionsTable = 'role_permissions';

    /**
     * @var numeric The role id to which the permissions will be applied
     */
    private $roleId = '1';

    /**
     * @var string The name of the role key in the role_permissions table
     */
    private $roleKey = 'role_id';

	/**
	 * @var string The name of the permissions table
	 */
	private $tableName = 'permissions';

	//--------------------------------------------------------------------

	/**
	 * Install this version
	 *
	 * @return void
	 */
	public function up()
	{
		$rolePermissionsData = array();
		foreach ($this->permissionValues as $permissionValue) {
			$this->db->insert($this->tableName, $permissionValue);

			$rolePermissionsData[] = array(
                $this->roleKey       => $this->roleId,
                $this->permissionKey => $this->db->insert_id(),
			);
		}

		$this->db->insert_batch($this->rolePermissionsTable, $rolePermissionsData);
	}

	/**
	 * Uninstall this version
	 *
	 * @return void
	 */
	public function down()
	{
        $permissionNames = array();
		foreach ($this->permissionValues as $permissionValue) {
            $permissionNames[] = $permissionValue[$this->permissionNameField];
        }

        $query = $this->db->select($this->permissionKey)
                          ->where_in($this->permissionNameField, $permissionNames)
                          ->get($this->tableName);

        if ( ! $query->num_rows()) {
            return;
        }

        $permissionIds = array();
        foreach ($query->result() as $row) {
            $permissionIds[] = $row->{$this->permissionKey};
        }

        $this->db->where_in($this->permissionKey, $permissionIds)
                 ->delete($this->rolePermissionsTable);

        $this->db->where_in($this->permissionNameField, $permissionNames)
                 ->delete($this->tableName);
	}
}