<?php defined('BASEPATH') || exit('No direct script access allowed');

$lang['editor_of_diplomas_manage']      = 'Editor of Diplomas &#151; Manage Diploma Templates';
$lang['editor_of_diplomas_edit']        = 'Edit';
$lang['editor_of_diplomas_clone']       = 'Clone';
$lang['editor_of_diplomas_true']        = 'True';
$lang['editor_of_diplomas_false']       = 'False';
$lang['editor_of_diplomas_create']      = 'Create';
$lang['editor_of_diplomas_delete']      = 'Delete';
$lang['editor_of_diplomas_help']        = 'Help';
$lang['editor_of_diplomas_help_manual'] = 'User Manual for this Module';
$lang['editor_of_diplomas_help_heading']        = 'Help for Editor of Diplomas';
$lang['editor_of_diplomas_list']                = 'List';
$lang['editor_of_diplomas_list_index']          = 'List of Diploma Templates';
$lang['editor_of_diplomas_new']                 = 'New';
$lang['editor_of_diplomas_edit_text']           = 'Edit this Diploma Template';
$lang['editor_of_diplomas_clone_text']          = 'Clone this Diploma Template';
$lang['editor_of_diplomas_delete_text']         = 'Delete this Diploma Template';
$lang['editor_of_diplomas_no_records']          = 'There are no editor_of_diplomas in the system.';
$lang['editor_of_diplomas_create_new']          = 'Create a New Diploma Template.';
$lang['editor_of_diplomas_create_success']      = 'Diploma Template successfully created.';
$lang['editor_of_diplomas_create_failure']      = 'There was a problem creating the editor_of_diplomas: ';
$lang['editor_of_diplomas_create_new_button']   = 'Create a New Diploma Template';
$lang['editor_of_diplomas_invalid_id']          = 'Invalid Diploma Template ID.';
$lang['editor_of_diplomas_invalid_action']      = 'Invalid Action for Diploma Field.';
$lang['editor_of_diplomas_edit_success']        = 'Diploma Template successfully saved.';
$lang['editor_of_diplomas_edit_failure']        = 'There was a problem saving the editor_of_diplomas: ';
$lang['editor_of_diplomas_delete_success']      = 'record(s) successfully deleted.';
$lang['editor_of_diplomas_delete_failure']      = 'We could not delete the record: ';
$lang['editor_of_diplomas_delete_error']        = 'You have not selected any records to delete.';
$lang['editor_of_diplomas_actions']             = 'Actions';
$lang['editor_of_diplomas_cancel']              = 'Cancel';
$lang['editor_of_diplomas_delete_record']       = 'Delete this Diploma Template';
$lang['editor_of_diplomas_delete_confirm']      = 'Are you sure you want to delete this editor_of_diplomas?';
$lang['editor_of_diplomas_edit_heading']        = 'Edit Diploma Template';
$lang['editor_of_diplomas_view_heading']        = 'View Diploma Template';
$lang['editor_of_diplomas_config_heading']      = 'Config Diploma Template';
$lang['editor_of_diplomas_clone_confirm']       = 'Are you sure you want to clone this Diploma Template??';
$lang['editor_of_diplomas_delete_template_confirm']= 'Are you sure you want to delete this Diploma Template??';

// Create/Edit Buttons
$lang['editor_of_diplomas_action_edit']   = 'Editor of Diplomas &#151; Edit Diploma Template';
$lang['editor_of_diplomas_action_create'] = 'Editor of Diplomas &#151; Create Diploma Template';

// Activities
$lang['editor_of_diplomas_act_create_record']   = 'Created record with ID';
$lang['editor_of_diplomas_act_edit_record']     = 'Updated record with ID';
$lang['editor_of_diplomas_act_delete_record']   = 'Deleted record with ID';

//Listing Specifics
$lang['editor_of_diplomas_records_empty']    = 'No records found that match your selection.';
$lang['editor_of_diplomas_errors_message']    = 'Please fix the following errors:';

// Column Headings
$lang['editor_of_diplomas_column_created']  = 'Created';
$lang['editor_of_diplomas_column_deleted']  = 'Deleted';
$lang['editor_of_diplomas_column_modified'] = 'Modified';
$lang['editor_of_diplomas_column_deleted_by'] = 'Deleted By';
$lang['editor_of_diplomas_column_created_by'] = 'Created By';
$lang['editor_of_diplomas_column_modified_by'] = 'Modified By';

// Module Details
$lang['editor_of_diplomas_module_name'] = 'Diploma Template';
$lang['editor_of_diplomas_module_description'] = 'Your module description';
$lang['editor_of_diplomas_area_title'] = 'Diploma Template';

// Fields

$lang['editor_of_diplomas_form_config_title']                                   = 'Config Diploma Template';
$lang['editor_of_diplomas_form_config_label_diploma_template']                  = 'Diploma Config Template';
$lang['editor_of_diplomas_form_config_field_id']                                = 'Diploma&nbsp;Id';
$lang['editor_of_diplomas_form_config_field_title']                             = 'Diploma&nbsp;Title';
$lang['editor_of_diplomas_form_config_field_type']                              = 'Diploma&nbsp;Type';
$lang['editor_of_diplomas_form_config_field_form']                              = 'Diploma&nbsp;Format';
$lang['editor_of_diplomas_form_config_field_default']                           = 'Diploma&nbsp;Config&nbsp;Default&nbsp;Fields';
$lang['editor_of_diplomas_form_config_field_more']                              = 'Diploma&nbsp;Config&nbsp;Additional&nbsp;Fields';
$lang['editor_of_diplomas_form_config_field_additional']                        = 'Additional';
$lang['editor_of_diplomas_form_config_label_images']                            = 'Diploma&nbsp;Images';
$lang['editor_of_diplomas_form_config_label_images__max_size_MB']               = 'max&nbsp;size';
$lang['editor_of_diplomas_form_config_label_images__max_width_PX']              = 'max&nbsp;width';
$lang['editor_of_diplomas_form_config_label_images__max_height_PX']             = 'max&nbsp;height';
$lang['editor_of_diplomas_form_config_label_images__resolution_DPI']            = 'resolution';
$lang['editor_of_diplomas_form_config_field_image_front']                       = 'Front&nbsp;Image';
$lang['editor_of_diplomas_form_config_field_image_verso']                       = 'Verso&nbsp;Image';
$lang['editor_of_diplomas_form_config_label_fields_count']                      = 'Fields&nbsp;Count';
$lang['editor_of_diplomas_form_config_title_fields_count_front']                = 'Front&nbsp;Fields&nbsp;Count';
$lang['editor_of_diplomas_form_config_title_fields_count_verso']                = 'Verso&nbsp;Fields&nbsp;Count';
$lang['editor_of_diplomas_form_config_button_submit']                           = 'Submit';
$lang['editor_of_diplomas_form_config_button_cancel']                           = 'Cancel';
$lang['editor_of_diplomas_form_config_label_diploma_fields']                    = 'Diploma Attashed Fields';
$lang['editor_of_diplomas_form_config_label_config_fields_4_front']             = 'Front Fields';
$lang['editor_of_diplomas_form_config_label_config_edit_field_2_front']         = 'Edit Front Field';
$lang['editor_of_diplomas_form_config_label_config_add_field_2_front']          = 'Add Field to Front Template';
$lang['editor_of_diplomas_form_config_label_config_btn_refresh_front']          = 'Refresh Front Fields';
$lang['editor_of_diplomas_form_config_label_config_fields_4_verso']             = 'Verso Fields';
$lang['editor_of_diplomas_form_config_label_config_edit_field_2_verso']         = 'Edit Verso Field';
$lang['editor_of_diplomas_form_config_label_config_add_field_2_verso']          = 'Add Field to Verso Template';
$lang['editor_of_diplomas_form_config_label_config_btn_refresh_verso']          = 'Refresh Verso Fields';
