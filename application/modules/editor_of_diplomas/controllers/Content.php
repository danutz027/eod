<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * Settings controller
 */
class Content extends Admin_Controller
{
    protected $permissionCreate =   'Editor_of_diplomas.Content.Create';
    protected $permissionDelete =   'Editor_of_diplomas.Content.Delete';
    protected $permissionEdit   =   'Editor_of_diplomas.Content.Edit';
    protected $permissionView   =   'Editor_of_diplomas.Content.View';
    protected $prototipSTRINGS  =   'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium totam rem aperiam eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est qui dolorem ipsum quia dolor sit amet consectetur adipisci velit sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam quis nostrum exercitationem ullam corporis suscipit laboriosam nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur vel illum qui dolorem eum fugiat quo voluptas nulla pariatur.';
    protected $prototipNUMBERS  =   '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567';
    protected $prototipDATETIME =   1234567890;
    protected $prototipDEFAULT  =   array(
                                            'Diploma_Field_Type'                =>  'alfanumeric-text',
                                            'Diploma_Field_Coord_Y'             =>  0,
                                            'Diploma_Field_Coord_X'             =>  0,
                                            'Diploma_Field_Area_Width'          =>  100,
                                            'Diploma_Field_Area_Height'         =>  14,
                                            'Diploma_Field_Font_Size'           =>  12,
                                            'Diploma_Field_Font_Family'         =>  'Arial',
                                            'Diploma_Field_Font_Style'          =>  'normal',
                                            'Diploma_Field_Font_Weight'         =>  'normal',
                                            'Diploma_Field_Line_Height'         =>  14,
                                            'Diploma_Field_Text_Transform'      =>  'none',
                                            'Diploma_Field_Text_Align'          =>  'left',
                                            'Diploma_Field_Indent_Width'        =>  0,
                                            'Diploma_Field_Format_Prefix'       =>  11,
                                            'Diploma_Field_Format_Sufix'        =>  0,
                                            'Diploma_Field_Format_Datetime'     =>  1234567890,
                                            'Diploma_Field_Status'              =>  'live',
                                         );
    public function __construct()
    {
        parent::__construct();
        
        $this->auth->restrict($this->permissionView);
        $this->lang->load( 'editor_of_diplomas' );
        $this->load->model( 'diplomas_table__diplomas_codes__model' );
        $this->load->model( 'diplomas_table__diplomas_config__model' );
        $this->load->model( 'diplomas_table__diplomas_config_fields__model' );
        $this->prototipDATETIME = time();
        
        $this->form_validation->set_error_delimiters("<span class='error'>", "</span>");
        
        Template::set_block('sub_nav', 'content/_sub_nav');

        //Assets::add_module_js( 'editor_of_diplomas', 'editor_of_diplomas.js' );
        
        Template::set( 'diplomas_design_type',              $this->config->item( 'diplomas_design_type' ) ); 
        Template::set( 'diplomas_design_form',              $this->config->item( 'diplomas_design_form' ) ); 
        Template::set( 'diplomas_design_field_type',        $this->config->item( 'diplomas_design_field_type' ) ); 
        Template::set( 'diplomas_design_field_default',     $this->config->item( 'diplomas_design_field_default' ) ); 
        Template::set( 'diploma_img_max_size_in_MB',        $this->config->item( 'diploma_img_max_size_in_MB' ) ); 
        Template::set( 'diploma_img_max_width___PX',        $this->config->item( 'diploma_img_max_width___PX' ) ); 
        Template::set( 'diploma_img_max_height__PX',        $this->config->item( 'diploma_img_max_height__PX' ) ); 
        Template::set( 'diploma_img_resolution_DPI',        $this->config->item( 'diploma_img_resolution_DPI' ) ); 
        Template::set( 'diploma_img_upload_folder_image',   $this->config->item( 'diploma_img_upload_folder_image' ) ); 
        Template::set( 'diploma_img_upload_folder_thumb',   $this->config->item( 'diploma_img_upload_folder_thumb' ) ); 
        Template::set_theme('admin');
    }

    public function index()
    {
        Template::set(  'DIPLOMAS_CONFIG',  
                        $this->diplomas_table__diplomas_config__model
                            ->select( 'Diploma_Config_Id' )
                            ->select( 'Diploma_Config_Title' )
                            ->select( 'Diploma_Config_Type' )
                            ->select( 'Diploma_Config_Image_Front' )
                            ->where('deleted', null)
                            ->select(   '( SELECT count( Diploma_Field_ID ) '
                                            . ' FROM  ' . $this->db->dbprefix . $this->diplomas_table__diplomas_config_fields__model->table_name . ' '
                                            . ' WHERE ' . 
                                                        $this->db->dbprefix . $this->diplomas_table__diplomas_config_fields__model->table_name . '.Diploma_Config_Id = ' . 
                                                        $this->db->dbprefix . $this->diplomas_table__diplomas_config__model->table_name . '.Diploma_Config_Id' 
                                        . ' ) AS Diploma_Config_Fields_Count' )
                            ->select( 'Diploma_Config_Image_Verso' )
                            ->find_all() );
        Template::set(  'toolbar_title',    get_class( $this ) . ': ' . lang('editor_of_diplomas_manage'));
        Template::render();
    }

    public function form( $Diploma_Config_Id = NULL )
    {
        $ID = 0;
        $DIPLOMA_IMAGES = array();
        $DIPLOMA_CONFIG = array();
        if  (   is_int ( ( $id = intval( trim(  isset( $_GET['Diploma_Config_Id'] ) && $_GET['Diploma_Config_Id'] 
                                                ?   $_GET['Diploma_Config_Id'] 
                                                :   (   isset( $_POST['Diploma_Config_Id'] ) && $_POST['Diploma_Config_Id'] 
                                                        ?   $_POST['Diploma_Config_Id'] 
                                                        :   (   isset( $_REQUEST['Diploma_Config_Id'] ) && $_REQUEST['Diploma_Config_Id'] 
                                                                ?   $_REQUEST['Diploma_Config_Id'] 
                                                                :   (   isset( $Diploma_Config_Id ) && $Diploma_Config_Id 
                                                                    ?   $Diploma_Config_Id 
                                                                    :   '' ) ) ) ) ) ) ) 
                &&
                $id
                &&
                !empty( ( $DIPLOMA_CONFIG = $this->diplomas_table__diplomas_config__model->find( $id ) ) )
            )
        {
            $this->auth->restrict( $this->permissionEdit );
            $DIPLOMA_CONFIG = ( array ) $DIPLOMA_CONFIG;
            
            if(strpos($Diploma_Config_Id, lang('editor_of_diplomas_clone')))
            {
                $this->auth->restrict( $this->permissionCreate );

                unset($DIPLOMA_CONFIG['Diploma_Config_Id']);
                $DIPLOMA_CONFIG['Diploma_Config_Title'] = $DIPLOMA_CONFIG['Diploma_Config_Title'] . '_' . lang('editor_of_diplomas_clone');             
                $Fields = ( array )$this->diplomas_table__diplomas_config_fields__model->find_all_by( array( 'Diploma_Config_Id' => $id) ) ;
                if ( $id = $this->diplomas_table__diplomas_config__model->insert( $DIPLOMA_CONFIG ) )  
                {
                    $DIPLOMA_CONFIG['Diploma_Config_Id'] = $id;
                }
                foreach ( $Fields as $field ) 
                {
                    $cloned_field = (array) $field;
                        if(isset($cloned_field['Diploma_Field_Id']))
                        {
                                unset($cloned_field['Diploma_Field_Id']);
                                $cloned_field['Diploma_Config_Id'] = $id;
                                $this->diplomas_table__diplomas_config_fields__model->insert( $cloned_field );
                        }
                }              
            }
        } else {
            $id = 0;
            $this->auth->restrict( $this->permissionCreate );
            $DIPLOMA_CONFIG = array();
        } 

        if( isset( $_POST ) && !empty( $_POST ) )
        {
            $data   =   array   (
                                    'Diploma_Config_Title'          =>  ( isset( $_POST['Diploma_Config_Title'] ) && ( $_POST['Diploma_Config_Title'] = trim( $_POST['Diploma_Config_Title'] ) ) ? $_POST['Diploma_Config_Title'] : NULL ), 
                                    'Diploma_Config_Type'           =>  ( isset( $_POST['Diploma_Config_Type'] ) && in_array( ( $_POST['Diploma_Config_Type'] = trim( $_POST['Diploma_Config_Type'] ) ), $this->config->item( 'diplomas_design_type' ) ) ? $_POST['Diploma_Config_Type'] : NULL ), 
                                    'Diploma_Config_Form'           =>  ( isset( $_POST['Diploma_Config_Form'] ) && in_array( ( $_POST['Diploma_Config_Form'] = trim( $_POST['Diploma_Config_Form'] ) ), $this->config->item( 'diplomas_design_form' ) ) ? $_POST['Diploma_Config_Form'] : NULL ), 
                                    'Diploma_Config_Field_001_Type' =>  ( $Diploma_Config_Field_001_Type = ( isset( $_POST['Diploma_Config_Field_001_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_001_Type'] = trim( $_POST['Diploma_Config_Field_001_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_001_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_001_Text' =>  ( $Diploma_Config_Field_001_Type && ( isset( $_POST['Diploma_Config_Field_001_Text'] ) && ( $_POST['Diploma_Config_Field_001_Text'] = trim( $_POST['Diploma_Config_Field_001_Text'] ) ) ) ? $_POST['Diploma_Config_Field_001_Text'] : NULL ), 
                                    'Diploma_Config_Field_002_Type' =>  ( $Diploma_Config_Field_002_Type = ( isset( $_POST['Diploma_Config_Field_002_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_002_Type'] = trim( $_POST['Diploma_Config_Field_002_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_002_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_002_Text' =>  ( $Diploma_Config_Field_002_Type && ( isset( $_POST['Diploma_Config_Field_002_Text'] ) && ( $_POST['Diploma_Config_Field_002_Text'] = trim( $_POST['Diploma_Config_Field_002_Text'] ) ) ) ? $_POST['Diploma_Config_Field_002_Text'] : NULL ), 
                                    'Diploma_Config_Field_003_Type' =>  ( $Diploma_Config_Field_003_Type = ( isset( $_POST['Diploma_Config_Field_003_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_003_Type'] = trim( $_POST['Diploma_Config_Field_003_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_003_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_003_Text' =>  ( $Diploma_Config_Field_003_Type && ( isset( $_POST['Diploma_Config_Field_003_Text'] ) && ( $_POST['Diploma_Config_Field_003_Text'] = trim( $_POST['Diploma_Config_Field_003_Text'] ) ) ) ? $_POST['Diploma_Config_Field_003_Text'] : NULL ), 
                                    'Diploma_Config_Field_004_Type' =>  ( $Diploma_Config_Field_004_Type = ( isset( $_POST['Diploma_Config_Field_004_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_004_Type'] = trim( $_POST['Diploma_Config_Field_004_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_004_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_004_Text' =>  ( $Diploma_Config_Field_004_Type && ( isset( $_POST['Diploma_Config_Field_004_Text'] ) && ( $_POST['Diploma_Config_Field_004_Text'] = trim( $_POST['Diploma_Config_Field_004_Text'] ) ) ) ? $_POST['Diploma_Config_Field_004_Text'] : NULL ), 
                                    'Diploma_Config_Field_005_Type' =>  ( $Diploma_Config_Field_005_Type = ( isset( $_POST['Diploma_Config_Field_005_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_005_Type'] = trim( $_POST['Diploma_Config_Field_005_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_005_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_005_Text' =>  ( $Diploma_Config_Field_005_Type && ( isset( $_POST['Diploma_Config_Field_005_Text'] ) && ( $_POST['Diploma_Config_Field_005_Text'] = trim( $_POST['Diploma_Config_Field_005_Text'] ) ) ) ? $_POST['Diploma_Config_Field_005_Text'] : NULL ), 
                                    'Diploma_Config_Field_006_Type' =>  ( $Diploma_Config_Field_006_Type = ( isset( $_POST['Diploma_Config_Field_006_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_006_Type'] = trim( $_POST['Diploma_Config_Field_006_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_006_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_006_Text' =>  ( $Diploma_Config_Field_006_Type && ( isset( $_POST['Diploma_Config_Field_006_Text'] ) && ( $_POST['Diploma_Config_Field_006_Text'] = trim( $_POST['Diploma_Config_Field_006_Text'] ) ) ) ? $_POST['Diploma_Config_Field_006_Text'] : NULL ), 
                                    'Diploma_Config_Field_007_Type' =>  ( $Diploma_Config_Field_007_Type = ( isset( $_POST['Diploma_Config_Field_007_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_007_Type'] = trim( $_POST['Diploma_Config_Field_007_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_007_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_007_Text' =>  ( $Diploma_Config_Field_007_Type && ( isset( $_POST['Diploma_Config_Field_007_Text'] ) && ( $_POST['Diploma_Config_Field_007_Text'] = trim( $_POST['Diploma_Config_Field_007_Text'] ) ) ) ? $_POST['Diploma_Config_Field_007_Text'] : NULL ), 
                                    'Diploma_Config_Field_008_Type' =>  ( $Diploma_Config_Field_008_Type = ( isset( $_POST['Diploma_Config_Field_008_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_008_Type'] = trim( $_POST['Diploma_Config_Field_008_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_008_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_008_Text' =>  ( $Diploma_Config_Field_008_Type && ( isset( $_POST['Diploma_Config_Field_008_Text'] ) && ( $_POST['Diploma_Config_Field_008_Text'] = trim( $_POST['Diploma_Config_Field_008_Text'] ) ) ) ? $_POST['Diploma_Config_Field_008_Text'] : NULL ), 
                                    'Diploma_Config_Field_009_Type' =>  ( $Diploma_Config_Field_009_Type = ( isset( $_POST['Diploma_Config_Field_009_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_009_Type'] = trim( $_POST['Diploma_Config_Field_009_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_009_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_009_Text' =>  ( $Diploma_Config_Field_009_Type && ( isset( $_POST['Diploma_Config_Field_009_Text'] ) && ( $_POST['Diploma_Config_Field_009_Text'] = trim( $_POST['Diploma_Config_Field_009_Text'] ) ) ) ? $_POST['Diploma_Config_Field_009_Text'] : NULL ), 
                                    'Diploma_Config_Field_010_Type' =>  ( $Diploma_Config_Field_010_Type = ( isset( $_POST['Diploma_Config_Field_010_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_010_Type'] = trim( $_POST['Diploma_Config_Field_010_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_010_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_010_Text' =>  ( $Diploma_Config_Field_010_Type && ( isset( $_POST['Diploma_Config_Field_010_Text'] ) && ( $_POST['Diploma_Config_Field_010_Text'] = trim( $_POST['Diploma_Config_Field_010_Text'] ) ) ) ? $_POST['Diploma_Config_Field_010_Text'] : NULL ), 
                                    'Diploma_Config_Field_011_Type' =>  ( $Diploma_Config_Field_011_Type = ( isset( $_POST['Diploma_Config_Field_011_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_011_Type'] = trim( $_POST['Diploma_Config_Field_011_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_011_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_011_Text' =>  ( $Diploma_Config_Field_011_Type && ( isset( $_POST['Diploma_Config_Field_011_Text'] ) && ( $_POST['Diploma_Config_Field_011_Text'] = trim( $_POST['Diploma_Config_Field_011_Text'] ) ) ) ? $_POST['Diploma_Config_Field_011_Text'] : NULL ), 
                                    'Diploma_Config_Field_012_Type' =>  ( $Diploma_Config_Field_012_Type = ( isset( $_POST['Diploma_Config_Field_012_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_012_Type'] = trim( $_POST['Diploma_Config_Field_012_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_012_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_012_Text' =>  ( $Diploma_Config_Field_012_Type && ( isset( $_POST['Diploma_Config_Field_012_Text'] ) && ( $_POST['Diploma_Config_Field_012_Text'] = trim( $_POST['Diploma_Config_Field_012_Text'] ) ) ) ? $_POST['Diploma_Config_Field_012_Text'] : NULL ), 
                                    'Diploma_Config_Field_013_Type' =>  ( $Diploma_Config_Field_013_Type = ( isset( $_POST['Diploma_Config_Field_013_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_013_Type'] = trim( $_POST['Diploma_Config_Field_013_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_013_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_013_Text' =>  ( $Diploma_Config_Field_013_Type && ( isset( $_POST['Diploma_Config_Field_013_Text'] ) && ( $_POST['Diploma_Config_Field_013_Text'] = trim( $_POST['Diploma_Config_Field_013_Text'] ) ) ) ? $_POST['Diploma_Config_Field_013_Text'] : NULL ), 
                                    'Diploma_Config_Field_014_Type' =>  ( $Diploma_Config_Field_014_Type = ( isset( $_POST['Diploma_Config_Field_014_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_014_Type'] = trim( $_POST['Diploma_Config_Field_014_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_014_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_014_Text' =>  ( $Diploma_Config_Field_014_Type && ( isset( $_POST['Diploma_Config_Field_014_Text'] ) && ( $_POST['Diploma_Config_Field_014_Text'] = trim( $_POST['Diploma_Config_Field_014_Text'] ) ) ) ? $_POST['Diploma_Config_Field_014_Text'] : NULL ), 
                                    'Diploma_Config_Field_015_Type' =>  ( $Diploma_Config_Field_015_Type = ( isset( $_POST['Diploma_Config_Field_015_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_015_Type'] = trim( $_POST['Diploma_Config_Field_015_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_015_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_015_Text' =>  ( $Diploma_Config_Field_015_Type && ( isset( $_POST['Diploma_Config_Field_015_Text'] ) && ( $_POST['Diploma_Config_Field_015_Text'] = trim( $_POST['Diploma_Config_Field_015_Text'] ) ) ) ? $_POST['Diploma_Config_Field_015_Text'] : NULL ), 
                                    'Diploma_Config_Field_016_Type' =>  ( $Diploma_Config_Field_016_Type = ( isset( $_POST['Diploma_Config_Field_016_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_016_Type'] = trim( $_POST['Diploma_Config_Field_016_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_016_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_016_Text' =>  ( $Diploma_Config_Field_016_Type && ( isset( $_POST['Diploma_Config_Field_016_Text'] ) && ( $_POST['Diploma_Config_Field_016_Text'] = trim( $_POST['Diploma_Config_Field_016_Text'] ) ) ) ? $_POST['Diploma_Config_Field_016_Text'] : NULL ), 
                                    'Diploma_Config_Field_017_Type' =>  ( $Diploma_Config_Field_017_Type = ( isset( $_POST['Diploma_Config_Field_017_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_017_Type'] = trim( $_POST['Diploma_Config_Field_017_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_017_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_017_Text' =>  ( $Diploma_Config_Field_017_Type && ( isset( $_POST['Diploma_Config_Field_017_Text'] ) && ( $_POST['Diploma_Config_Field_017_Text'] = trim( $_POST['Diploma_Config_Field_017_Text'] ) ) ) ? $_POST['Diploma_Config_Field_017_Text'] : NULL ), 
                                    'Diploma_Config_Field_018_Type' =>  ( $Diploma_Config_Field_018_Type = ( isset( $_POST['Diploma_Config_Field_018_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_018_Type'] = trim( $_POST['Diploma_Config_Field_018_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_018_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_018_Text' =>  ( $Diploma_Config_Field_018_Type && ( isset( $_POST['Diploma_Config_Field_018_Text'] ) && ( $_POST['Diploma_Config_Field_018_Text'] = trim( $_POST['Diploma_Config_Field_018_Text'] ) ) ) ? $_POST['Diploma_Config_Field_018_Text'] : NULL ), 
                                    'Diploma_Config_Field_019_Type' =>  ( $Diploma_Config_Field_019_Type = ( isset( $_POST['Diploma_Config_Field_019_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_019_Type'] = trim( $_POST['Diploma_Config_Field_019_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_019_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_019_Text' =>  ( $Diploma_Config_Field_019_Type && ( isset( $_POST['Diploma_Config_Field_019_Text'] ) && ( $_POST['Diploma_Config_Field_019_Text'] = trim( $_POST['Diploma_Config_Field_019_Text'] ) ) ) ? $_POST['Diploma_Config_Field_019_Text'] : NULL ), 
                                    'Diploma_Config_Field_020_Type' =>  ( $Diploma_Config_Field_020_Type = ( isset( $_POST['Diploma_Config_Field_020_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_020_Type'] = trim( $_POST['Diploma_Config_Field_020_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_020_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_020_Text' =>  ( $Diploma_Config_Field_020_Type && ( isset( $_POST['Diploma_Config_Field_020_Text'] ) && ( $_POST['Diploma_Config_Field_020_Text'] = trim( $_POST['Diploma_Config_Field_020_Text'] ) ) ) ? $_POST['Diploma_Config_Field_020_Text'] : NULL ), 
                                    'Diploma_Config_Field_021_Type' =>  ( $Diploma_Config_Field_021_Type = ( isset( $_POST['Diploma_Config_Field_021_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_021_Type'] = trim( $_POST['Diploma_Config_Field_021_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_021_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_021_Text' =>  ( $Diploma_Config_Field_021_Type && ( isset( $_POST['Diploma_Config_Field_021_Text'] ) && ( $_POST['Diploma_Config_Field_021_Text'] = trim( $_POST['Diploma_Config_Field_021_Text'] ) ) ) ? $_POST['Diploma_Config_Field_021_Text'] : NULL ), 
                                    'Diploma_Config_Field_022_Type' =>  ( $Diploma_Config_Field_022_Type = ( isset( $_POST['Diploma_Config_Field_022_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_022_Type'] = trim( $_POST['Diploma_Config_Field_022_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_022_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_022_Text' =>  ( $Diploma_Config_Field_022_Type && ( isset( $_POST['Diploma_Config_Field_022_Text'] ) && ( $_POST['Diploma_Config_Field_022_Text'] = trim( $_POST['Diploma_Config_Field_022_Text'] ) ) ) ? $_POST['Diploma_Config_Field_022_Text'] : NULL ), 
                                    'Diploma_Config_Field_023_Type' =>  ( $Diploma_Config_Field_023_Type = ( isset( $_POST['Diploma_Config_Field_023_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_023_Type'] = trim( $_POST['Diploma_Config_Field_023_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_023_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_023_Text' =>  ( $Diploma_Config_Field_023_Type && ( isset( $_POST['Diploma_Config_Field_023_Text'] ) && ( $_POST['Diploma_Config_Field_023_Text'] = trim( $_POST['Diploma_Config_Field_023_Text'] ) ) ) ? $_POST['Diploma_Config_Field_023_Text'] : NULL ), 
                                    'Diploma_Config_Field_024_Type' =>  ( $Diploma_Config_Field_024_Type = ( isset( $_POST['Diploma_Config_Field_024_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_024_Type'] = trim( $_POST['Diploma_Config_Field_024_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_024_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_024_Text' =>  ( $Diploma_Config_Field_024_Type && ( isset( $_POST['Diploma_Config_Field_024_Text'] ) && ( $_POST['Diploma_Config_Field_024_Text'] = trim( $_POST['Diploma_Config_Field_024_Text'] ) ) ) ? $_POST['Diploma_Config_Field_024_Text'] : NULL ), 
                                    'Diploma_Config_Field_025_Type' =>  ( $Diploma_Config_Field_025_Type = ( isset( $_POST['Diploma_Config_Field_025_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_025_Type'] = trim( $_POST['Diploma_Config_Field_025_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_025_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_025_Text' =>  ( $Diploma_Config_Field_025_Type && ( isset( $_POST['Diploma_Config_Field_025_Text'] ) && ( $_POST['Diploma_Config_Field_025_Text'] = trim( $_POST['Diploma_Config_Field_025_Text'] ) ) ) ? $_POST['Diploma_Config_Field_025_Text'] : NULL ), 
                                    'Diploma_Config_Field_026_Type' =>  ( $Diploma_Config_Field_026_Type = ( isset( $_POST['Diploma_Config_Field_026_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_026_Type'] = trim( $_POST['Diploma_Config_Field_026_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_026_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_026_Text' =>  ( $Diploma_Config_Field_026_Type && ( isset( $_POST['Diploma_Config_Field_026_Text'] ) && ( $_POST['Diploma_Config_Field_026_Text'] = trim( $_POST['Diploma_Config_Field_026_Text'] ) ) ) ? $_POST['Diploma_Config_Field_026_Text'] : NULL ), 
                                    'Diploma_Config_Field_027_Type' =>  ( $Diploma_Config_Field_027_Type = ( isset( $_POST['Diploma_Config_Field_027_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_027_Type'] = trim( $_POST['Diploma_Config_Field_027_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_027_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_027_Text' =>  ( $Diploma_Config_Field_027_Type && ( isset( $_POST['Diploma_Config_Field_027_Text'] ) && ( $_POST['Diploma_Config_Field_027_Text'] = trim( $_POST['Diploma_Config_Field_027_Text'] ) ) ) ? $_POST['Diploma_Config_Field_027_Text'] : NULL ), 
                                    'Diploma_Config_Field_028_Type' =>  ( $Diploma_Config_Field_028_Type = ( isset( $_POST['Diploma_Config_Field_028_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_028_Type'] = trim( $_POST['Diploma_Config_Field_028_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_028_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_028_Text' =>  ( $Diploma_Config_Field_028_Type && ( isset( $_POST['Diploma_Config_Field_028_Text'] ) && ( $_POST['Diploma_Config_Field_028_Text'] = trim( $_POST['Diploma_Config_Field_028_Text'] ) ) ) ? $_POST['Diploma_Config_Field_028_Text'] : NULL ), 
                                    'Diploma_Config_Field_029_Type' =>  ( $Diploma_Config_Field_029_Type = ( isset( $_POST['Diploma_Config_Field_029_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_029_Type'] = trim( $_POST['Diploma_Config_Field_029_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_029_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_029_Text' =>  ( $Diploma_Config_Field_029_Type && ( isset( $_POST['Diploma_Config_Field_029_Text'] ) && ( $_POST['Diploma_Config_Field_029_Text'] = trim( $_POST['Diploma_Config_Field_029_Text'] ) ) ) ? $_POST['Diploma_Config_Field_029_Text'] : NULL ), 
                                    'Diploma_Config_Field_030_Type' =>  ( $Diploma_Config_Field_030_Type = ( isset( $_POST['Diploma_Config_Field_030_Type'] ) && in_array( ( $_POST['Diploma_Config_Field_030_Type'] = trim( $_POST['Diploma_Config_Field_030_Type'] ) ), $this->config->item( 'diplomas_design_field_type' ) ) ? $_POST['Diploma_Config_Field_030_Type'] : NULL ) ), 
                                    'Diploma_Config_Field_030_Text' =>  ( $Diploma_Config_Field_030_Type && ( isset( $_POST['Diploma_Config_Field_030_Text'] ) && ( $_POST['Diploma_Config_Field_030_Text'] = trim( $_POST['Diploma_Config_Field_030_Text'] ) ) ) ? $_POST['Diploma_Config_Field_030_Text'] : NULL ), 
                                );
            if( $id && !empty( $DIPLOMA_CONFIG ))
            {               
				if( $this->diplomas_table__diplomas_config__model->update( $id, $data ) )
				{
					$ID = $id;
				}
            } else {
                if ( ( $id = $this->diplomas_table__diplomas_config__model->insert( $data ) )  ) //&& is_int( $id )
                {
                    $ID = $id;
                }
            }
			if ( $ID && isset( $_FILES['Diploma_Config_Image_Front']['name'] ) && $_FILES['Diploma_Config_Image_Front']['name'] ) 
			{

				$folder_uploads         =   realpath( APPPATH . '../public/' . $this->config->item( 'diploma_img_upload_folder_image' ) ); 
				$folder_uploads_thumbs  =   realpath( APPPATH . '../public/' . $this->config->item( 'diploma_img_upload_folder_thumb' ) ); 
				if  ( 
						(   
							$ID && 
							(   
								( isset( $DIPLOMA_CONFIG ) && !empty( $DIPLOMA_CONFIG ) ) && 
								( 
									isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Front'] ) && 
									$DIPLOMA_CONFIG['Diploma_Config_Image_Front'] 
								) 
							) 
						) 
					)
				{
					$image = $folder_uploads . '/' . $DIPLOMA_CONFIG['Diploma_Config_Image_Front'];
					$thumb = $folder_upldiplomas_design_typeoads_thumbs . '/' . $DIPLOMA_CONFIG['Diploma_Config_Image_Front'];
					if ( file_exists( $image ) ) 
					{
						@unlink( $image );
					}
					if ( file_exists( $thumb ) ) 
					{
						@unlink( $thumb );
					}
				}

				$config = array(
									'upload_path'   =>  $folder_uploads,
									'file_name'     =>  ( $the_file__name = $ID . '-front-' . time() ),
									'allowed_types' =>  'jpg|jpeg|gif|png|bmp|tif',
									'max_size'      =>  ( 1024 * $this->config->item( 'diploma_img_max_size_in_MB' ) ),
									'max_width'     =>  $this->config->item( 'diploma_img_max_width___PX' ),
									'max_height'    =>  $this->config->item( 'diploma_img_max_height__PX' ),
									'overwrite'     =>  true
								);

				$this->load->library( 'upload', $config );
				$this->upload->initialize( $config );

				if ( $this->upload->do_upload( 'Diploma_Config_Image_Front' ) ) 
				{
					$the_file = $this->upload->data();
					if( !empty( $the_file ) )
					{
					//echo( 'ID='.$ID );
						if( ( $the_file__name = $the_file['file_name'] ) )
						{
							@self::make_thumb(  ( $folder_uploads . '/' . $the_file__name ), 
												( $folder_uploads_thumbs . '/' . $the_file__name ), 
												( $thumb___height = 200 ) );
						}
						$DIPLOMA_IMAGES['Diploma_Config_Image_Front'] = $the_file__name;
					}
				}
			}
			if ( $ID && isset( $_FILES['Diploma_Config_Image_Verso']['name'] ) && $_FILES['Diploma_Config_Image_Verso']['name'] ) 
			{
				$folder_uploads         =   realpath( APPPATH . '../public/' . $this->config->item( 'diploma_img_upload_folder_image' ) ); 
				$folder_uploads_thumbs  =   realpath( APPPATH . '../public/' . $this->config->item( 'diploma_img_upload_folder_thumb' ) ); 
				if  ( 
						(   
							$id && 
							(   
								( isset( $DIPLOMA_CONFIG ) && !empty( $DIPLOMA_CONFIG ) ) && 
								( 
									isset( $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] ) && 
									$DIPLOMA_CONFIG['Diploma_Config_Image_Verso'] 
								) 
							) 
						) 
					)
				{
					$image = $folder_uploads . '/' . $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'];
					$thumb = $folder_uploads_thumbs . '/' . $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'];
					if ( file_exists( $image ) ) 
					{
						@unlink( $image );
					}
					if ( file_exists( $thumb ) ) 
					{
						@unlink( $thumb );
					}
				}

				$config = array(
									'upload_path'   =>  $folder_uploads,
									'file_name'     =>  ( $the_file__name = $ID . '-verso-' . time() ),
									'allowed_types' =>  'jpg|jpeg|gif|png|bmp|tif',
									'max_size'      =>  ( 1024 * $this->config->item( 'diploma_img_max_size_in_MB' ) ),
									'max_width'     =>  $this->config->item( 'diploma_img_max_width___PX' ),
									'max_height'    =>  $this->config->item( 'diploma_img_max_height__PX' ),
									'overwrite'     =>  true
								);

				$this->load->library( 'upload', $config );
				$this->upload->initialize( $config );

				if ( $this->upload->do_upload( 'Diploma_Config_Image_Verso' ) ) 
				{
					$the_file = $this->upload->data();
					if( !empty( $the_file ) )
					{
						if( ( $the_file__name = $the_file['file_name'] ) )
						{
							@self::make_thumb(  ( $folder_uploads . '/' . $the_file__name ), 
												( $folder_uploads_thumbs . '/' . $the_file__name ), 
												( $thumb___height = 200 ) );
						}
						$DIPLOMA_IMAGES['Diploma_Config_Image_Verso'] = $the_file__name;
					}
				}
			}
            if( $ID && ( isset( $DIPLOMA_IMAGES ) && !empty( $DIPLOMA_IMAGES ) ) )
            {
                $this->diplomas_table__diplomas_config__model->update( $ID, $DIPLOMA_IMAGES );
            }
            if( $ID )
            {
                echo( $ID ); die();
            }
        } else {
            Template::set(  'DIPLOMA_CONFIG', $DIPLOMA_CONFIG ); 
            Template::set('toolbar_title', get_class( $this ) . ': ' . lang('editor_of_diplomas_action_create'));
        }

        Template::render();
    }

    public function field   (  
                                $action, 
                                $Diploma_Config_Id      = NULL, 
                                $Diploma_Config_Face    = 'front', 
                                $Diploma_Field_Id       = NULL 
                            )
    {
        if( in_array( $action, array( 'ajax', 'insert', 'update', 'delete' ) ) )
        {
            if( ( $action == 'ajax' ) && isset( $_POST ) && !empty( $_POST ) )
            {
                $Diploma_Config_Id = ( isset( $_POST['Diploma_Config_Id'] ) && is_int( ( $_POST['Diploma_Config_Id'] = intval( trim( $_POST['Diploma_Config_Id'] ) ) ) ) ? $_POST['Diploma_Config_Id'] : NULL );
                $Diploma_Config_Face = ( isset( $_POST['Diploma_Config_Face'] ) && in_array( $_POST['Diploma_Config_Face'], array( 'front','verso' ) ) ? $_POST['Diploma_Config_Face'] : NULL );
                if( $Diploma_Config_Id && $Diploma_Config_Face )
                {
                    $Diploma_Field_Id = ( isset( $_POST['Diploma_Field_Id'] ) && is_int( ( $_POST['Diploma_Field_Id'] = intval( trim( $_POST['Diploma_Field_Id'] ) ) ) ) ? $_POST['Diploma_Field_Id'] : NULL );
                    $data = array(
                                    'Diploma_Config_Id'             => $Diploma_Config_Id,
                                    'Diploma_Config_Face'           => $Diploma_Config_Face,
                                    'Diploma_Field_Type'            => ( isset( $_POST['Diploma_Field_Type'] ) && ( $_POST['Diploma_Field_Type'] = trim( $_POST['Diploma_Field_Type'] ) ) ? $_POST['Diploma_Field_Type'] : NULL ),
                                    'Diploma_Field_Name'            => ( isset( $_POST['Diploma_Field_Name'] ) && ( $_POST['Diploma_Field_Name'] = trim( $_POST['Diploma_Field_Name'] ) ) ? $_POST['Diploma_Field_Name'] : NULL ),
                                    'Diploma_Field_Coord_Y'         => ( isset( $_POST['Diploma_Field_Coord_Y'] ) && is_int( ( $_POST['Diploma_Field_Coord_Y'] = intval( trim( $_POST['Diploma_Field_Coord_Y'] ) ) ) ) ? $_POST['Diploma_Field_Coord_Y'] : NULL ),
                                    'Diploma_Field_Coord_X'         => ( isset( $_POST['Diploma_Field_Coord_X'] ) && is_int( ( $_POST['Diploma_Field_Coord_X'] = intval( trim( $_POST['Diploma_Field_Coord_X'] ) ) ) ) ? $_POST['Diploma_Field_Coord_X'] : NULL ),
                                    'Diploma_Field_Area_Width'      => ( isset( $_POST['Diploma_Field_Area_Width'] ) && is_int( ( $_POST['Diploma_Field_Area_Width'] = intval( trim( $_POST['Diploma_Field_Area_Width'] ) ) ) ) ? $_POST['Diploma_Field_Area_Width'] : NULL ),
                                    'Diploma_Field_Area_Height'     => ( isset( $_POST['Diploma_Field_Area_Height'] ) && is_int( ( $_POST['Diploma_Field_Area_Height'] = intval( trim( $_POST['Diploma_Field_Area_Height'] ) ) ) ) ? $_POST['Diploma_Field_Area_Height'] : NULL ),
                                    'Diploma_Field_Font_Family'     => ( isset( $_POST['Diploma_Field_Font_Family'] ) && ( $_POST['Diploma_Field_Font_Family'] = trim( $_POST['Diploma_Field_Font_Family'] ) ) ? $_POST['Diploma_Field_Font_Family'] : NULL ),
                                    'Diploma_Field_Font_Size'       => ( isset( $_POST['Diploma_Field_Font_Size'] ) && is_numeric( ( $_POST['Diploma_Field_Font_Size'] = trim( $_POST['Diploma_Field_Font_Size'] ) ) ) ? $_POST['Diploma_Field_Font_Size'] : NULL ),
                                    'Diploma_Field_Font_Style'      => ( isset( $_POST['Diploma_Field_Font_Style'] ) && ( $_POST['Diploma_Field_Font_Style'] = trim( $_POST['Diploma_Field_Font_Style'] ) ) ? $_POST['Diploma_Field_Font_Style'] : NULL ),
                                    'Diploma_Field_Font_Weight'     => ( isset( $_POST['Diploma_Field_Font_Weight'] ) && ( $_POST['Diploma_Field_Font_Weight'] = trim( $_POST['Diploma_Field_Font_Weight'] ) ) ? $_POST['Diploma_Field_Font_Weight'] : NULL ),
                                    'Diploma_Field_Line_Height'     => ( isset( $_POST['Diploma_Field_Line_Height'] ) && is_int( ( $_POST['Diploma_Field_Line_Height'] = intval( trim( $_POST['Diploma_Field_Line_Height'] ) ) ) ) ? $_POST['Diploma_Field_Line_Height'] : NULL ),
                                    'Diploma_Field_Text_Transform'  => ( isset( $_POST['Diploma_Field_Text_Transform'] ) && ( $_POST['Diploma_Field_Text_Transform'] = trim( $_POST['Diploma_Field_Text_Transform'] ) ) ? $_POST['Diploma_Field_Text_Transform'] : NULL ),
                                    'Diploma_Field_Text_Align'      => ( isset( $_POST['Diploma_Field_Text_Align'] ) && ( $_POST['Diploma_Field_Text_Align'] = trim( $_POST['Diploma_Field_Text_Align'] ) ) ? $_POST['Diploma_Field_Text_Align'] : NULL ),
                                    'Diploma_Field_Indent_Width'    => ( isset( $_POST['Diploma_Field_Indent_Width'] ) && is_int( ( $_POST['Diploma_Field_Indent_Width'] = intval( trim( $_POST['Diploma_Field_Indent_Width'] ) ) ) ) ? $_POST['Diploma_Field_Indent_Width'] : NULL ),
                                    'Diploma_Field_Format_Value'    => ( isset( $_POST['Diploma_Field_Format_Value'] ) && ( $_POST['Diploma_Field_Format_Value'] = intval( trim( $_POST['Diploma_Field_Format_Value'] ) ) ) ? $_POST['Diploma_Field_Format_Value'] : NULL ),
                                    'Diploma_Field_Status'          => ( isset( $_POST['Diploma_Field_Status'] ) && in_array( $_POST['Diploma_Field_Status'], array( 'live','front' ) ) ? $_POST['Diploma_Field_Status'] : 'draft' ),
                            );
                    if( $Diploma_Field_Id )
                    {
                        $this->diplomas_table__diplomas_config_fields__model->update( $Diploma_Field_Id, $data );
                        echo( $Diploma_Field_Id ); 
                    } else {
                        $Diploma_Field_Id = $this->diplomas_table__diplomas_config_fields__model->insert( $data );
                        echo( $Diploma_Field_Id ); 
                    }
                    //echo( $this->diplomas_table__diplomas_config_fields__model->last_query() );
                    die();
                } else {
                    die( 0 );
                } 
            } 
            else if ( 
                        in_array( $action, array( 'insert', 'update', 'delete' ) ) &&
                        (   
                            isset( $Diploma_Config_Id ) 
                            && 
                            is_int( ( $Diploma_Config_Id = intval( $Diploma_Config_Id ) ) ) 
                        )   
                        && 
                        (   
                            isset( $Diploma_Config_Face ) 
                            && 
                            in_array( $Diploma_Config_Face, array( 'front', 'verso' ) ) 
                        )   
                        && 
                        ( $DIPLOMA_CONFIG = $this->diplomas_table__diplomas_config__model->find( $Diploma_Config_Id ) )
                        &&
                        !empty ( $DIPLOMA_CONFIG ) 
                    )
            {
                Template::set(  'Diploma_Config_Face',  $Diploma_Config_Face );
                Template::set(  'DIPLOMA_CONFIG',       ( $DIPLOMA_CONFIG = ( array ) $DIPLOMA_CONFIG ) );
                if( in_array( $action, array( 'update', 'delete' ) ) &&
                    ( isset( $Diploma_Field_Id ) && is_int( ( $Diploma_Field_Id = intval( $Diploma_Field_Id ) ) ) ) && 
                    ( $DIPLOMA_CONFIG_FIELD = $this->diplomas_table__diplomas_config_fields__model->find_by( array( 'Diploma_Config_Id' => $Diploma_Config_Id, 'Diploma_Field_Id' => $Diploma_Field_Id ) ) ) &&
                    !empty( $DIPLOMA_CONFIG_FIELD )
                )
                {
                    $DIPLOMA_CONFIG_FIELD = ( array ) $DIPLOMA_CONFIG_FIELD;
                    if( $action == 'delete' )
                    {
                        $this->diplomas_table__diplomas_config_fields__model
                                ->delete_where( array(  'Diploma_Config_Id'     => $Diploma_Config_Id, 
                                                        'Diploma_Field_Id'      => $Diploma_Field_Id ) );
                        redirect( SITE_AREA . '/content/editor_of_diplomas/config/' . $Diploma_Config_Id );
                    } else {
                        Template::set(  'DIPLOMA_CONFIG_FIELD', $DIPLOMA_CONFIG_FIELD );
                    } 
                } 
                else 
                {
                    Template::set(  'DIPLOMA_CONFIG_FIELD', array() );
                }
                Template::set(  'prototipSTRINGS',  $this->prototipSTRINGS );
                Template::set(  'prototipNUMBERS',  $this->prototipNUMBERS );
                Template::set(  'prototipDATETIME', $this->prototipDATETIME );
                Template::set(  'prototipDEFAULT',  $this->prototipDEFAULT );
                Template::set_view('content/field'); 
                Template::render( 'blank' );
            } else {
                Template::set_message( lang( 'editor_of_diplomas_invalid_id' ), 'error' );
                redirect( SITE_AREA . '/content/editor_of_diplomas' );
            }
        } else {
            Template::set_message( lang( 'editor_of_diplomas_invalid_action' ), 'error' );
            redirect( SITE_AREA . '/content/editor_of_diplomas' );
        }
    }
    
    public function delete( $Diploma_Config_Id = NULL )
    {
        if(is_int(($Diploma_Config_Id = intval($Diploma_Config_Id))) && 
        ($DIPLOMA_CONFIG = $this->diplomas_table__diplomas_config__model->find($Diploma_Config_Id)) &&
        !empty($DIPLOMA_CONFIG))
        {
            $this->diplomas_table__diplomas_config__model->soft_delete(true)->delete($Diploma_Config_Id);     
        } 
        else
        {
            Template::set_message(lang('editor_of_diplomas_invalid_id'), 'error');
        }
        redirect(SITE_AREA . '/content/editor_of_diplomas');
    }
    
    public function config( $Diploma_Config_Id )
    {
        if (    is_int( ( $Diploma_Config_Id = intval( $Diploma_Config_Id ) ) ) && 
                ( $DIPLOMA_CONFIG =   $this->diplomas_table__diplomas_config__model->find( $Diploma_Config_Id ) ) &&
                !empty( $DIPLOMA_CONFIG ) ) 
        {
            $DIPLOMA_CONFIG = ( array ) $DIPLOMA_CONFIG;
            Template::set(  'DIPLOMA_CONFIG', $DIPLOMA_CONFIG );
            Template::set(  'DIPLOMA_FIELDS_FRONT', $this->diplomas_table__diplomas_config_fields__model->order_by( $this->diplomas_table__diplomas_config_fields__model->table_name . '.' . $this->diplomas_table__diplomas_config_fields__model->key )->find_all_by( array( 'Diploma_Config_Id' => $DIPLOMA_CONFIG['Diploma_Config_Id'], 'Diploma_Config_Face' => 'front' ) ) );
            Template::set(  'DIPLOMA_FIELDS_VERSO', $this->diplomas_table__diplomas_config_fields__model->order_by( $this->diplomas_table__diplomas_config_fields__model->table_name . '.' . $this->diplomas_table__diplomas_config_fields__model->key )->find_all_by( array( 'Diploma_Config_Id' => $DIPLOMA_CONFIG['Diploma_Config_Id'], 'Diploma_Config_Face' => 'verso' ) ) );
            Template::set( 'toolbar_title', get_class( $this ) . ': ' . lang( 'editor_of_diplomas_view_heading' ) );
            Template::render();
        } 
        else 
        {
            Template::set_message( lang('editor_of_diplomas_invalid_id'), 'error');

            redirect(SITE_AREA . '/content/editor_of_diplomas');
        }
    }
    public function pdf( $Diploma_Config_Id, $Diploma_Config_Face = "front" )
    {
        if (    is_int( ( $Diploma_Config_Id = intval( $Diploma_Config_Id ) ) ) && 
                ( $DIPLOMA_CONFIG = $this->diplomas_table__diplomas_config__model
                                        ->select( 'Diploma_Config_Id' )
                                        ->select( 'Diploma_Config_Title' )
                                        ->select( 'Diploma_Config_Type' )
                                        ->select( 'Diploma_Config_Image_Front' )
                                        ->select( 'Diploma_Config_Image_Verso' )
                                        ->find( $Diploma_Config_Id ) ) &&
                !empty( $DIPLOMA_CONFIG ) &&
                in_array( $Diploma_Config_Face, array( 'front', 'verso' ) )
           ) 
        {
            $DIPLOMA_CONFIG = ( array ) $DIPLOMA_CONFIG;
            if( $Diploma_Config_Face == 'front' )
            {
                $MyHtml = '';
                $BkgImg = $DIPLOMA_CONFIG['Diploma_Config_Image_Front'];
                $Fields = $this->diplomas_table__diplomas_config_fields__model->find_all_by( array( 'Diploma_Config_Id' => $DIPLOMA_CONFIG['Diploma_Config_Id'], 'Diploma_Config_Face' => 'front', 'Diploma_Field_Status' => 'live' ) );
            } else {
                $MyHtml = '';
                $BkgImg = $DIPLOMA_CONFIG['Diploma_Config_Image_Verso'];
                $Fields = $this->diplomas_table__diplomas_config_fields__model->find_all_by( array( 'Diploma_Config_Id' => $DIPLOMA_CONFIG['Diploma_Config_Id'], 'Diploma_Config_Face' => 'verso', 'Diploma_Field_Status' => 'live' ) );
            }
                
            if  ( 
                    ( isset( $MyHtml ) && $MyHtml == '' ) && 
                    ( isset( $BkgImg ) && ( $BkgImg != '' ) && file_exists( $this->config->item( 'diploma_img_upload_folder_image' ) . $BkgImg ) ) && 
                    ( isset( $Fields ) && !empty( $Fields ) ) 
                )
            {
                list( $width, $height ) = getimagesize( $this->config->item( 'diploma_img_upload_folder_image' ) . $BkgImg );
                $MyHtml .=  '<html style="padding: 0; margin: 0; position: relative; display: table; width: ' . $width . 'px; height: ' . $height . 'px;">' . PHP_EOL; 
                $MyHtml .=      '<body style="padding: 0; margin: 0; position: relative; display: table-cell; width: ' . $width . 'px; height: ' . $height . 'px; background-color: lightyellow; background-image: url( \''.base_url( $this->config->item( 'diploma_img_upload_folder_image' ) . $BkgImg ).'\' ); background-repeat: no-repeat;">' . PHP_EOL;
                foreach ( $Fields as $field ) 
                {

                    $MyHtml .=      '<div '
                                        . ' id="Field' . $field->Diploma_Field_Id . '"'
                                        . ' style="z-index: ' . $field->Diploma_Field_Id . ';'
                                                . ' position: absolute;'
                                                . ' display: table; ' 
                                                . ' top: ' . $field->Diploma_Field_Coord_Y . 'px;'
                                                . ' left: ' . $field->Diploma_Field_Coord_X . 'px;'
                                                . ' width: ' . $field->Diploma_Field_Area_Width . 'px;'
                                                . ' height: ' . $field->Diploma_Field_Area_Height . 'px;'
                                                . ' font-family: ' . $field->Diploma_Field_Font_Family . ';'
                                                . ' font-size: ' . $field->Diploma_Field_Font_Size . 'px;'
                                                . ' font-style: ' . $field->Diploma_Field_Font_Style . ';'
                                                . ' font-weight: ' . $field->Diploma_Field_Font_Weight . ';'
                                                . ' line-height: ' . $field->Diploma_Field_Line_Height . 'px;'
                                                . ' text-transform: ' . $field->Diploma_Field_Text_Transform . ';'
                                                . ' text-align: ' . $field->Diploma_Field_Text_Align . ';'
                                                . ' ">';
                    $MyHtml .=          (   $field->Diploma_Field_Indent_Width 
                                        ?   '<span style="float: left; display: inline-block; width: ' . $field->Diploma_Field_Indent_Width . 'px; height: ' . $field->Diploma_Field_Line_Height . 'px;"></span>' 
                                        :  '' );
                            switch( $field->Diploma_Field_Type ) 
                            {
                                case 'alfanumeric-text':
                                    $MyHtml .= '<span>' . substr ( $this->prototipSTRINGS, 0, $field->Diploma_Field_Format_Value ) . '</span>';
                                    break;
                                case 'alfanumeric-html':
                                    $MyHtml .= '<span>' . substr ( $this->prototipSTRINGS, 0, $field->Diploma_Field_Format_Value ) . '</span>';
                                    break;
                                case 'numeric-integer':
                                    $MyHtml .= '<span>' . substr ( $this->prototipNUMBERS, 0, $field->Diploma_Field_Format_Value ) . '</span>';
                                    break;
                                case 'numeric-decimal':
                                    $Diploma_Field_Format = explode( ',', $field->Diploma_Field_Format_Value );
                                    
                                    $MyHtml .= '<span>' . 
                                                    substr ( $this->prototipNUMBERS, 0, ( isset( $Diploma_Field_Format[0] ) && ( $Diploma_Field_Format[0] = intval( $Diploma_Field_Format[0] ) ) ? $Diploma_Field_Format[0] : $this->prototipDEFAULT['Diploma_Field_Format_Prefix'] ) ) . 
                                                    ',' . 
                                                    substr ( $this->prototipNUMBERS, 0, ( isset( $Diploma_Field_Format[1] ) && ( $Diploma_Field_Format[1] = intval( $Diploma_Field_Format[1] ) ) ? $Diploma_Field_Format[1] : $this->prototipDEFAULT['Diploma_Field_Format_Sufix'] ) ) . '</span>';
                                    break;
                                case 'datetime':
                                    //$MyHtml .= '<span>' . date( $field->Diploma_Field_Format_Prefix ) . '</span>';
                                    break;
                                default:
                                    $MyHtml .= '<span></span>';
                            } 
                    $MyHtml .= '</div>' . PHP_EOL;

                }
                $MyHtml .=      '</body>' . PHP_EOL;
                $MyHtml .=  '</html>'; 
                die( $MyHtml );
                
            } else {
                die( 'ERROR' );
            }
            Template::set(  'DIPLOMA_CONFIG', ( array ) $DIPLOMA_CONFIG );
            Template::set(  'DIPLOMA_FIELDS_FRONT', $this->diplomas_table__diplomas_config_fields__model->find_all_by( array( 'Diploma_Config_Id' => $DIPLOMA_CONFIG['Diploma_Config_Id'], 'Diploma_Config_Face' => 'front' ) ) );
            Template::set(  'DIPLOMA_FIELDS_VERSO', $this->diplomas_table__diplomas_config_fields__model->find_all_by( array( 'Diploma_Config_Id' => $DIPLOMA_CONFIG['Diploma_Config_Id'], 'Diploma_Config_Face' => 'verso' ) ) );
            Template::set( 'toolbar_title', get_class( $this ) . ': ' . lang( 'editor_of_diplomas_view_heading' ) );
            //            $this->load->library( 'mpdf60/mpdf' );
            //            $mpdf = new mPDF( 'utf-8', 'A4', 0, '', 10, 5, 10, 10, 10, 10 );
            //            $mpdf->SetDirectionality('ltr');
            //            $mpdf->useOnlyCoreFonts = true;
            //            $mpdf->use_kwt = true;
            //            $mpdf->allow_charset_conversion=true;
            //            $mpdf->charset_in='UTF-8';
            //            $mpdf->WriteHTML( $HTML );
            //            $mpdf->Output();
        } 
        else 
        {
            Template::set_message( lang('editor_of_diplomas_invalid_id'), 'error');

            redirect(SITE_AREA . '/content/editor_of_diplomas');
        }
    }

    public function edit( $id )
    {
        if ( empty( $id ) ) 
        {
            Template::set_message(lang('editor_of_diplomas_invalid_id'), 'error');

            redirect(SITE_AREA . '/content/editor_of_diplomas');
        }
        Template::set( 'toolbar_title', lang( 'editor_of_diplomas_edit_heading' ) );
        Template::render();
    }
    
    public function help()
    {
        Template::set( 'toolbar_title', get_class( $this ) . ': ' . lang( 'editor_of_diplomas_help_heading' ) );
        Template::render();
    }
    ////////////////////////////////////////////////////////////////////////////
    private function make_thumb( $src_files, $src_thumb, $thumb_height ) 
    {
        $type = strtolower( end( explode( '.', $src_files ) ) );

        if( in_array( $type, array( 'jpg', 'jpeg', 'gif', 'png' ) ) )
        {
            if( $type == 'jpg' || $type == 'jpeg' ) $source = imagecreatefromjpeg( $src_files );
            if( $type == 'gif' )                    $source = imagecreatefromgif( $src_files );
            if( $type == 'png' )                    $source = imagecreatefrompng( $src_files );
            if( $source )
            {
                $width  = imagesx( $source );
                $height = imagesy( $source );
                $thumb_width = floor( $width * ( $thumb_height / $height ) );
                $virtual_image = imagecreatetruecolor( $thumb_width, $thumb_height );
                imagecopyresampled( $virtual_image, $source, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height );

                if( $type == 'jpg' || $type == 'jpeg' ) imagejpeg( $virtual_image, $src_thumb );
                if( $type == 'gif' ) imagegif( $virtual_image, $src_thumb );
                if( $type == 'png' ) imagepng( $virtual_image, $src_thumb );
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
}